from fastapi import (
    FastAPI,
    BackgroundTasks,
    Depends,
    HTTPException,
    status
)
from fastapi.encoders import jsonable_encoder
from fastapi.exceptions import RequestValidationError

from fastapi.middleware.gzip import GZipMiddleware
from starlette.requests import Request
from starlette.responses import JSONResponse

import settings

from routers.system import (
    security,
    logs
)
from routers.clients import organization
from routers.manage import (
    users as manage_users,
    organizations as manage_organizations,
    work_servers,
    tasks,
    data
)

from routers.clients import (
    users as client_users,
    logs as client_logs,
    foreign_systems,
    data as client_data,
    works as client_works
)


api = FastAPI()

@api.exception_handler(RequestValidationError)
async def validation_exception_handler(request: Request, exc: RequestValidationError):
    return JSONResponse(
        status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
        content=jsonable_encoder({"detail": exc.errors(), "body": exc.body}),
    )

api.add_middleware(GZipMiddleware, minimum_size=500)

api.include_router(security.router, prefix='/system/security', tags=['system.security', ])
api.include_router(logs.router, prefix='/system/logs', tags=['system.logs', ])
api.include_router(organization.router, prefix='/clients/organization', tags=['clients.organization', ])
api.include_router(manage_users.router, prefix='/manage/users', tags=['manage.users', ])
api.include_router(manage_organizations.router, prefix='/manage/organizations', tags=['manage.organizations', ])
api.include_router(work_servers.router, prefix='/manage/work-servers', tags=['manage.work-servers', ])
api.include_router(tasks.router, prefix='/manage/tasks', tags=['manage.tasks', ])

api.include_router(data.router, prefix='/manage/data', tags=['manage.data', ])

api.include_router(client_users.router, prefix='/clients/users', tags=['clients.users', ])
api.include_router(client_logs.router, prefix='/clients/logs', tags=['clients.logs', ])
api.include_router(foreign_systems.router, prefix='/clients/foreign-systems', tags=['clients.foreign-systems', ])
api.include_router(client_data.router, prefix='/clients/data', tags=['clients.data', ])
api.include_router(client_works.router, prefix='/clients/works', tags=['clients.works', ])






