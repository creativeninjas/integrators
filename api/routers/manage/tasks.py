from typing import List, Optional

from fastapi import (
    APIRouter,
    Depends,
    Security,
    Query,
)

from starlette.background import BackgroundTasks

from background_tasks import add_user_log
from db.constants import scope_manager

from db.session import (
    Session,
    get_db
)
from models.management import Task, UpdateTask

from models.utils import (
    Filter,
    filter_data
)

from security import (
    APISession,
    get_api_session
)

router = APIRouter()

#<editor-fold desc="Tasks functions">
@router.get('/', response_model=List[Task])
async def get_tasks(
        background_tasks: BackgroundTasks,
        fs: Optional[List[str]] = Query(None),
        status: Optional[List[int]] = Query(None),
        data_filter: Filter = Depends(filter_data),
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select management.filter_tasks($1, $2, $3, $4, $5, $6, $7, $8)',
        api_session.db_token,
        data_filter.skip,
        data_filter.limit,
        data_filter.order_by,
        data_filter.direction,
        data_filter.q,
        fs,
        status
    )

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        'Requested task list',
        'get.manage.tasks.get_tasks')

    return record


@router.get('/{task_id:int}', response_model=Task)
async def get_task(
        task_id: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select management.task($1, $2)', api_session.db_token, task_id)

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        f'Requested task {task_id}',
        'get.manage.tasks.get_task')

    return record


@router.put('/{task_id:int}', response_model=Task)
async def put_task(
        task: UpdateTask,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    log_message = 'Updated task'
    log_code = 'put.manage.tasks.put_task'

    record = await db.fetch_val(
        'SELECT management.update_task($1, $2, $3, $4)',
        api_session.db_token,
        task.id,
        task.title,
        task.available if task.available is not None else False
    )

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record
#</editor-fold>