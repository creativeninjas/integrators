from typing import List, Optional

from asyncpg import UniqueViolationError
from fastapi import (
    APIRouter,
    Depends,
    Security,
    Query,
    HTTPException,
    status
)

import ujson as json

from starlette.background import BackgroundTasks

from background_tasks import add_user_log
from db.constants import scope_manager

from db.session import (
    Session,
    get_db
)
from models.data import ImportUnit, IntegratedUnit, ItemToIntegrate, ImportTaxCategory, ImportProductTaxCategory, \
    IntegratedTaxCategory, IntegratedProductTaxCategory, ImportProduct, IntegratedProduct, ImportFisCof, \
    IntegratedFisCof, ImportOnlineOrder, IntegratedOnlineOrder, SetCompletedOnlineOrder

from security import (
    APISession,
    get_api_session
)

router = APIRouter()

#<editor-fold desc="Unit related functions">
@router.put('/units')
async def put_unit(
        unit: ImportUnit,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    log_message = 'Imported unit'
    log_code = 'put.manage.data.put_unit'

    record = await db.fetch_val(
        'SELECT management.import_unit($1, $2, $3, $4, $5)',
        api_session.db_token,
        unit.id_fsc,
        unit.source_id,
        unit.short_title,
        unit.title
    )

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record


@router.get('/units/integrated', response_model=List[IntegratedUnit])
async def get_integrated_units(
        task_code: str,
        id_fsc: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    log_message = 'Get integrated unit list'
    log_code = 'get.manage.data.get_integrated_units'

    record = await db.fetch_val(
        'SELECT management.integrated_units($1, $2, $3)',
        api_session.db_token,
        task_code,
        id_fsc,
    )

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record


@router.post('/units/integrated')
async def post_unit_to_integrate(
        task_code: str,
        unit: ItemToIntegrate,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    log_message = 'Integrate unit'
    log_code = 'post.manage.data.post_unit_to_integrate'

    record = await db.fetch_val(
        'SELECT management.save_integrated_unit($1, $2, $3, $4, $5, $6)',
        api_session.db_token,
        task_code,
        unit.id_item,
        unit.id_fsc,
        unit.target_id,
        json.dumps(unit.attributes)
    )

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record

# @router.delete('/{work_server_id:int}')
# async def delete_work_server(
#         work_server_id: int,
#         background_tasks: BackgroundTasks,
#         api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
#         db: Session = Depends(get_db)):
#
#     record = await db.fetch_val(
#         'SELECT management.delete_work_server($1, $2)',
#         api_session.db_token,
#         work_server_id
#     )
#
#     log_message = 'Deleted work server'
#     log_code = f'delete.manage.work_servers.delete_work_server.{work_server_id}:{record}'
#     if not record:
#         log_message = 'Unsuccessfully deleted work server'
#
#     background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)
#
#     return record

#</editor-fold>




# @router.get('/{work_server_id:int}/jobs/{job_id:int}/next-task', response_model=WorkTask)
# async def get_work_server_job_next_task(
#         work_server_id: int,
#         job_id: int,
#         background_tasks: BackgroundTasks,
#         api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
#         db: Session = Depends(get_db)):
#
#     record = await db.fetch_val(
#         'select management.get_job_next_task($1, $2)', api_session.db_token, job_id)
#
#     background_tasks.add_task(
#         add_user_log,
#         api_session.db_token,
#         f'Requested work server jobs next task {work_server_id} {job_id}',
#         'get.manage.work_servers.get_work_server_job_next_task')
#
#     return record

@router.put('/tax-categories')
async def put_tax_category(
        tax_category: ImportTaxCategory,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    log_message = 'Imported tax category'
    log_code = 'put.manage.data.put_tax_category'

    record = await db.fetch_val(
        'SELECT management.import_tax_category($1, $2, $3, $4, $5, $6,$7)',
        api_session.db_token,
        tax_category.id_fsc,
        tax_category.source_id,
        tax_category.code,
        tax_category.rate,
        tax_category.non_taxable,
        tax_category.title
    )

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record


@router.put('/tax-categories/{id_tax_category:int}/product-tax-categories')
async def put_product_tax_category(
        id_tax_category: int,
        product_tax_category: ImportProductTaxCategory,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    log_message = 'Imported product tax category'
    log_code = 'put.manage.data.put_product_tax_category'

    record = await db.fetch_val(
        'SELECT management.import_product_tax_category($1, $2, $3, $4)',
        api_session.db_token,
        id_tax_category,
        product_tax_category.source_id,
        product_tax_category.title
    )

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record


@router.get('/tax-categories/integrated', response_model=List[IntegratedTaxCategory])
async def get_integrated_tax_categories(
        task_code: str,
        id_fsc: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    log_message = 'Get integrated tax category list'
    log_code = 'get.manage.data.get_integrated_tax_categories'

    record = await db.fetch_val(
        'SELECT management.integrated_tax_categories($1, $2, $3)',
        api_session.db_token,
        task_code,
        id_fsc,
    )

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record


@router.post('/tax-categories/integrated')
async def post_tax_category_to_integrate(
        task_code: str,
        tax_category: ItemToIntegrate,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    log_message = 'Integrate tax category'
    log_code = 'post.manage.data.post_tax_category_to_integrate'

    record = await db.fetch_val(
        'SELECT management.save_integrated_tax_category($1, $2, $3, $4, $5, $6)',
        api_session.db_token,
        task_code,
        tax_category.id_item,
        tax_category.id_fsc,
        tax_category.target_id,
        json.dumps(tax_category.attributes)
    )

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record



@router.get('/tax-categories/{id_tax_category:int}/product-tax-categories/integrated', response_model=List[IntegratedProductTaxCategory])
async def get_integrated_product_tax_categories(
        id_tax_category: int,
        task_code: str,
        id_fsc: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    log_message = 'Get integrated product tax category list'
    log_code = 'get.manage.data.get_integrated_product_tax_categories'

    record = await db.fetch_val(
        'SELECT management.integrated_product_tax_categories($1, $2, $3, $4)',
        api_session.db_token,
        id_tax_category,
        task_code,
        id_fsc,
    )

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record


@router.post('/tax-categories/{id_tax_category:int}/product-tax-categories/integrated')
async def post_product_tax_category_to_integrate(
        task_code: str,
        product_tax_category: ItemToIntegrate,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    log_message = 'Integrate product tax category'
    log_code = 'post.manage.data.post_product_tax_category_to_integrate'

    record = await db.fetch_val(
        'SELECT management.save_integrated_product_tax_category($1, $2, $3, $4, $5, $6)',
        api_session.db_token,
        task_code,
        product_tax_category.id_item,
        product_tax_category.id_fsc,
        product_tax_category.target_id,
        json.dumps(product_tax_category.attributes)
    )

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record


@router.put('/products')
async def put_product(
        product: ImportProduct,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    log_message = 'Imported product'
    log_code = 'put.manage.data.put_product'

    barcodes_list = None
    if product.barcodes:
        barcodes_list = []
        for b in product.barcodes:
            barcodes_list.append({'barcode': b.barcode, 'is_main': b.is_main})

    record = await db.fetch_val(
        'SELECT management.import_product($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15)',
        api_session.db_token,
        product.id_fsc,
        product.source_id,
        product.source_id_tax_category,
        product.source_id_product_tax_category,
        product.source_id_unit,
        product.title,
        product.print_title,
        product.short_code,
        product.price,
        barcodes_list,
        product.qrcode,
        product.description,
        product.combo,
        product.combo_product_source_ids
    )

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record


@router.get('/products/integrated', response_model=List[IntegratedProduct])
async def get_integrated_products(
        task_code: str,
        id_fsc: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    log_message = 'Get integrated product list'
    log_code = 'get.manage.data.get_integrated_products'

    record = await db.fetch_val(
        'SELECT management.integrated_products($1, $2, $3)',
        api_session.db_token,
        task_code,
        id_fsc,
    )

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record


@router.post('/products/integrated')
async def post_product_to_integrate(
        task_code: str,
        product: ItemToIntegrate,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    log_message = 'Integrate product'
    log_code = 'post.manage.data.post_product_to_integrate'

    record = await db.fetch_val(
        'SELECT management.save_integrated_product($1, $2, $3, $4, $5, $6)',
        api_session.db_token,
        task_code,
        product.id_item,
        product.id_fsc,
        product.target_id,
        json.dumps(product.attributes)
    )

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record


@router.put('/fiscof')
async def put_fiscof(
        fiscof: ImportFisCof,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    log_message = 'Imported fiscof'
    log_code = 'put.manage.data.put_fiscof'

    record = await db.fetch_val(
        'SELECT management.import_fiscof($1, $2, $3, $4, $5)',
        api_session.db_token,
        fiscof.id_fsc,
        fiscof.source_id,
        fiscof.device_serial_number,
        fiscof.doc
    )

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record


@router.get('/fiscof/non-integrated/next', response_model=IntegratedFisCof)
async def get_next_non_integrated_fiscof(
        task_code: str,
        id_fsc: int,
        background_tasks: BackgroundTasks,
        dsn: Optional[List[str]] = Query(None),  # dsn is device_serial_numbers
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    log_message = 'Get next non integrated fiscof'
    log_code = 'get.manage.data.get_next_non_integrated_fiscof'

    record = await db.fetch_val(
        'SELECT management.next_non_integrated_fiscof($1, $2, $3, $4)',
        api_session.db_token,
        task_code,
        id_fsc,
        dsn
    )

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record


@router.post('/fiscof/integrated')
async def post_fiscof_to_integrate(
        task_code: str,
        fiscof: ItemToIntegrate,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    log_message = 'Integrate fiscof'
    log_code = 'post.manage.data.post_fiscof_to_integrate'

    record = await db.fetch_val(
        'SELECT management.save_integrated_fiscof($1, $2, $3, $4, $5, $6)',
        api_session.db_token,
        task_code,
        fiscof.id_item,
        fiscof.id_fsc,
        fiscof.target_id,
        json.dumps(fiscof.attributes)
    )

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record


@router.get('/online_orders/integrated', response_model=List[IntegratedOnlineOrder])
async def get_integrated_online_orders(
        task_code: str,
        id_fsc: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    log_message = 'Get integrated online order list'
    log_code = 'get.manage.data.get_integrated_online_orders'

    record = await db.fetch_val(
        'SELECT management.integrated_online_orders($1, $2, $3)',
        api_session.db_token,
        task_code,
        id_fsc,
    )

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record


@router.get('/online_orders/integrated/not_completed', response_model=List[IntegratedOnlineOrder])
async def get_not_completed_integrated_online_orders(
        task_code: str,
        id_fsc: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    log_message = 'Get not completed integrated online order list'
    log_code = 'get.manage.data.get_not_completed_integrated_online_orders'

    record = await db.fetch_val(
        'SELECT management.not_completed_integrated_online_orders($1, $2, $3)',
        api_session.db_token,
        task_code,
        id_fsc,
    )

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record


@router.get('/online_orders/non_integrated/completed/next', response_model=IntegratedOnlineOrder)
async def get_next_completed_non_integrated_online_orders(
        task_code: str,
        id_fsc: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    log_message = 'Get next not completed non integrated online order list'
    log_code = 'get.manage.data.get_next_completed_non_integrated_online_orders'

    record = await db.fetch_val(
        'SELECT management.next_completed_non_integrated_online_order($1, $2, $3)',
        api_session.db_token,
        task_code,
        id_fsc,
    )

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record


@router.put('/online_orders')
async def put_online_order(
        online_order: ImportOnlineOrder,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    log_message = 'Imported online order'
    log_code = 'put.manage.data.put_online order'

    try:
        record = await db.fetch_val(
            'SELECT management.import_online_order($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13)',
            api_session.db_token,
            online_order.id_fsc,
            online_order.source_id,
            online_order.short_number,
            online_order.long_number,
            online_order.buyer_number,
            online_order.buyer_vat_number,
            online_order.buyer_title,
            online_order.buyer_address,
            online_order.buyer_email,
            online_order.buyer_extra_information,
            online_order.document,
            online_order.products
        )
    except UniqueViolationError:
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail=[{'loc': ['number'],
                     'msg': "Online order number used",
                     'error_msg_code': 'uq_fsc_online_order_number',
                     'type': 'unique_violation'}])

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record


@router.post('/online_orders/integrated')
async def post_online_order_to_integrate(
        task_code: str,
        online_order: ItemToIntegrate,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    log_message = 'Integrate online order'
    log_code = 'post.manage.data.post_online_order_to_integrate'

    record = await db.fetch_val(
        'SELECT management.save_integrated_online_order($1, $2, $3, $4, $5, $6)',
        api_session.db_token,
        task_code,
        online_order.id_item,
        online_order.id_fsc,
        online_order.target_id,
        json.dumps(online_order.attributes)
    )

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record


@router.put('/online_orders/set_completed')
async def put_set_completed_online_order(
        online_order: SetCompletedOnlineOrder,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    log_message = 'Set completed online order'
    log_code = 'put.manage.data.put_set_completed_online_order'

    record = await db.fetch_val(
        'SELECT management.set_completed_online_order($1, $2, $3, $4)',
        api_session.db_token,
        online_order.id,
        online_order.completed_at,
        online_order.document
    )

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record
