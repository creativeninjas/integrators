from typing import List

from fastapi import (
    APIRouter,
    Depends,
    Security
)

from starlette.background import BackgroundTasks

from background_tasks import (
    add_user_log,
    send_new_user_password,
    send_user_reset_password
)

from db.constants import scope_manager

from db.session import (
    Session,
    get_db
)

from models.security import (
    User,
    Profile,
    NewUser
)

from models.utils import (
    Filter,
    filter_data
)

from security import (
    APISession,
    get_api_session
)

router = APIRouter()


@router.get('/', response_model=List[User])
async def get_manage_users(
        background_tasks: BackgroundTasks,
        data_filter: Filter = Depends(filter_data),
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select management.filter_management_users($1, $2, $3, $4, $5, $6)',
        api_session.db_token,
        data_filter.skip,
        data_filter.limit,
        data_filter.order_by,
        data_filter.direction,
        data_filter.q
    )

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        'Requested management user list',
        'get.manage.users.get_manage_users')

    return record


@router.post('/', response_model=User)
async def post_manage_user(
        user: NewUser,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'SELECT management.insert_manage_user($1, $2, $3, $4)',
        api_session.db_token,
        user.email,
        user.first_name,
        user.last_name
    )

    log_message = 'Created manage user'
    log_code = f"post.manage.users.post_manage_user.{record['id']}"

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    if user.password_to_email:
        background_tasks.add_task(send_new_user_password, User(**record))

    return record


@router.get('/{user_id:int}', response_model=User)
async def get_manage_user(
        user_id: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select management.user($1, $2)', api_session.db_token, user_id)

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        f'Requested management user {user_id}',
        'get.manage.users.get_manage_user')

    return record


@router.put('/{user_id:int}', response_model=User)
async def put_session_manage_user(
        user: User,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    log_message = 'Updated manage user profile'
    log_code = 'put.manage.users.put_manage_user'

    record = await db.fetch_val(
        'SELECT management.update_manage_user($1, $2, $3, $4, $5)',
        api_session.db_token,
        user.id,
        user.email,
        user.first_name,
        user.last_name
    )

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record


@router.delete('/{user_id:int}')
async def delete_manage_user(
        user_id: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'SELECT management.delete_manage_user($1, $2)',
        api_session.db_token,
        user_id
    )

    log_message = 'Deleted manage user'
    log_code = f'delete.manage.users.delete_manage_user.{user_id}:{record}'
    if not record:
        log_message = 'Unsuccessfully deleted manage user'

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record


@router.post('/{user_id:int}/reset-password')
async def post_manage_user_reset_password(
        user_id: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'SELECT management.reset_manage_user_password($1, $2)',
        api_session.db_token,
        user_id
    )

    result = False
    log_message = 'Reset manage user password'
    log_code = f'post.manage.users.reset_manage_user_password.{user_id}:True'
    if not record:
        log_message = 'Unsuccessfully reset manage user password'
        log_code = f'post.manage.users.reset_manage_user_password.{user_id}:False'

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)
    if record:
        background_tasks.add_task(send_user_reset_password, User(**record))
        result = True

    return result


@router.put('/profile', response_model=User)
async def put_session_manage_user(
        profile: Profile,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    log_message = 'Updated session manage user profile'
    log_code = 'put.manage.users.put_session_manage_user'

    record = await db.fetch_val(
        'SELECT management.update_session_manage_user($1, $2, $3, $4)',
        api_session.db_token,
        profile.email,
        profile.first_name,
        profile.last_name
    )

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record
