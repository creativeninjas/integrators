from typing import List, Optional

from fastapi import (
    APIRouter,
    Depends,
    Security,
    Query
)

from starlette.background import BackgroundTasks

from background_tasks import add_user_log, send_work_error_to_organization
from db.constants import scope_manager

from db.session import (
    Session,
    get_db
)
from models.clients import Organization, Work
from models.management import WorkServer, WorkServerStatus, WorkTask, ErrorMessage

from models.utils import (
    Filter,
    filter_data
)

from security import (
    APISession,
    get_api_session
)

router = APIRouter()

#<editor-fold desc="Work server functions">
@router.get('/', response_model=List[WorkServer])
async def get_work_servers(
        background_tasks: BackgroundTasks,
        status: Optional[List[int]] = Query(None),
        data_filter: Filter = Depends(filter_data),
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select management.filter_work_servers($1, $2, $3, $4, $5, $6, $7)',
        api_session.db_token,
        data_filter.skip,
        data_filter.limit,
        data_filter.order_by,
        data_filter.direction,
        data_filter.q,
        status
    )

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        'Requested work server list',
        'get.manage.work_servers.get_work_servers')

    return record


@router.get('/{work_server_id:int}', response_model=WorkServer)
async def get_work_server(
        work_server_id: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select management.work_server($1, $2)', api_session.db_token, work_server_id)

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        f'Requested work server {work_server_id}',
        'get.manage.work_servers.get_work_server')

    return record


@router.post('/', response_model=WorkServer)
async def post_work_server(
        work_server: WorkServer,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select management.insert_work_server($1, $2, $3)',
        api_session.db_token,
        work_server.address,
        work_server.disabled if work_server.disabled is not None else False
    )

    log_message = 'Created work server'
    log_code = f"post.manage.work_servers.post_work_server.{record['id']}"

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record


@router.put('/{work_server_id:int}', response_model=WorkServer)
async def put_work_server(
        work_server: WorkServer,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    log_message = 'Updated work server'
    log_code = 'put.manage.work_servers.put_work_server'

    record = await db.fetch_val(
        'SELECT management.update_work_server($1, $2, $3, $4)',
        api_session.db_token,
        work_server.id,
        work_server.address,
        work_server.disabled if work_server.disabled is not None else False
    )

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record


@router.put('/{work_server_id:int}/status')
async def put_work_server_status(
        work_server_status: WorkServerStatus,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    log_message = 'Updated work server status'
    log_code = 'put.manage.work_servers.put_work_server_status'

    record = await db.fetch_val(
        'SELECT management.update_work_server_status($1, $2, $3, $4, $5)',
        api_session.db_token,
        work_server_status.id,
        work_server_status.available,
        work_server_status.unavailability_reason,
        work_server_status.error_message
    )

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record


@router.delete('/{work_server_id:int}')
async def delete_work_server(
        work_server_id: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'SELECT management.delete_work_server($1, $2)',
        api_session.db_token,
        work_server_id
    )

    log_message = 'Deleted work server'
    log_code = f'delete.manage.work_servers.delete_work_server.{work_server_id}:{record}'
    if not record:
        log_message = 'Unsuccessfully deleted work server'

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record
#</editor-fold>


@router.get('/{work_server_id:int}/jobs/actual', response_model=List[int])
async def get_work_server_actual_jobs(
        work_server_id: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select management.get_actual_job_list($1, $2)', api_session.db_token, work_server_id)

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        f'Requested work server actual jobs {work_server_id}',
        'get.manage.work_servers.get_work_server_actual_jobs')

    return record


@router.get('/{work_server_id:int}/jobs/{job_id:int}/next-task', response_model=WorkTask)
async def get_work_server_job_next_task(
        work_server_id: int,
        job_id: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select management.get_job_next_task($1, $2)', api_session.db_token, job_id)

    # if record and record['attributes']:
    #     record['attributes'] = json.loads(record['attributes'])

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        f'Requested work server jobs next task {work_server_id} {job_id}',
        'get.manage.work_servers.get_work_server_job_next_task')

    return record


@router.put('/{work_server_id:int}/jobs/{job_id:int}/cancel-with-error')
async def put_cancel_job_with_error(
        error_message: ErrorMessage,
        work_server_id: int,
        job_id: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select management.cancel_job_with_error($1, $2, $3)',
        api_session.db_token, job_id, error_message.execution_error_message)

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        f'Put work server jobs cancel with error {work_server_id} {job_id}',
        'put.manage.work_servers.put_cancel_job_with_error')

    organization_record = await db.fetch_val(
        'SELECT management.organization_by_job_id($1, $2)', api_session.db_token, job_id)

    job_record = await db.fetch_val(
        'SELECT management.work_by_job_id($1, $2)', api_session.db_token, job_id)

    background_tasks.add_task(
        send_work_error_to_organization,
        Organization(**organization_record),
        Work(**job_record),
        error_message.execution_error_message)

    return record
