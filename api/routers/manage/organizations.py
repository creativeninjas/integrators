from typing import List

from fastapi import (
    APIRouter,
    Depends,
    Security,
)

from starlette.background import BackgroundTasks

from background_tasks import add_user_log, send_new_user_password, send_new_organization_user_password
from db.constants import scope_manager

from db.session import (
    Session,
    get_db
)
from models.clients import Organization
from models.security import User, NewUser

from models.utils import (
    Filter,
    filter_data
)

from security import (
    APISession,
    get_api_session
)

router = APIRouter()

#<editor-fold desc="Organization functions">
@router.get('/', response_model=List[Organization])
async def get_organizations(
        background_tasks: BackgroundTasks,
        data_filter: Filter = Depends(filter_data),
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select management.filter_organizations($1, $2, $3, $4, $5, $6)',
        api_session.db_token,
        data_filter.skip,
        data_filter.limit,
        data_filter.order_by,
        data_filter.direction,
        data_filter.q
    )

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        'Requested organization list',
        'get.manage.organizations.get_organizations')

    return record


@router.get('/{organization_id:int}', response_model=Organization)
async def get_manage_user(
        organization_id: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select management.organization($1, $2)', api_session.db_token, organization_id)

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        f'Requested organization {organization_id}',
        'get.manage.organizations.get_organization')

    return record


@router.post('/', response_model=Organization)
async def post_organization(
        organization: Organization,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select management.insert_organization($1, $2, $3, $4, $5)',
        api_session.db_token,
        organization.title,
        organization.contact_person,
        organization.contact_email,
        organization.contact_phone
    )

    log_message = 'Created organization'
    log_code = f"post.manage.organization.post_organization.{record['id']}"

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record


@router.put('/{organization_id:int}', response_model=Organization)
async def put_organization(
        organization: Organization,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    log_message = 'Updated organization'
    log_code = 'put.manage.organization.put_organization'

    record = await db.fetch_val(
        'SELECT management.update_organization($1, $2, $3, $4, $5, $6)',
        api_session.db_token,
        organization.id,
        organization.title,
        organization.contact_person,
        organization.contact_email,
        organization.contact_phone
    )

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record


@router.delete('/{organization_id:int}')
async def delete_organization(
        organization_id: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'SELECT management.delete_organization($1, $2)',
        api_session.db_token,
        organization_id
    )

    log_message = 'Deleted organization'
    log_code = f'delete.manage.organizations.delete_organization.{organization_id}:{record}'
    if not record:
        log_message = 'Unsuccessfully deleted organization'

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record

#</editor-fold>

#<editor-fold desc="Organization users functions">
@router.get('/{id_organization:int}/users', response_model=List[User])
async def get_organization_users(
        id_organization: int,
        background_tasks: BackgroundTasks,
        data_filter: Filter = Depends(filter_data),
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select management.filter_organization_users($1, $2, $3, $4, $5, $6, $7)',
        api_session.db_token,
        id_organization,
        data_filter.skip,
        data_filter.limit,
        data_filter.order_by,
        data_filter.direction,
        data_filter.q
    )

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        'Requested organization user list',
        f'get.organization.users.get_organization_users.{id_organization}')

    return record


@router.post('/{id_organization:int}/users', response_model=User)
async def post_organization_user(
        id_organization: int,
        user: NewUser,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'SELECT management.insert_organization_user($1, $2, $3, $4, $5)',
        api_session.db_token,
        id_organization,
        user.email,
        user.first_name,
        user.last_name
    )

    log_message = 'Created organization user'
    log_code = f"post.organization.users.post_organization_user.{id_organization}.{record['id']}"

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    if user.password_to_email:
        organization_record = await db.fetch_val(
            'SELECT management.organization($1, $2)', api_session.db_token, id_organization)

        background_tasks.add_task(
            send_new_organization_user_password,
            Organization(**organization_record),
            User(**record))

    return record


@router.get('/{id_organization:int}/users/{user_id:int}', response_model=User)
async def get_organization_user(
        id_organization: int,
        user_id: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select management.organization_user($1, $2, $3)', api_session.db_token, id_organization, user_id)

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        f'Requested organization user',
        f'get.organization.users.get_organization_user.{id_organization}.{user_id}')

    return record


@router.put('/{id_organization:int}/users/{user_id:int}', response_model=User)
async def put_organization_user(
        id_organization: int,
        user_id: int,
        user: User,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    log_message = 'Updated organization user profile'
    log_code = f'put.organization.users.put_organization_user.{id_organization}.{user_id}'

    record = await db.fetch_val(
        'SELECT management.update_organization_user($1, $2, $3, $4, $5, $6)',
        api_session.db_token,
        user.id,
        id_organization,
        user.email,
        user.first_name,
        user.last_name
    )

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record


@router.delete('/{id_organization:int}/users/{user_id:int}')
async def delete_organization_user(
        id_organization: int,
        user_id: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'SELECT management.delete_organization_user($1, $2, $3)',
        api_session.db_token,
        id_organization,
        user_id
    )

    log_message = 'Deleted organization user'
    log_code = f'delete.organization.users.delete_organization_user.{id_organization}.{user_id}:{record}'
    if not record:
        log_message = 'Unsuccessfully deleted organization user'

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record
#</editor-fold>
