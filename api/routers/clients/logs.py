from typing import List, Optional

from fastapi import (
    APIRouter,
    Depends,
    Security,
    Query
)

from starlette.background import BackgroundTasks

from background_tasks import add_user_log
from db.constants import scope_manager, scope_client

from db.session import (
    Session,
    get_db
)

from models.system import Log, ClientLog

from models.utils import (
    Filter,
    filter_data
)

from security import (
    APISession,
    get_api_session
)

router = APIRouter()


#<editor-fold desc="Log functions">
@router.get('/', response_model=List[ClientLog])
async def get_logs(
        background_tasks: BackgroundTasks,
        level: Optional[List[str]] = Query(None),
        data_filter: Filter = Depends(filter_data),
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select clients.filter_system_logs($1, $2, $3, $4, $5, $6, $7)',
        api_session.db_token,
        data_filter.skip,
        data_filter.limit,
        data_filter.order_by,
        1,
        data_filter.q,
        level
    )

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        'Requested organization system log list',
        'get.clients.logs.get_logs')

    return record


@router.get('/{log_id:int}', response_model=Log)
async def get_log(
        log_id: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select clients.system_log($1, $2)', api_session.db_token, log_id)

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        f'Requested organization system log {log_id}',
        'get.clients.logs.get_log')

    return record
#</editor-fold>