from typing import List, Optional

from fastapi import (
    APIRouter,
    Depends,
    Security,
    Query
)

from starlette.background import BackgroundTasks

from background_tasks import add_user_log, check_foreign_system_connection
from db.constants import scope_client

from db.session import (
    Session,
    get_db
)
from models.management import ForeignSystem, ForeignSystemConnection

from models.utils import (
    Filter,
    filter_data
)

from security import (
    APISession,
    get_api_session
)

router = APIRouter()


#<editor-fold desc="Foreign system functions">
@router.get('/', response_model=List[ForeignSystem])
async def get_foreign_systems(
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select clients.foreign_systems($1)', api_session.db_token
    )

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        'Requested foreign systems list',
        'get.clients.foreign_systems.get_foreign_systems')

    return record


@router.get('/{id_foreign_system:str}', response_model=ForeignSystem)
async def get_foreign_system(
        id_foreign_system: str,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select clients.foreign_system($1, $2)', api_session.db_token, id_foreign_system
    )

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        'Requested foreign systems',
        f'get.clients.foreign_systems.get_foreign_system.{id_foreign_system}')

    return record
#</editor-fold>


#<editor-fold desc="Foreign system connections functions">
@router.get('/any/connections', response_model=List[ForeignSystemConnection])
async def get_all_foreign_system_connections(
        background_tasks: BackgroundTasks,
        data_filter: Filter = Depends(filter_data),
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select clients.filter_foreign_system_connections($1, $2, $3, $4, $5, $6)',
        api_session.db_token,
        data_filter.skip,
        data_filter.limit,
        data_filter.order_by,
        data_filter.direction,
        data_filter.q
    )

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        'Requested organization all foreign system connections',
        f'get.clients.foreign_systems.get_all_foreign_system_connections.')

    return record


@router.get('/{id_foreign_system:str}/connections', response_model=List[ForeignSystemConnection])
async def get_foreign_system_connections(
        id_foreign_system: str,
        background_tasks: BackgroundTasks,
        data_filter: Filter = Depends(filter_data),
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select clients.filter_foreign_system_connections($1, $2, $3, $4, $5, $6, $7)',
        api_session.db_token,
        data_filter.skip,
        data_filter.limit,
        data_filter.order_by,
        data_filter.direction,
        data_filter.q,
        id_foreign_system
    )

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        'Requested organization foreign system connections',
        f'get.clients.foreign_systems.get_foreign_system_connections.{id_foreign_system}')

    return record


@router.post('/{id_foreign_system:str}/connections', response_model=ForeignSystemConnection)
async def post_foreign_system_connection(
        id_foreign_system: str,
        foreign_system_connection: ForeignSystemConnection,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'SELECT clients.insert_foreign_system_connection($1, $2, $3, $4, $5, $6)',
        api_session.db_token,
        id_foreign_system,
        foreign_system_connection.title,
        foreign_system_connection.username,
        foreign_system_connection.pwd,
        foreign_system_connection.address
    )

    log_message = 'Created organization foreign system connection'
    log_code = f"post.clients.foreign_systems.post_foreign_system_connection.{id_foreign_system}.{record['id']}"

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    if record is not None:
        background_tasks.add_task(check_foreign_system_connection, record)

    return record


@router.get('/{id_foreign_system:str}/connections/{id_connection:int}', response_model=ForeignSystemConnection)
async def get_foreign_system_connection(
        id_foreign_system: str,
        id_connection: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select clients.foreign_system_connection($1, $2)', api_session.db_token, id_connection)

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        f'Requested organization foreign system connection',
        f'get.clients.foreign-systems.get_foreign_system_connection.{id_foreign_system}.{id_connection}')

    return record


@router.put('/{id_foreign_system:str}/connections/{id_connection:int}', response_model=ForeignSystemConnection)
async def put_foreign_system_connection(
        id_foreign_system: str,
        id_connection: int,
        foreign_system_connection: ForeignSystemConnection,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    log_message = 'Updated organization foreign system connection'
    log_code = f'put.clients.foreign_systems.put_foreign_system_connection.{id_foreign_system}.{id_connection}'

    record = await db.fetch_val(
        'SELECT clients.update_foreign_system_connection($1, $2, $3, $4, $5, $6, $7, $8)',
        api_session.db_token,
        foreign_system_connection.id,
        foreign_system_connection.title,
        foreign_system_connection.username,
        foreign_system_connection.pwd,
        foreign_system_connection.address,
        foreign_system_connection.available,
        foreign_system_connection.unavailability_error
    )

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    if record is not None:
        background_tasks.add_task(check_foreign_system_connection, record)

    return record


@router.delete('/{id_foreign_system:str}/connections/{id_connection:int}')
async def delete_foreign_system_connection(
        id_foreign_system: str,
        id_connection: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'SELECT clients.delete_foreign_system_connection($1, $2)',
        api_session.db_token,
        id_connection
    )

    log_message = 'Deleted organization foreign system connection'
    log_code = f'delete.clients.foreign_system.delete_foreign_system_connection.{id_foreign_system}.{id_connection}:{record}'
    if not record:
        log_message = 'Unsuccessfully deleted foreign system connection'

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record
#</editor-fold>