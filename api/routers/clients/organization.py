from fastapi import (
    APIRouter,
    Depends,
    Security,
)

from starlette.background import BackgroundTasks

from background_tasks import add_user_log
from db.constants import scope_client

from db.session import (
    Session,
    get_db
)
from models.clients import Organization

from security import (
    APISession,
    get_api_session
)

router = APIRouter()


@router.get('/', response_model=Organization)
async def get_user_session_organization(
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    log_message = 'Requested user session organization'
    log_code = 'get.clients.organization.get_user_session_organization'

    record = await db.fetch_organization_val(
        'SELECT clients.organization($1)',
        api_session.db_token,
        log_message=log_message,
        log_code=log_code
    )

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record

