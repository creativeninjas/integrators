from typing import (
    List,
    Optional
)

from fastapi import (
    APIRouter,
    Depends,
    Security,
    Query
)

import ujson as json

from starlette.background import BackgroundTasks

from background_tasks import add_user_log
from db.constants import scope_client

from db.session import (
    Session,
    get_db
)
from models.clients import WorkAsJob, WorkAndJob, AddWork
from models.management import RunPeriod, Task

from models.utils import (
    Filter,
    filter_data
)

from security import (
    APISession,
    get_api_session
)

router = APIRouter()


@router.get('/tasks', response_model=List[Task])
async def get_tasks(
        background_tasks: BackgroundTasks,
        fs: Optional[List[str]] = Query(None),
        status: Optional[List[int]] = Query(None),
        data_filter: Filter = Depends(filter_data),
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select clients.filter_tasks($1, $2, $3, $4, $5, $6, $7, $8)',
        api_session.db_token,
        data_filter.skip,
        data_filter.limit,
        data_filter.order_by,
        data_filter.direction,
        data_filter.q,
        fs,
        status
    )

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        'Requested task list',
        'get.clients.works.get_tasks')

    return record


@router.get('/tasks/{task_id:int}', response_model=Task)
async def get_task(
        task_id: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select clients.task($1, $2)', api_session.db_token, task_id)

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        f'Requested task {task_id}',
        'get.clients.works.get_task')

    return record


# <editor-fold desc="Work functions">
@router.get('/', response_model=List[WorkAsJob])
async def get_works(
        background_tasks: BackgroundTasks,
        status: Optional[List[int]] = Query(None),
        data_filter: Filter = Depends(filter_data),
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select clients.filter_work($1, $2, $3, $4, $5, $6, $7)',
        api_session.db_token,
        data_filter.skip,
        data_filter.limit,
        data_filter.order_by,
        1,
        data_filter.q,
        status
    )

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        'Requested organization work list',
        'get.clients.works.get_works')

    return record


@router.post('/', response_model=WorkAndJob)
async def post_work(
        work: AddWork,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    tasks = None
    if work.tasks:
        tasks = []
        for i in work.tasks:
            t = i.dict()
            if t['attributes'] and isinstance(t['attributes'], dict):
                t['attributes'] = json.dumps(t['attributes'])
            tasks.append(t)

    record = await db.fetch_val(
        'SELECT clients.insert_work($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)',
        api_session.db_token,
        work.title,
        work.disabled,
        work.repeat,
        work.id_run_period,
        work.minute,
        work.hour,
        work.week_day,
        work.month_day,
        work.month,
        tasks
    )

    log_message = 'Created organization work'
    log_code = f"post.clients.works.post_work.{record['work']['id']}"

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record


@router.get('/{work_id:int}', response_model=WorkAndJob)
async def get_work_and_job(
        work_id: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select clients.work_and_job($1, $2)', api_session.db_token, work_id)

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        f'Requested organization work and job with  id_work {work_id}',
        'get.clients.works.get_work_and_job')

    return record


@router.put('/{work_id:int}', response_model=WorkAndJob)
async def put_work(
        work: AddWork,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    tasks = None
    if work.tasks:
        tasks = []
        for i in work.tasks:
            t = i.dict()
            if t['attributes'] and isinstance(t['attributes'], dict):
                t['attributes'] = json.dumps(t['attributes'])
            tasks.append(t)

    record = await db.fetch_val(
        'SELECT clients.update_work($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15)',
        api_session.db_token,
        work.id,
        work.title,
        work.disabled,
        work.repeat,
        work.id_run_period,
        work.minute,
        work.hour,
        work.week_day,
        work.month_day,
        work.month,
        work.max_retry_attempts,
        work.retry_interval_minutes,
        work.retry_on_false,
        tasks
    )

    log_message = 'Update organization work'
    log_code = f"put.clients.works.put_work.{record['work']['id']}"

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record


@router.delete('/{work_id:int}')
async def delete_work(
        work_id: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'SELECT clients.delete_work($1, $2)',
        api_session.db_token,
        work_id
    )

    log_message = 'Deleted organization work'
    log_code = f'delete.clients.works.delete_work.{work_id}:{record}'
    if not record:
        log_message = 'Unsuccessfully deleted organization work'

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record

#</editor-fold>


@router.get('/run-periods', response_model=List[RunPeriod])
async def get_run_periods(
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val('select clients.run_periods($1)', api_session.db_token)

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        'Requested work run periods',
        'get.clients.works.get_run_periods')

    return record
