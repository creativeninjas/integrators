from fastapi import (
    APIRouter,
    Depends,
    Security,
)

from starlette.background import BackgroundTasks

from background_tasks import add_user_log
from db.constants import scope_client

from db.session import (
    Session,
    get_db
)

from models.security import User, Profile

from security import (
    APISession,
    get_api_session
)

router = APIRouter()


@router.put('/profile', response_model=User)
async def put_session_organization_user(
        profile: Profile,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    log_message = 'Updated session organization user profile'
    log_code = 'put.clients.users.put_session_organization_user'

    record = await db.fetch_val(
        'SELECT clients.update_session_organization_user($1, $2, $3, $4)',
        api_session.db_token,
        profile.email,
        profile.first_name,
        profile.last_name
    )

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record


@router.get('/profile/unread-log-count')
async def get_session_organization_user_unread_system_log_count(
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    log_message = 'Requested session organization user unread system log count'
    log_code = 'get.clients.users.get_session_organization_user_unread_system_log_count'

    record = await db.fetch_val('SELECT clients.unread_system_log_count($1)', api_session.db_token)

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)
    return record
