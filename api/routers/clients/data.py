from typing import List, Optional

from fastapi import (
    APIRouter,
    Depends,
    Security,
    Query
)

from starlette.background import BackgroundTasks

from background_tasks import add_user_log
from db.constants import scope_client

from db.session import (
    Session,
    get_db
)
from models.data import (
    FSCDataSummary,
    TikiTakaSummary,
    DeviceFiscof,
    DeviceDocument,
    OnlineDocument,
    DeviceFiscofDocument,
    Unit,
    DataVersion,
    UnitVersion,
    TaxCategory,
    TaxCategoryVersion,
    Product,
    ProductVersion,
    OnlineOrder
)
from models.utils import filter_data, Filter

from security import (
    APISession,
    get_api_session
)

router = APIRouter()


@router.get('/summary', response_model=List[FSCDataSummary])
async def get_summary(
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    log_message = 'Get foreign systems data summary'
    log_code = 'get.clients.data.get_summary'

    record = await db.fetch_val('select clients.fsc_data_summary($1)', api_session.db_token)

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record


@router.get('/tikitaka-summary', response_model=List[TikiTakaSummary])
async def get_tikitaka_summary(
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    log_message = 'Get tikitaka data summary'
    log_code = 'get.clients.data.get_tikitaka_summary'

    record = await db.fetch_val('select clients.fsc_tikitaka_summary($1)', api_session.db_token)

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record


@router.get('/{id_fsc:int}/fiscofs', response_model=List[DeviceFiscof])
async def get_fiscofs(
        id_fsc: int,
        background_tasks: BackgroundTasks,
        dsn: Optional[List[str]] = Query(None),
        data_filter: Filter = Depends(filter_data),
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select clients.filter_fiscofs($1, $2, $3, $4, $5, $6, $7, $8)',
        api_session.db_token,
        id_fsc,
        data_filter.skip,
        data_filter.limit,
        data_filter.order_by,
        1,
        data_filter.q,
        dsn
    )

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        'Requested organization Tiki-Taka fiscofs',
        'get.clients.data.get_fiscofs')

    return record


@router.get('/{id_fsc:int}/fiscofs/{id_fiscof:int}', response_model=DeviceFiscof)
async def get_fiscof(
        id_fsc: int,
        id_fiscof: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select clients.fiscof($1, $2)',
        api_session.db_token,
        id_fiscof
    )

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        f'Requested organization Tiki-Taka fiscof. id_fsc:{id_fsc}, id_fiscof:{id_fiscof}',
        'get.clients.data.get_fiscof')

    return record


@router.get('/{id_fsc:int}/documents', response_model=List[DeviceFiscofDocument])
async def get_documents(
        id_fsc: int,
        background_tasks: BackgroundTasks,
        dsn: Optional[List[str]] = Query(None),
        data_filter: Filter = Depends(filter_data),
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select clients.filter_documents($1, $2, $3, $4, $5, $6, $7, $8)',
        api_session.db_token,
        id_fsc,
        data_filter.skip,
        data_filter.limit,
        data_filter.order_by,
        1,
        data_filter.q,
        dsn
    )

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        'Requested organization Tiki-Taka documents',
        'get.clients.data.get_documents')

    return record


@router.get('/{id_fsc:int}/documents/{id_document:int}', response_model=DeviceFiscofDocument)
async def get_document(
        id_fsc: int,
        id_document: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select clients.document($1, $2)',
        api_session.db_token,
        id_document
    )

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        f'Requested organization Tiki-Taka fiscof document. id_fsc:{id_fsc}, id_document:{id_document}',
        'get.clients.data.get_document')

    return record


@router.get('/{id_fsc:int}/online-documents', response_model=List[OnlineDocument])
async def get_online_documents(
        id_fsc: int,
        background_tasks: BackgroundTasks,
        dsn: Optional[List[str]] = Query(None),
        data_filter: Filter = Depends(filter_data),
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select clients.filter_online_documents($1, $2, $3, $4, $5, $6, $7, $8)',
        api_session.db_token,
        id_fsc,
        data_filter.skip,
        data_filter.limit,
        data_filter.order_by,
        1,
        data_filter.q,
        dsn
    )

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        'Requested organization Tiki-Taka online documents',
        'get.clients.data.get_online_documents')

    return record


@router.get('/{id_fsc:int}/online-documents/{id_online_document:int}', response_model=OnlineDocument)
async def get_online_document(
        id_fsc: int,
        id_online_document: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select clients.online_document($1, $2)',
        api_session.db_token,
        id_online_document
    )

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        f'Requested organization Tiki-Taka online document. id_fsc:{id_fsc}, id_online_document:{id_online_document}',
        'get.clients.data.get_online_document')

    return record


# <editor-fold desc="Foreign system connection unit functions">
@router.get('/{id_fsc:int}/units', response_model=List[Unit])
async def get_units(
        id_fsc: int,
        background_tasks: BackgroundTasks,
        data_filter: Filter = Depends(filter_data),
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select clients.filter_units($1, $2, $3, $4, $5, $6, $7)',
        api_session.db_token,
        id_fsc,
        data_filter.skip,
        data_filter.limit,
        data_filter.order_by,
        1,
        data_filter.q
    )

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        'Requested organization fsc Units',
        'get.clients.data.get_units')

    return record


@router.get('/{id_fsc:int}/units/{id_unit:int}', response_model=Unit)
async def get_unit(
        id_fsc: int,
        id_unit: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select clients.unit($1, $2)',
        api_session.db_token,
        id_unit
    )

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        f'Requested organization FSC unit. id_fsc:{id_fsc}, id_unit:{id_unit}',
        'get.clients.data.get_unit')

    return record


@router.get('/{id_fsc:int}/units/{id_unit:int}/versions', response_model=List[DataVersion])
async def get_unit_versions(
        id_fsc: int,
        id_unit: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select clients.unit_versions($1, $2)',
        api_session.db_token,
        id_unit
    )

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        f'Requested organization FSC unit versions id_fsc:{id_fsc}, id_unit:{id_unit}',
        'get.clients.data.get_unit_versions')

    return record


@router.get('/{id_fsc:int}/units/{id_unit:int}/versions/{id_version:int}', response_model=UnitVersion)
async def get_unit_version(
        id_fsc: int,
        id_unit: int,
        id_version: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select clients.unit_version($1, $2)',
        api_session.db_token,
        id_version
    )

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        f'Requested organization FSC unit version id_fsc:{id_fsc}, id_unit:{id_unit}, id_version: {id_version}',
        'get.clients.data.get_unit_version')

    return record
# </editor-fold>


# <editor-fold desc="Foreign system connection tax category functions">
@router.get('/{id_fsc:int}/tax-categories', response_model=List[TaxCategory])
async def get_tax_categories(
        id_fsc: int,
        background_tasks: BackgroundTasks,
        data_filter: Filter = Depends(filter_data),
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select clients.filter_tax_categories($1, $2, $3, $4, $5, $6, $7)',
        api_session.db_token,
        id_fsc,
        data_filter.skip,
        data_filter.limit,
        data_filter.order_by,
        1,
        data_filter.q
    )

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        'Requested organization fsc Tax categories',
        'get.clients.data.get_tax_categories')

    return record


@router.get('/{id_fsc:int}/tax-categories/{id_tax_category:int}', response_model=TaxCategory)
async def get_tax_category(
        id_fsc: int,
        id_tax_category: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select clients.tax_category($1, $2)',
        api_session.db_token,
        id_tax_category
    )

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        f'Requested organization FSC tax category. id_fsc:{id_fsc}, id_tax_category:{id_tax_category}',
        'get.clients.data.get_tax_category')

    return record


@router.get('/{id_fsc:int}/tax-categories/{id_tax_category:int}/versions', response_model=List[DataVersion])
async def get_tax_category_versions(
        id_fsc: int,
        id_tax_category: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select clients.tax_category_versions($1, $2)',
        api_session.db_token,
        id_tax_category
    )

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        f'Requested organization FSC tax category versions id_fsc:{id_fsc}, id_tax_category:{id_tax_category}',
        'get.clients.data.get_tax_category_versions')

    return record


@router.get('/{id_fsc:int}/tax-categories/{id_tax_category:int}/versions/{id_version:int}',
            response_model=TaxCategoryVersion)
async def get_tax_category_version(
        id_fsc: int,
        id_tax_category: int,
        id_version: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select clients.tax_category_version($1, $2)',
        api_session.db_token,
        id_version
    )

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        f'Requested organization FSC tax category version id_fsc:{id_fsc}, id_tax_category:{id_tax_category},'
        f' id_version: {id_version}',
        'get.clients.data.get_tax_category_version')

    return record
# </editor-fold>


# <editor-fold desc="Foreign system connection product functions">
@router.get('/{id_fsc:int}/products', response_model=List[Product])
async def get_products(
        id_fsc: int,
        background_tasks: BackgroundTasks,
        data_filter: Filter = Depends(filter_data),
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select clients.filter_products($1, $2, $3, $4, $5, $6, $7)',
        api_session.db_token,
        id_fsc,
        data_filter.skip,
        data_filter.limit,
        data_filter.order_by,
        1,
        data_filter.q
    )

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        'Requested organization fsc Products',
        'get.clients.data.get_products')

    return record


@router.get('/{id_fsc:int}/products/{id_product:int}', response_model=Product)
async def get_product(
        id_fsc: int,
        id_product: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select clients.product($1, $2)',
        api_session.db_token,
        id_product
    )

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        f'Requested organization FSC product. id_fsc:{id_fsc}, id_product:{id_product}',
        'get.clients.data.get_product')

    return record


@router.get('/{id_fsc:int}/products/{id_product:int}/versions', response_model=List[DataVersion])
async def get_product_versions(
        id_fsc: int,
        id_product: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select clients.product_versions($1, $2)',
        api_session.db_token,
        id_product
    )

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        f'Requested organization FSC product versions id_fsc:{id_fsc}, id_product:{id_product}',
        'get.clients.data.get_product_versions')

    return record


@router.get('/{id_fsc:int}/products/{id_product:int}/versions/{id_version:int}',
            response_model=ProductVersion)
async def get_product_version(
        id_fsc: int,
        id_product: int,
        id_version: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select clients.product_version($1, $2)',
        api_session.db_token,
        id_version
    )

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        f'Requested organization FSC product version id_fsc:{id_fsc}, id_product:{id_product},'
        f' id_version: {id_version}',
        'get.clients.data.get_product_version')

    return record
# </editor-fold>


# <editor-fold desc="Foreign system connection online orders functions">
@router.get('/{id_fsc:int}/online-orders', response_model=List[OnlineOrder])
async def get_online_orders(
        id_fsc: int,
        background_tasks: BackgroundTasks,
        data_filter: Filter = Depends(filter_data),
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    # print(f"select clients.filter_online_orders('{api_session.db_token}', {id_fsc}, {data_filter.skip}, {data_filter.limit}, '{data_filter.order_by}', 1, null)")

    record = await db.fetch_val(
        'select clients.filter_online_orders($1, $2, $3, $4, $5, $6, $7)',
        api_session.db_token,
        id_fsc,
        data_filter.skip,
        data_filter.limit,
        data_filter.order_by,
        1,
        data_filter.q
    )

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        'Requested organization fsc Online Orders',
        'get.clients.data.get_online_orders')

    return record


@router.get('/{id_fsc:int}/online-orders/{id_online_order:int}', response_model=OnlineOrder)
async def get_online_order(
        id_fsc: int,
        id_online_order: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select clients.online_order($1, $2)',
        api_session.db_token,
        id_online_order
    )

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        f'Requested organization FSC online order. id_fsc:{id_fsc}, id_online_order:{id_online_order}',
        'get.clients.data.get_online_order')

    return record
# </editor-fold>
