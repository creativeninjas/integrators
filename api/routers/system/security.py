from datetime import timedelta
from typing import List

from asyncpg import UndefinedObjectError

from fastapi import (
    APIRouter,
    Depends,
    HTTPException,
    Security,
)
from fastapi.security import OAuth2PasswordRequestForm
from starlette import status

from starlette.background import BackgroundTasks

from background_tasks import add_user_log
from db.constants import (
    scope_manager,
    scope_client
)

from db.session import (
    Session,
    get_db
)
from models.security import SessionUser, UserOrganization, ChangePassword, PwdReset, ResetPwd
from notifications.users import email_set_new_password_url

from security import (
    APISession,
    AccessToken,
    get_api_session,
    create_access_token
)

import settings

router = APIRouter()


@router.post('/token', response_model=AccessToken)
async def login_for_token(
        background_tasks: BackgroundTasks,
        form_data: OAuth2PasswordRequestForm = Depends(),
        db: Session = Depends(get_db)):
    try:
        db_token = await db.fetch_val(
            'SELECT security.authorize_user($1, $2, $3)',
            form_data.username,
            form_data.password,
            settings.ACCESS_TOKEN_EXPIRE_MINUTES)
    except UndefinedObjectError:
        # Custom error raised equals wrong authorization data
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail='Incorrect username or password',
            headers={'WWW-Authenticate': 'Bearer'},
        )

    db_token_data = {
        'db-token': str(db_token['db_token']),
        'scopes': db_token['scopes']
    }

    access_token_expires = timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(data=db_token_data, expires_delta=access_token_expires)

    background_tasks.add_task(
        add_user_log, db_token['db_token'], 'Acquired APISession token', 'post.system.security.token')

    return {'access_token': access_token, 'token_type': 'bearer', 'scopes': db_token['scopes']}


@router.get('/session_user_organizations', response_model=List[UserOrganization])
async def get_session_user_organizations(
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'SELECT security.session_user_organizations($1)',
        api_session.db_token
    )

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        'Requested self organization list',
        'get.system.security.session_user_organizations')

    return record


@router.put('/session_user_organization')
async def put_session_user_organization(
        id_organization: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select security.set_session_user_organization($1, $2)',
        api_session.db_token,
        id_organization
    )

    message = f'Unsuccessfully changed session organization'
    if record:
        message = f'Successfully changed session organization'

    background_tasks.add_task(
        add_user_log, api_session.db_token, message, f'put.system.security.session_user_organization.{id_organization}')

    return record


@router.get('/organization_session_user', response_model=SessionUser)
async def get_organization_session_user(
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    log_message = 'Requested organization session user'
    log_code = 'get.system.security.get_organization_session_user'

    record = await db.fetch_organization_val(
        'SELECT security.organization_session_user($1)',
        api_session.db_token,
        log_message=log_message,
        log_code=log_code
    )
    # try:
    #     record = await db.fetch_val(
    #         'SELECT security.organization_session_user($1)',
    #         api_session.db_token
    #     )
    # except UndefinedObjectError:
    #     # Custom error raised equals no session organization is set
    #     await db.fetch_val(
    #         'SELECT security.add_user_log($1, $2, $3)',
    #         api_session.db_token,
    #         'Requested organization session user without defined session organization',
    #         'get.system.security.get_organization_session_user.null')
    #
    #     raise HTTPException(
    #         status_code=status.HTTP_403_FORBIDDEN,
    #         detail='Session organization is not set',
    #         headers={'WWW-Authenticate': 'Bearer'},
    #     )

    background_tasks.add_task(add_user_log, api_session.db_token, log_message, log_code)

    return record


@router.get('/manage_session_user', response_model=SessionUser)
async def get_manage_session_user(
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'SELECT security.manage_session_user($1)',
        api_session.db_token
    )

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        'Requested manage session user',
        'get.system.security.manage_session_user')

    return record


@router.get('/organization_session_user', response_model=SessionUser)
async def get_organization_session_user(
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_client]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'SELECT security.organization_session_user($1)',
        api_session.db_token
    )

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        'Requested organization session user',
        'get.system.security.organization_session_user')

    return record


@router.put('/session_user_change_password')
async def put_session_user_change_password(
        change_password: ChangePassword,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_client, scope_manager]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select security.change_password($1, $2, $3)',
        api_session.db_token,
        change_password.password,
        change_password.new_password
    )

    message = f'Unsuccessfully changed session user password'
    if record:
        message = f'Successfully changed session user password'

    background_tasks.add_task(
        add_user_log, api_session.db_token, message, f'put.system.security.session_user_change_password')

    if not record:
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail=[{'loc': ['body', 'password'],
                     'msg': 'Current password incorrect',
                     'type': 'value_error.incorrect'}]
        )

    return record


@router.post('/pwd-reset')
async def post_pwd_reset(
        pwd_reset: PwdReset,
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select security.set_pwd_reset($1)',
        pwd_reset.email
    )

    if record:
        email_set_new_password_url(pwd_reset.email, pwd_reset.url.replace('reset-code', record))

    return True


@router.get('/pwd-reset/can-reset/{reset_code:str}')
async def get_can_reset_pwd(
        reset_code: str,
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select security.can_reset($1)',
        reset_code
    )

    return record


@router.post('/pwd-reset/{reset_code:str}/reset')
async def post_reset_pwd(
        reset_pwd: ResetPwd,
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select security.reset_pwd($1, $2)',
        reset_pwd.reset_code,
        reset_pwd.new_password
    )

    return record
