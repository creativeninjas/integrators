from typing import List, Optional

from fastapi import (
    APIRouter,
    Depends,
    Security,
    Query
)

from starlette.background import BackgroundTasks

from background_tasks import add_user_log
from db.constants import scope_manager

from db.session import (
    Session,
    get_db
)

from models.system import Log, WorkerLogEntry

from models.utils import (
    Filter,
    filter_data
)

from security import (
    APISession,
    get_api_session
)

router = APIRouter()


#<editor-fold desc="Log functions">
@router.get('/', response_model=List[Log])
async def get_logs(
        background_tasks: BackgroundTasks,
        level: Optional[List[str]] = Query(None),
        data_filter: Filter = Depends(filter_data),
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select management.filter_system_logs($1, $2, $3, $4, $5, $6, $7)',
        api_session.db_token,
        data_filter.skip,
        data_filter.limit,
        data_filter.order_by,
        1,
        data_filter.q,
        level
    )

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        'Requested system log list',
        'get.system.logs.get_logs')

    return record


@router.get('/{log_id:int}', response_model=Log)
async def get_log(
        log_id: int,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select management.system_log($1, $2)', api_session.db_token, log_id)

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        f'Requested system log {log_id}',
        'get.system.logs.get_log')

    return record


@router.put('/workers/logs')
async def put_worker_log(
        worker_log: WorkerLogEntry,
        background_tasks: BackgroundTasks,
        api_session: APISession = Security(get_api_session, scopes=[scope_manager]),
        db: Session = Depends(get_db)):

    record = await db.fetch_val(
        'select management.worker_server_add_system_log($1, $2, $3, $4, $5, $6, $7)',
        api_session.db_token,
        worker_log.id_fsc,
        worker_log.id_work,
        worker_log.task_code,
        worker_log.level,
        worker_log.entry,
        worker_log.message,
    )

    background_tasks.add_task(
        add_user_log,
        api_session.db_token,
        f'Added worker system log',
        'get.system.logs.put_worker_log')

    return record
#</editor-fold>