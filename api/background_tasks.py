import json

import aiohttp
from asyncpg import Record, UndefinedObjectError

import settings
from db.session import Session
from models.clients import Organization, Work
from models.security import User
from notifications.users import email_set_new_password_url, email_user_password_reset, \
    email_new_organization_user_password, email_new_manage_user_password
from notifications.works import email_work_error


async def add_user_log(db_token: str, message: str, code: str):
    db = Session()
    await db.connect()
    await db.fetch_val('SELECT security.add_user_log($1, $2, $3)', db_token, message, code)
    await db.close()


async def set_status(id_foreign_system_connection: int, available: bool, unavailability_error: str):
    db = Session()
    await db.connect()
    try:
        db_token = await db.fetch_val(
            'SELECT security.authorize_user($1, $2, $3)', settings.SYSTEM_USER, settings.SYSTEM_USER_PASSWORD, 10)

        result = await db.fetch_val(
            'SELECT management.update_foreign_system_connection_status($1, $2, $3, $4)',
            str(db_token['db_token']),
            id_foreign_system_connection,
            available,
            unavailability_error)
    except UndefinedObjectError as  ex:
        print(ex)
        # Custom error raised equals wrong authorization data
        pass

    await db.close()


async def check_foreign_system_connection(foreign_system_connection: Record):
    try:
        if foreign_system_connection['id_foreign_system'] == 'tiki-taka':
            headers = {
                'accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded'
            }

            payload = {'grant_type': '',
                       'username': foreign_system_connection['username'],
                       'password': foreign_system_connection['pwd'],
                       'scope': '',
                       'client_id': '',
                       'client_secret': ''}

            url = f"{foreign_system_connection['address']}/clients/token"
            async with aiohttp.ClientSession() as session:
                async with session.post(url, data=payload, headers=headers) as response:
                    if response.status == 200:
                        available = True
                        unavailability_error = None
                    else:
                        available = False
                        unavailability_error = await response.text()

                    await set_status(foreign_system_connection['id'], available, unavailability_error)
        elif foreign_system_connection['id_foreign_system'] == 'horizon':
            ba = aiohttp.BasicAuth(login=foreign_system_connection['username'], password=foreign_system_connection['pwd'])
            url = f"{foreign_system_connection['address']}/rest/TNdmNomSar"
            async with aiohttp.ClientSession(auth=ba) as session:
                async with session.get(url) as response:
                    if response.status == 200:
                        available = True
                        unavailability_error = None
                    else:
                        available = False
                        unavailability_error = await response.text()

                await set_status(foreign_system_connection['id'], available, unavailability_error)
        elif foreign_system_connection['id_foreign_system'] == 'jumis-pro':
            check_connection = False
            try:
                address, port, db_name = foreign_system_connection['address'].split(',')
                if address and port and db_name:
                    check_connection = True
            except Exception:
                available = False
                unavailability_error = 'Adrese norādīta nekorekti. Nepieciešamais formāts: serveris,ports,db-nosaukums'

                await set_status(foreign_system_connection['id'], available, unavailability_error)

            if check_connection:
                xml_doc = ''
                headers = {
                    'Content-Type': 'application/json',
                    'accept': 'application/json'
                }
                url = f"{settings.JUMIS_URL}/export"
                payload = {
                    'username': foreign_system_connection['username'],
                    'password': foreign_system_connection['pwd'],
                    'database': foreign_system_connection['address'],
                    'apikey': settings.JUMIS_APIKEY,
                    'XMLrequest': xml_doc
                }
                async with aiohttp.ClientSession() as session:
                    async with session.post(url, data=json.dumps(payload).encode('utf-8'), headers=headers) as response:
                        if response.status == 200:
                            available = True
                            unavailability_error = None

                            response_txt = await response.text()
                            if response_txt == '"Neizdevās pieslēgties!"':
                                available = False
                                unavailability_error = response_txt
                            elif response_txt == '"nav datubāzē"':
                                available = False
                                unavailability_error = response_txt
                        else:
                            available = False
                            unavailability_error = await response.text()

                        await set_status(foreign_system_connection['id'], available, unavailability_error)

    except Exception as e:
        if hasattr(e, 'message'):
            message = e.message
        else:
            message = f"{e.__class__}: {str(e)}"
        await set_status(foreign_system_connection['id'], False, message)


async def send_new_user_password(user: User):
    email_new_manage_user_password(user)


async def send_new_organization_user_password(organization: Organization, user: User):
    email_new_organization_user_password(organization, user)


async def send_user_reset_password(user: User):
    email_user_password_reset(user)


async def send_work_error_to_organization(organization: Organization, work: Work, error_message):
    email_work_error(organization, work, error_message)
