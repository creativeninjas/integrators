import emails

from models.clients import (
    Work,
    Organization
)

import settings


def email_work_error(organization: Organization, work: Work, error_message: str):
    if organization.contact_email:
        text_version = f'TikiTaka Integratora darba "{work.title}" izpildes  kļūda.\n' \
                       f'Darbs ir deaktivizēts. Novērsiet kļūdu un aktivizējiet darbu.\n' \
                       f'Kļūdas paziņojums: \n' \
                       f'{error_message}'

        html_version = f'<p>TikiTaka Integratora darba "{work.title}" izpildes  kļūda.</p>' \
                       f'<p>Darbs ir deaktivizēts. Novērsiet kļūdu un aktivizējiet darbu.</p>' \
                       f'<p>Kļūdas paziņojums:</p>' \
                       f'<p>{error_message}</p>'

        m = emails.Message(
            text=text_version,
            html=html_version,
            subject='Integrācijas portāla darba izpildes kļūda',
            mail_from=(settings.EMAIL_FROM_TITLE, settings.EMAIL_FROM_EMAIL))

        response = m.send(to=organization.contact_email, smtp=settings.SMTP)

        if response.status_code not in [250, ]:
            # send again?
            pass
        else:
            return True
