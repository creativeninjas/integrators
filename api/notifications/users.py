import emails

from models.clients import Organization
from models.security import User

import settings


def email_new_manage_user_password(user: User):
    text_version = f'TikiTaka Integratora administrācijas lietotājs ar e-pastu "{user.email}" ir izveidots.\n' \
                   f'Autorizācijas parole: {user.pwd}'

    html_version = f'<p>TikiTaka Integratora administrācijas lietotājs ar e-pastu "{user.email}" ir izveidots.</p>' \
                   f'<p>Autorizācijas parole: {user.pwd}</p>'

    m = emails.Message(
        text=text_version,
        html=html_version,
        subject='Integrācijas portāla administrācija lietotājs izveidots',
        mail_from=(settings.EMAIL_FROM_TITLE, settings.EMAIL_FROM_EMAIL))

    response = m.send(to=user.email, smtp=settings.SMTP)

    if response.status_code not in [250, ]:
        # send again?
        pass
    else:
        return True


def email_new_organization_user_password(organization: Organization, user: User):
    text_version = f'TikiTaka Integratora organizācijas' \
                   f' "{organization.title}" lietotājs ar e-pastu "{user.email}" ir izveidots.\n' \
                   f'Autorizācijas parole: {user.pwd}'

    html_version = f'<p>TikiTaka Integratora organizācijas ' \
                   f'"{organization.title}" lietotājs ar e-pastu "{user.email}" ir izveidots.</p>' \
                   f'<p>Autorizācijas parole: {user.pwd}</p>'

    m = emails.Message(
        text=text_version,
        html=html_version,
        subject='Integrācijas portāla organizācijas lietotājs izveidots',
        mail_from=(settings.EMAIL_FROM_TITLE, settings.EMAIL_FROM_EMAIL))

    response = m.send(to=user.email, smtp=settings.SMTP)

    if response.status_code not in [250, ]:
        # send again?
        pass
    else:
        return True


def email_user_password_reset(user: User):
    text_version = f'TikiTaka Integratora lietotājam ar e-pastu "{user.email}" ir uzstādīt jauna parole.\n' \
                   f'Jaunā parole: {user.pwd}'

    html_version = f'<p>TikiTaka Integratora lietotājam ar e-pastu "{user.email}" ir uzstādīt jauna parole.</p>' \
                   f'<p>Jaunā parole: {user.pwd}</p>'

    m = emails.Message(
        text=text_version,
        html=html_version,
        subject='Integrācijas portāla jaunā parole',
        mail_from=(settings.EMAIL_FROM_TITLE, settings.EMAIL_FROM_EMAIL))

    response = m.send(to=user.email, smtp=settings.SMTP)

    if response.status_code not in [250, ]:
        # send again?
        pass
    else:
        return True


def email_set_new_password_url(email, reset_url):
    text_version = f'TikiTaka Integratora lietotājam ar e-pastu "{email}" ir pieteikta paroles atjaunošana.\n' \
                   f'Ja tas bijāt Jūs tad paroli varat atjaunot šeit: {reset_url}\n' \
                   f'ja tas nebijāt Jūs tad ignorējiet šo ziņu.'

    html_version = f'<p>TikiTaka Integratora lietotājam ar e-pastu "{email}" ir pieteikta paroles atjaunošana.</p>' \
                   f'<p>Ja tas bijāt Jūs tad paroli varat atjaunot šeit: <a href="{reset_url}" target="_blank">{reset_url}</a><br/>' \
                   f'ja tas nebijāt Jūs tad ignorējiet šo ziņu.</p>'

    m = emails.Message(
        text=text_version,
        html=html_version,
        subject='Integrācijas portāla paroles atjaunošana',
        mail_from=(settings.EMAIL_FROM_TITLE, settings.EMAIL_FROM_EMAIL))

    response = m.send(to=email, smtp=settings.SMTP)

    if response.status_code not in [250, ]:
        # send again?
        pass
    else:
        return True
