from typing import List

import uuid

from datetime import (
    datetime,
    timedelta
)

import jwt
from jwt import PyJWTError

from pydantic import (
    BaseModel,
    ValidationError
)

from fastapi import (
    Depends,
    HTTPException,
    status,
    Security
)

from fastapi.security import (
    OAuth2PasswordBearer,
    SecurityScopes
)

from asyncpg.exceptions import RaiseError, UndefinedObjectError
from asyncpg.exceptions._base import UnknownPostgresError

import ujson as json

from db.constants import authorization_user_not_found_errcode
from db.session import (
    get_db,
    Session
)

import settings


oauth2_scheme = OAuth2PasswordBearer(
    tokenUrl='/system/security/token'
)


class Token(BaseModel):
    access_token: str
    token_type: str


class AccessToken(BaseModel):
    access_token: str = None
    token_type: str
    scopes: List[str] = []


class APISession(BaseModel):
    id_user: int
    db_token: uuid.UUID
    disabled: bool = False


def create_access_token(*, data: dict, expires_delta: timedelta = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({'exp': expire})
    encoded_jwt = jwt.encode(to_encode, settings.SECRET_KEY, algorithm=settings.ALGORITHM)
    return encoded_jwt


async def get_api_session(
        security_scopes: SecurityScopes, 
        token: str = Depends(oauth2_scheme),
        db: Session = Depends(get_db)):

    if security_scopes.scopes:
        authenticate_value = f'Bearer scope="{security_scopes.scope_str}"'
    else:
        authenticate_value = f"Bearer anonymous"

    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail='Could not validate credentials',
        headers={'WWW-Authenticate': authenticate_value},
    )

    try:
        payload = jwt.decode(token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM])
        db_token: uuid.UUID = payload.get('db-token')
        
        if db_token is None:
            raise credentials_exception

    except (PyJWTError, ValidationError):
        raise credentials_exception

    try:
        record = await db.fetch_val('SELECT security.api_session($1, $2, $3)',
                                    db_token,
                                    security_scopes.scopes,
                                    settings.ACCESS_TOKEN_EXPIRE_MINUTES)

        api_session = APISession(**record)
    except RaiseError:
        api_session = None
    except UnknownPostgresError:
        api_session = None
    except UndefinedObjectError:
        # Add user log on no permission situation
        await db.fetch_val(
            'SELECT security.add_user_log($1, $2, $3)',
            db_token,
            f'Not enough permissions: ({security_scopes.scopes})',
            'system.security.forbidden')

        # Custom error raised equals no permission
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail='Not enough permissions',
            headers={'WWW-Authenticate': authenticate_value},
        )

    if api_session is None:
        raise credentials_exception

    if api_session.disabled:
        # Add user log if user is disabled
        await db.fetch_val(
            'SELECT security.add_user_log($1, $2, $3)',
            db_token,
            f'User disabled: ({security_scopes.scopes})',
            'system.security.forbidden.disabled')
        raise HTTPException(status_code=403, detail='User is disabled')

    return api_session

