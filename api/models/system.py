from datetime import datetime

from pydantic import BaseModel

from models.clients import Organization


class Log(BaseModel):
    id: int
    organization: Organization = None
    entry: str
    message: str = None
    level: str
    created_at: datetime


class WorkerLogEntry(BaseModel):
    id_fsc: int
    id_work: int
    task_code: str
    level: int
    entry: str
    message: str


class ClientLog(BaseModel):
    log: Log
    viewed: bool
