import datetime
from typing import List

from pydantic import BaseModel


class ImportUnit(BaseModel):
    id_fsc: int
    source_id: str
    short_title: str
    title: str


class IntegratedUnit(BaseModel):
    id: int
    target_id: str = None
    id_fsc: int = None
    id_task: int = None
    short_title: str
    title: str
    created_at: datetime.datetime
    checksum: str
    attributes: str = None


class ItemToIntegrate(BaseModel):
    id_item: int
    target_id: str = None
    id_fsc: int = None
    #id_task: int = None
    attributes: dict = None


class ImportTaxCategory(BaseModel):
    id_fsc: int
    source_id: str
    code: str
    rate: float = None
    non_taxable: bool
    title: str


class IntegratedTaxCategory(BaseModel):
    id: int
    target_id: str = None
    id_fsc: int = None
    id_task: int = None
    code: str
    rate: float = None
    non_taxable: bool
    title: str
    created_at: datetime.datetime
    checksum: str
    attributes: str = None


class ImportProductTaxCategory(BaseModel):
    id_tax_category: int
    source_id: str
    title: str


class IntegratedProductTaxCategory(BaseModel):
    id: int
    target_id: str = None
    id_fsc: int = None
    id_task: int = None
    title: str
    created_at: datetime.datetime
    checksum: str
    attributes: str = None


class Barcode(BaseModel):
    barcode: str
    is_main: bool


class ImportProduct(BaseModel):
    id_fsc: int
    source_id: str
    source_id_tax_category: str = None
    source_id_product_tax_category: str = None
    source_id_unit: str
    title: str
    print_title: str
    short_code: str
    price: float = None
    barcodes: List[Barcode] = None
    qrcode: str = None
    description: str = None
    combo: bool
    combo_product_source_ids: List[str] = None


class ProductTaxCategory(BaseModel):
    id: int
    id_tax_category: int
    source_id: str
    title: str
    created_at: datetime.datetime
    checksum: str


class TaxCategory(BaseModel):
    id: int
    id_fsc: int
    source_id: str
    code: str
    rate: float = None
    non_taxable: bool = None
    title: str
    created_at: datetime.datetime
    checksum: str
    product_tax_categories: List[ProductTaxCategory] = None


class TaxCategoryVersion(BaseModel):
    id: int
    id_tax_category: int
    code: str
    rate: float = None
    non_taxable: bool = None
    title: str
    created_at: datetime.datetime
    checksum: str
    product_tax_categories: List[ProductTaxCategory] = None


class Unit(BaseModel):
    id: int
    id_fsc: int
    source_id: str
    short_title: str
    title: str
    created_at: datetime.datetime
    checksum: str


class UnitVersion(BaseModel):
    id: int
    id_unit: int
    short_title: str
    title: str
    created_at: datetime.datetime
    checksum: str


class Product(BaseModel):
    id: int
    source_id: str = None
    id_fsc: int = None
    product_tax_category: ProductTaxCategory
    unit: Unit
    title: str
    print_title: str
    short_code: str
    price: float
    barcodes: List[Barcode] = None
    qrcode: str = None
    description: str = None
    combo: bool
    created_at: datetime.datetime
    checksum: str


class ProductVersion(BaseModel):
    id: int
    id_product: int
    product_tax_category: ProductTaxCategory
    unit: Unit
    title: str
    print_title: str
    short_code: str
    price: float
    barcodes: List[Barcode] = None
    qrcode: str = None
    description: str = None
    combo: bool
    created_at: datetime.datetime
    checksum: str
    combo_products: List[Product] = None


class IntegratedProduct(BaseModel):
    id: int
    target_id: str = None
    id_fsc: int = None
    id_task: int = None
    product_tax_category: ProductTaxCategory
    unit: Unit
    title: str
    print_title: str
    short_code: str
    price: float
    barcodes: List[Barcode] = None
    qrcode: str = None
    description: str = None
    combo: bool
    created_at: datetime.datetime
    checksum: str
    combo_products: List[Product] = None
    attributes: str = None


class FSCDataSummary(BaseModel):
    fsc_id: int
    fsc_title: str
    fsc_available: bool = None
    tax_categories: int
    units: int
    products: int
    employees: int
    discounts: int
    sections: int
    groups: int
    combo: int
    orders: int
    online_orders: int


class DeviceDocument(BaseModel):
    id: int
    id_device_fiscof: int
    title: str
    z: bool
    doc: str
    doc_datetime: datetime.datetime
    created_at: datetime.datetime


class DeviceFiscofDocument(DeviceDocument):
    device_serial_number: str


class ImportFisCof(BaseModel):
    id_fsc: int
    source_id: str
    device_serial_number: str
    doc: str


class IntegratedFisCof(BaseModel):
    id: int
    target_id: str = None
    id_fsc: int = None
    id_task: int = None
    device_serial_number: str
    document_count: int
    doc: str
    doc_datetime: datetime.datetime
    documents: List[DeviceDocument]
    created_at: datetime.datetime
    checksum: str = None
    attributes: str = None


class TikiTakaSummary(BaseModel):
    fsc_id: int
    fsc_title: str
    fsc_available: bool = None
    fiscofs: int = None
    documents: int = None
    online_documents: int = None


class DeviceFiscof(BaseModel):
    id: int
    id_fsc: int
    source_id: str
    device_serial_number: str
    document_count: int
    doc: str
    doc_datetime: datetime.datetime
    created_at: datetime.datetime
    documents: List[DeviceDocument] = None


class OnlineDocument(BaseModel):
    id: int
    id_fsc: int
    device_serial_number: str
    source_id: str
    title: str
    doc: str
    doc_datetime: datetime.datetime
    created_at: datetime.datetime


class DataVersion(BaseModel):
    id: int
    created_at: datetime.datetime


class ImportOnlineOrder(BaseModel):
    id_fsc: int
    source_id: str
    short_number: str
    long_number: str
    buyer_number: str = None
    buyer_vat_number: str = None
    buyer_title: str
    buyer_address: str = None
    buyer_email: str = None
    buyer_extra_information: str = None
    document: str
    products: str


class SetCompletedOnlineOrder(BaseModel):
    id: int
    completed_at: datetime.datetime
    document: str


class OnlineOrder(BaseModel):
    id: int
    id_fsc: int
    source_id: str
    short_number: str
    long_number: str
    client: str = None
    address: str = None
    comment: str = None
    buyer_number: str = None
    buyer_vat_number: str = None
    buyer_title: str
    buyer_address: str = None
    buyer_email: str = None
    buyer_extra_information: str = None
    products: str
    created_at: datetime.datetime
    completed_at: datetime.datetime = None
    completed: bool
    document: str


class IntegratedOnlineOrder(BaseModel):
    id: int
    target_id: str = None
    d_fsc: int = None
    id_task: int = None
    short_number: str
    long_number: str
    client: str = None
    address: str = None
    comment: str = None
    buyer_number: str = None
    buyer_vat_number: str = None
    buyer_title: str
    buyer_address: str = None
    buyer_email: str = None
    buyer_extra_information: str = None
    products: str
    created_at: datetime.datetime
    completed_at: datetime.datetime = None
    completed: bool
    checksum: str
    attributes: str = None
    document: str = None
