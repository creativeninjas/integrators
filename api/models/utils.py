from pydantic import BaseModel


class Filter(BaseModel):
    page: int = 1
    limit: int = 20
    order_by: str = 'id'
    direction: int = 0
    q: str = None

    @property
    def skip(self):
        return (self.page - 1) * self.limit


def filter_data(page: int = 1, limit: int = 20, order_by: str = 'id', direction: int = 0, q: str = None):
    return Filter(page=page, limit=limit, order_by=order_by, direction=direction, q=q)
