import datetime
from typing import List, Union

from pydantic import (
    BaseModel,
    EmailStr
)

from models.management import RunPeriod, ForeignSystemConnection


class Organization(BaseModel):
    id: int
    title: str
    contact_person: str
    contact_email: EmailStr
    contact_phone: str = None
    created_at: datetime.datetime = None


class WorkAsJob(BaseModel):
    id_work: int
    id_job: int
    title: str
    disabled: bool
    repeat: bool
    execute_at: datetime.datetime
    started_at: datetime.datetime = None
    completed_at: datetime.datetime = None
    canceled_at: datetime.datetime = None
    cancel_requested_at: datetime.datetime = None
    running: bool
    execution_error_message: str = None


class Work(BaseModel):
    id: int
    id_organization: int
    run_period: RunPeriod
    title: str
    disabled: bool
    repeat: bool
    minute: int = None
    hour: int = None
    week_day: int = None
    month_day: int = None
    month: int = None
    max_retry_attempts: int = None
    retry_interval_minutes: int = None
    retry_on_false: bool = None


class WorkTask(BaseModel):
    id: int
    id_work: int
    task_code: str
    foreign_system_connection: ForeignSystemConnection
    sequence_number: int
    attributes: Union[str, dict] = None


class WorkAndJob(BaseModel):
    work: Work
    work_as_job: WorkAsJob
    tasks: List[WorkTask]
    work_server_address: str = None


class AddWork(BaseModel):
    id: int = None
    title: str
    disabled: bool = False
    repeat: bool = False
    id_run_period: str
    minute: int = None
    hour: int = None
    week_day: int = None
    month_day: int = None
    month: int = None
    max_retry_attempts: int = None
    retry_interval_minutes: int = None
    retry_on_false: bool = None
    tasks: List[WorkTask]
