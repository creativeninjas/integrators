import datetime

from pydantic import BaseModel


class AvailabilityStatus(BaseModel):
    id: int
    id_work_server: int
    checked_at: datetime.datetime
    available: bool
    latest: bool
    unavailability_reason: str = None
    error_message: str = None


class WorkServer(BaseModel):
    id: int
    address: str
    disabled: bool = None
    status: int = None
    created_at: datetime.datetime = None
    availability_status: AvailabilityStatus = None


class WorkServerStatus(BaseModel):
    id: int
    available: bool
    unavailability_reason: str = None
    error_message: str = None


class ForeignSystem(BaseModel):
    id: str
    title: str
    system_type: str
    logo_base64: str = None
    disabled: bool
    created_at: datetime.datetime = None


class ForeignSystemConnection(BaseModel):
    id: int
    id_foreign_system: str = None
    id_organization: int = None
    title: str
    username: str
    pwd: str
    address: str
    available: bool = None
    unavailability_error: str = None
    created_at: datetime.datetime = None


class RunPeriod(BaseModel):
    id: str
    title: str
    frequency: int


class Task(BaseModel):
    id: int
    foreign_system: ForeignSystem
    title: str
    code: str
    available: bool
    description: str = None
    min_run_period: RunPeriod
    times_executed: int
    status: int


class UpdateTask(BaseModel):
    id: int
    title: str
    available: bool = None


class WorkTask(BaseModel):
    id: int
    id_work: int
    task_code: str
    foreign_system_connection: ForeignSystemConnection
    sequence_number: int
    attributes: str = None


class ErrorMessage(BaseModel):
    execution_error_message: str
