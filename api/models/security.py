from pydantic import (
    BaseModel,
    EmailStr,
    validator
)


class SessionUser(BaseModel):
    id: int
    email: EmailStr
    first_name: str
    last_name: str
    manager: bool
    id_organization: int = None


class User(BaseModel):
    id: int
    email: EmailStr
    first_name: str
    last_name: str
    pwd: str = None


class NewUser(User):
    password_to_email: bool = False


class Profile(BaseModel):
    email: EmailStr
    first_name: str
    last_name: str


class UserOrganization(BaseModel):
    id_organization: int
    organization_title: str


class ChangePassword(BaseModel):
    password: str
    new_password: str
    new_password_again: str

    @validator('new_password_again')
    def passwords_match(cls, v, values, **kwargs):
        if 'new_password' in values and v != values['new_password']:
            raise ValueError('Paroles nesakrīt')
        return v


class PwdReset(BaseModel):
    url: str
    email: str


class ResetPwd(BaseModel):
    reset_code: str
    new_password: str
    new_password_again: str

    @validator('new_password_again')
    def passwords_match(cls, v, values, **kwargs):
        if 'new_password' in values and v != values['new_password']:
            raise ValueError('Paroles nesakrīt')
        return v
