DB_CONNECTION_STRING = 'postgresql://user:pwd@127.0.0.1:5432/integrator'

# to get a string like this run:
# openssl rand -hex 32
SECRET_KEY = 'a3043fcbc7a1714f86538ad68894518d766a456971fbbe5248f14cd80fb68769'
ALGORITHM = 'HS256'
ACCESS_TOKEN_EXPIRE_MINUTES = 3000

# Integrator API user with Manager permissions
SYSTEM_USER = 'email'
SYSTEM_USER_PASSWORD = 'pwd'