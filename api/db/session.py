# import logging
from functools import lru_cache

import asyncpg

# import ujson as json
from asyncpg import UndefinedObjectError
from fastapi import HTTPException
from starlette import status

import settings


class Session(object):
    def __init__(self):
        self.connection = None

    # async def insert_management_user_activity(self, user_id, description):
    #     try:
    #         result = await self.fetchval('SELECT management.insert_user_activity($1, $2)', user_id, description)
    #     except Exception as e:
    #         logging.error(f'Error inserting management user activity: {str(e)}')

    # async def set_json(self):
    #     await self.connection.set_type_codec(
    #             'json',
    #             encoder=json.dumps,
    #             decoder=json.loads,
    #             schema='pg_catalog'
    #         )

    async def fetch(self, qry: str, *args):
        records = await self.connection.fetch(qry, *args)
        return records

    async def fetch_row(self, qry: str, *args):
        record = await self.connection.fetchrow(qry, *args)
        return record

    async def fetch_val(self, qry: str, *args):
        record = await self.connection.fetchval(qry, *args)
        return record

    async def fetch_organization_val(self, qry: str, *args, log_message: str, log_code: str):
        try:
            record = await self.connection.fetchval(qry, *args)

            return record
        except UndefinedObjectError:
            # Custom error raised equals no session organization is set
            await self.fetch_val(
                'SELECT security.add_user_log($1, $2, $3)',
                args[0],
                f'{log_message} without defined session organization',
                f'{log_code}.null')

            raise HTTPException(
                status_code=status.HTTP_403_FORBIDDEN,
                detail='Session organization is not set',
                headers={'WWW-Authenticate': 'Bearer'},
            )

    async def connect(self):
        self.connection = await asyncpg.connect(settings.DB_CONNECTION_STRING)

    async def close(self):
        await self.connection.close()


async def get_db() -> Session:
    db = None
    try:
        db = Session()
        await db.connect()
        # await db.set_json()
        yield db
    finally:
        if db:
            await db.close()
