-- Verify integrator:tables_and_types on pg

BEGIN;
    -- verify system log table and type
    DO $$
    declare
        _item system.t_log;
        _o clients.t_organization;
    BEGIN
        _item.organization := _o;
        select
            id, entry, message, level, created_at
        into
            _item.id, _item.entry, _item.message, _item.level, _item.created_at
        from system.log where false;
    END $$;

    -- verify work server  and work server availability status tables and types
    DO $$
    declare
        _item management.t_work_server;
        _item_status management.t_work_server_availability_status;
    BEGIN
        select
               id, id_work_server, checked_at, available, latest, unavailability_reason, error_message
        into
            _item_status.id, _item_status.id_work_server, _item_status.checked_at, _item_status.available, _item_status.latest, _item_status.unavailability_reason, _item_status.error_message
        from management.work_server_availability_status where false;

        select
               id, address, disabled, _item_status
        into
            _item.id, _item.address, _item.disabled, _item.availability_status

        from management.work_server where false;

    END $$;

    -- verify foreign system table and type
    DO $$
    declare
        _item management.t_foreign_system;
    BEGIN
        select
            id, title, logo_base64, disabled, created_at
        into
            _item.id, _item.title, _item.logo_base64, _item.disabled, _item.created_at
        from management.foreign_system where false;
    END $$;

    -- verify foreign system connection table and type
    DO $$
    declare
        _item clients.t_foreign_system_connection;
    BEGIN
        select id,
               id_foreign_system,
               id_organization,
               username,
               pwd,
               address,
               available,
               unavailability_error,
               created_at
        into _item.id,
             _item.id_foreign_system,
             _item.id_organization,
             _item.username,
             _item.pwd,
             _item.address,
             _item.available,
             _item.unavailability_error,
             _item.created_at
        from clients.foreign_system_connection where false;
    END $$;

    -- verify run period table and type
    DO $$
    declare
        _item management.t_run_period;
    BEGIN
        select id, title, frequency into _item.id, _item.title, _item.frequency from management.run_period where false;
    END $$;

    -- verify task table and type
    DO $$
    declare
        _item management.t_task;
    BEGIN
        select
            id, title, available, description, times_executed
        into
            _item.id, _item.title, _item.available, _item.description, _item.times_executed
        from management.task where false;
    END $$;

    -- verify work table and type
    DO $$
    declare
        _item clients.t_work;
        _rp management.t_run_period;
    BEGIN
        _item.run_period := _rp;

        select id,
               id_organization,
               title,
               repeat,
               minute,
               hour,
               week_day,
               month_day,
               month
        into
            _item.id,
            _item.id_organization,
            _item.title,
            _item.repeat,
            _item.minute,
            _item.hour,
            _item.week_day,
            _item.month_day,
            _item.month
        from clients.work where false;
    END $$;

     -- verify work task table and type
    DO $$
    declare
        _item clients.t_work_task;
        _fsc clients.t_foreign_system_connection;
    BEGIN
        _item.foreign_system_connection := _fsc;

        select id, id_work, sequence_number, attributes
        into _item.id, _item.id_work, _item.sequence_number, _item.attributes
        from clients.work_task where false;
    END $$;

    -- nodokļi
    -- darbinieki
    -- mervienibas
    -- produkti
    -- komplekti
    -- Atlaides
    -- Sekcijas
    -- Grupas
    -- Pasūtījumi
    -- Tiešsaistes pasūtījumi

ROLLBACK;
