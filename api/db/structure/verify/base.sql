-- Verify integrator:base on pg

BEGIN;

    select pg_catalog.has_schema_privilege('constants', 'usage');

    select pg_catalog.has_schema_privilege('system', 'usage');

    select pg_catalog.has_schema_privilege('security', 'usage');

    select pg_catalog.has_schema_privilege('clients', 'usage');

    select pg_catalog.has_schema_privilege('management', 'usage');

    select has_function_privilege('system.gen_tsvector(text[])', 'execute');

    do $$
    begin
        assert (select to_tsvector('english'::regconfig, 'orange ninja') = system.gen_tsvector('orange', 'ninja'));
    end $$;

    select id, email, first_name, last_name, password_hash, tsv
    from security.user where false;

    select id, title, description from security.scope where false;

    select id_user, id_scope from security.user_scope where false;

    select id, title, tsv from clients.organization where false;

    select id_user, id_organization from clients.organization_user where false;

--     DO $$
--     BEGIN
--         ASSERT (SELECT has_schema_privilege('flipr', 'usage'));
--     END $$;

ROLLBACK;