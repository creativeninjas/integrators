-- Revert integrator:tables_and_types from pg

begin;

    -- Sections
    drop table clients.integrated_section;
    drop table clients.section_version;
    drop table clients.section_product;
    drop table clients.section_group;
    drop table clients.section;

    drop type management.t_integrated_section;
    drop type clients.t_section;

    -- Groups
    drop table clients.integrated_group;
    drop table clients.group_version;
    drop table clients.group_product;
    drop table clients.group;

    drop type management.t_integrated_group;
    drop type clients.t_group;

    -- Discounts
    drop table clients.integrated_discount;
    drop table clients.discount_version;
    drop table clients.discount_product;
    drop table clients.discount;

    drop type management.t_integrated_discount;
    drop type clients.t_discount;

    -- Foreign system data - employee
    drop table clients.integrated_employee;
    drop table clients.employee_version;
    drop table clients.employee;

    drop type management.t_integrated_employee;
    drop type clients.t_employee;

     -- Orders
    drop table clients.integrated_order;
    drop table clients.order_version;
    drop table clients.order_product;
    drop table clients."order";

    drop type management.t_integrated_order;
    drop type clients.t_order;
    drop type clients.t_order_product;

    -- Online orders
    drop table clients.online_order_version;
    drop table clients.integrated_online_order;
    drop table clients.online_order;

    drop type management.t_integrated_online_order;
    drop type clients.t_online_order;

    -- Device fiscof and fiscof documents
    drop table clients.integrated_device_fiscof;
    drop table clients.device_doc;
    drop table clients.device_fiscof;

    drop type management.t_integrated_device_fiscof;
    drop type clients.t_device_fiscof;
    drop type clients.t_device_doc;

    -- Device online doc
    drop table clients.integrated_device_online_doc;
    drop table clients.device_online_doc;

    drop type management.t_integrated_device_online_doc;
    drop type clients.t_device_online_doc;

    -- Products
    drop table clients.integrated_product;
    drop table clients.product_version;
    drop table clients.combo_product;
    drop table clients.product;

    drop type management.t_integrated_product;
    drop type clients.t_product;
    drop type clients.t_barcode;

    -- Tax category and product tax category
    drop table clients.integrated_product_tax_category;
    drop table clients.integrated_tax_category;
    drop table clients.product_tax_category_version;
    drop table clients.product_tax_category;
    drop table clients.tax_category_version;
    drop table clients.tax_category;

    drop type management.t_integrated_product_tax_category;
    drop type management.t_integrated_tax_category;
    drop type clients.t_tax_category;
    drop type clients.t_product_tax_category;


    -- Units
    drop table clients.integrated_unit;
    drop table clients.unit_version;
    drop table clients.unit;

    drop type management.t_integrated_unit;
    drop type clients.t_unit;

    drop function constants.log_level_info();
    drop function constants.log_level_warning();
    drop function constants.log_level_error();

    drop table system.user_viewed_log;
    drop table system.log;
    drop type system.t_log;

    drop table clients.job;
    drop table clients.work_task;
    drop table clients.work;

    drop table management.task;
    drop table management.run_period;

    drop type clients.t_job;
    drop type clients.t_work_task;
    drop type clients.t_work;
    drop type clients.t_work_as_job;

    drop type management.t_task;
    drop type management.t_run_period;

    drop type management.t_work_server;
    drop type management.t_work_server_availability_status;

    drop table management.work_server_availability_status;
    drop table management.work_server;

    drop function constants.foreign_system_tiki_taka();
    drop function constants.foreign_system_horizon();
    drop function constants.foreign_system_jumis_pro();

    drop function constants.minute();
    drop function constants.hour();
    drop function constants.week_day();
    drop function constants.month();

    drop table clients.foreign_system_connection;
    drop table management.foreign_system;

    drop type management.t_foreign_system;
    drop type clients.t_foreign_system_connection;

    drop type clients.t_fsc_data_summary;
commit;
