-- Revert integrator:view_fiscof from pg

BEGIN;

    drop function clients.fsc_tikitaka_summary(uuid);

    drop function clients.online_document(uuid, bigint);
    drop function clients.document(uuid, bigint);
    drop function clients.fiscof(uuid, bigint);

    drop function clients.filter_fiscofs(uuid, bigint, bigint, bigint, text, smallint, text, text[]);
    drop function clients.filter_documents(uuid, bigint, bigint, bigint, text, smallint, text, text[]);
    drop function clients.filter_online_documents(uuid, bigint, bigint, bigint, text, smallint, text, text[]);

    drop type clients.t_tikitaka_summary;
    drop type clients.t_device_fiscof_doc;

COMMIT;
