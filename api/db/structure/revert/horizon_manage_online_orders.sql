-- Revert integrator:horizon_manage_online_orders from pg

BEGIN;

    drop function clients.filter_online_orders(uuid, bigint, bigint, bigint, text, smallint, text);
    drop function clients.online_order(uuid, bigint);

    drop function management.integrated_online_orders(uuid, text, bigint);
    drop function management.save_integrated_online_order(uuid, text, bigint, bigint, text, jsonb);

    drop function management.import_online_order(uuid, bigint, text, text, text, text, json, json);

    drop function management.set_completed_online_order(uuid, bigint, timestamp, json);
    drop function management.not_completed_integrated_online_orders(uuid, text, bigint);
    drop function management.next_completed_non_integrated_online_order(uuid, text, bigint);

    delete from management.task where id_foreign_system = 'horizon' and code = 'horizon_export_online_order';
    delete from management.task where id_foreign_system = 'horizon' and code = 'horizon_import_online_order';

    delete from management.task where id_foreign_system = 'tiki-taka' and code = 'tiki_taka_import_online_order';
    delete from management.task where id_foreign_system = 'tiki-taka' and code = 'tiki_taka_export_online_order_status';

    drop index clients.idx_not_completed_online_order;
    drop index clients.idx_completed_online_order;

    alter table clients.online_order drop column document;
    alter table clients.online_order drop column products;
    alter table clients.online_order add column products json[] not null;

    alter type clients.t_online_order drop attribute document;
    alter type clients.t_online_order alter attribute products type json[];

    alter type management.t_integrated_online_order drop attribute document;
    alter type management.t_integrated_online_order alter attribute products type json[];

COMMIT;
