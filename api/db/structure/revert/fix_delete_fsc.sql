-- Revert integrator:fix_delete_fsc from pg

BEGIN;

    create or replace function clients.delete_foreign_system_connection(
            in_token uuid,
            in_id bigint)
        returns boolean
        language plpgsql
        cost 1
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            delete from clients.foreign_system_connection where id = in_id and id_organization = _id_organization;

            return found;
        end if;

        return false;
    end;
    $$;

COMMIT;
