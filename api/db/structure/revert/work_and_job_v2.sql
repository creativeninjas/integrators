-- Revert integrator:work_and_job_v2 from pg

BEGIN;

    alter type clients.t_work_and_job drop attribute work_server_address;

    create or replace function clients.work_and_job(in_token uuid, in_id bigint)
        returns clients.t_work_and_job
        parallel safe
        cost 1000
        language plpgsql
    as $$
    declare
        _id_user bigint;
        _item clients.t_work_and_job := null;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            _item.work := clients.work(in_token,in_id);
            _item.work_as_job := clients.work_as_job(in_token,in_id);
            _item.tasks := clients.work_tasks(in_token,in_id);

            return _item;
        end if;

        return _item;
    end;
    $$;

COMMIT;
