-- Revert integrator:horizon_import_fiscof_with_return_recipe from pg

BEGIN;

    delete from management.task where id_foreign_system = 'horizon' and code = 'horizon_import_fiscof_with_return_recipe';

COMMIT;
