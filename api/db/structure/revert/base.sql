-- Revert integrator:base from pg

BEGIN;

    drop schema system cascade;

    drop schema security cascade;

    drop schema management cascade;

    drop schema clients cascade;

    drop schema constants cascade;

COMMIT;
