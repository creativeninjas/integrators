-- Revert integrator:functions from pg

BEGIN;
    drop function clients.run_periods(uuid);

    drop function management.work_server(uuid, bigint);
    drop function management.insert_work_server(uuid, text, boolean);
    drop function management.update_work_server(uuid, bigint, text, boolean);
    drop function management.filter_work_servers(uuid, bigint, bigint, text, smallint, text, int[]);
    drop function management.delete_work_server(uuid, bigint);

    drop function management.task(uuid, bigint);
    drop function management.insert_task(uuid, bigint, character varying(340), boolean, text, bigint);
    drop function management.update_task(uuid, bigint, bigint, character varying(340), boolean, text, bigint);
    drop function management.update_task(uuid, bigint, character varying(340), boolean);
    drop function management.filter_tasks(uuid, bigint, bigint, text, smallint, text, text[], int[]);
    drop function clients.filter_tasks(uuid, bigint, bigint, text, smallint, text, text[], int[]);
    drop function clients.task(uuid, bigint);
    drop function management.delete_task(uuid, bigint);
    drop function management.update_foreign_system_connection_status(uuid, bigint, bool, text);
    drop function management.update_work_server_status(uuid, bigint, bool, text, text);
    drop function management.get_actual_job_list(uuid, bigint);
    drop function management.get_job_next_task(uuid, bigint);
    drop function management.cancel_job_with_error(uuid, bigint, text);
    drop function management.integrated_units(uuid, text, bigint);
    drop function management.save_integrated_unit(uuid, text, bigint, bigint, text, jsonb);

    drop function management.import_product(uuid, bigint, text, text, text, text, character varying(250), character varying(40), character varying(50), numeric, clients.t_barcode[], text, text, bool, text[]);
    drop function management.integrated_products(uuid, text, bigint);
    drop function management.save_integrated_product(uuid, text, bigint, bigint, text, jsonb);

    drop function management.worker_server_add_system_log(uuid, bigint, bigint, text, int, text, text);

    drop function management.import_tax_category(uuid, bigint, text, character(1), numeric, bool, character varying(50));
    drop function management.import_product_tax_category(uuid, bigint, text, character varying(50));

    drop function management.integrated_tax_categories(uuid, text, bigint);
    drop function management.integrated_product_tax_categories(uuid, bigint, text, bigint);
    drop function management.save_integrated_tax_category(uuid, text, bigint, bigint, text, jsonb);
    drop function management.save_integrated_product_tax_category(uuid, text, bigint, bigint, text, jsonb);

    drop function system.gen_checksum(variadic text[]);
    drop function management.import_unit(uuid, bigint, text, character varying(10), character varying(40));

    drop function management.system_log(uuid, bigint);
    drop function management.filter_system_logs(uuid, bigint, bigint, text, smallint, text, text[]);
    drop function management.unread_system_log_count(uuid);

    drop function clients.system_log(uuid, bigint);
    drop function clients.filter_system_logs(uuid, bigint, bigint, text, smallint, text, text[]);
    drop function clients.unread_system_log_count(uuid);

    drop function clients.foreign_systems(uuid);
    drop function clients.foreign_system(uuid, text);
    drop function clients.foreign_system_connection(uuid, bigint);
    drop function clients.insert_foreign_system_connection(uuid, text, text, text, text, text);
    drop function clients.update_foreign_system_connection(uuid, bigint, text, text, text, text, bool, text);
    drop function clients.delete_foreign_system_connection(uuid, bigint);
    drop function clients.filter_foreign_system_connections(uuid, bigint, bigint, text, smallint, text, text);

    drop function clients.fsc_data_summary(uuid);

    drop function management.import_fiscof(uuid, bigint, text, text, xml);
    drop function management.next_non_integrated_fiscof(uuid, text, bigint, text[]);
    drop function management.save_integrated_fiscof(uuid, text, bigint, bigint, text, jsonb);

    drop function clients.filter_work(uuid, bigint, bigint, text, smallint, text, int[]);
    drop function clients.work_tasks(uuid, bigint);
    drop function clients.work(uuid, bigint);
    drop function clients.work_as_job(uuid, bigint);
    drop function clients.work_and_job(uuid, bigint);
    drop function clients.insert_work(uuid, text, boolean, boolean, text, smallint, smallint, smallint, smallint, smallint, clients.t_work_task[]);
    drop function clients.update_work(uuid, bigint, text, boolean, boolean, text, smallint, smallint, smallint, smallint, smallint, clients.t_work_task[]);

    drop function clients.get_random_work_server_id();
    drop function clients.calculate_work_next_runtime(text, smallint, smallint, smallint, smallint, smallint);
    drop function clients.delete_work(uuid, bigint);
    drop function management.organization_by_job_id(uuid, bigint);
    drop function management.work_by_job_id(uuid, bigint);

    drop type clients.t_work_and_job;

COMMIT;
