-- Revert integrator:reset_pwd from pg

BEGIN;

    drop function security.reset_pwd(text, text);
    drop function security.set_pwd_reset(text);
    drop function security.can_reset(text);

    drop table security.pwd_reset;

COMMIT;
