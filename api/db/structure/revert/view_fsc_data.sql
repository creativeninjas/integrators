-- Revert integrator:view_fsc_data from pg

BEGIN;
    drop function clients.product_versions(uuid, bigint);
    drop function clients.filter_products(uuid, bigint, bigint, bigint, text,smallint, text, boolean);
    drop function clients.combo_products(uuid, bigint, bigint);
    drop function clients.product(uuid, bigint);
    drop function clients.product_version(uuid, bigint);

    drop function clients.filter_units(uuid, bigint, bigint, bigint, text, smallint, text);
    drop function clients.unit(uuid, bigint);
    drop function clients.unit_versions(uuid, bigint);
    drop function clients.unit_version(uuid, bigint);

    drop function clients.tax_category_versions(uuid, bigint);
    drop function clients.tax_category_version(uuid, bigint);
    drop function clients.tax_category(uuid, bigint);
    drop function clients.filter_tax_categories(uuid, bigint, bigint, bigint, text, smallint, text);

    drop type clients.t_tax_category_version;
    drop type clients.t_unit_version;
    drop type clients.t_product_version;
    drop type clients.t_data_version;

COMMIT;
