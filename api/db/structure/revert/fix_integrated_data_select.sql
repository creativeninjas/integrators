-- Revert integrator:fix_integrated_data_select from pg

BEGIN;

    -- Integrated tax
    create or replace function management.integrated_tax_categories(
            in_token uuid,
            in_task_code text,
            in_id_fsc bigint
            )
        returns management.t_integrated_tax_category[]
        language plpgsql
        cost 1000
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _id_task bigint;
        _items management.t_integrated_tax_category[];
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            select id into _id_task from management.task where code = in_task_code;

            select
                array_agg((tc.id,
                itc.target_id,
                itc.id_fsc,
                itc.id_task,
                tc.code,
                tc.rate,
                tc.non_taxable,
                tc.title,
                tc.created_at,
                tc.checksum,
                itc.attributes)::management.t_integrated_tax_category) into _items
            from clients.tax_category as tc
            left join clients.integrated_tax_category as itc on (tc.id = itc.id_tax_category)
            where (itc.id_task = _id_task and itc.id_fsc = in_id_fsc) or (itc.id_task is null and itc.id_fsc is null);

            return _items;
        end if;

        return null;
    end;
    $$;

    -- Integrated product
    create or replace function management.integrated_products(
            in_token uuid,
            in_task_code text,
            in_id_fsc bigint
            )
        returns management.t_integrated_product[]
        language plpgsql
        cost 1000
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _id_task bigint;
        _items management.t_integrated_product[];
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            select id into _id_task from management.task where code = in_task_code;

            select
                array_agg((p.id,
                ip.target_id,
                ip.id_fsc,
                ip.id_task,
                (select (id, id_tax_category, (
                    select target_id
                    from clients.product_tax_category as ptc
                    left join clients.integrated_product_tax_category as iptc on (ptc.id = iptc.id_product_tax_category)
                    where iptc.id_fsc = in_id_fsc and id_product_tax_category = p.id_product_tax_category
                    ), title, created_at, checksum)::clients.t_product_tax_category from clients.product_tax_category where id = p.id_product_tax_category),
                (select (id, id_fsc, (
                    select target_id
                    from clients.unit as u
                    left join clients.integrated_unit as iu on (u.id = iu.id_unit)
                    where iu.id_fsc = in_id_fsc and id_unit = p.id_unit
                    ), short_title, title, created_at, checksum)::clients.t_unit from clients.unit where id = p.id_unit),
                p.title,
                p.print_title,
                p.short_code,
                p.price,
                p.barcodes,
                p.qrcode,
                p.description,
                p.combo,
                p.created_at,
                p.checksum,
                (
                    select array_agg((
                        cp.id,
                        (select (id, id_tax_category, source_id, title, created_at, checksum)::clients.t_product_tax_category from clients.product_tax_category where id = cp.id_product_tax_category),
                        cp.id_fsc,
                        (select (id, id_fsc, source_id, short_title, title, created_at, checksum)::clients.t_unit from clients.unit where id = cp.id_unit),
                        cp.source_id,
                        cp.title,
                        cp.print_title,
                        cp.short_code,
                        cp.price,
                        cp.barcodes,
                        cp.qrcode,
                        cp.description,
                        cp.combo,
                        cp.created_at,
                        cp.checksum
                    )::clients.t_product) from clients.combo_product
                    inner join clients.product as cp on (combo_product.id_combo = cp.id)
                    where combo_product.id_combo = p.id
                ),
                ip.attributes)::management.t_integrated_product) into _items
            from clients.product as p
            left join clients.integrated_product as ip on (p.id = ip.id_product)
            where (ip.id_task = _id_task and ip.id_fsc = in_id_fsc) or (ip.id_task is null and ip.id_fsc is null);

            return _items;
        end if;

        return null;
    end;
    $$;

    -- Integrated unit
    create or replace function management.integrated_units(
            in_token uuid,
            in_task_code text,
            in_id_fsc bigint
            )
        returns management.t_integrated_unit[]
        language plpgsql
        cost 1000
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _id_task bigint;
        _items management.t_integrated_unit[];
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            select id into _id_task from management.task where code = in_task_code;

            select
                array_agg((u.id,
                iu.target_id,
                iu.id_fsc,
                iu.id_task,
                u.short_title,
                u.title,
                u.created_at,
                u.checksum,
                iu.attributes)::management.t_integrated_unit) into _items
            from clients.unit as u
            left join clients.integrated_unit as iu on (u.id = iu.id_unit)
            where (iu.id_task = _id_task and iu.id_fsc = in_id_fsc) or (iu.id_task is null and iu.id_fsc is null);

            return _items;
        end if;

        return null;
    end;
    $$;



COMMIT;
