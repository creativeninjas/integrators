-- Deploy integrator:tables_and_types to pg
-- requires: base

begin;
    -- System log
    create function constants.log_level_info()
        returns character varying(8)
        language plpgsql
        immutable parallel safe
    as $$
    begin
        return 'INFO';
    end;
    $$;

    create function constants.log_level_warning()
        returns character varying(8)
        language plpgsql
        immutable parallel safe
    as $$
    begin
        return 'WARNING';
    end;
    $$;

    create function constants.log_level_error()
        returns character varying(8)
        language plpgsql
        immutable parallel safe
    as $$
    begin
        return 'ERROR';
    end;
    $$;


    create table system.log(
        id              bigserial,
        id_organization bigint,
        entry           text not null,
        message         text,
        level           character varying(8),
        created_at      timestamp default now() not null,
        tsv             tsvector generated always as (system.gen_tsvector(id::text, id_organization::text, entry, level, message)) stored,

        primary key(id),
        constraint fk_system_log_organization
            foreign key(id_organization) references clients.organization(id) on delete cascade on update cascade
    );

    create table system.user_viewed_log(
        id_user bigint,
        id_log bigint,
        created_at timestamp default now() not null,

        primary key(id_user, id_log),
        constraint fk_system_log_viewed_usr
            foreign key(id_user) references security."user"(id) on delete cascade on update cascade,
        constraint fk_system_log_viewed_log
            foreign key(id_log) references system.log(id) on delete cascade on update cascade
    );

    create type system.t_log as(
        id              bigint,
        organization    clients.t_organization,
        entry           text,
        message         text,
        level           character varying(8),
        created_at      timestamp
    );

    -- Work server
    create table management.work_server(
        id          bigserial,
        address     text not null,
        disabled    boolean default true not null,
        status      smallint not null default 0,
        created_at  timestamp default now() not null,
        tsv         tsvector generated always as (system.gen_tsvector(id::text, address::text, status::text)) stored,

        primary key(id),
        constraint uq_work_server_address unique(address)
    );

    create table management.work_server_availability_status(
        id                     bigserial,
        id_work_server         bigint not null,
        checked_at             timestamp default now() not null,
        available              boolean not null,
        latest                 boolean,
        unavailability_reason  character varying(50),
        error_message          text,
        tsv                    tsvector generated always as (system.gen_tsvector(id::text, id_work_server::text, unavailability_reason, error_message)) stored,

        primary key(id),
        constraint uq_latest_availability_status unique(id_work_server, latest),
        constraint fk_work_server
            foreign key(id_work_server) references management.work_server(id) on delete cascade on update cascade
    );

    create type management.t_work_server_availability_status as (
        id                     bigint,
        id_work_server         bigint,
        checked_at             timestamp,
        available              boolean,
        latest                 boolean,
        unavailability_reason  character varying(50),
        error_message          text
    );

    create type management.t_work_server as (
        id                   bigint,
        address              text,
        disabled             boolean,
        status               smallint,
        created_at           timestamp,
        availability_status  management.t_work_server_availability_status
    );

    -- Foreign systems

    create function constants.foreign_system_tiki_taka()
        returns character varying(18)
        language plpgsql
        immutable parallel safe
    as $$
    begin
        return 'tiki-taka';
    end;
    $$;

    create function constants.foreign_system_horizon()
        returns character varying(18)
        language plpgsql
        immutable parallel safe
    as $$
    begin
        return 'horizon';
    end;
    $$;

    create function constants.foreign_system_jumis_pro()
        returns character varying(18)
        language plpgsql
        immutable parallel safe
    as $$
    begin
        return 'jumis-pro';
    end;
    $$;

    create table management.foreign_system(
        id           character varying(18),
        title        character varying(250) not null,
        system_type  text,
        logo_base64  text,
        disabled     boolean default false not null,
        created_at   timestamp default now() not null,
        tsv tsvector generated always as (system.gen_tsvector(id::text, title)) stored,

        primary key(id)
    );

    -- Foreign system connections
    create table clients.foreign_system_connection(
        id                    bigserial,
        id_foreign_system     character varying(18) not null,
        id_organization       bigint not null,
        title                 text,
        username              character varying(250),
        pwd                   text,
        address               text not null,
        available             boolean,
        unavailability_error  text,
        created_at            timestamp default now() not null,
        tsv                   tsvector generated always as (system.gen_tsvector(id::text, username, address, unavailability_error)) stored,

        primary key(id),
        constraint fk_foreign_system
            foreign key(id_foreign_system) references management.foreign_system(id) on delete cascade on update cascade,
        constraint fk_foreign_system_connection_organization
            foreign key(id_organization) references clients.organization(id) on delete cascade on update cascade
    );

    create type management.t_foreign_system as (
        id character varying(18),
        title character varying(250),
        system_type text,
        logo_base64 text,
        disabled boolean,
        created_at timestamp
    );

    create type clients.t_foreign_system_connection as (
        id bigint,
        id_foreign_system text,
        id_organization bigint,
        title text,
        username character varying(250),
        pwd text,
        address text,
        available boolean,
        unavailability_error text,
        created_at timestamp
    );

    -- Task

    create function constants.minute()
        returns character varying(12)
        language plpgsql
        immutable parallel safe
    as $$
    begin
        return 'minute';
    end;
    $$;

    create function constants.hour()
        returns character varying(12)
        language plpgsql
        immutable parallel safe
    as $$
    begin
        return 'hour';
    end;
    $$;

    create function constants.week_day()
        returns character varying(12)
        language plpgsql
        immutable parallel safe
    as $$
    begin
        return 'week_day';
    end;
    $$;

    create function constants.month()
        returns character varying(12)
        language plpgsql
        immutable parallel safe
    as $$
    begin
        return 'month';
    end;
    $$;

    create table management.run_period
    (
        id        character varying(12),
        title     character varying(340) not null,
        frequency smallint not null,
        tsv       tsvector generated always as (system.gen_tsvector(id::text, title)) stored,

        primary key(id),
        constraint uq_frequency unique(frequency)
    );

    create table management.task(
        id                 serial,
        id_foreign_system  character varying(18) not null,
        title              character varying(340) not null,
        code               character varying(72) not null,
        available          boolean not null,
        description        text,
        id_min_run_period  character varying(12) not null, -- Example: long running task min_run_period is set to day
        times_executed     bigint default 0 not null,
        status             int generated always as (case when not available then 0 when available and times_executed = 0 then 1 when available and times_executed > 0 then 2 end) stored,
        tsv                tsvector generated always as (system.gen_tsvector(id::text, code, title, description)) stored,

        primary key(id),
        constraint uq_task_code unique(code),
        constraint fk_task_foreign_system
            foreign key(id_foreign_system) references management.foreign_system(id) on delete cascade on update cascade,
        constraint fk_task_min_run_period
            foreign key(id_min_run_period) references management.run_period(id) on delete cascade on update cascade
    );

    -- Work
    create table clients.work(
        id               bigserial,
        id_organization  bigint not null,
        id_run_period    character varying(12) not null,
        title            character varying(240),
        disabled         boolean not null default false,
        repeat           boolean not null,
        minute           smallint,
        hour             smallint,
        week_day         smallint,
        month_day        smallint,
        month            smallint,
        tsv              tsvector generated always as (system.gen_tsvector(id::text, title)) stored,

        primary key(id),
        constraint fk_work_organization
            foreign key(id_organization) references clients.organization(id) on delete cascade on update cascade,
        constraint fk_work_run_period
            foreign key(id_run_period) references management.run_period(id) on delete cascade on update cascade,
        constraint check_minute check((minute = -1) or (minute >= 0 and minute < 60)),
        constraint check_hour check((hour = -1) or (hour >= 0 and hour < 24)),
        constraint check_week_day check((week_day = -1) or (week_day > 0 and week_day < 8)),
        constraint check_month_day check((month_day = -1) or (month_day > 0 and month_day < 32)),
        constraint check_month check((month = -1) or (month > 0 and month < 13))
    );

    create table clients.work_task(
        id                            bigserial,
        id_work                       bigint not null,
        id_task                       int,
        id_foreign_system_connection  bigint not null,
        sequence_number               int not null,
        attributes                    jsonb,

        primary key(id),
        constraint fk_work_task_work
            foreign key(id_work) references clients.work(id) on delete cascade on update cascade,
        constraint fk_work_task_task
            foreign key(id_task) references management.task(id) on delete cascade on update cascade,
        constraint fk_work_task_foreign_system_connection
            foreign key(id_foreign_system_connection) references clients.foreign_system_connection(id) on delete cascade on update cascade,
        constraint uq_sequence_number unique(id_work, sequence_number)
    );

    create table clients.job(
        id                      bigserial,
        id_work_server          bigint not null,
        id_work                 bigint not null,
        id_current_work_task    bigint not null,
        execute_at              timestamp not null,
        started_at              timestamp,
        completed_at            timestamp,
        running                 boolean default false not null,
        cancel_requested_at     timestamp,
        canceled_at             timestamp,
        execution_error_message text,
        latest                  boolean,

        primary key(id),
        constraint fk_job_work_server
            foreign key(id_work_server) references management.work_server(id) on delete restrict on update cascade,
        constraint fk_job_work
            foreign key(id_work) references clients.work(id) on delete cascade on update cascade,
        constraint fk_job_work_task
            foreign key(id_current_work_task) references clients.work_task(id) on delete cascade on update cascade,
        constraint uq_latest_job unique(id_work, latest)
    );

    comment on table clients.job is 'Job is work executed or prepared for execution';

    create type management.t_run_period as(
        id        character varying(12),
        title     character varying(340),
        frequency smallint
    );

     create type management.t_task as(
        id                int,
        foreign_system    management.t_foreign_system,
        title             character varying(340),
        code              character varying(72),
        available         boolean,
        description       text,
        min_run_period    management.t_run_period,
        times_executed    bigint,
        status            int
    );

    -- Work
    create type clients.t_work as(
        id              bigint,
        id_organization bigint,
        run_period      management.t_run_period,
        title           character varying(240),
        disabled        boolean,
        repeat          boolean,
        minute          smallint,
        hour            smallint,
        week_day        smallint,
        month_day       smallint,
        month           smallint
    );

    create type clients.t_work_task as(
        id                        bigint,
        id_work                   bigint,
        task_code                 character varying(72),
        foreign_system_connection clients.t_foreign_system_connection,
        sequence_number           int,
        attributes                jsonb
    );

    create type clients.t_job as(
        id                      bigint,
        id_work_server          bigint,
        id_work                 bigint,
        id_current_work_task    bigint,
        execute_at              timestamp,
        started_at              timestamp,
        completed_at            timestamp,
        running                 boolean,
        cancel_requested_at     timestamp,
        canceled_at             timestamp,
        execution_error_message text,
        latest                  boolean
    );

     create type clients.t_work_as_job as(
        id_work                  bigint,
        id_job                   bigint,
        title                    character varying(240),
        disabled                 boolean,
        repeat                   boolean,
        execute_at               timestamp,
        started_at               timestamp,
        completed_at             timestamp,
        canceled_at              timestamp,
        cancel_requested_at      timestamp,
        running                  boolean,
        execution_error_message  text
    );

    -- Foreign system data - tax
    create table clients.tax_category(
	    id              bigserial,
	    id_fsc          bigint not null,
	    source_id       text not null,
	    code            character(1) not null,
	    rate            numeric,
	    non_taxable     boolean default false not null,
	    title           character varying(50) not null,
	    created_at      timestamp default now() not null,
	    checksum        character(64) not null,
	    tsv             tsvector generated always as (system.gen_tsvector(id::text, code, title, rate::text, source_id::text, id_fsc::text)) stored,

        primary key(id),
	    constraint fk_tax_category_foreign_system_connection
           foreign key(id_fsc) references clients.foreign_system_connection(id) on delete cascade on update cascade,
        constraint uq_fsc_tax_category_code unique(id_fsc, code),
        constraint uq_fsc_tax_category_source_id unique(id_fsc, source_id),
        constraint uq_fsc_tax_category_checksum unique(id_fsc, checksum)
    );

    comment on column clients.tax_category.code is 'Single letter code [A-Z]';

    create table clients.tax_category_version
    (
	    id              bigserial,
	    id_tax_category bigint,
	    code            character(1) not null,
	    rate            numeric,
	    non_taxable     boolean default false not null,
	    title           character varying(50) not null,
	    created_at      timestamp default now() not null,
	    checksum        character(64) not null,
	    tsv             tsvector generated always as (system.gen_tsvector(id::text, code, title, rate::text)) stored,

        primary key(id),
	    constraint fk_main_tax_category
           foreign key(id_tax_category) references clients.tax_category(id) on delete cascade on update cascade,
        constraint uq_tax_category_version_checksum unique(id_tax_category, checksum)
    );

    create table clients.product_tax_category
    (
	    id              bigserial,
	    id_tax_category bigint,
	    source_id       text not null,
	    title           character varying (50) not null,
	    created_at      timestamp default now() not null,
        checksum        character(64) not null,
	    tsv             tsvector generated always as (system.gen_tsvector(id::text, source_id, title)) stored,

	    primary key(id),
	    constraint fk_product_tax_category_tax_category
           foreign key(id_tax_category) references clients.tax_category(id) on delete cascade on update cascade,
        constraint uq_product_tax_category_source_id unique(id_tax_category, source_id),
        constraint uq_product_tax_category_checksum unique(id_tax_category, checksum)
    );

    create table clients.product_tax_category_version(
	    id                      bigserial,
	    id_product_tax_category bigint not null,
	    title                   character varying (50) not null,
	    created_at              timestamp default now() not null,
        checksum                character(64) not null,
	    tsv                     tsvector generated always as (system.gen_tsvector(id::text, title)) stored,

	    primary key(id),
	    constraint fk_main_product_tax_category
           foreign key(id_product_tax_category) references clients.product_tax_category(id) on delete cascade on update cascade,
        constraint uq_product_tax_category_version_checksum unique(id_product_tax_category, checksum)
    );

    create type clients.t_product_tax_category as(
         id              bigint,
         id_tax_category bigint,
         source_id       text,
         title           character varying(50),
         created_at      timestamp,
         checksum        character(64)
     );

    create type clients.t_tax_category as(
	    id                     bigint,
	    id_fsc                 bigint,
	    source_id              text,
	    code                   character(1),
	    rate                   numeric,
	    non_taxable            boolean,
	    title                  character varying(50),
	    created_at             timestamp,
	    checksum               character(64),
	    product_tax_categories clients.t_product_tax_category[]
    );

    create type management.t_integrated_product_tax_category as(
        id             bigint,
        target_id      text,
        id_fsc         bigint,
        id_task        bigint,
        title          character varying(50),
        created_at     timestamp,
        checksum       character(64),
        attributes     jsonb
    );

    create type management.t_integrated_tax_category as(
        id             bigint,
        target_id      text,
        id_fsc         bigint,
        id_task        bigint,
        code           character(1),
        rate           numeric,
	    non_taxable    boolean,
        title          character varying(50),
        created_at     timestamp,
        checksum       character(64),
        attributes     jsonb
    );

    create table clients.integrated_tax_category(
        id_tax_category bigint not null,
        target_id       text not null,
        id_fsc          bigint not null,
        id_task         bigint,
        attributes      jsonb,

        primary key(id_tax_category, target_id, id_fsc, id_task),
        constraint fk_source_tax_category
           foreign key(id_tax_category) references clients.tax_category(id) on delete cascade on update cascade,
        constraint fk_fsc_integrated_tax_category
           foreign key(id_fsc) references clients.foreign_system_connection(id) on delete cascade on update cascade,
        constraint fk_task_integrated_tax_category
           foreign key(id_task) references management.task(id) on delete cascade on update cascade
    );

    create table clients.integrated_product_tax_category(
        id_product_tax_category bigint not null,
        target_id               text not null,
        id_fsc                  bigint not null,
        id_task                 bigint,
        attributes              jsonb,

        primary key(id_product_tax_category, target_id, id_fsc, id_task),
        constraint fk_source_product_tax_category
           foreign key(id_product_tax_category) references clients.product_tax_category(id) on delete cascade on update cascade,
        constraint fk_fsc_integrated_product_tax_category
           foreign key(id_fsc) references clients.foreign_system_connection(id) on delete cascade on update cascade,
        constraint fk_task_integrated_product_tax_category
           foreign key(id_task) references management.task(id) on delete cascade on update cascade
    );

    -- Foreign system data - employee
    create table clients.employee(
        id         bigserial,
        id_fsc     bigint not null,
        source_id  text not null,
	    code       character varying(30) not null,
	    full_name  character varying(150) not null,
	    pincode    int,
	    created_at timestamp default now() not null,
	    checksum   character(64) not null,
	    tsv        tsvector generated always as (system.gen_tsvector(id::text, code, full_name, source_id, id_fsc::text)) stored,

	    primary key (id),
	    constraint fk_employee_foreign_system_connection
           foreign key(id_fsc) references clients.foreign_system_connection(id) on delete cascade on update cascade,
        constraint uq_fsc_employee unique(id_fsc, code),
        constraint uq_fsc_employee_source_id unique(id_fsc, source_id),
        constraint uq_fsc_employee_checksum unique(id_fsc, checksum)
    );

    create table clients.employee_version(
        id          bigserial,
        id_employee bigint not null,
	    code        character varying(30) not null,
	    full_name   character varying(150) not null,
	    pincode     int,
	    created_at  timestamp default now() not null,
	    checksum    character(64) not null,

	    primary key (id),
	    constraint fk_main_employee
           foreign key(id_employee) references clients.employee(id) on delete cascade on update cascade,
	    constraint uq_employee_version_checksum unique(id_employee, checksum)
    );

    create type clients.t_employee as(
        id         bigint,
        id_fsc     bigint,
        source_id  text,
        code       character varying(30),
        full_name  character varying(150),
        pincode    int,
        created_at timestamp,
        checksum   character(64)
    );

    create type management.t_integrated_employee as(
        id             bigint,
        target_id      text,
        id_fsc         bigint,
        id_task        bigint,
        code           character varying(30),
        full_name      character varying(150),
        pincode        int,
        created_at     timestamp,
        checksum       character(64),
        attributes     jsonb
    );

    create table clients.integrated_employee(
        id_employee bigint not null,
        target_id   text not null,
        id_fsc      bigint not null,
        id_task     bigint,
        attributes  jsonb,

        primary key(id_employee, target_id, id_fsc, id_task),
        constraint fk_source_employee
           foreign key(id_employee) references clients.employee(id) on delete cascade on update cascade,
        constraint fk_fsc_integrated_employee
           foreign key(id_fsc) references clients.foreign_system_connection(id) on delete cascade on update cascade,
        constraint fk_task_integrated_employee
           foreign key(id_task) references management.task(id) on delete cascade on update cascade
    );

    -- Units
    create table clients.unit(
	    id          bigserial,
	    id_fsc      bigint not null,
	    source_id   text not null,
	    short_title character varying(10) not null,
	    title       character varying(40) not null,
	    created_at  timestamp default now() not null,
	    checksum    character(64) not null,
	    tsv         tsvector generated always as (system.gen_tsvector(id::text, short_title, title, source_id)) stored,

	    primary key (id),
	    constraint fk_unit_organization
           foreign key(id_fsc) references clients.foreign_system_connection(id) on delete cascade on update cascade,
	    constraint uq_fsc_unit_short_title unique (short_title, id_fsc),
	    constraint uq_fsc_unit_source_id unique (source_id, id_fsc),
	    constraint uq_fsc_unit_checksum unique(id_fsc, checksum)
    );

    create table clients.unit_version(
	    id          bigserial,
	    id_unit     bigint,
	    short_title character varying(10) not null,
	    title       character varying(40) not null,
	    created_at  timestamp default now() not null,
	    checksum    character(64) not null,

	    primary key (id),
	    constraint fk_main_unit
           foreign key(id_unit) references clients.unit(id) on delete cascade on update cascade,
	    constraint uq_unit_version_checksum unique(id_unit, checksum)
    );

    create type clients.t_unit as(
        id          bigint,
        id_fsc      bigint,
        source_id   text,
        short_title character varying(10),
        title       character varying(40),
        created_at  timestamp,
        checksum    character(64)
    );

    create type management.t_integrated_unit as(
        id           bigint,
        target_id    text,
        id_fsc       bigint,
        id_task      bigint,
        short_title  character varying(10),
        title        character varying(40),
        created_at   timestamp,
        checksum     character(64),
        attributes   jsonb
    );

    create table clients.integrated_unit(
        id_unit    bigint not null,
        target_id  text not null,
        id_fsc     bigint not null,
        id_task    bigint,
        attributes jsonb,

        primary key(id_unit, target_id, id_fsc, id_task),
        constraint fk_source_unit
           foreign key(id_unit) references clients.unit(id) on delete cascade on update cascade,
        constraint fk_fsc_integrated_unit
           foreign key(id_fsc) references clients.foreign_system_connection(id) on delete cascade on update cascade,
        constraint fk_work_integrated_unit
           foreign key(id_task) references management.task(id) on delete cascade on update cascade
    );

    -- Products
    create type clients.t_barcode as(
        barcode text,
        is_main boolean
    );

    create table clients.product(
	    id                      bigserial,
	    id_product_tax_category bigint,
	    id_fsc                  bigint not null,
	    id_unit                 bigint,
	    source_id               text not null,
	    title                   character varying(250) not null,
	    print_title             character varying(40) not null,
	    short_code              character varying(50),
	    price                   numeric,
	    barcodes                clients.t_barcode[],
	    qrcode                  text,
	    description             text,
	    combo                   boolean default false not null,
	    created_at              timestamp default now() not null,
	    checksum                character(64) not null,
	    tsv                     tsvector generated always as (system.gen_tsvector(id::text, title, print_title, short_code, source_id, id_fsc::text)) stored,

        primary key (id),
	    constraint fk_product_organization
           foreign key(id_fsc) references clients.foreign_system_connection(id) on delete cascade on update cascade,
        constraint fk_product_product_tax_category
           foreign key(id_product_tax_category) references clients.product_tax_category(id) on delete restrict on update cascade,
        constraint fk_product_unit
           foreign key(id_unit) references clients.unit(id) on delete restrict on update cascade,
	    constraint uq_fsc_product_short_code unique (short_code, id_fsc),
	    constraint uq_fsc_product_source_id unique (source_id, id_fsc),
	    constraint uq_fsc_product_checksum unique(id_fsc, checksum),
        constraint product_tax_category_if_not_combo
            check ((combo AND (id_product_tax_category IS NULL)) OR ((NOT combo) AND (id_product_tax_category IS NOT NULL))),
        constraint price_if_not_combo
            check ((combo AND (price IS NULL)) OR ((NOT combo) AND (price IS NOT NULL)))
    );

    create table clients.combo_product(
        id_combo   bigint not null,
        id_product bigint not null,
        primary key(id_combo, id_product),
        constraint fk_combo_combo
           foreign key(id_combo) references clients.product(id) on delete cascade on update cascade,
        constraint fk_combo_product
           foreign key(id_product) references clients.product(id) on delete cascade on update cascade
    );

    create type clients.t_product as(
        id                   bigint,
        product_tax_category clients.t_product_tax_category,
        id_fsc               bigint,
        unit                 clients.t_unit,
        source_id            text,
        title                character varying(250),
        print_title          character varying(40),
        short_code           character varying(50),
        price                numeric,
        barcodes             clients.t_barcode[],
        qrcode               text,
        description          text,
        combo                boolean,
        created_at           timestamp,
        checksum             character(64)
    );

    create type management.t_integrated_product as(
        id                   bigint,
        target_id            text,
        id_fsc               bigint,
        id_task              bigint,
        product_tax_category clients.t_product_tax_category,
        unit                 clients.t_unit,
        title                character varying(250),
        print_title          character varying(40),
        short_code           character varying(50),
        price                numeric,
        barcodes             clients.t_barcode[],
        qrcode               text,
        description          text,
        combo                boolean,
        created_at           timestamp,
        checksum             character(64),
        combo_products       clients.t_product[],
        attributes           jsonb
    );

    create table clients.product_version(
	    id                      bigserial,
	    id_product              bigint not null,
	    id_product_tax_category clients.t_product_tax_category,
	    id_unit                 clients.t_unit,
	    title                   character varying(250) not null,
	    print_title             character varying(40) not null,
	    short_code              character varying(50),
	    price                   numeric,
	    barcodes                clients.t_barcode[],
	    qrcode                  text,
	    description             text,
	    combo                   boolean default false not null,
	    created_at              timestamp default now() not null,
	    checksum                character(64) not null,
	    combo_products          clients.t_product[],

        primary key (id),
	    constraint fk_main_product
           foreign key(id_product) references clients.product(id) on delete cascade on update cascade,
	    constraint uq_product_version_checksum unique(id_product, checksum)
    );

    create table clients.integrated_product(
        id_product bigint not null,
        target_id  text not null,
        id_fsc     bigint not null,
        id_task    bigint,
        attributes jsonb,

        primary key(id_product, target_id, id_fsc, id_task),
        constraint fk_source_product
           foreign key(id_product) references clients.product(id) on delete cascade on update cascade,
        constraint fk_fsc_integrated_product
           foreign key(id_fsc) references clients.foreign_system_connection(id) on delete cascade on update cascade,
        constraint fk_work_integrated_product
           foreign key(id_task) references management.task(id) on delete cascade on update cascade
    );

    -- Discounts
    create table clients.discount(
	    id                 bigserial,
	    id_fsc             bigint not null,
	    source_id          text not null,
	    title              text not null,
	    value              numeric not null,
	    money_discount     boolean not null,
	    date_from          date,
	    time_from          time,
	    date_to            date,
	    time_to            time,
	    is_manual_discount boolean not null,
	    created_at         timestamp default now() not null,
	    checksum           character(64) not null,
	    tsv                tsvector generated always as (system.gen_tsvector(id::text, title, source_id, id_fsc::text)) stored,

        primary key (id),
	    constraint fk_discount_foreign_system_connection
           foreign key(id_fsc) references clients.foreign_system_connection(id) on delete cascade on update cascade,
	    constraint uq_fsc_discount_source_id unique (source_id, id_fsc),
	    constraint uq_fsc_discount_checksum unique(id_fsc, checksum)
    );

    comment on column clients.discount.value is 'Discount value - 5% or 1.23 money';
    comment on column clients.discount.money_discount is 'Whether discount is in money value. If value false then discount is in %';
    comment on column clients.discount.is_manual_discount is 'Whether discount is applied manually';

    create table clients.discount_version(
	    id                 bigserial,
	    id_discount        bigint not null,
	    title              text not null,
	    value              numeric not null,
	    money_discount     boolean not null,
	    date_from          date,
	    time_from          time,
	    date_to            date,
	    time_to            time,
	    is_manual_discount boolean not null,
	    created_at         timestamp default now() not null,
	    checksum           character(64) not null,

        primary key (id),
	    constraint fk_main_discount
           foreign key(id_discount) references clients.organization(id) on delete cascade on update cascade,
	    constraint uq_discount_version_checksum unique(id_discount, checksum)
    );

    create type clients.t_discount as(
	    id                 bigint,
	    id_fsc             bigint,
	    source_id          text,
	    title              text,
	    value              numeric,
	    money_discount     boolean,
	    date_from          date,
	    time_from          time,
	    date_to            date,
	    time_to            time,
	    is_manual_discount boolean,
	    created_at         timestamp
    );

    create type management.t_integrated_discount as(
        id                   bigint,
        target_id            text,
        id_fsc               bigint,
        id_task              bigint,
        title                text,
	    value                numeric,
	    money_discount       boolean,
	    date_from            date,
	    time_from            time,
	    date_to              date,
	    time_to              time,
	    is_manual_discount   boolean,
        created_at           timestamp,
        checksum             character(64),
        attributes           jsonb
    );

    create table clients.discount_product(
        id_discount   bigint not null,
        id_product    bigint not null,
        display_order int,

        primary key(id_discount, id_product),
        constraint fk_discount_product_discount
           foreign key(id_discount) references clients.discount(id) on delete cascade on update cascade,
        constraint fk_discount_product_product
           foreign key(id_product) references clients.product(id) on delete cascade on update cascade
    );

    create table clients.integrated_discount(
        id_discount bigint not null,
        target_id   text not null,
        id_fsc      bigint not null,
        id_task     bigint,
        attributes  jsonb,

        primary key(id_discount, target_id, id_fsc, id_task),
        constraint fk_source_discount
           foreign key(id_discount) references clients.discount(id) on delete cascade on update cascade,
        constraint fk_fsc_integrated_discount
           foreign key(id_fsc) references clients.foreign_system_connection(id) on delete cascade on update cascade,
        constraint fk_work_integrated_discount
           foreign key(id_task) references management.task(id) on delete cascade on update cascade
    );

    -- Sections
    create table clients.section
    (
        id            bigserial,
        id_fsc        bigint not null,
        source_id     text not null,
        title         character varying(50) not null,
        display_order int,
        created_at    timestamp default now() not null,
        checksum      character(64) not null,
	    tsv           tsvector generated always as (system.gen_tsvector(id::text, title, source_id, id_fsc::text)) stored,

        primary key (id),
	    constraint fk_section_foreign_system_connection
           foreign key(id_fsc) references clients.foreign_system_connection(id) on delete cascade on update cascade,
	    constraint uq_fsc_section_source_id unique (source_id, id_fsc),
	    constraint uq_fsc_section_checksum unique(id_fsc, checksum)
    );

    create table clients.section_version
    (
        id              bigserial,
        id_section      bigint not null,
        title           character varying(50) not null,
        display_order   int,
        created_at      timestamp default now() not null,
        checksum        character(64) not null,

        primary key (id),
	    constraint fk_main_section
           foreign key(id_section) references clients.organization(id) on delete cascade on update cascade,
	    constraint uq_section_version_checksum unique(id_section, checksum)
    );

    create type clients.t_section as(
        id            bigint,
        id_fsc        bigint,
        source_id     text,
        title         character varying(50),
        display_order int,
        created_at    timestamp
    );

    create type management.t_integrated_section as(
        id             bigint,
        target_id      text,
        id_fsc         bigint,
        id_task        bigint,
        title          character varying(50),
        display_order  int,
        created_at     timestamp,
        checksum       character(64),
        attributes     jsonb
    );

    create table clients.section_product(
        id_section    bigint not null,
        id_product    bigint not null,
        display_order int,

        primary key(id_section, id_product),
        constraint fk_section_product_section
           foreign key(id_section) references clients.section(id) on delete cascade on update cascade,
        constraint fk_section_product_product
           foreign key(id_product) references clients.product(id) on delete cascade on update cascade
    );

    create table clients.integrated_section(
        id_section bigint not null,
        target_id  text not null,
        id_fsc     bigint not null,
        id_task    bigint,
        attributes jsonb,

        primary key(id_section, target_id, id_fsc, id_task),
        constraint fk_source_section
           foreign key(id_section) references clients.section(id) on delete cascade on update cascade,
        constraint fk_fsc_integrated_section
           foreign key(id_fsc) references clients.foreign_system_connection(id) on delete cascade on update cascade,
        constraint fk_work_integrated_section
           foreign key(id_task) references management.task(id) on delete cascade on update cascade
    );

    -- Groups
    create table clients.group
    (
        id            bigserial,
        id_fsc        bigint not null,
        source_id     text not null,
        title         character varying(50) not null,
        display_order int,
        created_at    timestamp default now() not null,
        checksum      character(64) not null,
	    tsv           tsvector generated always as (system.gen_tsvector(id::text, title, source_id)) stored,

        primary key (id),
	    constraint fk_group_foreign_system_connection
           foreign key(id_fsc) references clients.foreign_system_connection(id) on delete cascade on update cascade,
	    constraint uq_fsc_group_source_id unique (source_id, id_fsc),
	    constraint uq_fsc_group_checksum unique(id_fsc, checksum)
    );

    create table clients.group_version
    (
        id              bigserial,
        id_group        bigint not null,
        title           character varying(50) not null,
        display_order   int,
        created_at      timestamp default now() not null,
        checksum        character(64) not null,

        primary key (id),
	    constraint fk_main_group
           foreign key(id_group) references clients.group(id) on delete cascade on update cascade,
	    constraint uq_group_version_checksum unique(id_group, checksum)
    );

    create type clients.t_group as(
        id            bigint,
        id_fsc        bigint,
        source_id     text,
        title         character varying(50),
        display_order int,
        created_at    timestamp
    );

    create type management.t_integrated_group as(
        id             bigint,
        target_id      text,
        id_fsc         bigint,
        id_task        bigint,
        title          character varying(50),
        display_order  int,
        created_at     timestamp,
        checksum       character(64),
        attributes     jsonb
    );

    create table clients.section_group(
        id_section    bigint not null,
        id_group      bigint not null,
        display_order int,

        primary key(id_section, id_group),
        constraint fk_section_group_section
           foreign key(id_section) references clients.section(id) on delete cascade on update cascade,
        constraint fk_section_group_group
           foreign key(id_group) references clients.group(id) on delete cascade on update cascade
    );

    create table clients.group_product(
        id_group      bigint not null,
        id_product    bigint not null,
        display_order int,

        primary key(id_group, id_product),
        constraint fk_group_product_group
           foreign key(id_group) references clients.group(id) on delete cascade on update cascade,
        constraint fk_group_product_product
           foreign key(id_product) references clients.product(id) on delete cascade on update cascade
    );

    create table clients.integrated_group(
        id_group   bigint not null,
        target_id  text not null,
        id_fsc     bigint not null,
        id_task    bigint,
        attributes jsonb,

        primary key(id_group, target_id, id_fsc, id_task),
        constraint fk_source_group
           foreign key(id_group) references clients.group(id) on delete cascade on update cascade,
        constraint fk_fsc_integrated_group
           foreign key(id_fsc) references clients.foreign_system_connection(id) on delete cascade on update cascade,
        constraint fk_work_integrated_group
           foreign key(id_task) references management.task(id) on delete cascade on update cascade
    );

    -- Orders
    create table clients."order"(
	    id                      bigserial,
	    id_fsc                  bigint not null,
	    source_id               text not null,
	    short_number            character varying(30),
	    long_number             character varying(150),
	    client                  character varying(500),
	    address                 text,
	    comment                 text,
	    buyer_number            character varying(500),
	    buyer_vat_number        character varying(400),
	    buyer_title             character varying(400) not null,
	    buyer_address           text,
	    buyer_email             character varying(255),
	    buyer_extra_information text,
	    created_at              timestamp default now() not null,
	    checksum                character(64) not null,
	    tsv                     tsvector generated always as (system.gen_tsvector(id::text, short_number, long_number, client, address, comment, buyer_address, buyer_email, buyer_extra_information, buyer_number, buyer_title, buyer_vat_number, source_id)) stored,

        primary key (id),
	    constraint fk_order_organization
           foreign key(id_fsc) references clients.foreign_system_connection(id) on delete cascade on update cascade,
	    constraint uq_fsc_order_source_id unique (source_id, id_fsc),
	    constraint uq_fsc_order_checksum unique(id_fsc, checksum)
    );

    comment on column clients."order".client is 'Store client full name or company name';

    create table clients.order_product(
	    id         bigserial,
	    id_order   bigint not null,
	    id_unit    bigint,
	    id_product bigint not null,
	    quantity   numeric not null,
	    price      numeric,

	    primary key(id),
	    constraint fk_order_product_order
           foreign key(id_order) references clients."order"(id) on delete cascade on update cascade,
        constraint fk_order_product_unit
           foreign key(id_unit) references clients.unit(id) on delete restrict on update cascade,
        constraint fk_order_product_product
           foreign key(id_product) references clients.product(id) on delete restrict on update cascade
    );

    comment on column clients.order_product.price is 'Overrides product price';

    create type clients.t_order_product as(
	    id         bigint,
	    unit       clients.t_unit,
	    id_product clients.t_product,
	    quantity   numeric,
	    price      numeric
    );

    create type clients.t_order as(
        id                      bigint,
        id_fsc                  bigint,
        source_id               text,
        short_number            character varying(30),
        long_number             character varying(150),
        client                  character varying(500),
        address                 text,
        comment                 text,
        buyer_number            character varying(500),
        buyer_vat_number        character varying(400),
        buyer_title             character varying(400),
        buyer_address           text,
        buyer_email             character varying(255),
        buyer_extra_information text,
        products                clients.t_order_product[],
        created_at              timestamp
    );

    create type management.t_integrated_order as(
        id                      bigint,
        target_id               text,
        id_fsc                  bigint,
        id_task                 bigint,
        short_number            character varying(30),
        long_number             character varying(150),
        client                  character varying(500),
        address                 text,
        comment                 text,
        buyer_number            character varying(500),
        buyer_vat_number        character varying(400),
        buyer_title             character varying(400),
        buyer_address           text,
        buyer_email             character varying(255),
        buyer_extra_information text,
        products                clients.t_order_product[],
        created_at              timestamp,
        checksum                character(64),
        attributes              jsonb
    );

    create table clients.order_version(
	    id                      bigserial,
	    id_order                bigint not null,
	    short_number            character varying(30),
	    long_number             character varying(150),
	    client                  character varying(500),
	    address                 text,
	    comment                 text,
	    buyer_number            character varying(500),
	    buyer_vat_number        character varying(400),
	    buyer_title             character varying(400) not null,
	    buyer_address           text,
	    buyer_email             character varying(255),
	    buyer_extra_information text,
	    created_at              timestamp default now() not null,
	    checksum                character(64) not null,
	    order_products          clients.t_order_product[],

        primary key (id),
	    constraint fk_main_order
           foreign key(id_order) references clients.order(id) on delete cascade on update cascade,
	    constraint uq_order_version_checksum unique(id_order, checksum)
    );

    create table clients.integrated_order(
        id_order   bigint not null,
        target_id  text not null,
        id_fsc     bigint not null,
        id_task    bigint,
        attributes jsonb,

        primary key(id_order, target_id, id_fsc, id_task),
        constraint fk_source_order
           foreign key(id_order) references clients.order(id) on delete cascade on update cascade,
        constraint fk_fsc_integrated_order
           foreign key(id_fsc) references clients.foreign_system_connection(id) on delete cascade on update cascade,
        constraint fk_work_integrated_order
           foreign key(id_task) references management.task(id) on delete cascade on update cascade
    );

    -- Online orders
    create table clients.online_order
    (
	    id                      bigserial,
	    id_fsc                  bigint not null,
	    source_id               text not null,
	    short_number            character varying(30) not null,
	    long_number             character varying(150) not null,
	    client                  character varying(500),
	    address                 text,
	    comment                 text,
	    buyer_number            character varying(500),
	    buyer_vat_number        character varying(400),
	    buyer_title             character varying(400) not null,
	    buyer_address           text,
	    buyer_email             character varying(255),
	    buyer_extra_information text,
	    products                json[] not null,
	    created_at              timestamp default now() not null,
	    completed_at            timestamp,
	    completed               boolean default false,
	    checksum                character(64) not null,
	    tsv                     tsvector generated always as (system.gen_tsvector(id::text, short_number, long_number, client, address, comment, buyer_address, buyer_email, buyer_extra_information, buyer_number, buyer_title, buyer_vat_number, source_id)) stored,

        primary key (id),
	    constraint fk_online_order_foreign_system_connection
           foreign key(id_fsc) references clients.foreign_system_connection(id) on delete cascade on update cascade,
	    constraint uq_fsc_online_order_source_id unique (source_id, id_fsc),
	    constraint uq_fsc_online_order_checksum unique(id_fsc, checksum),
	    constraint uq_fsc_online_order_short_number unique (short_number, id_fsc),
	    constraint uq_fsc_online_order_long_number unique (long_number, id_fsc)
    );

    comment on column clients.online_order.client is 'Store client full name or company name';
    comment on column clients.online_order.products is 'Products is simple json object list';

    create table clients.online_order_version
    (
	    id                      bigserial,
	    id_online_order         bigint not null,
	    short_number            character varying(30) not null,
	    long_number             character varying(150) not null,
	    client                  character varying(500),
	    address                 text,
	    comment                 text,
	    buyer_number            character varying(500),
	    buyer_vat_number        character varying(400),
	    buyer_title             character varying(400) not null,
	    buyer_address           text,
	    buyer_email             character varying(255),
	    buyer_extra_information text,
	    products                json[] not null,
	    created_at              timestamp default now() not null,
	    completed_at            timestamp,
	    completed               boolean default false,
	    checksum                character(64) not null,

        primary key (id),
	    constraint fk_main_online_order
           foreign key(id_online_order) references clients.online_order(id) on delete cascade on update cascade,
	    constraint uq_online_order_version_checksum unique(id_online_order, checksum)
    );

    create type clients.t_online_order as(
        id                      bigint,
        id_fsc                  bigint,
        source_id               text,
        short_number            character varying(30),
        long_number             character varying(150),
        client                  character varying(500),
        address                 text,
        comment                 text,
        buyer_number            character varying(500),
        buyer_vat_number        character varying(400),
        buyer_title             character varying(400),
        buyer_address           text,
        buyer_email             character varying(255),
        buyer_extra_information text,
        products                json[],
        created_at              timestamp,
        completed_at            timestamp,
        completed               boolean
    );

    create type management.t_integrated_online_order as(
        id                      bigint,
        target_id               text,
        id_fsc                  bigint,
        id_task                 bigint,
        short_number            character varying(30),
        long_number             character varying(150),
        client                  character varying(500),
        address                 text,
        comment                 text,
        buyer_number            character varying(500),
        buyer_vat_number        character varying(400),
        buyer_title             character varying(400),
        buyer_address           text,
        buyer_email             character varying(255),
        buyer_extra_information text,
        products                json[],
        completed_at            timestamp,
        completed               boolean,
        created_at              timestamp,
        checksum                character(64),
        attributes              jsonb
    );

    create table clients.integrated_online_order(
        id_online_order bigint not null,
        target_id       text not null,
        id_fsc          bigint not null,
        id_task         bigint,
        attributes      jsonb,

        primary key(id_online_order, target_id, id_fsc, id_task),
        constraint fk_source_online_order
           foreign key(id_online_order) references clients.online_order(id) on delete cascade on update cascade,
        constraint fk_fsc_integrated_unit
           foreign key(id_fsc) references clients.foreign_system_connection(id) on delete cascade on update cascade,
        constraint fk_work_integrated_unit
           foreign key(id_task) references management.task(id) on delete cascade on update cascade
    );

    -- Arējās sistēmas dati

    create table clients.device_fiscof(
        id                   bigserial,
        id_fsc               bigint not null,
        source_id            text not null,
        device_serial_number text not null,
        document_count       int not null,
        doc                  xml not null,
        doc_datetime         timestamp not null,
        created_at           timestamp default now() not null,
        tsv                  tsvector generated always as (system.gen_tsvector(id::text, device_serial_number, source_id)) stored,

        primary key (id),
	    constraint fk_device_fiscof_foreign_system_connection
           foreign key(id_fsc) references clients.foreign_system_connection(id) on delete cascade on update cascade,
	    constraint uq_fsc_device_fiscof_source_id unique (source_id, id_fsc)
    );

    create table clients.device_doc(
        id               bigserial,
        id_device_fiscof bigint not null,
        title            character varying(84) not null,
        z                boolean not null,
        doc              xml not null,
        doc_datetime     timestamp not null,
        created_at       timestamp default now() not null,
        tsv              tsvector generated always as (system.gen_tsvector(id::text, title)) stored,

        primary key (id),
	    constraint fk_device_doc_fiscof
           foreign key(id_device_fiscof) references clients.device_fiscof(id) on delete cascade on update cascade
    );

    create type clients.t_device_doc as(
        id               bigint,
        id_device_fiscof bigint,
        title            character varying(84),
        z                boolean,
        doc              xml,
        doc_datetime     timestamp,
        created_at       timestamp
    );

    create type clients.t_device_fiscof as(
        id                   bigint,
        id_fsc               bigint,
        source_id            text,
        device_serial_number text,
        document_count       int,
        doc                  xml,
        doc_datetime         timestamp,
        created_at           timestamp,
        documents            clients.t_device_doc[]
    );

    create type management.t_integrated_device_fiscof as(
        id                      bigint,
        target_id               text,
        id_fsc                  bigint,
        id_task                 bigint,
        device_serial_number    text,
        document_count          int,
        doc                     xml,
        doc_datetime            timestamp,
        documents               clients.t_device_doc[],
        created_at              timestamp,
        checksum                character(64),
        attributes              jsonb
    );

    create table clients.integrated_device_fiscof(
        id_device_fiscof     bigint not null,
        target_id            text not null,
        id_fsc               bigint not null,
        id_task              bigint,
        attributes           jsonb,

        primary key(id_device_fiscof, target_id, id_fsc, id_task),
        constraint fk_source_device_fiscof
           foreign key(id_device_fiscof) references clients.device_fiscof(id) on delete cascade on update cascade,
        constraint fk_fsc_integrated_device_fiscof
           foreign key(id_fsc) references clients.foreign_system_connection(id) on delete cascade on update cascade,
        constraint fk_work_integrated_device_fiscof
           foreign key(id_task) references management.task(id) on delete cascade on update cascade
    );

    -- Device online doc
    create table clients.device_online_doc(
        id                   bigserial,
        id_fsc               bigint not null,
        device_serial_number text,
        source_id            text not null,
        title                character varying(84) not null,
        doc                  xml not null,
        doc_datetime         timestamp not null,
        created_at           timestamp default now() not null,
        tsv                  tsvector generated always as (system.gen_tsvector(id::text, title)) stored,

        primary key (id),
	    constraint uq_fsc_device_online_doc unique (source_id, id_fsc)
    );

    create type clients.t_device_online_doc as(
        id                   bigint,
        id_fsc               bigint,
        device_serial_number text,
        source_id            text,
        title                character varying(84),
        doc                  xml,
        doc_datetime         timestamp,
        created_at           timestamp
    );

    create type management.t_integrated_device_online_doc as(
        id                      bigint,
        target_id               text,
        id_fsc                  bigint,
        id_task                 bigint,
        device_serial_number    text,
        title                   character varying(84),
        doc                     xml,
        doc_datetime            timestamp,
        created_at              timestamp,
        checksum                character(64),
        attributes              jsonb
    );


    create table clients.integrated_device_online_doc(
        id_device_online_doc bigint not null,
        target_id            text not null,
        id_fsc               bigint not null,
        id_task              bigint,
        attributes           jsonb,

        primary key(id_device_online_doc, target_id, id_fsc, id_task),
        constraint fk_source_device_online_doc
           foreign key(id_device_online_doc) references clients.device_online_doc(id) on delete cascade on update cascade,
        constraint fk_fsc_integrated_device_online_doc
           foreign key(id_fsc) references clients.foreign_system_connection(id) on delete cascade on update cascade,
        constraint fk_work_integrated_device_online_doc
           foreign key(id_task) references management.task(id) on delete cascade on update cascade
    );

    create type clients.t_fsc_data_summary as(
        fsc_id bigint,
        fsc_title text,
        fsc_available bool,
        tax_categories bigint,
        units bigint,
        products bigint,
        employees bigint,
        discounts bigint,
        sections bigint,
        "groups" bigint,
        combo bigint,
        orders bigint,
        online_orders bigint
    );

    -- Pieejamie izpildes laiki
    insert into management.run_period(id, title, frequency) values (constants.minute(), 'Minūte', 1::smallint);
    insert into management.run_period(id, title, frequency) values (constants.hour(), 'Stunda', 2::smallint);
    insert into management.run_period(id, title, frequency) values (constants.week_day(), 'Nedēļas diena', 3::smallint);
    insert into management.run_period(id, title, frequency) values (constants.month(), 'Mēnesis', 5::smallint);

    -- Pieejamās sistēmas
    insert into management.foreign_system(id, title, system_type) values (constants.foreign_system_tiki_taka(), 'Tiki-Taka Manage', 'Kases sistēma');
    insert into management.foreign_system(id, title, system_type) values (constants.foreign_system_horizon(), 'Horizon', 'Grāmatvedības sistēma');
    insert into management.foreign_system(id, title, system_type) values (constants.foreign_system_jumis_pro(), 'JumisPRO', 'Grāmatvedības sistēma');

    -- Piejamie uzdevumi
    insert into management.task(id_foreign_system, title, code, available, description, id_min_run_period)
    values('tiki-taka', 'Ielādet mērvienības', 'tiki_taka_import_units', true, 'Ielādēt mērvienības Tiki-Taka', 'week_day'); -- 1

    insert into management.task(id_foreign_system, title, code, available, description, id_min_run_period)
    values('tiki-taka', 'Ielādet produktus', 'tiki_taka_import_products', true, 'Ielādēt produktus Tiki-Taka', 'week_day'); -- 2

    insert into management.task(id_foreign_system, title, code, available, description, id_min_run_period)
    values('tiki-taka', 'Ielādet nodokļu grupas', 'tiki_taka_import_tax', true, 'Ielādēt nodokļu kategoirjas un grupas Tiki-Taka', 'week_day'); -- 3

    insert into management.task(id_foreign_system, title, code, available, description, id_min_run_period)
    values('tiki-taka', 'Ielādet darbiniekus', 'tiki_taka_import_employees', true, 'Ielādēt darbiniekus Tiki-Taka', 'week_day'); -- 4

    insert into management.task(id_foreign_system, title, code, available, description, id_min_run_period)
    values('horizon', 'Izgūt mērvienības', 'horizon_export_units', true, 'Mērvienību izgūšana no sistēmas', 'week_day'); -- 5

    insert into management.task(id_foreign_system, title, code, available, description, id_min_run_period)
    values('horizon', 'Izgūt nodokļu grupas', 'horizon_export_tax', true, 'Nodoklu kategoriju un nodokļu grupu izgūšana', 'week_day'); -- 6

    insert into management.task(id_foreign_system, title, code, available, description, id_min_run_period)
    values('horizon', 'Izgūt darbiniekus', 'horizon_export_employees', true, 'Darbinieku izgūšana', 'week_day'); -- 7

    insert into management.task(id_foreign_system, title, code, available, description, id_min_run_period)
    values('horizon', 'Izgūt produktus', 'horizon_export_products', true, 'Produktu izgūšana no sistēmas', 'week_day'); -- 8

    insert into management.task(id_foreign_system, title, code, available, description, id_min_run_period)
    values('jumis-pro', 'Izgūt produktus', 'jumis_pro_export_products', true, 'Produktu izgūšana no sistēmas', 'week_day'); -- 9

    insert into management.task(id_foreign_system, title, code, available, description, id_min_run_period)
    values('tiki-taka', 'Izgūt fiscof', 'tiki_taka_export_fiscof', true, 'Fiscof izgūšana', 'week_day'); -- 10

    insert into management.task(id_foreign_system, title, code, available, description, id_min_run_period)
    values('horizon', 'Veidots kases pavadzīmes', 'horizon_import_fiscof', true, 'Vediot kases pavadzīmes no fiscof', 'week_day'); -- 11

commit;
