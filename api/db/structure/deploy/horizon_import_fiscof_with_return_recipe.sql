-- Deploy integrator:horizon_import_fiscof_with_return_recipe to pg

BEGIN;

    insert into management.task(id_foreign_system, title, code, available, description, id_min_run_period)
    values('horizon', 'Veidot kases pavadzīmes ar Tiki-Taka atmaksas čekiem', 'horizon_import_fiscof_with_return_recipe', true, 'Veidot kases pavadzīmes ar Tiki-Taka atmaksas čekiem', 'week_day'); -- 12

COMMIT;
