-- Deploy integrator:work_and_job_v2 to pg
-- requires: horizon_manage_online_orders

BEGIN;

    alter type clients.t_work_and_job add attribute work_server_address text;

    create or replace function clients.work_and_job(in_token uuid, in_id bigint)
        returns clients.t_work_and_job
        parallel safe
        cost 1000
        language plpgsql
    as $$
    declare
        _id_user bigint;
        _item clients.t_work_and_job := null;
        _work_server_id bigint;
        _work_as_job clients.t_work_as_job;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            _item.work := clients.work(in_token,in_id);
            _work_as_job := clients.work_as_job(in_token,in_id);
            _item.work_as_job := _work_as_job;
            _item.tasks := clients.work_tasks(in_token,in_id);

            if _work_as_job.id_job is not null then
                select id_work_server into _work_server_id from clients.job where id = _work_as_job.id_job;
                select work_server.address into _item.work_server_address from management.work_server where id = _work_server_id;
            else
                _item.work_server_address := null;
            end if;

            return _item;
        end if;

        return _item;
    end;
    $$;



COMMIT;
