-- Deploy integrator:functions to pg
-- requires: tables_and_types

BEGIN;
    create function clients.run_periods(in_token uuid)
        returns management.t_run_period[]
        language plpgsql
        cost 1000
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _run_periods management.t_run_period[];
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            select array_agg(data.item) into _run_periods from(
                select (id, title, frequency)::management.t_run_period as item
                from management.run_period order by frequency asc
            ) as data;

            return _run_periods;
        end if;

        return null;
    end;
    $$;

    -- Work server
    create function clients.calculate_work_next_runtime(in_id_run_period text, in_minute smallint, in_hour smallint, in_week_day smallint, in_month_day smallint, in_month smallint)
        returns timestamp
        language plpgsql
        cost 1000
        volatile parallel safe
    as $$
    declare
        _next_run_time timestamp := null;
        _week_day_ts timestamp;
        _week_day int;
        _week_day_loop_failsafe int := 10;
        _year int;
    begin
        if in_id_run_period = constants.minute() then
            -- after defined minute interval
            if in_minute is not null and in_minute > -1 then
                if in_minute = 0 then
                    in_minute := 1;
                end if;
                _next_run_time := CURRENT_TIMESTAMP + (in_minute * interval '1 minute');
            end if;
        elseif in_id_run_period = constants.hour() then
            -- every hour in same minute
            if (in_minute is not null and in_minute > -1) and in_hour is null then
                _next_run_time := date_trunc('hour', CURRENT_TIMESTAMP + interval '1 hour') + (in_minute * interval '1 minute');
            -- defined hour and minute
            elseif (in_minute is not null and in_minute > -1) and (in_hour is not null and in_hour > -1) then
                _next_run_time := date_trunc('day', CURRENT_TIMESTAMP + interval '1 day') + (in_hour * interval '1 hour') + (in_minute * interval '1 minute');
            end if;
        elseif in_id_run_period = constants.week_day() then
            -- defined week day hour and minute
            if in_week_day is not null and in_hour is not null and in_minute is not null then
                if in_week_day = 7 then
                    in_week_day := 0;
                end if;

                _week_day_ts := CURRENT_TIMESTAMP;
                while _week_day_loop_failsafe > 0
                loop
                    _week_day_ts := _week_day_ts + interval '1 day';
                    _week_day := extract(DOW from _week_day_ts);
                    if _week_day = in_week_day then
                        _next_run_time := date_trunc('day', _week_day_ts) + (in_hour * interval '1 hour') + (in_minute * interval '1 minute');
                        exit;
                    end if;
                    _week_day_loop_failsafe := _week_day_loop_failsafe - 1;
                end loop;
            end if;
        elseif in_id_run_period = constants.month() then
            -- defined month, month date, hour and minute
            if extract(MONTH from CURRENT_TIMESTAMP) < in_month then
                _year := extract(YEAR from CURRENT_TIMESTAMP);
            elseif extract(MONTH from CURRENT_TIMESTAMP) = in_month then
                if extract(DAY from CURRENT_TIMESTAMP) < in_month_day then
                    _year := extract(YEAR from CURRENT_TIMESTAMP);
                else
                    _year := extract(YEAR from CURRENT_TIMESTAMP + interval '1 year');
                end if;
            else
                _year := extract(YEAR from CURRENT_TIMESTAMP + interval '1 year');
            end if;

            _next_run_time := to_timestamp(format('%s.%s.%s %s:%s', _year, in_month, in_month_day, in_hour, in_minute), 'YYYY.MM.DD HH24:MI');
        end if;

        return _next_run_time;
    end;
    $$;

    create function clients.get_random_work_server_id()
        returns bigint
        language plpgsql
        cost 1000
        volatile parallel safe
    as $$
    declare
        _id_work_server bigint;
    begin
        select id into _id_work_server from management.work_server where not disabled and status = 2 order by random() limit 1;
        return _id_work_server;
    end;
    $$;

    create function management.work_server(in_token uuid, in_id_work_server bigint)
        returns management.t_work_server
        language plpgsql
        cost 1
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _work_server management.t_work_server;
        _status management.t_work_server_availability_status;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            select id, address, disabled, status, created_at into _work_server
            from management.work_server
            where id = in_id_work_server;

            if found then
                select id, id_work_server, checked_at, available, latest, unavailability_reason, error_message
                into _status
                from management.work_server_availability_status
                where id_work_server = _work_server.id and latest;

                if found then
                    _work_server.availability_status := _status;
                else
                    _work_server.availability_status := null;
                end if;

                return _work_server;
            end if;
        end if;

        return null;
    end;
    $$;

    create function management.insert_work_server(
            in_token uuid,
            in_address text,
            in_disabled boolean)
        returns management.t_work_server
        language plpgsql
        cost 1
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _id_work_server bigint;
        _status smallint;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            if in_disabled then
                -- set status disabled
                _status := 1::smallint;
            else
                -- set status not checked
                _status := 0::smallint;
            end if;
            insert into management.work_server(address, disabled, status)
            values(in_address, in_disabled, _status) returning id into _id_work_server;

            return management.work_server(in_token, _id_work_server);
        end if;

        return null;
    end;
    $$;

    create function management.update_work_server(
            in_token uuid,
            in_id bigint,
            in_address text,
            in_disabled boolean)
        returns management.t_work_server
        language plpgsql
        cost 1
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _status smallint;
        _available bool := null;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            if in_disabled then
                -- set status disabled
                _status := 1::smallint;
            else
                select available into _available from management.work_server_availability_status where id_work_server = in_id;
                if _available is null then
                    -- no status
                    _status := 0::smallint;
                elseif _available then
                    -- server available
                    _status := 2::smallint;
                else
                    -- server unavailable
                    _status := 3::smallint;
                end if;
            end if;

            update management.work_server
            set address = in_address,
                disabled = in_disabled,
                status = _status
            where id = in_id;

            return management.work_server(in_token, in_id);
        end if;

        return null;
    end;
    $$;

    create function management.update_work_server_status(
            in_token uuid,
            in_id bigint,
            in_available bool,
            in_unavailability_reason text,
            in_error_message text)
        returns bool
        language plpgsql
        cost 1
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _status smallint;
        _disabled bool;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            select disabled into _disabled from management.work_server where id = in_id;
            if not _disabled then
                update management.work_server_availability_status set latest = null where id_work_server = in_id;

                insert into management.work_server_availability_status(id_work_server, available, latest, unavailability_reason, error_message)
                values(in_id, in_available, true, in_unavailability_reason, in_error_message);

                if in_available then
                    _status := 2;
                else
                    _status := 3;
                end if;

                update management.work_server
                set status = _status
                where id = in_id;

                return found;
            end if;

            return false;
        end if;

        return false;
    end;
    $$;

    create function management.get_actual_job_list(
            in_token uuid,
            in_id_work_server bigint)
        returns bigint[]
        language plpgsql
        cost 100
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _job_list bigint[];
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            select array_agg(job.id) into _job_list
            from clients.job
            inner join clients.work as w on (job.id_work = w.id)
            where
                id_work_server = in_id_work_server
            and
                latest
            and
                execute_at <= now()
            and
                not running and completed_at is null
            and
                not w.disabled;

            return _job_list;
        end if;

        return null;
    end;
    $$;

    create function management.get_job_next_task(
            in_token uuid,
            in_id_job bigint)
        returns clients.t_work_task
        language plpgsql
        cost 1000
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _running boolean;
        _id_work bigint;
        _next_task_id bigint;
        _current_task_id bigint;
        _current_task_sequence int;
        _work_task clients.t_work_task;

        _repeat boolean;
        _minute smallint;
        _hour smallint;
        _week_day smallint;
        _month_day smallint;
        _month smallint;
        _id_run_period text;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            select id_work, running into _id_work, _running from clients.job where id = in_id_job;

            select id_current_work_task into _current_task_id from clients.job where id = in_id_job;
            if not _running then
                update clients.job set running = true, started_at = now() where id = in_id_job;
                _next_task_id := _current_task_id;
            else
                select sequence_number into _current_task_sequence from clients.work_task where id = _current_task_id;
                select id into _next_task_id from clients.work_task where id_work = _id_work and sequence_number > _current_task_sequence limit 1;

                if _next_task_id is null then
                    update clients.job set running = false, completed_at = now() where id = in_id_job;

                    -- set next job if task is repeated
                    select repeat into _repeat from clients.work where id = _id_work;
                    if _repeat then
                        update clients.job set latest = null where id = in_id_job;

                        select id into _current_task_id from clients.work_task where id_work = _id_work order by sequence_number asc limit 1;
                        select
                               id_run_period, minute, hour, week_day, month_day, month
                        into
                               _id_run_period, _minute, _hour, _week_day, _month_day, _month
                        from clients.work where id = _id_work;

                        insert into clients.job(id_work_server, id_work, id_current_work_task, execute_at, started_at, completed_at, running, cancel_requested_at, canceled_at, execution_error_message, latest)
                        values (clients.get_random_work_server_id(),
                                _id_work,
                                _current_task_id,
                                clients.calculate_work_next_runtime(_id_run_period, _minute, _hour, _week_day, _month_day, _month),
                                null, null, false, null, null, null, true
                                );
                    end if;

                    return null;
                else
                    update clients.job set id_current_work_task = _next_task_id where id = in_id_job;
                end if;
            end if;

            select
                   wt.id, wt.id_work, t.code,
                   (fsc.id,
                    fsc.id_foreign_system,
                    fsc.id_organization,
                    fsc.title,
                    fsc.username,
                    fsc.pwd,
                    fsc.address,
                    fsc.available,
                    fsc.unavailability_error,
                    fsc.created_at)::clients.t_foreign_system_connection,
                   wt.sequence_number, wt.attributes into _work_task
            from clients.work_task as wt
            inner join management.task as t on (wt.id_task = t.id)
            inner join clients.foreign_system_connection as fsc on (wt.id_foreign_system_connection = fsc.id)
            where wt.id = _next_task_id;

            -- Increase execution times
            update management.task set times_executed = times_executed + 1 where code = _work_task.task_code;

            return _work_task;

        end if;

        return null;
    end;
    $$;

    create function management.filter_work_servers(in_token uuid, in_skip bigint, in_limit bigint, in_order_by text default 'id', in_order_direction smallint default 0, in_fts_qry text default null, in_status int[] default null)
        returns management.t_work_server[]
        language plpgsql
        cost 100
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _items management.t_work_server[];
        _content_type text := 'management.t_work_server';
        _sql text := 'select %s from management.work_server as ws ' ||
                     'left join management.work_server_availability_status as wsas on (ws.id = wsas.id_work_server and wsas.latest)';
        _type_select text := format(
            '(ws.id, ws.address, ws.disabled, ws.status, ws.created_at, ' ||
            'case when wsas.id is not null then (wsas.id, wsas.id_work_server, wsas.checked_at, wsas.available, wsas.latest, wsas.unavailability_reason, wsas.error_message)::%s else null end)::%s',
            'management.t_work_server_availability_status', _content_type);
        _where text;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            _where := concat_ws(' AND ',
                case when in_fts_qry is not null then 'ws.tsv @@ websearch_to_tsquery($3)' end,
                case when in_status is not null then 'ws.status = any($4)' end);

            _sql := system.build_filter_sql(
                _sql,
                _content_type,
                _where,
                in_order_by,
                in_order_direction,
                _type_select,
                'ws');

            execute _sql into _items using in_limit, in_skip, in_fts_qry, in_status;
        end if;

        return _items;
    end;
    $$;

    create function management.delete_work_server(
            in_token uuid,
            in_id bigint)
        returns boolean
        language plpgsql
        cost 1
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            delete from management.work_server where id = in_id;

            return found;
        end if;

        return false;
    end;
    $$;

    -- Task

    create function management.task(in_token uuid, in_id_task bigint)
        returns management.t_task
        language plpgsql
        cost 1
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _task management.t_task;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            select task.id,
                   (fs.id, fs.title, fs.system_type, fs.logo_base64, fs.disabled, fs.created_at)::management.t_foreign_system,
                   task.title,
                   task.code,
                   task.available,
                   task.description,
                   (rp.id, rp.title, rp.frequency)::management.t_run_period,
                   task.times_executed,
                   task.status
            into
                _task
            from management.task
            inner join management.foreign_system as fs on (task.id_foreign_system = fs.id)
            inner join management.run_period as rp on (task.id_min_run_period = rp.id)
            where task.id = in_id_task;

            if found then
                return _task;
            end if;
        end if;

        return null;
    end;
    $$;

    create function management.insert_task(
            in_token uuid,
            in_id_foreign_system bigint,
            in_title character varying(340),
            in_available boolean,
            in_description text,
            in_id_min_run_period bigint
    )
        returns management.t_task
        language plpgsql
        cost 1
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _id_task bigint;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            insert into management.task(id_foreign_system, title, available, description, id_min_run_period, times_executed)
            values(in_id_foreign_system, in_title, in_available, in_description, in_id_min_run_period, 0) returning id into _id_task;

            return management.task(in_token, _id_task);
        end if;

        return null;
    end;
    $$;

    create function management.update_task(
            in_token uuid,
            in_id bigint,
            in_id_foreign_system bigint,
            in_title character varying(340),
            in_available boolean,
            in_description text,
            in_id_min_run_period bigint)
        returns management.t_task
        language plpgsql
        cost 1
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            update management.task
            set id_foreign_system = in_id_foreign_system,
                title = in_title,
                available = in_available,
                description = in_description,
                id_min_run_period = in_id_min_run_period
            where id = in_id;

            return management.task(in_token, in_id);
        end if;

        return null;
    end;
    $$;

    create function management.update_task(
            in_token uuid,
            in_id bigint,
            in_title character varying(340),
            in_available boolean)
        returns management.t_task
        language plpgsql
        cost 1
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            update management.task
            set title = in_title, available = in_available
            where id = in_id;

            return management.task(in_token, in_id);
        end if;

        return null;
    end;
    $$;

    create function management.filter_tasks(in_token uuid, in_skip bigint, in_limit bigint, in_order_by text default 'id', in_order_direction smallint default 0, in_fts_qry text default null, in_foreign_systems text[] default null, in_status int[] default null)
        returns management.t_task[]
        language plpgsql
        cost 100
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _items management.t_task[];
        _content_type text := 'management.t_task';
        _sql text := 'select %s from management.task as t ' ||
                     'inner join management.foreign_system as fs on (t.id_foreign_system = fs.id) ' ||
                     'inner join management.run_period as rp on (t.id_min_run_period = rp.id) ';
        _type_select text := format(
            '(t.id,  (fs.id, fs.title, fs.system_type, fs.logo_base64, fs.disabled, fs.created_at)::%s, t.title, t.code, ' ||
            't.available, t.description, (rp.id, rp.title, rp.frequency)::%s, t.times_executed, t.status)::%s',
            'management.t_foreign_system', 'management.t_run_period', _content_type);
        _where text;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then

            _where := concat_ws(' AND ',
                case when in_fts_qry is not null then 't.tsv @@ websearch_to_tsquery($3)' end,
                case when in_foreign_systems is not null then 't.id_foreign_system = any($4)' end,
                case when in_status is not null then 't.status = any($5)' end);

            _sql := system.build_filter_sql(
                _sql,
                _content_type,
                _where,
                in_order_by,
                in_order_direction,
                _type_select,
                't');

            raise notice '%s', _sql;

            execute _sql into _items using in_limit, in_skip, in_fts_qry, in_foreign_systems, in_status;
        end if;

        return _items;
    end;
    $$;

    create function clients.filter_tasks(in_token uuid, in_skip bigint, in_limit bigint, in_order_by text default 'id', in_order_direction smallint default 0, in_fts_qry text default null, in_foreign_systems text[] default null, in_status int[] default null)
        returns management.t_task[]
        language plpgsql
        cost 100
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _items management.t_task[];
        _content_type text := 'management.t_task';
        _sql text := 'select %s from management.task as t ' ||
                     'inner join management.foreign_system as fs on (t.id_foreign_system = fs.id) ' ||
                     'inner join management.run_period as rp on (t.id_min_run_period = rp.id) ';
        _type_select text := format(
            '(t.id,  (fs.id, fs.title, fs.system_type, fs.logo_base64, fs.disabled, fs.created_at)::%s, t.title, t.code, ' ||
            't.available, t.description, (rp.id, rp.title, rp.frequency)::%s, t.times_executed, t.status)::%s',
            'management.t_foreign_system', 'management.t_run_period', _content_type);
        _where text;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then

            _where := concat_ws(' AND ',
                case when in_fts_qry is not null then 't.tsv @@ websearch_to_tsquery($3)' end,
                case when in_foreign_systems is not null then 't.id_foreign_system = any($4)' end,
                case when in_status is not null then 't.status = any($5)' end);

            _sql := system.build_filter_sql(
                _sql,
                _content_type,
                _where,
                in_order_by,
                in_order_direction,
                _type_select,
                't');

            raise notice '%s', _sql;

            execute _sql into _items using in_limit, in_skip, in_fts_qry, in_foreign_systems, in_status;
        end if;

        return _items;
    end;
    $$;

    create function management.delete_task(
            in_token uuid,
            in_id bigint)
        returns boolean
        language plpgsql
        cost 1
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            delete from management.task where id = in_id;

            return found;
        end if;

        return false;
    end;
    $$;

    -- System log

    create function management.unread_system_log_count(in_token uuid)
        returns int
        language plpgsql
        cost 1
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _count bigint;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            select sum(case when uvl.id_user is null then 1 else 0 end) into _count from system.log
            left join system.user_viewed_log as uvl on (log.id = uvl.id_log and uvl.id_user = _id_user);

            return _count;
        end if;

        return null;
    end;
    $$;

    create function clients.unread_system_log_count(in_token uuid)
        returns int
        language plpgsql
        cost 1000
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _id_organization bigint := clients.session_organization_id(in_token);
        _count bigint;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            select sum(case when uvl.id_user is null then 1 else 0 end) into _count from system.log
            left join system.user_viewed_log as uvl on (log.id = uvl.id_log and uvl.id_user = _id_user)
            where log.id_organization = _id_organization and (level = constants.log_level_error() or level = constants.log_level_warning());

            return _count;
        end if;

        return null;
    end;
    $$;

    create function management.system_log(in_token uuid, in_id bigint)
        returns system.t_log
        language plpgsql
        cost 1000
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _log system.t_log;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            select
                   log.id,
                   case when id_organization is not null then (o.id, o.title, o.contact_person, o.contact_email, o.contact_phone, o.created_at)::clients.t_organization else null end,
                   log.entry, log.message, log.level, log.created_at into _log
            from system.log
            left join clients.organization as o on (log.id_organization = o.id)
            where log.id = in_id;

            if found then
                -- if not viewed set log es viewed for this user
                perform from system.user_viewed_log where id_user = _id_user and id_log = in_id;
                if not found then
                    insert into system.user_viewed_log(id_user, id_log) values(_id_user, in_id);
                end if;

                return _log;
            end if;
        end if;

        return null;
    end;
    $$;

    create function clients.system_log(in_token uuid, in_id bigint)
        returns system.t_log
        language plpgsql
        cost 1000
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _log system.t_log;
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            select
                   log.id,
                   case when id_organization is not null then (o.id, o.title, o.contact_person, o.contact_email, o.contact_phone, o.created_at)::clients.t_organization else null end,
                   log.entry, log.message, log.level, log.created_at into _log
            from system.log
            left join clients.organization as o on (log.id_organization = o.id)
            where log.id = in_id and id_organization = _id_organization;

            if found then
                -- if not viewed set log es viewed for this user
                perform from system.user_viewed_log where id_user = _id_user and id_log = in_id;
                if not found then
                    insert into system.user_viewed_log(id_user, id_log) values(_id_user, in_id);
                end if;

                return _log;
            end if;
        end if;

        return null;
    end;
    $$;

    create function management.worker_server_add_system_log(in_token uuid, in_id_fsc bigint, in_id_work bigint, in_task_code text, in_level int, in_entry text, in_message text)
        returns bool
        language plpgsql
        cost 1000
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _id_organization bigint;
        _fsc_title text;
        _message text;
        _work_id bigint;
        _work_title text;
        _task_title text;
        _level text;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            select title into _task_title from management.task where code = in_task_code;
            select id_organization, title into _id_organization, _fsc_title from clients.foreign_system_connection where id = in_id_fsc;

            select id, title into _work_id, _work_title from clients.work where id = in_id_work;

            _message := format('Uzdevuma nosaukums: "%s", Savienojuma nosaukums: "%s", Savienojuma id: "%s", Darba nosaukums: "%s", Darba id: "%s"'||E'\n'||'%s',
                _task_title, _fsc_title, in_id_fsc, _work_title, _work_id, in_message);

            if in_level = 1 then
                _level := constants.log_level_warning();
            elseif in_level = 2 then
                _level := constants.log_level_error();
            else
                _level := constants.log_level_info();
            end if;

            insert into system.log(entry, message, level, id_organization)
            VALUES (in_entry, _message, _level, _id_organization);

            return true;
        end if;

        return false;
    end;
    $$;

    -- List all management system logs
    create function management.filter_system_logs(in_token uuid, in_skip bigint, in_limit bigint, in_order_by text default 'id', in_order_direction smallint default 0, in_fts_qry text default null, in_level text[] default null)
        returns system.t_log[]
        language plpgsql
        cost 100
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _items system.t_log[];
        _content_type text := 'system.t_log';
        _sql text := 'select %s from system.log as sl left join clients.organization as o on (sl.id_organization = o.id) ';
        _type_select text := format('(sl.id, case when sl.id_organization is not null then (o.id, o.title, o.contact_person, o.contact_email, o.contact_phone, o.created_at)::%s else null end, sl.entry, sl.message, sl.level, sl.created_at)::%s', 'clients.t_organization', _content_type);
        _where text;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            _where := concat_ws(' AND ',
                case when in_fts_qry is not null then 'sl.tsv @@ websearch_to_tsquery($3)' end,
                case when in_level is not null then 'level = any($4)' end);

            _sql := system.build_filter_sql(
                _sql,
                _content_type,
                _where,
                in_order_by,
                in_order_direction,
                _type_select,
                'sl');

            execute _sql into _items using in_limit, in_skip, in_fts_qry, in_level;
        end if;

        return _items;
    end;
    $$;

    create function clients.filter_system_logs(in_token uuid, in_skip bigint, in_limit bigint, in_order_by text default 'id', in_order_direction smallint default 0, in_fts_qry text default null, in_level text[] default null)
        returns system.t_log[]
        language plpgsql
        cost 1000
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _items system.t_log[];
        _content_type text := 'system.t_log';
        _sql text := 'select %s from system.log as sl left join clients.organization as o on (sl.id_organization = o.id) ';
        _type_select text := format('(sl.id, case when sl.id_organization is not null then (o.id, o.title, o.contact_person, o.contact_email, o.contact_phone, o.created_at)::%s else null end, sl.entry, sl.message, sl.level, sl.created_at)::%s', 'clients.t_organization', _content_type);
        _where text;
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            _where := concat_ws(' AND ',
                case when in_fts_qry is not null then 'tsv @@ websearch_to_tsquery($3)' end,
                case when in_level is not null then 'level = any($4)' end,
                'id_organization = $5');

            _sql := system.build_filter_sql(
                _sql,
                _content_type,
                _where,
                in_order_by,
                in_order_direction,
                _type_select,
                'sl');

            execute _sql into _items using in_limit, in_skip, in_fts_qry, in_level, _id_organization;
        end if;

        return _items;
    end;
    $$;

    create function clients.foreign_systems(in_token uuid)
        returns management.t_foreign_system[]
        language plpgsql
        cost 100
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _items management.t_foreign_system[];
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            select array_agg(data.fs) into _items from (
                select (id, title, system_type, logo_base64, disabled, created_at)::management.t_foreign_system as fs
                from management.foreign_system
            ) as data;
        else
            _items := null;
        end if;

        return _items;
    end;
    $$;

    create function clients.foreign_system(in_token uuid, in_id_foreign_system text)
        returns management.t_foreign_system
        language plpgsql
        cost 100
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _item management.t_foreign_system := null;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            select id, title, system_type, logo_base64, disabled, created_at into _item
            from management.foreign_system where id = in_id_foreign_system;
        end if;

        return _item;
    end;
    $$;


    create function clients.foreign_system_connection(in_token uuid, in_id bigint)
        returns clients.t_foreign_system_connection
        language plpgsql
        cost 100
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _item clients.t_foreign_system_connection := null;
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            select id,
                   id_foreign_system,
                   id_organization,
                   title,
                   username,
                   pwd,
                   address,
                   available,
                   unavailability_error,
                   created_at
            into _item
            from clients.foreign_system_connection where id = in_id and id_organization = _id_organization;
        end if;

        return _item;
    end;
    $$;


    create function clients.insert_foreign_system_connection(
            in_token uuid,
            in_id_foreign_system text,
            in_title text,
            in_username text,
            in_pwd text,
            in_address text)
        returns clients.t_foreign_system_connection
        language plpgsql
        cost 1
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _id_foreign_system_connection bigint;
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            insert into clients.foreign_system_connection(id_foreign_system, id_organization, title, username, pwd, address, available)
            VALUES (in_id_foreign_system, _id_organization, in_title, in_username, in_pwd, in_address, null) returning id into _id_foreign_system_connection;

            return clients.foreign_system_connection(in_token, _id_foreign_system_connection);
        end if;

        return null;
    end;
    $$;

    create function clients.update_foreign_system_connection(
            in_token uuid,
            in_id bigint,
            in_title text,
            in_username text,
            in_pwd text,
            in_address text,
            in_available bool,
            in_unavailability_error text)
        returns clients.t_foreign_system_connection
        language plpgsql
        cost 1
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then

            update clients.foreign_system_connection
            set
                title = in_title,
                username = in_username,
                pwd = in_pwd,
                address = in_address,
                available = in_available,
                unavailability_error = in_unavailability_error
            where id = in_id and id_organization = _id_organization;

            return clients.foreign_system_connection(in_token, in_id);
        end if;

        return null;
    end;
    $$;

    create function clients.delete_foreign_system_connection(
            in_token uuid,
            in_id bigint)
        returns boolean
        language plpgsql
        cost 1
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            delete from clients.foreign_system_connection where id = in_id and id_organization = _id_organization;

            return found;
        end if;

        return false;
    end;
    $$;

    create function clients.filter_foreign_system_connections(in_token uuid, in_skip bigint, in_limit bigint, in_order_by text default 'id', in_order_direction smallint default 0, in_fts_qry text default null, in_id_foreign_system text default null)
        returns clients.t_foreign_system_connection[]
        language plpgsql
        cost 100
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _items clients.t_foreign_system_connection[];
        _content_type text := 'clients.t_foreign_system_connection';
        _sql text := 'select %s from clients.foreign_system_connection';
        _where text;
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then

            _where := concat_ws(' AND ',
                case when in_fts_qry is not null then 'tsv @@ websearch_to_tsquery($3)' end,
                case when in_id_foreign_system is not null then 'id_foreign_system = $4' end,
                case when _id_organization is not null then 'id_organization = $5' end);

            _sql := system.build_filter_sql(
                _sql,
                _content_type,
                _where,
                in_order_by,
                in_order_direction);

            raise notice '%s', _sql;

            execute _sql into _items using in_limit, in_skip, in_fts_qry, in_id_foreign_system, _id_organization;
        end if;

        return _items;
    end;
    $$;

    create function management.update_foreign_system_connection_status(
            in_token uuid,
            in_id bigint,
            in_available bool,
            in_unavailability_error text)
        returns bool
        language plpgsql
        cost 1
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then

            update clients.foreign_system_connection
            set
                available = in_available,
                unavailability_error = in_unavailability_error
            where id = in_id;

            return found;
        end if;

        return false;
    end;
    $$;

    -- Import/export

    create function system.gen_checksum(variadic in_str text[])
        returns character varying(64)
        language plpgsql
        immutable parallel safe
        called on null input
        security invoker
        cost 1000
        as $$
    declare
        word text;
        full_str text := '';
    begin
        foreach word in array in_str
        loop
            full_str := full_str || coalesce(word, '');
        end loop;

        return encode(sha256(full_str::bytea), 'hex');
    end;$$;

    create function management.import_unit(
            in_token uuid,
            in_id_fsc bigint,
            in_source_id text,
            in_short_title character varying(10),
            in_title character varying(40)
            )
        returns bigint
        language plpgsql
        cost 1
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _checksum character varying(64) := system.gen_checksum(in_short_title, in_title);
        _id_unit bigint;
        _current_checksum character varying(64);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then

            select id, checksum into _id_unit, _current_checksum from clients.unit where id_fsc = in_id_fsc and source_id = in_source_id;

            if found then
                -- Jau ir importēts, pārbaudam versijas
                perform id from clients.unit where id_fsc = in_id_fsc and source_id = in_source_id and checksum = _checksum;
                -- šī nav esošā versija atjaunojam datus
                if not found then
                    -- pirms atjaunojam parbaudam vai jaunie dati nav bijus kadreiz ka versija
                    perform id from clients.unit_version where id_unit = _id_unit and checksum = _current_checksum;
                    if not found then
                        -- parnesam uz versiju
                        insert into clients.unit_version(id_unit, short_title, title, checksum)
                        select id, short_title, title, checksum from clients.unit where id = _id_unit;
                    end if;

                    update clients.unit set short_title = in_short_title, title = in_title, checksum = _checksum where id_fsc = in_id_fsc and source_id = in_source_id;
                end if;
            else
                insert into clients.unit(id_fsc, source_id, short_title, title, checksum)
                values(in_id_fsc, in_source_id, in_short_title, in_title, _checksum) returning id into _id_unit;
            end if;

            return _id_unit;
        end if;

        return null;
    end;
    $$;

    create function management.cancel_job_with_error(
            in_token uuid,
            in_id_job bigint,
            in_error text
            )
        returns bool
        language plpgsql
        cost 1000
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _id_work bigint;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            -- Disable work on error
            select id_work into _id_work from clients.job where id = in_id_job;
            update clients.work set disabled = true where id = _id_work;

            -- set latest job as completed and canceled and also set error message
            update clients.job
            set execution_error_message = in_error,
                running = false,
                completed_at = now(),
                canceled_at = now()
            where id = in_id_job;

            return found;
        end if;

        return false;
    end;
    $$;

    create function management.integrated_units(
            in_token uuid,
            in_task_code text,
            in_id_fsc bigint
            )
        returns management.t_integrated_unit[]
        language plpgsql
        cost 1000
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _id_task bigint;
        _items management.t_integrated_unit[];
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            select id into _id_task from management.task where code = in_task_code;

            select
                array_agg((u.id,
                iu.target_id,
                iu.id_fsc,
                iu.id_task,
                u.short_title,
                u.title,
                u.created_at,
                u.checksum,
                iu.attributes)::management.t_integrated_unit) into _items
            from clients.unit as u
            left join clients.integrated_unit as iu on (u.id = iu.id_unit)
            where (iu.id_task = _id_task and iu.id_fsc = in_id_fsc) or (iu.id_task is null and iu.id_fsc is null);

            return _items;
        end if;

        return null;
    end;
    $$;

    create function management.save_integrated_unit(
            in_token uuid,
            in_task_code text,
            in_id_unit bigint,
            in_id_fsc bigint,
            in_target_id text,
            in_attributes jsonb
            )
        returns bool
        language plpgsql
        cost 1000
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _id_task bigint;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            select id into _id_task from management.task where code = in_task_code;

            perform from clients.integrated_unit
            where id_unit = in_id_unit and target_id = in_target_id and id_fsc = in_id_fsc and id_task = _id_task;

            if found then
                update clients.integrated_unit set attributes = in_attributes
                where id_unit = in_id_unit and target_id = in_target_id and id_fsc = in_id_fsc and id_task = _id_task;
            else
                insert into clients.integrated_unit(id_unit, target_id, id_fsc, id_task, attributes)
                values (in_id_unit, in_target_id, in_id_fsc, _id_task, in_attributes);
            end if;


            return true;
        end if;

        return false;
    end;
    $$;

    create function management.import_tax_category(
            in_token uuid,
            in_id_fsc bigint,
            in_source_id text,
            in_code character(1),
            in_rate numeric,
            in_non_taxable bool,
            in_title character varying(50)
            )
        returns bigint
        language plpgsql
        cost 1
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _checksum character varying(64) := system.gen_checksum(in_code, in_rate::text, in_non_taxable::text, in_title);
        _id_tc bigint;
        _current_checksum character varying(64);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then

            select id, checksum into _id_tc, _current_checksum from clients.tax_category where id_fsc = in_id_fsc and source_id = in_source_id;

            if found then
                -- Jau ir importēts, pārbaudam versijas
                perform id from clients.tax_category where id_fsc = in_id_fsc and source_id = in_source_id and checksum = _checksum;
                -- šī nav esošā versija atjaunojam datus
                if not found then
                    -- pirms atjaunojam parbaudam vai jaunie dati nav bijus kadreiz ka versija
                    perform id from clients.tax_category_version where id_tax_category = _id_tc and checksum = _current_checksum;
                    if not found then
                        -- parnesam uz versiju
                        insert into clients.tax_category_version(id_tax_category, code, rate, non_taxable, title, checksum)
                        select id, code, rate, non_taxable, title, checksum from clients.tax_category where id = _id_tc;
                    end if;

                    update clients.tax_category
                    set code = in_code, rate = in_rate, non_taxable = in_non_taxable, title = in_title, checksum = _checksum
                    where id_fsc = in_id_fsc and source_id = in_source_id;
                end if;
            else
                insert into clients.tax_category(id_fsc, source_id, code, rate, non_taxable, title, checksum)
                values(in_id_fsc, in_source_id, in_code, in_rate, in_non_taxable, in_title, _checksum) returning id into _id_tc;
            end if;

            return _id_tc;
        end if;

        return null;
    end;
    $$;

    create function management.import_product_tax_category(
            in_token uuid,
            in_id_tax_category bigint,
            in_source_id text,
            in_title character varying(50)
            )
        returns bigint
        language plpgsql
        cost 1
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _checksum character varying(64) := system.gen_checksum(in_title);
        _id_ptc bigint;
        _current_checksum character varying(64);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then

            select id, checksum into _id_ptc, _current_checksum from clients.product_tax_category where id_tax_category = in_id_tax_category and source_id = in_source_id;

            if found then
                -- Jau ir importēts, pārbaudam versijas
                perform id from clients.product_tax_category where id_tax_category = in_id_tax_category and source_id = in_source_id and checksum = _checksum;
                -- šī nav esošā versija atjaunojam datus
                if not found then
                    -- pirms atjaunojam parbaudam vai jaunie dati nav bijus kadreiz ka versija
                    perform id from clients.product_tax_category_version where id_product_tax_category = _id_ptc and checksum = _current_checksum;
                    if not found then
                        -- parnesam uz versiju
                        insert into clients.product_tax_category_version(id_product_tax_category, title, checksum)
                        select id, title, checksum from clients.product_tax_category where id = _id_ptc;
                    end if;

                    update clients.product_tax_category set id_tax_category = in_id_tax_category, title = in_title, checksum = _checksum where id_tax_category = in_id_tax_category and source_id = in_source_id;
                end if;
            else
                insert into clients.product_tax_category(source_id, id_tax_category, title, checksum)
                values(in_source_id, in_id_tax_category, in_title, _checksum) returning id into _id_ptc;
            end if;

            return _id_ptc;
        end if;

        return null;
    end;
    $$;

    create function management.integrated_tax_categories(
            in_token uuid,
            in_task_code text,
            in_id_fsc bigint
            )
        returns management.t_integrated_tax_category[]
        language plpgsql
        cost 1000
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _id_task bigint;
        _items management.t_integrated_tax_category[];
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            select id into _id_task from management.task where code = in_task_code;

            select
                array_agg((tc.id,
                itc.target_id,
                itc.id_fsc,
                itc.id_task,
                tc.code,
                tc.rate,
                tc.non_taxable,
                tc.title,
                tc.created_at,
                tc.checksum,
                itc.attributes)::management.t_integrated_tax_category) into _items
            from clients.tax_category as tc
            left join clients.integrated_tax_category as itc on (tc.id = itc.id_tax_category)
            where (itc.id_task = _id_task and itc.id_fsc = in_id_fsc) or (itc.id_task is null and itc.id_fsc is null);

            return _items;
        end if;

        return null;
    end;
    $$;

    create function management.integrated_product_tax_categories(
            in_token uuid,
            in_id_tax_category bigint,
            in_task_code text,
            in_id_fsc bigint
            )
        returns management.t_integrated_product_tax_category[]
        language plpgsql
        cost 1000
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _id_task bigint;
        _items management.t_integrated_product_tax_category[];
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            select id into _id_task from management.task where code = in_task_code;

            select
                array_agg((ptc.id,
                iptc.target_id,
                iptc.id_fsc,
                iptc.id_task,
                ptc.title,
                ptc.created_at,
                ptc.checksum,
                iptc.attributes)::management.t_integrated_product_tax_category) into _items
            from clients.product_tax_category as ptc
            left join clients.integrated_product_tax_category as iptc on (ptc.id = iptc.id_product_tax_category)
            where
                ((iptc.id_task = _id_task and iptc.id_fsc = in_id_fsc) or (iptc.id_task is null and iptc.id_fsc is null))
                and ptc.id_tax_category = in_id_tax_category;

            return _items;
        end if;

        return null;
    end;
    $$;

    create function management.save_integrated_tax_category(
            in_token uuid,
            in_task_code text,
            in_id_tax_category bigint,
            in_id_fsc bigint,
            in_target_id text,
            in_attributes jsonb
            )
        returns bool
        language plpgsql
        cost 1000
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _id_task bigint;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            select id into _id_task from management.task where code = in_task_code;

            perform from clients.integrated_tax_category
            where id_tax_category = in_id_tax_category and target_id = in_target_id and id_fsc = in_id_fsc and id_task = _id_task;

            if found then
                update clients.integrated_tax_category set attributes = in_attributes
                where id_tax_category = in_id_tax_category and target_id = in_target_id and id_fsc = in_id_fsc and id_task = _id_task;
            else
                insert into clients.integrated_tax_category(id_tax_category, target_id, id_fsc, id_task, attributes)
                values (in_id_tax_category, in_target_id, in_id_fsc, _id_task, in_attributes);
            end if;

            return true;
        end if;

        return false;
    end;
    $$;

    create function management.save_integrated_product_tax_category(
            in_token uuid,
            in_task_code text,
            in_id_product_tax_category bigint,
            in_id_fsc bigint,
            in_target_id text,
            in_attributes jsonb
            )
        returns bool
        language plpgsql
        cost 1000
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _id_task bigint;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            select id into _id_task from management.task where code = in_task_code;

            perform from clients.integrated_product_tax_category
            where id_product_tax_category = in_id_product_tax_category and target_id = in_target_id and id_fsc = in_id_fsc and id_task = _id_task;

            if found then
                update clients.integrated_product_tax_category set attributes = in_attributes
                where id_product_tax_category = in_id_product_tax_category and target_id = in_target_id and id_fsc = in_id_fsc and id_task = _id_task;
            else
                insert into clients.integrated_product_tax_category(id_product_tax_category, target_id, id_fsc, id_task, attributes)
                values (in_id_product_tax_category, in_target_id, in_id_fsc, _id_task, in_attributes);
            end if;

            return true;
        end if;

        return false;
    end;
    $$;

    create function management.import_product(
            in_token uuid,
            in_id_fsc bigint,
            in_source_id text,
            in_tax_category_source_id text,
            in_product_tax_category_source_id text,
            in_unit_source_id text,
            in_title character varying(250),
            in_print_title character varying(40),
            in_short_code character varying(50),
            in_price numeric,
            in_barcodes clients.t_barcode[],
            in_qrcode text,
            in_description text,
            in_combo bool,
            in_combo_products_source_ids text[]
            )
        returns bigint
        language plpgsql
        cost 1
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _checksum character varying(64) := system.gen_checksum(in_product_tax_category_source_id, in_unit_source_id, in_title, in_print_title, in_short_code, in_price::text, in_barcodes::text, in_qrcode::text, in_description::text, in_combo::text, in_combo_products_source_ids::text);
        _id_product bigint;
        _current_checksum character varying(64);
        _product_tax_category_id bigint;
        _unit_id bigint;
        _combo_products_ids bigint[] := null;
        _combo_product_source_id text;
        _combo_product_id bigint;
        _combo_products clients.t_product[];
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            select ptc.id into _product_tax_category_id
            from clients.product_tax_category as ptc
            inner join clients.tax_category as tc on (ptc.id_tax_category = tc.id)
            where tc.id_fsc = in_id_fsc and ptc.source_id = in_product_tax_category_source_id and tc.source_id = in_tax_category_source_id;

            select id into _unit_id from clients.unit where id_fsc = in_id_fsc and source_id = in_unit_source_id;

            if in_combo_products_source_ids is not null then
                foreach _combo_product_source_id in array in_combo_products_source_ids
                loop
                    select id into _combo_product_id from clients.product where source_id = _combo_product_source_id and id_fsc = in_id_fsc;
                    _combo_products_ids := array_append(_combo_products_ids, _combo_product_id);
                end loop;
            end if;

            select id, checksum into _id_product, _current_checksum from clients.product where id_fsc = in_id_fsc and source_id = in_source_id;

            if found then
                -- Jau ir importēts, pārbaudam versijas
                perform id from clients.product where id_fsc = in_id_fsc and source_id = in_source_id and checksum = _checksum;
                -- šī nav esošā versija atjaunojam datus
                if not found then
                    -- pirms atjaunojam parbaudam vai jaunie dati nav bijus kadreiz ka versija
                    perform id from clients.product_version where id_product = _id_product and checksum = _current_checksum;
                    if not found then
                        -- parnesam uz versiju

                        -- savācam kombo produkta produktus
                        select array_agg((
                                p.id,
                                (select (id, id_tax_category, source_id, title, created_at, checksum)::clients.t_product_tax_category from clients.product_tax_category where id = p.id_product_tax_category),
                                p.id_fsc,
                                (select (id, id_fsc, source_id, short_title, title, created_at, checksum)::clients.t_unit from clients.unit where id = p.id_unit),
                                p.source_id,
                                p.title,
                                p.print_title,
                                p.short_code,
                                p.price,
                                p.barcodes,
                                p.qrcode,
                                p.description,
                                p.combo,
                                p.created_at,
                                p.checksum
                            )::clients.t_product) into _combo_products from clients.combo_product
                            inner join clients.product as p on (combo_product.id_combo = p.id)
                        where combo_product.id_combo = _id_product;

                        insert into clients.product_version(id_product, id_product_tax_category, id_unit, title, print_title, short_code, price, barcodes, qrcode, description, combo, checksum, combo_products)
                        select id,
                               (select (id, id_tax_category, source_id, title, created_at, checksum)::clients.t_product_tax_category from clients.product_tax_category where id = id_product_tax_category),
                               (select (id, id_fsc, source_id, short_title, title, created_at, checksum)::clients.t_unit from clients.unit where id = id_unit),
                               title, print_title, short_code, price, barcodes, qrcode, description, combo, checksum, _combo_products from clients.product where id = _id_product;
                    end if;

                    update clients.product
                    set id_product_tax_category = _product_tax_category_id,
                        id_unit = _unit_id,
                        title = in_title,
                        print_title = in_print_title,
                        short_code = in_short_code,
                        price = in_price,
                        barcodes= in_barcodes,
                        qrcode = in_qrcode,
                        description = in_description,
                        combo = in_combo,
                        checksum = _checksum where id_fsc = in_id_fsc and source_id = in_source_id;

                    if _combo_products_ids is null then
                        delete from clients.combo_product where id_combo = _id_product;
                    else
                        delete from clients.combo_product where id_combo = _id_product;

                        foreach _combo_product_id in ARRAY _combo_products_ids
                        loop
                            insert into clients.combo_product(id_combo, id_product) values (_id_product, _combo_product_id);
                        end loop;
                    end if;

                end if;
            else
                insert into clients.product(id_product_tax_category, id_fsc, id_unit, source_id, title, print_title, short_code, price, barcodes, qrcode, description, combo, checksum)
                values(_product_tax_category_id, in_id_fsc, _unit_id, in_source_id, in_title, in_print_title, in_short_code, in_price, in_barcodes, in_qrcode, in_description, in_combo, _checksum) returning id into _id_product;

                if _combo_products_ids is not null then
                    foreach _combo_product_id in ARRAY _combo_products_ids
                    loop
                        insert into clients.combo_product(id_combo, id_product) values (_id_product, _combo_product_id);
                    end loop;
                end if;
            end if;

            return _id_product;
        end if;

        return null;
    end;
    $$;

    create function management.integrated_products(
            in_token uuid,
            in_task_code text,
            in_id_fsc bigint
            )
        returns management.t_integrated_product[]
        language plpgsql
        cost 1000
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _id_task bigint;
        _items management.t_integrated_product[];
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            select id into _id_task from management.task where code = in_task_code;

            select
                array_agg((p.id,
                ip.target_id,
                ip.id_fsc,
                ip.id_task,
                (select (id, id_tax_category, (
                    select target_id
                    from clients.product_tax_category as ptc
                    left join clients.integrated_product_tax_category as iptc on (ptc.id = iptc.id_product_tax_category)
                    where iptc.id_fsc = in_id_fsc and id_product_tax_category = p.id_product_tax_category
                    ), title, created_at, checksum)::clients.t_product_tax_category from clients.product_tax_category where id = p.id_product_tax_category),
                (select (id, id_fsc, (
                    select target_id
                    from clients.unit as u
                    left join clients.integrated_unit as iu on (u.id = iu.id_unit)
                    where iu.id_fsc = in_id_fsc and id_unit = p.id_unit
                    ), short_title, title, created_at, checksum)::clients.t_unit from clients.unit where id = p.id_unit),
                p.title,
                p.print_title,
                p.short_code,
                p.price,
                p.barcodes,
                p.qrcode,
                p.description,
                p.combo,
                p.created_at,
                p.checksum,
                (
                    select array_agg((
                        cp.id,
                        (select (id, id_tax_category, source_id, title, created_at, checksum)::clients.t_product_tax_category from clients.product_tax_category where id = cp.id_product_tax_category),
                        cp.id_fsc,
                        (select (id, id_fsc, source_id, short_title, title, created_at, checksum)::clients.t_unit from clients.unit where id = cp.id_unit),
                        cp.source_id,
                        cp.title,
                        cp.print_title,
                        cp.short_code,
                        cp.price,
                        cp.barcodes,
                        cp.qrcode,
                        cp.description,
                        cp.combo,
                        cp.created_at,
                        cp.checksum
                    )::clients.t_product) from clients.combo_product
                    inner join clients.product as cp on (combo_product.id_combo = cp.id)
                    where combo_product.id_combo = p.id
                ),
                ip.attributes)::management.t_integrated_product) into _items
            from clients.product as p
            left join clients.integrated_product as ip on (p.id = ip.id_product)
            where (ip.id_task = _id_task and ip.id_fsc = in_id_fsc) or (ip.id_task is null and ip.id_fsc is null);

            return _items;
        end if;

        return null;
    end;
    $$;

    create function management.save_integrated_product(
            in_token uuid,
            in_task_code text,
            in_id_product bigint,
            in_id_fsc bigint,
            in_target_id text,
            in_attributes jsonb
            )
        returns bool
        language plpgsql
        cost 1000
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _id_task bigint;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            select id into _id_task from management.task where code = in_task_code;

            perform from clients.integrated_product
            where id_product = in_id_product and target_id = in_target_id and id_fsc = in_id_fsc and id_task = _id_task;

            if found then
                update clients.integrated_product set attributes = in_attributes
                where id_product = in_id_product and target_id = in_target_id and id_fsc = in_id_fsc and id_task = _id_task;
            else
                insert into clients.integrated_product(id_product, target_id, id_fsc, id_task, attributes)
                values (in_id_product, in_target_id, in_id_fsc, _id_task, in_attributes);
            end if;

            return true;
        end if;

        return false;
    end;
    $$;

    create function clients.fsc_data_summary(in_token uuid)
        returns clients.t_fsc_data_summary[]
        language plpgsql
        cost 5000
    as $$
    declare
        _id_user bigint;
        _summary clients.t_fsc_data_summary;
        _summaries clients.t_fsc_data_summary[];
        _fsc record;
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            for _fsc in select id, title, available from clients.foreign_system_connection
            where id_organization = _id_organization and id_foreign_system <> 'tiki-taka' order by title asc
            loop
                _summary.fsc_id         := _fsc.id;
                _summary.fsc_title      := _fsc.title;
                _summary.fsc_available  := _fsc.available;

                select
                    summary.tax_categories,
                    summary.units,
                    summary.products,
                    summary.employees,
                    summary.discounts,
                    summary.sections,
                    summary."groups",
                    summary.combo,
                    summary.orders,
                    summary.online_orders
                into
                    _summary.tax_categories,
                    _summary.units,
                    _summary.products,
                    _summary.employees,
                    _summary.discounts,
                    _summary.sections,
                    _summary."groups",
                    _summary.combo,
                    _summary.orders,
                    _summary.online_orders
                from (select
                    (select count(id) from clients.tax_category where id_fsc = _fsc.id) as tax_categories,
                    (select count(id) from clients.unit where id_fsc = _fsc.id) as units,
                    (select count(id) from clients.product where id_fsc = _fsc.id and not combo) as products,
                    (select count(id) from clients.employee where id_fsc = _fsc.id) as employees,
                    (select count(id) from clients.discount where id_fsc = _fsc.id) as discounts,
                    (select count(id) from clients.section where id_fsc = _fsc.id) as sections,
                    (select count(id) from clients.group where id_fsc = _fsc.id) as groups,
                    (select count(id) from clients.product where id_fsc = _fsc.id and combo) as combo,
                    (select count(id) from clients.order where id_fsc = _fsc.id) as orders,
                    (select count(id) from clients.online_order where id_fsc = _fsc.id) as online_orders
                ) as summary;

                _summaries := array_append(_summaries, _summary);
            end loop;

            return _summaries;
        end if;

        return null;
    end;
    $$;

    create function management.import_fiscof(
            in_token uuid,
            in_id_fsc bigint,
            in_source_id text,
            in_device_serial_number text,
            in_doc xml
            )
        returns bigint
        language plpgsql
        cost 1
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _id_fiscof bigint;
        _doc xml;
        _first_doc bool := true;
        _fiscof_timestamp timestamp;
        _doc_count int := 0;
        _doc_title text;
        _doc_datetime timestamp;
        _z bool := false;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then

            select id into _id_fiscof from clients.device_fiscof where id_fsc = in_id_fsc and source_id = in_source_id;

            if found then
                -- Already imported - return -1 to indicate this state
                return -1;
            else
                FOREACH _doc IN ARRAY (xpath('/E-log/Document', in_doc))
                LOOP
                    _doc_count := _doc_count + 1;
                    -- Get data from first doc and save fiscof
                    IF _first_doc THEN
                        _fiscof_timestamp := ((xpath('/Document/@dok_laiks', _doc))[1])::text::timestamp;

                        _first_doc := false;

                        INSERT INTO clients.device_fiscof(id_fsc, source_id, device_serial_number, document_count, doc, doc_datetime)
                        VALUES(in_id_fsc, in_source_id, in_device_serial_number, 0, in_doc, _fiscof_timestamp) RETURNING id INTO _id_fiscof;
                    END IF;

                    -- Process doc
                    IF _id_fiscof IS NOT NULL THEN
                        _doc_title := ((xpath('/Document/@dok_nosaukums', _doc))[1])::text;
                        _doc_datetime := ((xpath('/Document/@dok_laiks', _doc))[1])::text::timestamp;
                        _z := xmlexists('/Document/z_parskats' PASSING BY VALUE _doc);

                        INSERT INTO clients.device_doc(title, doc_datetime, doc, z, id_device_fiscof)
                        VALUES(_doc_title, _doc_datetime, _doc, _z, _id_fiscof);
                    END IF;

                END LOOP;
                -- set document count
                update clients.device_fiscof set document_count = _doc_count where id = _id_fiscof;
            end if;

            return _id_fiscof;
        end if;

        return null;
    end;
    $$;

    create function management.next_non_integrated_fiscof(
            in_token uuid,
            in_task_code text,
            in_id_fsc bigint,
            in_device_serial_numbers text[] default null
            )
        returns management.t_integrated_device_fiscof
        language plpgsql
        cost 1000
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _id_task bigint;
        _id_organization bigint;
        _item management.t_integrated_device_fiscof;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            select id into _id_task from management.task where code = in_task_code;
            select id_organization into _id_organization from clients.foreign_system_connection where id = in_id_fsc;

            select
                 fiscof.id,
                 i_fiscof.target_id,
                 i_fiscof.id_fsc,
                 i_fiscof.id_task,
                 fiscof.device_serial_number,
                 fiscof.document_count,
                 fiscof.doc,
                 fiscof.doc_datetime,
                 (
                    select array_agg((
                        device_doc.id,
                        device_doc.id_device_fiscof,
                        device_doc.title,
                        device_doc.z,
                        device_doc.doc,
                        device_doc.doc_datetime,
                        device_doc.created_at
                    )::clients.t_device_doc) from clients.device_doc where id_device_fiscof = fiscof.id
                )::clients.t_device_doc[],
                fiscof.created_at,
                null,
                i_fiscof.attributes
            into _item
            from clients.device_fiscof as fiscof
            inner join clients.foreign_system_connection as fsc on (fiscof.id_fsc = fsc.id)
            left join clients.integrated_device_fiscof as i_fiscof on (fiscof.id = i_fiscof.id_device_fiscof and i_fiscof.id_fsc = in_id_fsc and i_fiscof.id_task = _id_task)
            where
                i_fiscof.id_task is null
            and
                i_fiscof.id_fsc is null
            and
                fsc.id_organization = _id_organization
            and
                case when in_device_serial_numbers is null then true
                     else device_serial_number = any(in_device_serial_numbers) end
            limit 1;

            if _item.id is null then
                return null;
            end if;

            return _item;
        end if;

        return null;
    end;
    $$;

    create function management.save_integrated_fiscof(
            in_token uuid,
            in_task_code text,
            in_id_fiscof bigint,
            in_id_fsc bigint,
            in_target_id text,
            in_attributes jsonb
            )
        returns bool
        language plpgsql
        cost 1000
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _id_task bigint;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            select id into _id_task from management.task where code = in_task_code;

            perform from clients.integrated_device_fiscof
            where id_device_fiscof = in_id_fiscof and target_id = in_target_id and id_fsc = in_id_fsc and id_task = _id_task;

            if found then
                update clients.integrated_device_fiscof set attributes = in_attributes
                where id_device_fiscof = in_id_fiscof and target_id = in_target_id and id_fsc = in_id_fsc and id_task = _id_task;
            else
                insert into clients.integrated_device_fiscof(id_device_fiscof, target_id, id_fsc, id_task, attributes)
                values (in_id_fiscof, in_target_id, in_id_fsc, _id_task, in_attributes);
            end if;

            return true;
        end if;

        return false;
    end;
    $$;

    create function clients.filter_work(in_token uuid, in_skip bigint, in_limit bigint, in_order_by text default 'id', in_order_direction smallint default 0, in_fts_qry text default null, in_status int[] default null)
        returns clients.t_work_as_job[]
        language plpgsql
        cost 100
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _items clients.t_work_as_job[];
        _content_type text := 'clients.t_work_as_job';
        _sql text := 'select %s from clients.work as w ' ||
                     'left join clients.job as j on (w.id = j.id_work and j.latest)';
        _type_select text := format(
            '(w.id, j.id, w.title, w.disabled, w.repeat, j.execute_at, j.started_at, j.completed_at, j.canceled_at, ' ||
            'j.cancel_requested_at, j.running, j.execution_error_message)::%s', _content_type);
        _where text;
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            _where := concat_ws(' AND ',
                case when in_fts_qry is not null then 'w.tsv @@ websearch_to_tsquery($3)' end,
                case when in_status is not null then '(case when disabled then 0 when (select running from clients.job where id_work = w.id and latest) then 1 else 2 end) = any($4)' end,
                'w.id_organization = $5');

            _sql := system.build_filter_sql(
                _sql,
                _content_type,
                _where,
                in_order_by,
                in_order_direction,
                _type_select,
                'w');

            raise notice '%s', _sql;

            execute _sql into _items using in_limit, in_skip, in_fts_qry, in_status, _id_organization;
        end if;

        return _items;
    end;
    $$;

    create function clients.work(in_token uuid, in_id bigint)
        returns clients.t_work
        language plpgsql
        cost 1000
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _item clients.t_work := null;
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            select w.id,
                   id_organization,
                   (select (id, title, frequency)::management.t_run_period from management.run_period where id = w.id_run_period),
                   w.title,
                   w.disabled,
                   w.repeat,
                   w.minute,
                   w.hour,
                   w.week_day,
                   w.month_day,
                   w.month
            into _item
            from clients.work as w
            where w.id = in_id and w.id_organization = _id_organization;
        end if;

        return _item;
    end;
    $$;

    create function clients.work_tasks(in_token uuid, in_id_work bigint)
        returns clients.t_work_task[]
        language plpgsql
        cost 1000
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _item clients.t_work_task[] := null;
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            select array_agg(data.item) into _item from (
            select
                   (wt.id,
                    wt.id_work,
                    wt.id_task,
                    clients.foreign_system_connection(in_token, id_foreign_system_connection),
                    wt.sequence_number,
                    wt.attributes
                   )::clients.t_work_task as item
            from clients.work_task as wt
            inner join clients.work as w on (wt.id_work = w.id)
            where w.id = in_id_work and w.id_organization = _id_organization
            order by wt.sequence_number asc) as data;
        end if;

        return _item;
    end;
    $$;

    create function clients.work_as_job(in_token uuid, in_id bigint)
        returns clients.t_work_as_job
        language plpgsql
        cost 1000
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _item clients.t_work_as_job := null;
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then

            select w.id,
                   j.id,
                   w.title,
                   w.disabled,
                   w.repeat,
                   j.execute_at,
                   j.started_at,
                   j.completed_at,
                   j.canceled_at,
                   j.cancel_requested_at,
                   j.running,
                   j.execution_error_message
            into _item
            from clients.work as w
            left join clients.job as j on (w.id = j.id_work and j.latest)
            where w.id = in_id and w.id_organization = _id_organization;

        end if;

        return _item;
    end;
    $$;

    create type clients.t_work_and_job as (
        work clients.t_work,
        work_as_job clients.t_work_as_job,
        tasks clients.t_work_task[]
    );

    create function clients.work_and_job(in_token uuid, in_id bigint)
        returns clients.t_work_and_job
        language plpgsql
        cost 1000
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _item clients.t_work_and_job := null;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            _item.work := clients.work(in_token,in_id);
            _item.work_as_job := clients.work_as_job(in_token,in_id);
            _item.tasks := clients.work_tasks(in_token,in_id);

            return _item;
        end if;

        return _item;
    end;
    $$;

    create function clients.insert_work(in_token uuid, in_title text, in_disabled boolean, in_repeat boolean, in_id_run_period text, in_minute smallint, in_hour smallint, in_week_day smallint, in_month_day smallint, in_month smallint, in_tasks clients.t_work_task[])
        returns clients.t_work_and_job
        language plpgsql
        cost 1000
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _item clients.t_work_and_job := null;
        _id_organization bigint := clients.session_organization_id(in_token);
        _id_work bigint;
        _work_task clients.t_work_task;
        _sequence_number int := 0;
        _work_task_id bigint;
        _first_work_task_id bigint := null;
        _id_work_server bigint;
        _fsc clients.t_foreign_system_connection;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            insert into clients.work(id_organization, id_run_period, title, disabled, repeat, minute, hour, week_day, month_day, month)
            values (_id_organization, in_id_run_period, in_title, in_disabled, in_repeat, in_minute, in_hour, in_week_day, in_month_day, in_month) returning id into _id_work;

            foreach _work_task in array in_tasks
            loop
                _fsc := _work_task.foreign_system_connection;
                _sequence_number := _sequence_number + 1;
                insert into clients.work_task(id_work, id_task, id_foreign_system_connection, sequence_number, attributes)
                values (_id_work, _work_task.task_code::bigint, _fsc.id, _sequence_number, _work_task.attributes) returning id into _work_task_id;

                if _first_work_task_id is null then
                    _first_work_task_id := _work_task_id;
                end if;
            end loop;

            -- get random available work server id
            _id_work_server := clients.get_random_work_server_id();

            insert into clients.job(
                                    id_work_server,
                                    id_work,
                                    id_current_work_task,
                                    execute_at,
                                    started_at,
                                    completed_at,
                                    running,
                                    cancel_requested_at,
                                    canceled_at,
                                    execution_error_message,
                                    latest)
            values (
                    _id_work_server,
                    _id_work,
                    _first_work_task_id,
                    clients.calculate_work_next_runtime(in_id_run_period, in_minute, in_hour, in_week_day, in_month_day, in_month),
                    null,
                    null,
                    false,
                    null,
                    null,
                    null,
                    true
                    );


            _item := clients.work_and_job(in_token, _id_work);
        end if;

        return _item;
    end;
    $$;

    create function clients.update_work(in_token uuid, in_id_work bigint, in_title text, in_disabled boolean, in_repeat boolean, in_id_run_period text, in_minute smallint, in_hour smallint, in_week_day smallint, in_month_day smallint, in_month smallint, in_tasks clients.t_work_task[])
        returns clients.t_work_and_job
        language plpgsql
        cost 1000
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _item clients.t_work_and_job := null;
        _id_organization bigint := clients.session_organization_id(in_token);
        _work_task clients.t_work_task;
        _sequence_number int := 0;
        _work_task_id bigint;
        _first_work_task_id bigint := null;
        _id_work_server bigint;
        _fsc clients.t_foreign_system_connection;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            update
                clients.work
            set
                id_run_period = in_id_run_period,
                title = in_title,
                disabled = in_disabled,
                repeat = in_repeat,
                minute = in_minute,
                hour = in_hour,
                week_day = in_week_day,
                month_day = in_month_day,
                month = in_month where id = in_id_work and id_organization = _id_organization;

            if found then
                -- remove existing jobs
                delete from clients.job where id_work = in_id_work;
                delete from clients.work_task where id_work = in_id_work;

                -- add new work tasks
                foreach _work_task in array in_tasks
                loop
                    _fsc := _work_task.foreign_system_connection;
                    _sequence_number := _sequence_number + 1;
                    insert into clients.work_task(id_work, id_task, id_foreign_system_connection, sequence_number, attributes)
                    values (in_id_work, _work_task.task_code::bigint, _fsc.id, _sequence_number, _work_task.attributes) returning id into _work_task_id;

                    if _first_work_task_id is null then
                        _first_work_task_id := _work_task_id;
                    end if;
                end loop;

                -- get random available work server id
                _id_work_server := clients.get_random_work_server_id();

                insert into clients.job(
                                        id_work_server,
                                        id_work,
                                        id_current_work_task,
                                        execute_at,
                                        started_at,
                                        completed_at,
                                        running,
                                        cancel_requested_at,
                                        canceled_at,
                                        execution_error_message,
                                        latest)
                values (
                        _id_work_server,
                        in_id_work,
                        _first_work_task_id,
                        clients.calculate_work_next_runtime(in_id_run_period, in_minute, in_hour, in_week_day, in_month_day, in_month),
                        null,
                        null,
                        false,
                        null,
                        null,
                        null,
                        true
                        );


                _item := clients.work_and_job(in_token, in_id_work);
            end if;
        end if;

        return _item;
    end;
    $$;

    create function clients.task(in_token uuid, in_id_task bigint)
        returns management.t_task
        parallel safe
        cost 1
        language plpgsql
    as
    $$
    declare
        _id_user bigint;
        _task management.t_task;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            select task.id,
                   (fs.id, fs.title, fs.system_type, fs.logo_base64, fs.disabled, fs.created_at)::management.t_foreign_system,
                   task.title,
                   task.code,
                   task.available,
                   task.description,
                   (rp.id, rp.title, rp.frequency)::management.t_run_period,
                   task.times_executed,
                   task.status
            into
                _task
            from management.task
            inner join management.foreign_system as fs on (task.id_foreign_system = fs.id)
            inner join management.run_period as rp on (task.id_min_run_period = rp.id)
            where task.id = in_id_task;

            if found then
                return _task;
            end if;
        end if;

        return null;
    end;
    $$;

    create function clients.delete_work(
            in_token uuid,
            in_id bigint)
        returns boolean
        language plpgsql
        cost 1000
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            delete from clients.work where id = in_id and id_organization = _id_organization;

            return found;
        end if;

        return false;
    end;
    $$;

    create function management.organization_by_job_id(in_token uuid, in_id_job bigint)
        returns clients.t_organization
        parallel safe
        cost 1000
        language plpgsql
    as $$
    declare
        _id_user bigint;
        _organization clients.t_organization;
        _id_organization bigint;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            select id_organization into _id_organization from clients.job
            inner join clients.work as w on (job.id_work = w.id)
            where job.id = in_id_job;

            select id, title, contact_person, contact_email, contact_phone, created_at into _organization
            from clients.organization where id = _id_organization;
            if found then
                return _organization;
            end if;
        end if;

        return null;
    end;
    $$;

    create function management.work_by_job_id(in_token uuid, in_id_job bigint)
        returns clients.t_work
        parallel safe
        cost 1000
        language plpgsql
    as $$
    declare
        _id_user bigint;
        _item clients.t_work := null;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            select w.id,
                   id_organization,
                   (select (id, title, frequency)::management.t_run_period from management.run_period where id = w.id_run_period),
                   w.title,
                   w.disabled,
                   w.repeat,
                   w.minute,
                   w.hour,
                   w.week_day,
                   w.month_day,
                   w.month
            into _item
            from clients.work as w
            inner join clients.job as j on (w.id = j.id_work)
            where j.id = in_id_job;
        end if;

        return _item;
    end;
    $$;





COMMIT;
