-- Deploy integrator:return_recipe_from_fiscof to pg

BEGIN;

    insert into management.task(id_foreign_system, title, code, available, description, id_min_run_period)
    values('horizon', 'Veidot atmaksas čekus', 'horizon_import_return_recipe_from_fiscof', true, 'Veidot atmaksas čekus no fiscof', 'week_day'); -- 12

COMMIT;
