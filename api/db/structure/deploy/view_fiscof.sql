    -- Deploy integrator:view_fiscof to pg
-- requires: functions

BEGIN;

    create type clients.t_tikitaka_summary as (
        fsc_id           bigint,
        fsc_title        text,
        fsc_available    boolean,
        fiscofs          bigint,
        documents        bigint,
        online_documents bigint
    );

    create type clients.t_device_fiscof_doc as
    (
        id                   bigint,
        id_device_fiscof     bigint,
        device_serial_number text,
        title                varchar(84),
        z                    boolean,
        doc                  xml,
        doc_datetime         timestamp,
        created_at           timestamp
    );

    create function clients.fsc_tikitaka_summary(in_token uuid)
        returns clients.t_tikitaka_summary[]
        language plpgsql
        cost 2000
    as $$
    declare
        _id_user bigint;
        _summary clients.t_tikitaka_summary;
        _summaries clients.t_tikitaka_summary[];
        _fsc record;
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            for _fsc in select id, title, available from clients.foreign_system_connection
            where id_organization = _id_organization and id_foreign_system = 'tiki-taka' order by title asc
            loop
                _summary.fsc_id         := _fsc.id;
                _summary.fsc_title      := _fsc.title;
                _summary.fsc_available  := _fsc.available;

                select
                    summary.fiscofs[1],
                    summary.fiscofs[2],
                    summary.online_documents
                into
                    _summary.fiscofs,
                    _summary.documents,
                    _summary.online_documents
                from (select
                    (select array[count(id), sum(document_count)] from clients.device_fiscof where id_fsc = _fsc.id)::bigint[] as fiscofs,
                    (select count(id) from clients.device_online_doc where id_fsc = _fsc.id) as online_documents
                ) as summary;

                _summaries := array_append(_summaries, _summary);
            end loop;

            return _summaries;
        end if;

        return null;
    end;
    $$;


    create function clients.online_document(in_token uuid, in_id bigint)
        returns clients.t_device_online_doc
        language plpgsql
        cost 1000
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _item clients.t_device_online_doc := null;
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            select device_online_doc.id,
                    id_fsc,
                    device_serial_number,
                    source_id,
                    device_online_doc.title,
                    doc,
                    doc_datetime,
                    device_online_doc.created_at into _item
            from clients.device_online_doc
            inner join clients.foreign_system_connection as fsc on (device_online_doc.id_fsc = fsc.id)
            where device_online_doc.id = in_id and fsc.id_organization = _id_organization;
        end if;

        return _item;
    end;
    $$;

    create function clients.document(in_token uuid, in_id bigint)
        returns clients.t_device_fiscof_doc
        language plpgsql
        cost 1000
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _item clients.t_device_fiscof_doc := null;
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            select d.id,
                   id_device_fiscof,
                   device_serial_number,
                   d.title,
                   z,
                   d.doc,
                   d.doc_datetime,
                   d.created_at into _item
            from clients.device_doc as d
            inner join clients.device_fiscof as df on (d.id_device_fiscof = df.id)
            inner join clients.foreign_system_connection as fsc on (df.id_fsc = fsc.id)
            where d.id = in_id and fsc.id_organization = _id_organization;
        end if;

        return _item;
    end;
    $$;

    create function clients.fiscof(in_token uuid, in_id bigint)
        returns clients.t_device_fiscof
        language plpgsql
        cost 1000
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _item clients.t_device_fiscof := null;
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            select f.id,
                   id_fsc,
                   source_id,
                   device_serial_number,
                   document_count,
                   doc,
                   doc_datetime,
                   f.created_at,
                   (
                       select array_agg((
                           device_doc.id,
                           device_doc.id_device_fiscof,
                           device_doc.title,
                           device_doc.z,
                           device_doc.doc,
                           device_doc.doc_datetime,
                           device_doc.created_at
                       )::clients.t_device_doc) from clients.device_doc where id_device_fiscof = f.id
                   )::clients.t_device_doc[] into _item
            from clients.device_fiscof as f
            inner join clients.foreign_system_connection as fsc on (f.id_fsc = fsc.id)
            where f.id = in_id and fsc.id_organization = _id_organization;

        end if;

        return _item;
    end;
    $$;


    create function clients.filter_fiscofs(in_token uuid, in_id_fsc bigint, in_skip bigint, in_limit bigint, in_order_by text default 'id', in_order_direction smallint default 0, in_fts_qry text default null, in_device_serials text[] default null)
        returns clients.t_device_fiscof[]
        language plpgsql
        cost 1000
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _items clients.t_device_fiscof[];
        _content_type text := 'clients.t_device_fiscof';
        _sql text := 'select %s from clients.device_fiscof as f inner join clients.foreign_system_connection as fsc on (f.id_fsc = fsc.id)';
        _type_select text := format(
            '(f.id, f.id_fsc, f.source_id, f.device_serial_number, f.document_count, f.doc, f.doc_datetime, f.created_at, null)::%s', _content_type);
        _where text;
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then

            _where := concat_ws(' AND ',
                case when in_fts_qry is not null then 'f.tsv @@ websearch_to_tsquery($3)' end,
                case when in_id_fsc is not null then 'f.id_fsc = $4' end,
                'fsc.id_organization = $5',
                case when in_device_serials is not null then 'f.device_serial_number = any($6)' end);

            _sql := system.build_filter_sql(
                _sql,
                _content_type,
                _where,
                in_order_by,
                in_order_direction,
                _type_select,
                'f');

            execute _sql into _items using in_limit, in_skip, in_fts_qry, in_id_fsc, _id_organization, in_device_serials;
        end if;

        return _items;
    end;
    $$;

    create function clients.filter_documents(in_token uuid, in_id_fsc bigint, in_skip bigint, in_limit bigint, in_order_by text default 'id', in_order_direction smallint default 0, in_fts_qry text default null, in_device_serials text[] default null)
        returns clients.t_device_fiscof_doc[]
        language plpgsql
        cost 1000
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _items clients.t_device_fiscof_doc[];
        _content_type text := 'clients.t_device_fiscof_doc';
        _sql text := 'select %s from clients.device_doc as d inner join clients.device_fiscof as df on (df.id = d.id_device_fiscof) inner join clients.foreign_system_connection as fsc on (df.id_fsc = fsc.id)';
        _type_select text := format(
            '(d.id, d.id_device_fiscof, df.device_serial_number, d.title, d.z, d.doc, d.doc_datetime, d.created_at)::%s', _content_type);
        _where text;
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then

            _where := concat_ws(' AND ',
                case when in_fts_qry is not null then 'd.tsv @@ websearch_to_tsquery($3)' end,
                case when in_id_fsc is not null then 'df.id_fsc = $4' end,
                'fsc.id_organization = $5',
                case when in_device_serials is not null then 'df.device_serial_number = any($6)' end);

            _sql := system.build_filter_sql(
                _sql,
                _content_type,
                _where,
                in_order_by,
                in_order_direction,
                _type_select,
                'd');

            execute _sql into _items using in_limit, in_skip, in_fts_qry, in_id_fsc, _id_organization, in_device_serials;
        end if;

        return _items;
    end;
    $$;

    create function clients.filter_online_documents(in_token uuid, in_id_fsc bigint, in_skip bigint, in_limit bigint, in_order_by text default 'id', in_order_direction smallint default 0, in_fts_qry text default null, in_device_serials text[] default null)
        returns clients.t_device_online_doc[]
        language plpgsql
        cost 1000
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _items clients.t_device_online_doc[];
        _content_type text := 'clients.t_device_online_doc';
        _sql text := 'select %s from clients.device_online_doc as d inner join clients.foreign_system_connection as fsc on (d.id_fsc = fsc.id)';
        _type_select text := format(
            '(d.id, d.id_fsc, d.device_serial_number, d.source_id, d.title, d.doc, d.doc_datetime, d.created_at)::%s', _content_type);
        _where text;
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then

            _where := concat_ws(' AND ',
                case when in_fts_qry is not null then 'd.tsv @@ websearch_to_tsquery($3)' end,
                case when in_id_fsc is not null then 'd.id_fsc = $4' end,
                'fsc.id_organization = $5',
                case when in_device_serials is not null then 'd.device_serial_number = any($6)' end);

            _sql := system.build_filter_sql(
                _sql,
                _content_type,
                _where,
                in_order_by,
                in_order_direction,
                _type_select,
                'd');

            execute _sql into _items using in_limit, in_skip, in_fts_qry, in_id_fsc, _id_organization, in_device_serials;
        end if;

        return _items;
    end;
    $$;

COMMIT;
