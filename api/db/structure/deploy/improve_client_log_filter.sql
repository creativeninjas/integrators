-- Deploy integrator:improve_client_log_filter to pg
-- requires: functions

BEGIN;

    drop function system.build_filter_sql(text, text, text, text, smallint, text, text);

    create function system.build_filter_sql(in_sql text, in_content_type text, in_where text, in_order_by text, in_order_direction smallint, in_type_select text default null, in_cols_as text default null, in_custom_order_by text default null)
        returns text
        language plpgsql
        immutable parallel safe
        cost 1
        as $$
    declare
        _sql text := '';
        _type_select text := '(%s)::%s as t';
        _slice text := ' limit $1 offset $2';
        _cols text := system.t_attr_str(in_content_type);
        _order text := 'ASC';
    begin
        if in_type_select is null then
            _type_select := format(_type_select, _cols, in_content_type);
        else
            _type_select := in_type_select || ' as t';
        end if;
        _sql := format(in_sql, _type_select);

        if in_where <> '' then
            _sql := _sql || ' where ' || in_where;
        end if;

        if in_custom_order_by is not null then
            _sql := format('%s order by %s', _sql, in_custom_order_by);
        elseif in_order_by is not null and position(in_order_by in _cols) > 0 then
            if in_order_direction = 1 then
                _order = 'DESC';
            end if;

            if in_cols_as is null then
                _sql := format('%s order by %s %s', _sql, in_order_by, _order);
            else
                _sql := format('%s order by %s.%s %s', _sql, in_cols_as, in_order_by, _order);
            end if;
        end if;

        _sql := _sql || _slice;

        _sql = format('select array_agg(data.t) from (%s) as data', _sql);

        return _sql;
    end;
    $$;

    create type clients.t_log as (
        log system.t_log,
        viewed boolean
    );

    drop function clients.filter_system_logs(uuid, bigint, bigint, text, smallint, text, text[]);

    create function clients.filter_system_logs(in_token uuid, in_skip bigint, in_limit bigint, in_order_by text default 'id', in_order_direction smallint default 0, in_fts_qry text default null, in_level text[] default null)
        returns clients.t_log[]
        language plpgsql
        cost 1000
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _items clients.t_log[];
        _content_type text := 'clients.t_log';
        _sql text := 'select %s from system.log as sl left join clients.organization as o on (sl.id_organization = o.id) ';
        _type_select text := format('((sl.id, case when sl.id_organization is not null then (o.id, o.title, o.contact_person, o.contact_email, o.contact_phone, o.created_at)::%s else null end, sl.entry, sl.message, sl.level, sl.created_at)::system.t_log, case when uvl.id_user is null then false else true end)::%s', 'clients.t_organization', _content_type);
        _where text;
        _id_organization bigint := clients.session_organization_id(in_token);
        _order text := 'ASC';
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        _sql := _sql ||'left join system.user_viewed_log as uvl on (sl.id = uvl.id_log and uvl.id_user = '|| _id_user ||') ';

        if _id_user is not null then
            _where := concat_ws(' AND ',
                case when in_fts_qry is not null then 'tsv @@ websearch_to_tsquery($3)' end,
                case when in_level is not null then 'level = any($4)' end,
                'id_organization = $5');

            if in_order_direction = 1 then
                _order = 'DESC';
            end if;

            _sql := system.build_filter_sql(
                _sql,
                _content_type,
                _where,
                in_order_by,
                in_order_direction,
                _type_select,
                'sl',
                format('sl.%s %s', in_order_by, _order));

            execute _sql into _items using in_limit, in_skip, in_fts_qry, in_level, _id_organization;
        end if;

        return _items;
    end;
    $$;

COMMIT;
