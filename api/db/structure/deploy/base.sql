-- Deploy integrator:base to pg

BEGIN;

    -- Create constants schema, required to store different constant variables like hstore keys etc
    create schema constants;

    create function constants.password_block_size()
        returns smallint
        language plpgsql
        immutable parallel safe
    as $$
    begin
        return 3::smallint;
    end;
    $$;

    create function constants.password_block_count()
        returns smallint
        language plpgsql
        immutable parallel safe
    as $$
    begin
        return 4::smallint;
    end;
    $$;

    create function constants.scope_manager()
        returns text
        language plpgsql
        immutable parallel safe
    as $$
    begin
        return 'MANAGER';
    end;
    $$;

    create function constants.scope_client()
        returns text
        language plpgsql
        immutable parallel safe
    as $$
    begin
        return 'CLIENT';
    end;
    $$;

    create function constants.authorization_user_not_found_errcode()
        returns text
        language plpgsql
        immutable parallel safe
    as $$
    begin
        return 'AUTH_NOT_FOUND';
    end;
    $$;

    create function constants.session_organization_is_not_set()
        returns text
        language plpgsql
        immutable parallel safe
    as $$
    begin
        return 'SESSION_ORG_IS_NOT_SET';
    end;
    $$;

    -- System schema base db structure
    create schema system;

    create function system.build_filter_sql(in_sql text, in_content_type text, in_where text, in_order_by text, in_order_direction smallint, in_type_select text default null, in_cols_as text default null)
        returns text
        language plpgsql
        immutable parallel safe
        cost 1
        as $$
    declare
        _sql text := '';
        _type_select text := '(%s)::%s as t';
        _slice text := ' limit $1 offset $2';
        _cols text := system.t_attr_str(in_content_type);
        _order text := 'ASC';
    begin
        if in_type_select is null then
            _type_select := format(_type_select, _cols, in_content_type);
        else
            _type_select := in_type_select || ' as t';
        end if;
        _sql := format(in_sql, _type_select);

        if in_where <> '' then
            _sql := _sql || ' where ' || in_where;
        end if;

        if in_order_by is not null and position(in_order_by in _cols) > 0 then
            if in_order_direction = 1 then
                _order = 'DESC';
            end if;

            if in_cols_as is null then
                _sql := format('%s order by %s %s', _sql, in_order_by, _order);
            else
                _sql := format('%s order by %s.%s %s', _sql, in_cols_as, in_order_by, _order);
            end if;
        end if;

        _sql := _sql || _slice;

        _sql = format('select array_agg(data.t) from (%s) as data', _sql);

        return _sql;
    end;
    $$;

    create function system.t_attr_str(in_type_name text)
        returns text
        language plpgsql
        parallel unsafe
        cost 1
        as $$
    declare
        _cols text := '';
    begin
        select string_agg(c.c, ', ') into _cols from (
            with types as(
                select n.nspname, pg_catalog.format_type(t.oid, null) as obj_name,
                    case
                        when t.typrelid != 0 then cast('tuple' as pg_catalog.text)
                        when t.typlen < 0 then cast('var' as pg_catalog.text)
                        else cast(t.typlen as pg_catalog.text)
                    end as obj_type,
                    coalesce(pg_catalog.obj_description(t.oid, 'pg_type'), '') AS description
                from pg_catalog.pg_type t
                join pg_catalog.pg_namespace n on (n.oid = t.typnamespace)
                where (t.typrelid = 0 or (select c.relkind = 'c' from pg_catalog.pg_class c where c.oid = t.typrelid))
                      and not exists(select 1 from pg_catalog.pg_type el where el.oid = t.typelem and el.typarray = t.oid)
                      and n.nspname <> 'pg_catalog'
                      and n.nspname <> 'information_schema'
                      and n.nspname !~ '^pg_toast'
            ), cols as (
                select n.nspname::text as schema_name,
                       pg_catalog.format_type(t.oid, null) as obj_name,
                       a.attname::text as column_name,
                       pg_catalog.format_type(a.atttypid, a.atttypmod) as data_type,
                       a.attnotnull as is_required,
                       a.attnum as ordinal_position,
                       pg_catalog.col_description(a.attrelid, a.attnum) as description
                from pg_catalog.pg_attribute a
                join pg_catalog.pg_type t ON a.attrelid = t.typrelid
                join pg_catalog.pg_namespace n ON ( n.oid = t.typnamespace )
                join types on (types.nspname = n.nspname and types.obj_name = pg_catalog.format_type(t.oid, null))
                where a.attnum > 0 and not a.attisdropped
            )
            select cols.column_name as c from cols
            where obj_name = in_type_name order by cols.ordinal_position
        ) as c;

        return _cols;
    end;
    $$;

    create function system.gen_password(in_block_size smallint, in_block_count smallint)
        returns text
        language plpgsql
        immutable parallel safe
        called on null input
        security invoker
        cost 1
        as $$
    declare
        _password text := '';
        _chars text := 'abcdefghijklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ123456789';
        _char_pos smallint;
        _stop smallint := in_block_size * in_block_count + (in_block_count - 1) ;
        _block_counter smallint := 0;
    begin
        while length(_password) < _stop loop
            _char_pos := int4(random() * length(_chars));
            if _char_pos = 0 then
                _char_pos := _char_pos + 1;
            end if;
            _password := _password || substr(_chars, _char_pos, 1);
            _block_counter := _block_counter + 1;
            if _block_counter = in_block_size and length(_password) < _stop then
                _password := _password || '-';
                _block_counter := 0;
            end if;
        end loop;

        return _password;
    end;$$;

    create function system.gen_tsvector(variadic in_str text[])
        returns tsvector
        language plpgsql
        immutable parallel safe
        called on null input
        security invoker
        cost 1
        as $$
    declare
        word text;
        full_str text := '';
    begin
        foreach word in array in_str
        loop
            full_str := full_str || ' ' || coalesce(word, '');
        end loop;

        return to_tsvector('english'::regconfig, full_str);
    end;$$;

    -- Security schema base db structure
    create schema security;

    create table security.user(
        id bigserial,
        email character varying(255) not null,
        first_name character varying(160) not null,
        last_name character varying(160) not null,
        password_hash character varying(160) not null,
        tsv tsvector generated always as (system.gen_tsvector(id::text, first_name, last_name, email)) stored,
        primary key(id),
        constraint uq_email unique(email)
    );

    create table security.scope(
        id character varying(64),
        title character varying(100) not null,
        description text,
        primary key(id)
    );

    create table security.user_scope(
        id_user bigint,
        id_scope character varying(24),
        primary key(id_user, id_scope),
        constraint fk_user_scope_user
            foreign key(id_user) references security.user(id) on delete cascade on update cascade,
        constraint fk_user_scope_scope
            foreign key(id_scope) references security.scope(id) on delete cascade on update cascade
    );

    create table security.user_log(
        id bigserial,
        id_user bigint not null,
        message text not null,
        code text not null,
        created_at timestamp default now() not null,
        tsv tsvector generated always as (system.gen_tsvector(id::text, id_user::text, message, code)) stored,
        primary key(id),
        constraint fk_user_log_user
            foreign key(id_user) references security.user(id) on delete cascade on update cascade
    );

    create type security.t_session_user as (
        id bigint,
        email character varying(255),
        first_name character varying(160),
        last_name character varying(160),
        manager boolean,
        id_organization bigint
    );

    create type security.t_user as (
        id bigint,
        email character varying(255),
        first_name character varying(160),
        last_name character varying(160),
        pwd text -- is not null only when user is created
    );

    create type security.t_api_session as (
        id_user bigint,
        db_token uuid
    );

    create type security.t_db_session as(
        db_token uuid,
        scopes   text[]
    );

    create function security.add_user_log(in_token uuid, in_message text, in_code text)
        returns boolean
        language plpgsql
        cost 1
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
    begin
        _id_user := security.have_permission(in_token, null);

        if _id_user is not null then
            insert into security.user_log(id_user, message, code) values(_id_user, in_message, in_code);

            return true;
        else
            return false;
        end if;
    end;$$;


    create function security.manage_session_user(in_token uuid)
        returns security.t_session_user
        language plpgsql
        cost 1
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _session_user security.t_session_user;
    begin
        _id_user := security.have_permission(in_token, array[constants.scope_manager()]);

        if _id_user is not null then
            -- for security reasons remove password hash from result
            -- Don't check permission - session user can view self and  update session token till date
            select
                id,
                email,
                first_name,
                last_name,
                false,
                null -- no organization id
            into
                _session_user
            from
                security.user
            inner join security.user_session on ("user".id = user_session.id_user)
            where
                session_token = in_token;

            perform from security.user_scope
            where id_user = _session_user.id and id_scope = constants.scope_manager();

            if found then
                _session_user.manager  = true;

                return _session_user;
            else
                return null;
            end if;

        else
            return null;
        end if;
    end;
    $$;

    create function security.organization_session_user(in_token uuid)
        returns security.t_session_user
        language plpgsql
        cost 1
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _session_user security.t_session_user;
    begin
        _id_user := security.have_permission(in_token, array[constants.scope_client()]);

        if _id_user is not null then
            -- for security reasons remove password hash from result
            -- Don't check permission - session user can view self and  update session token till date
            select
                id,
                email,
                first_name,
                last_name,
                false,
                id_organization
            into
                _session_user
            from
                security.user
            inner join security.user_session on ("user".id = user_session.id_user)
            where
                session_token = in_token;

            if found then
                if _session_user.id_organization is not null then
                    select
                        organization_user.first_name,
                        organization_user.last_name,
                        id_organization
                    into
                        _session_user.first_name,
                        _session_user.last_name,
                        _session_user.id_organization
                    from clients.organization_user
                    where id_user = _session_user.id and organization_user.id_organization = _session_user.id_organization;
                else
                    -- Error no organization set
                    raise exception using
                        errcode=constants.session_organization_is_not_set(),
                        message='Session organization is not set',
                        hint='Set session user organization';
                end if;

                return _session_user;
            else
                return null;
            end if;

        else
            return null;
        end if;
    end;
    $$;

    create function security.authorize_user(
        in_email character varying(255),
        in_password character varying(255),
        in_minutes_valid smallint)
        returns security.t_db_session
        language plpgsql
        cost 1
    as $$
    declare
        _id_user bigint;
        _db_token uuid;
        _session_organization_id bigint := null;
        _session_token_valid_till timestamp;
        _db_session security.t_db_session;
    begin
        select id into _id_user from security.user
        where email = lower(in_email) and password_hash = crypt(in_password, password_hash);

        if found then
            -- Remove previous session token if exist
            delete from security.user_session where id_user = _id_user and not impersonate;

            -- Generate db token
            loop
                begin
                    _db_token := gen_random_uuid();
                    _session_token_valid_till := CURRENT_TIMESTAMP + (in_minutes_valid * interval '1 minute');

                    insert into security.user_session(id_user, id_organization, session_token, session_token_valid_till)
                    values (_id_user, _session_organization_id, _db_token, _session_token_valid_till);

                    if found then
                        _db_session.db_token := _db_token;
                        select scopes into _db_session.scopes from(
                            select array_agg(id_scope) as scopes
                            from security.user_scope where id_user = _id_user) as user_scopes;
                        return _db_session;
                    end if;

                    exception when unique_violation then
                        -- Do nothing, and loop to generate new random uuid and try the insert again.
                    end;
                end loop;
            else
                raise exception using
                    errcode=constants.authorization_user_not_found_errcode(),
                    message=format('Authorization user with e-mail "{%s}" and provided password not found', in_email),
                    hint='Check authorization data';
            end if;
        end;
    $$;

    create function security.api_session(
        in_db_token uuid,
        scopes text[],
        in_minutes_valid smallint)
        returns security.t_api_session
        language plpgsql
        cost 1
    as $$
    declare
        _id_user bigint;
        _db_token uuid;
        _api_session security.t_api_session;
    begin
        select "user".id, session_token into _id_user, _db_token
        from security.user_session
        inner join security."user" on (user_session.id_user = "user".id)
        where session_token = in_db_token and session_token_valid_till > now();

        if found then
            -- Check scopes
            perform from security.user_scope where id_user = _id_user and id_scope = any(scopes);

            if found then
                -- Update token expiration timestamp
                update
                    security.user_session
                set
                    session_token_valid_till = CURRENT_TIMESTAMP + (in_minutes_valid * interval '1 minute')
                where
                    session_token = _db_token;

               _api_session.id_user := _id_user;
               _api_session.db_token := _db_token;

               return _api_session;
            else
                raise exception using
                    errcode=constants.authorization_user_not_found_errcode(),
                    message='No permission',
                    hint='Check permissions';
            end if;
        else
            return null;
        end if;
    end;
    $$;

    create function security.have_permission(
        in_db_token uuid,
        scopes text[])
        returns bigint
        language plpgsql
        cost 1
    as $$
    declare
        _id_user bigint;
    begin
        select "user".id into _id_user
        from security.user_session
        inner join security."user" on (user_session.id_user = "user".id)
        where session_token = in_db_token and session_token_valid_till > now();

        if found then
            -- Check scopes
            if scopes is not null then
                perform from security.user_scope where id_user = _id_user and id_scope = any(scopes);

                if found then
                    return _id_user;
                else
                    return null;
                end if;
            else
                -- Scopes is not required and session is valid
                return _id_user;
            end if;
        else
            return null;
        end if;
    end;
    $$;

    -- Clients schema base db structure
    create schema clients;

    create table clients.organization(
        id bigserial,
        title character varying(255) not null,
        contact_person character varying (255) not null,
        contact_email character varying (255) not null,
        contact_phone character varying (50),
        created_at timestamp default now() not null,
        tsv tsvector generated always as (system.gen_tsvector(id::text, title, contact_person, contact_email)) stored,
        primary key(id)
    );

    -- Security and organization related structure
    create table clients.organization_user(
        id_user bigint not null,
        id_organization bigint not null,
        first_name character varying(160) not null,
        last_name character varying(160) not null,
        tsv tsvector generated always as (system.gen_tsvector(id_user::text, id_organization::text, first_name, last_name)) stored,
        primary key(id_user, id_organization),
        constraint fk_organization_user_user
            foreign key(id_user) references security.user(id) on delete cascade on update cascade,
        constraint fk_organization_user_organization
            foreign key(id_organization) references clients.organization(id) on delete cascade on update cascade
    );

    create table security.user_session(
        id_user bigint,
        id_organization bigint,
        impersonate bool default false not null,
        session_token uuid not null,
        session_token_valid_till timestamp not null,
        primary key(id_user, impersonate),
        constraint uq_session_token unique(session_token),
        constraint fk_user_session_user
            foreign key(id_user) references security.user(id) on delete cascade on update cascade,
        constraint fk_user_session_organization
            foreign key(id_organization) references clients.organization(id) on delete cascade on update cascade
    );

    create type clients.t_user_organization as (
        id_organization bigint,
        organization_title text
    );

    create function security.session_user_organizations(in_token uuid)
        returns clients.t_user_organization[]
        language plpgsql
        cost 10
        volatile parallel unsafe
    as $$
    declare
        _user_organizations clients.t_user_organization[];
        _id_user bigint;
    begin
        _id_user := security.have_permission(in_token, array[constants.scope_client()]);

        if _id_user is not null then

            select array_agg((id, title)::clients.t_user_organization) into _user_organizations
            from clients.organization_user
            inner join clients.organization on (organization_user.id_organization = organization.id)
            where id_user = _id_user;

            return _user_organizations;
        else
            return null;
        end if;
    end;
    $$;

    create function security.set_session_user_organization(in_token uuid, in_organization_id bigint)
        returns bool
        language plpgsql
        cost 1
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
    begin
        _id_user := security.have_permission(in_token, array[constants.scope_client()]);

        if _id_user is not null then
            perform from clients.organization_user
            inner join clients.organization on (organization_user.id_organization = organization.id)
            where id_user = _id_user and id_organization = in_organization_id;

            if found then
                update security.user_session set id_organization = in_organization_id where session_token = in_token;

                return true;
            else
                return false;
            end if;
        else
            return false;
        end if;
    end;
    $$;

    create function security.change_password(in_token uuid, in_password text, in_new_password text)
        returns bool
        language plpgsql
        cost 1
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
    begin
        _id_user := security.have_permission(
            in_token, array[constants.scope_manager(), constants.scope_client()]);

        if _id_user is not null then
            update security.user
            set password_hash = crypt(in_new_password, gen_salt('md5'))
            where id = _id_user and password_hash = crypt(in_password, password_hash);

            return found;
        else
            return false;
        end if;
    end;
    $$;



    -- Client organization
    create type clients.t_organization as (
        id bigint,
        title character varying (255),
        contact_person character varying (255),
        contact_email character varying (255),
        contact_phone character varying (255),
        created_at timestamp
    );

    create function clients.session_organization_id(in_token uuid)
        returns bigint
        language plpgsql
        cost 1
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _id_organization bigint;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            -- Get session organization id
            select id_organization into _id_organization from security.user_session where session_token = in_token;
            if found and _id_organization is not null then
                return _id_organization;
            end if;
        end if;

        -- Error no organization set
        raise exception using
            errcode=constants.session_organization_is_not_set(),
            message='Session organization is not set',
            hint='Set session user organization';
    end;
    $$;

    create function clients.organization(in_token uuid)
        returns clients.t_organization
        language plpgsql
        cost 1
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _organization clients.t_organization;
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            -- Return organization if user is organization user
            select o.id, o.title, o.contact_person, o.contact_email, o.contact_phone, o.created_at into _organization
            from clients.organization as o
            inner join clients.organization_user as ou on (o.id = ou.id_organization)
            where ou.id_organization = _id_organization and ou.id_user = _id_user;

            if found then
                return _organization;
            end if;
        end if;

        return null;
    end;
    $$;

    create function clients.filter_session_organization_users(in_token uuid, in_skip bigint, in_limit bigint, in_order_by text default 'id', in_order_direction smallint default 0, in_fts_qry text default null)
        returns security.t_user[]
        language plpgsql
        cost 100
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _items security.t_user[];
        _content_type text := 'security.t_user';
        _sql text := 'select %s from security.user as u inner join clients.organization_user as ou on (id_user = u.id)';
        _type_select text := format('(u.id, u.email, ou.first_name, ou.last_name, null)::%s', _content_type);
        _where text;
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then

            _where := concat_ws(' AND ',
                case when in_fts_qry is not null then 'ou.tsv @@ websearch_to_tsquery($3)' end,
                'id_organization = $4');

            _sql := system.build_filter_sql(
                _sql,
                _content_type,
                _where,
                in_order_by,
                in_order_direction,
                _type_select);

            raise notice '%s', _sql;

            execute _sql into _items using in_limit, in_skip, in_fts_qry, _id_organization;
        end if;

        return _items;
    end;
    $$;

    -- Management schema base db structure
    create schema management;

    create function management.organization(in_token uuid, in_id_organization bigint)
        returns clients.t_organization
        language plpgsql
        cost 1
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _organization clients.t_organization;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            select id, title, contact_person, contact_email, contact_phone, created_at into _organization
            from clients.organization where id = in_id_organization;
            if found then
                return _organization;
            end if;
        end if;

        return null;
    end;
    $$;

    create function management.user(in_token uuid, in_id_user bigint)
        returns security.t_user
        language plpgsql
        cost 1
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _user security.t_user;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            select id, email, first_name, last_name into _user
            from security."user" where id = in_id_user;
            if found then
                return _user;
            end if;
        end if;

        return null;
    end;
    $$;

    create function management.insert_organization(
            in_token uuid,
            in_title character varying(255),
            in_contact_person character varying(255),
            in_contact_email character varying(255),
            in_contact_phone character varying(50))
        returns clients.t_organization
        language plpgsql
        cost 1
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _id_organization bigint;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            insert into clients.organization(title, contact_person, contact_email, contact_phone)
            values(in_title, in_contact_person, in_contact_email, in_contact_phone) returning id into _id_organization;

            return management.organization(in_token, _id_organization);
        end if;

        return false;
    end;
    $$;

    create function management.update_organization(
            in_token uuid,
            in_id bigint,
            in_title character varying(255),
            in_contact_person character varying(255),
            in_contact_email character varying(255),
            in_contact_phone character varying(50))
        returns clients.t_organization
        language plpgsql
        cost 1
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            update clients.organization
            set title = in_title,
                contact_person = in_contact_person,
                contact_email = in_contact_email,
                contact_phone = in_contact_phone where id = in_id;

            return management.organization(in_token, in_id);
        end if;

        return false;
    end;
    $$;

    create function management.delete_organization(
            in_token uuid,
            in_id bigint)
        returns boolean
        language plpgsql
        cost 1
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            delete from clients.organization where id = in_id;

            return found;
        end if;

        return false;
    end;
    $$;

    create function management.insert_manage_user(
            in_token uuid,
            in_email character varying(255),
            in_first_name character varying(255),
            in_last_name character varying(255)
            )
        returns security.t_user
        language plpgsql
        cost 1
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _id_new_user bigint;
        _user_password text := null;
        _user security.t_user;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            -- if email is used and user is with scope manager then user exists
            perform from security.user inner join security.user_scope on "user".id = user_scope.id_user
            where email = in_email and id_scope = constants.scope_manager();

            if found then
                RAISE 'Management user with email exist: %', in_email USING ERRCODE = 'unique_violation';
            end if;

            -- if email exist but no manager role then update user table
            select id into _id_new_user from security.user where email = in_email;

            if found then
                update security."user"
                set first_name = in_first_name,
                    last_name = in_last_name
                where id = _id_new_user;
            else
                _user_password := system.gen_password(constants.password_block_size(), constants.password_block_count());

                insert into security."user"(email, first_name, last_name, password_hash)
                values (in_email, in_first_name, in_last_name, crypt(_user_password, gen_salt('md5')))
                returning id into _id_new_user;
            end if;

            -- Set manager scope if is not set
            perform from security.user_scope where id_scope = constants.scope_manager() and id_user = _id_new_user;
            if not found then
                insert into security.user_scope(id_user, id_scope) VALUES (_id_new_user, constants.scope_manager());
            end if;

            _user := management.user(in_token, _id_new_user);
            if _user_password is not null then
                -- Password is returned only on user insert
                _user.pwd := _user_password;
            end if;

            return _user;
        end if;

        return null;
    end;
    $$;

    create function management.update_manage_user(
            in_token uuid,
            in_id_user bigint,
            in_email character varying(255),
            in_first_name character varying(255),
            in_last_name character varying(255)
            )
        returns security.t_user
        language plpgsql
        cost 1
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            update security."user"
            set
                email = in_email,
                first_name = in_first_name,
                last_name = in_last_name
            where id = in_id_user;

            return management.user(in_token, in_id_user);
        end if;

        return null;
    end;
    $$;

    create function management.reset_manage_user_password(
            in_token uuid,
            in_id_user bigint
            )
        returns security.t_user
        language plpgsql
        cost 1
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _new_password text;
        _user security.t_user;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            _new_password := system.gen_password(constants.password_block_size(), constants.password_block_count());

            update security."user"
            set
                password_hash = crypt(_new_password, gen_salt('md5'))
            where id = in_id_user;

            _user := management.user(in_token, in_id_user);
            _user.pwd := _new_password;
            return _user;
        end if;

        return null;
    end;
    $$;

    create function management.update_session_manage_user(
            in_token uuid,
            in_email character varying(255),
            in_first_name character varying(255),
            in_last_name character varying(255)
            )
        returns security.t_user
        language plpgsql
        cost 1
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            update security."user"
            set
                email = in_email,
                first_name = in_first_name,
                last_name = in_last_name
            where id = _id_user;

            return management.user(in_token, _id_user);
        end if;

        return null;
    end;
    $$;

    create function management.delete_manage_user(in_token uuid, in_id_user bigint)
        returns boolean
        language plpgsql
        cost 1
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            -- if user have organization profile then remove manager scope
            perform from clients.organization_user where id_user = in_id_user;
            if found then
                delete from security.user_scope where id_user = in_id_user and id_scope = constants.scope_manager();

                return found;
            else -- if user have don't have manager scope - delete it
                delete from security."user" where id = in_id_user;

                return found;
            end if;

        end if;

        return false;
    end;
    $$;

    create function management.insert_organization_user(
            in_token uuid,
            in_id_organization bigint,
            in_email character varying(255),
            in_first_name character varying(255),
            in_last_name character varying(255)
            )
        returns security.t_user
        language plpgsql
        cost 1
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _id_new_user bigint;
        _user security.t_user;
        _user_password text := null;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            -- if email is used and user have organization profile with provided id then raise unique violation
            perform from security.user inner join clients.organization_user on "user".id = organization_user.id_user
            where email = in_email and id_organization = in_id_organization;

            if found then
                RAISE 'Organization user with email exist: %', in_email USING ERRCODE = 'unique_violation';
            end if;

            select id into _id_new_user from security.user where email = in_email;

            -- no user insert new user
            if not found then
                _user_password := system.gen_password(constants.password_block_size(), constants.password_block_count());

                insert into security."user"(email, first_name, last_name, password_hash)
                values (in_email, in_first_name, in_last_name, crypt(_user_password, gen_salt('md5')))
                returning id into _id_new_user;
            end if;

            -- and insert user profile and set client scope
            insert into clients.organization_user(id_user, id_organization, first_name, last_name)
            values (_id_new_user, in_id_organization, in_first_name, in_last_name);

            -- Set client scope if it is not set
            perform from security.user_scope where id_scope = constants.scope_client() and id_user = _id_new_user;
            if not found then
                insert into security.user_scope(id_user, id_scope) VALUES (_id_new_user, constants.scope_client());
            end if;

            _user := management.user(in_token, _id_new_user);

            if _user_password is not null then
                -- set password only when user is inserted
                _user.pwd := _user_password;
            end if;

            return _user;
        end if;

        return null;
    end;
    $$;

    create function management.update_organization_user(
            in_token uuid,
            in_id_user bigint,
            in_id_organization bigint,
            in_email character varying(255),
            in_first_name character varying(255),
            in_last_name character varying(255)
            )
        returns security.t_user
        language plpgsql
        cost 1
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            update clients.organization_user
            set
                first_name = in_first_name,
                last_name = in_last_name
            where id_organization = in_id_organization and id_user = in_id_user;

            update security.user set email = in_email where id = in_id_user;

            return management.user(in_token, in_id_user);
        end if;

        return null;
    end;
    $$;

    create function clients.reset_organization_user_password(
            in_token uuid,
            in_id_user bigint
            )
        returns security.t_user
        language plpgsql
        cost 1
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _id_organization bigint := clients.session_organization_id(in_token);
        _new_password text;
        _user security.t_user;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            perform id_user from clients.organization_user where id_user = in_id_user and id_organization = _id_organization;

            if found then
                _new_password := system.gen_password(constants.password_block_size(), constants.password_block_count());

                update
                    security."user"
                set
                    password_hash = crypt(_new_password, gen_salt('md5'))
                where id = in_id_user;

                _user := management.user(in_token, in_id_user);
                _user.pwd := _new_password;

                return _user;
            end if;

        end if;

        return null;
    end;
    $$;

    create function clients.update_session_organization_user(
            in_token uuid,
            in_email character varying(255),
            in_first_name character varying(255),
            in_last_name character varying(255)
            )
        returns security.t_user
        language plpgsql
        cost 1
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            update clients.organization_user
            set
                first_name = in_first_name,
                last_name = in_last_name
            where id_organization = _id_organization and id_user = _id_user;

            update security.user set email = in_email where id = _id_user;

            return management.user(in_token, _id_user);
        end if;

        return null;
    end;
    $$;

    create function management.organization_user(in_token uuid, in_id_organization bigint, in_id_user bigint)
        returns security.t_user
        language plpgsql
        cost 1
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _user security.t_user;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            select u.id, u.email, ou.first_name, ou.last_name into _user
            from security."user" as u
            inner join clients.organization_user as ou on (u.id = ou.id_user)
            where u.id = in_id_user and ou.id_organization = in_id_organization;
            if found then
                return _user;
            end if;
        end if;

        return null;
    end;
    $$;

    create function management.delete_organization_user(in_token uuid, in_id_organization bigint, in_id_user bigint)
        returns boolean
        language plpgsql
        cost 1
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _deleted boolean;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            -- if user have manager scope remove only organization user
            perform from security.user_scope where id_user = in_id_user and id_scope = constants.scope_manager();
            if found then
                delete from clients.organization_user where id_user = in_id_user and id_organization = in_id_organization;

                return found;
            else -- if user don't have manager scope delete organization profile
                delete from clients.organization_user where id_organization = in_id_organization and id_user = in_id_user;
                _deleted := found;

                -- if user don't have other organization profile delete it
                perform from clients.organization_user where id_user = in_id_user;
                if not found then
                    delete from security."user" where id = in_id_user;

                    return found;
                end if;

                return _deleted;
            end if;

        end if;

        return false;
    end;
    $$;

    create function management.filter_organizations(in_token uuid, in_skip bigint, in_limit bigint, in_order_by text default 'id', in_order_direction smallint default 0, in_fts_qry text default null)
        returns clients.t_organization[]
        language plpgsql
        cost 100
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _items clients.t_organization[];
        _content_type text := 'clients.t_organization';
        _sql text := 'select %s from clients.organization';
        _where text;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then

            _where := concat_ws(' AND ',
                case when in_fts_qry is not null then 'tsv @@ websearch_to_tsquery($3)' end);

            _sql := system.build_filter_sql(
                _sql,
                _content_type,
                _where,
                in_order_by,
                in_order_direction);

            execute _sql into _items using in_limit, in_skip, in_fts_qry;
        end if;

        return _items;
    end;
    $$;

    create function management.filter_management_users(in_token uuid, in_skip bigint, in_limit bigint, in_order_by text default 'id', in_order_direction smallint default 0, in_fts_qry text default null)
        returns security.t_user[]
        language plpgsql
        cost 100
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _items security.t_user[];
        _content_type text := 'security.t_user';
        _sql text := 'select %s from security.user inner join security.user_scope on (id_user = id)';
        _type_select text := format('(id, email, first_name, last_name, null)::%s', _content_type);
        _where text;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then

            _where := concat_ws(' AND ',
                case when in_fts_qry is not null then 'tsv @@ websearch_to_tsquery($3)' end,
                'id_scope = $4');

            _sql := system.build_filter_sql(
                _sql,
                _content_type,
                _where,
                in_order_by,
                in_order_direction,
                _type_select);

            raise notice '%s', _sql;

            execute _sql into _items using in_limit, in_skip, in_fts_qry, constants.scope_manager();
        end if;

        return _items;
    end;
    $$;

    create function management.filter_organization_users(in_token uuid, in_id_organization bigint, in_skip bigint, in_limit bigint, in_order_by text default 'id', in_order_direction smallint default 0, in_fts_qry text default null)
        returns security.t_user[]
        language plpgsql
        cost 100
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _items security.t_user[];
        _content_type text := 'security.t_user';
        _sql text := 'select %s from security.user as u inner join clients.organization_user as ou on (id_user = u.id)';
        _type_select text := format('(u.id, u.email, ou.first_name, ou.last_name, null)::%s', _content_type);
        _where text;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then

            _where := concat_ws(' AND ',
                case when in_fts_qry is not null then 'ou.tsv @@ websearch_to_tsquery($3)' end,
                'id_organization = $4');

            _sql := system.build_filter_sql(
                _sql,
                _content_type,
                _where,
                in_order_by,
                in_order_direction,
                _type_select);

            --raise notice '%s', _sql;

            execute _sql into _items using in_limit, in_skip, in_fts_qry, in_id_organization;
        end if;

        return _items;
    end;
    $$;

    -- Set scopes
    insert into security.scope(id, title, description)
    values(constants.scope_manager(), 'Menedžeris', 'Piekļūst /manage sadaļai');

    insert into security.scope(id, title, description)
    values(constants.scope_client(), 'Klients', 'Piekļūst klientu sadaļai');

    -- Test user
    insert into security."user"(email, first_name, last_name, password_hash)
    values('admin@example.com', 'AdminName', 'AdminSurname', crypt('admin', gen_salt('md5')));


    insert into security.user_scope(id_user, id_scope) VALUES (1, constants.scope_manager());

COMMIT;