-- Deploy integrator:fix_delete_fsc to pg
-- requires: functions

BEGIN;

    create or  replace function clients.delete_foreign_system_connection(
            in_token uuid,
            in_id bigint)
        returns boolean
        language plpgsql
        cost 1000
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _work_ids bigint[];
        _id_work bigint;
        _id_work_server bigint;
        _first_work_task_id bigint;
        _id_run_period text;
        _minute smallint;
        _hour smallint;
        _week_day smallint;
        _month_day smallint;
        _month smallint;
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            -- get all involved works
            select array_agg(w.id) into _work_ids
            from clients.work_task as wt inner join clients.work as w on wt.id_work = w.id
            where wt.id_foreign_system_connection = in_id;

            -- disable all involved works
            foreach _id_work in array _work_ids
            loop
                update clients.work set disabled = true where id = _id_work;
            end loop;

            delete from clients.foreign_system_connection where id = in_id and id_organization = _id_organization;

            -- delete works with no work tasks lefts
            foreach _id_work in array _work_ids
            loop
                perform id from clients.work_task where id_work = _id_work order by sequence_number asc limit 1;
                if not found then
                    delete from clients.work where id = _id_work;
                else
                    -- set new job if job is deleted
                    perform id from clients.job where id_work = _id_work and latest;
                    if not found then
                        select id into _first_work_task_id from clients.work_task where id_work = _id_work order by sequence_number asc limit 1;

                        select id_run_period, minute, hour, week_day, month_day, month
                        into _id_run_period, _minute, _hour, _week_day, _month_day, _month
                        from clients.work where id = _id_work;

                        -- get random available work server id
                        _id_work_server := clients.get_random_work_server_id();

                        insert into clients.job(
                                                id_work_server,
                                                id_work,
                                                id_current_work_task,
                                                execute_at,
                                                started_at,
                                                completed_at,
                                                running,
                                                cancel_requested_at,
                                                canceled_at,
                                                execution_error_message,
                                                latest)
                        values (
                                _id_work_server,
                                _id_work,
                                _first_work_task_id,
                                clients.calculate_work_next_runtime(_id_run_period, _minute, _hour, _week_day, _month_day, _month),
                                null,
                                null,
                                false,
                                null,
                                null,
                                null,
                                true
                                );
                    end if;
                end if;
            end loop;

            return found;
        end if;

        return false;
    end;
    $$;

COMMIT;
