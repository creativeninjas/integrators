-- Deploy integrator:cancel_job_with_error to pg

BEGIN;

    alter table clients.work add column max_retry_attempts smallint default 3 check ( max_retry_attempts > 0 and max_retry_attempts < 21);
    alter table clients.work add column retry_attempts smallint not null default 1;
    alter table clients.work add column retry_interval_minutes smallint default 5 check ( retry_interval_minutes > 0 and retry_interval_minutes < 61 );
    alter table clients.work add column retry_on_false boolean default false;

    alter type clients.t_work add attribute max_retry_attempts smallint;
    alter type clients.t_work add attribute retry_attempts smallint;
    alter type clients.t_work add attribute retry_interval_minutes smallint;
    alter type clients.t_work add attribute retry_on_false boolean;

    create or replace function clients.work(in_token uuid, in_id bigint)
        returns clients.t_work
        language plpgsql
        cost 1000
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _item clients.t_work := null;
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            select w.id,
                   id_organization,
                   (select (id, title, frequency)::management.t_run_period from management.run_period where id = w.id_run_period),
                   w.title,
                   w.disabled,
                   w.repeat,
                   w.minute,
                   w.hour,
                   w.week_day,
                   w.month_day,
                   w.month,
                   w.max_retry_attempts,
                   w.retry_attempts,
                   w.retry_interval_minutes,
                   w.retry_on_false
            into _item
            from clients.work as w
            where w.id = in_id and w.id_organization = _id_organization;
        end if;

        return _item;
    end;
    $$;

    create or replace function clients.update_work(in_token uuid, in_id_work bigint, in_title text, in_disabled boolean, in_repeat boolean, in_id_run_period text, in_minute smallint, in_hour smallint, in_week_day smallint, in_month_day smallint, in_month smallint, in_max_retry_attempts smallint, in_retry_interval_minutes smallint, in_retry_on_false boolean, in_tasks clients.t_work_task[])
        returns clients.t_work_and_job
        language plpgsql
        cost 1000
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _item clients.t_work_and_job := null;
        _id_organization bigint := clients.session_organization_id(in_token);
        _work_task clients.t_work_task;
        _sequence_number int := 0;
        _work_task_id bigint;
        _first_work_task_id bigint := null;
        _id_work_server bigint;
        _fsc clients.t_foreign_system_connection;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            update
                clients.work
            set
                id_run_period = in_id_run_period,
                title = in_title,
                disabled = in_disabled,
                repeat = in_repeat,
                minute = in_minute,
                hour = in_hour,
                week_day = in_week_day,
                month_day = in_month_day,
                max_retry_attempts = in_max_retry_attempts,
                retry_interval_minutes = in_retry_interval_minutes,
                retry_on_false = in_retry_on_false,
                month = in_month where id = in_id_work and id_organization = _id_organization;

            if found then
                -- remove existing jobs
                delete from clients.job where id_work = in_id_work;
                delete from clients.work_task where id_work = in_id_work;

                -- add new work tasks
                foreach _work_task in array in_tasks
                loop
                    _fsc := _work_task.foreign_system_connection;
                    _sequence_number := _sequence_number + 1;
                    insert into clients.work_task(id_work, id_task, id_foreign_system_connection, sequence_number, attributes)
                    values (in_id_work, _work_task.task_code::bigint, _fsc.id, _sequence_number, _work_task.attributes) returning id into _work_task_id;

                    if _first_work_task_id is null then
                        _first_work_task_id := _work_task_id;
                    end if;
                end loop;

                -- get random available work server id
                _id_work_server := clients.get_random_work_server_id();

                insert into clients.job(
                                        id_work_server,
                                        id_work,
                                        id_current_work_task,
                                        execute_at,
                                        started_at,
                                        completed_at,
                                        running,
                                        cancel_requested_at,
                                        canceled_at,
                                        execution_error_message,
                                        latest)
                values (
                        _id_work_server,
                        in_id_work,
                        _first_work_task_id,
                        clients.calculate_work_next_runtime(in_id_run_period, in_minute, in_hour, in_week_day, in_month_day, in_month),
                        null,
                        null,
                        false,
                        null,
                        null,
                        null,
                        true
                        );


                _item := clients.work_and_job(in_token, in_id_work);
            end if;
        end if;

        return _item;
    end;
    $$;

    create or replace function management.cancel_job_with_error(
            in_token uuid,
            in_id_job bigint,
            in_error text
            )
        returns bool
        language plpgsql
        cost 1000
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _id_work bigint;
        _max_retry_attempts smallint;
        _retry_on_false boolean;
        _retry_attempts smallint;
        _retry_interval_minutes smallint;
        _repeat boolean;

        _hour smallint;
        _week_day smallint;
        _month_day smallint;
        _month smallint;
        _id_run_period text;

        _current_task_id bigint;
        _next_task_id bigint;

    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            select id_work into _id_work from clients.job where id = in_id_job;
            select retry_on_false, retry_attempts, retry_interval_minutes into _retry_on_false, _retry_attempts, _retry_interval_minutes from clients.work where id = _id_work;

            if _retry_on_false is true then
                -- Add work retry attempts
                select id_work into _id_work from clients.job where id = in_id_job;
                select max_retry_attempts into _max_retry_attempts from clients.work where id = _id_work;
                update clients.work set retry_attempts = retry_attempts + 1 where id = _id_work;

                -- Repeat job
                update clients.job set running = true, started_at = now() where id = in_id_job;
                _next_task_id := _current_task_id;

                select repeat into _repeat from clients.work where id = _id_work;
                update clients.work set repeat = true where id = _id_work;

                update clients.job set latest = null where id = in_id_job;

                select id into _current_task_id from clients.work_task where id_work = _id_work order by sequence_number asc limit 1;
                select
                    id_run_period,  retry_interval_minutes
                into
                    _id_run_period, _retry_interval_minutes
                from clients.work where id = _id_work;

                insert into clients.job(id_work_server, id_work, id_current_work_task, execute_at, started_at, completed_at, running, cancel_requested_at, canceled_at, execution_error_message, latest)
                values (clients.get_random_work_server_id(),
                    _id_work,
                    _current_task_id,
                    clients.calculate_work_next_runtime(_id_run_period, _retry_interval_minutes, _hour, _week_day, _month_day, _month),
                    null, null, false, null, null, null, true
                    );

                if _retry_attempts >= _max_retry_attempts then
                    -- Disable work if no retry attempts left, set retry attempts to 1 and retry_on_false to true
                    select id_work into _id_work from clients.job where id = in_id_job;
                    update clients.work set disabled = true where id = _id_work;
                    update clients.work set retry_on_false = true where id = _id_work;
                    update clients.work set retry_attempts = 1 where id = _id_work;

                end if;

            else
                 -- Disable work on error
                select id_work into _id_work from clients.job where id = in_id_job;
                update clients.work set disabled = true where id = _id_work;

            end if;

            -- set latest job as completed and canceled and also set error message, reset retry attempts
            update clients.job
            set execution_error_message = in_error,
                running = false,
                completed_at = now(),
                canceled_at = now()
            where id = in_id_job;

            return found;
        end if;

        return false;
    end;
    $$;

    create or replace function management.get_job_next_task(
            in_token uuid,
            in_id_job bigint)
        returns clients.t_work_task
        language plpgsql
        cost 1000
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _running boolean;
        _id_work bigint;
        _next_task_id bigint;
        _current_task_id bigint;
        _current_task_sequence int;
        _work_task clients.t_work_task;

        _repeat boolean;
        _minute smallint;
        _hour smallint;
        _week_day smallint;
        _month_day smallint;
        _month smallint;
        _id_run_period text;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            select id_work, running into _id_work, _running from clients.job where id = in_id_job;

            select id_current_work_task into _current_task_id from clients.job where id = in_id_job;
            if not _running then
                update clients.job set running = true, started_at = now() where id = in_id_job;
                _next_task_id := _current_task_id;
            else
                select sequence_number into _current_task_sequence from clients.work_task where id = _current_task_id;
                select id into _next_task_id from clients.work_task where id_work = _id_work and sequence_number > _current_task_sequence limit 1;

                if _next_task_id is null then
                    update clients.job set running = false, completed_at = now() where id = in_id_job;

                    -- Set retry_attempts to 1
                    update clients.work set retry_attempts =  1 where id = _id_work;

                    -- set next job if task is repeated
                    select repeat into _repeat from clients.work where id = _id_work;
                    if _repeat then
                        update clients.job set latest = null where id = in_id_job;

                        select id into _current_task_id from clients.work_task where id_work = _id_work order by sequence_number asc limit 1;
                        select
                               id_run_period, minute, hour, week_day, month_day, month
                        into
                               _id_run_period, _minute, _hour, _week_day, _month_day, _month
                        from clients.work where id = _id_work;

                        insert into clients.job(id_work_server, id_work, id_current_work_task, execute_at, started_at, completed_at, running, cancel_requested_at, canceled_at, execution_error_message, latest)
                        values (clients.get_random_work_server_id(),
                                _id_work,
                                _current_task_id,
                                clients.calculate_work_next_runtime(_id_run_period, _minute, _hour, _week_day, _month_day, _month),
                                null, null, false, null, null, null, true
                                );
                    end if;

                    return null;
                else
                    update clients.job set id_current_work_task = _next_task_id where id = in_id_job;
                end if;
            end if;

            select
                   wt.id, wt.id_work, t.code,
                   (fsc.id,
                    fsc.id_foreign_system,
                    fsc.id_organization,
                    fsc.title,
                    fsc.username,
                    fsc.pwd,
                    fsc.address,
                    fsc.available,
                    fsc.unavailability_error,
                    fsc.created_at)::clients.t_foreign_system_connection,
                   wt.sequence_number, wt.attributes into _work_task
            from clients.work_task as wt
            inner join management.task as t on (wt.id_task = t.id)
            inner join clients.foreign_system_connection as fsc on (wt.id_foreign_system_connection = fsc.id)
            where wt.id = _next_task_id;

            -- Increase execution times
            update management.task set times_executed = times_executed + 1 where code = _work_task.task_code;

            return _work_task;

        end if;

        return null;
    end;
    $$;

COMMIT;
