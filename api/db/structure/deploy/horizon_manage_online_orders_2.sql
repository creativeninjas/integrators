-- Deploy integrator:horizon_manage_online_orders_2 to pg

BEGIN;

    create or replace function management.import_online_order(
            in_token uuid,
            in_id_fsc bigint,
            in_source_id text,
            in_short_number text,
            in_long_number text,
            in_buyer_number text,
            in_buyer_vat_number text,
            in_buyer_title text,
            in_buyer_address text,
            in_buyer_email text,
            in_buyer_extra_information text,
            in_document json,
            in_products json
            )
        returns bigint
        language plpgsql
        cost 1000
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _id_online_order bigint;
        _checksum character varying(64) := system.gen_checksum(in_short_number, in_long_number, in_buyer_title);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then

            perform id from clients.online_order where checksum = _checksum and id_fsc = in_id_fsc;
            if not found then
                insert into clients.online_order(id_fsc, source_id, short_number, long_number, buyer_number, buyer_vat_number, buyer_title, buyer_address, buyer_email, buyer_extra_information, products, document, checksum)
                values(
                       in_id_fsc,
                       in_source_id,
                       in_short_number,
                       in_long_number,
                       in_buyer_number,
                       in_buyer_vat_number,
                       in_buyer_title,
                       in_buyer_address,
                       in_buyer_email,
                       in_buyer_extra_information,
                       in_products,
                       in_document,
                       _checksum) returning id into _id_online_order;

                return _id_online_order;
            end if;

            return null;
        end if;

        return null;
    end;
    $$;

COMMIT;
