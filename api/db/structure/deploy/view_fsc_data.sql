-- Deploy integrator:view_fsc_data to pg
-- requires: view_fiscof

BEGIN;

    create type clients.t_data_version as (
        id         bigint,
        created_at timestamp
    );

    create type clients.t_unit_version as(
        id          bigint,
        id_unit     bigint,
        short_title character varying(10),
        title       character varying(40),
        created_at  timestamp,
        checksum    character(64)
    );

    create type clients.t_tax_category_version as(
        id                      bigint,
        id_tax_category         bigint,
        code                    character(1),
        rate                    numeric,
        non_taxable             boolean,
        title                   text,
        created_at              timestamp,
        checksum                character(64),
        product_tax_categories  clients.t_product_tax_category[]
    );

    create type clients.t_product_version as(
        id                    bigint,
        id_product            bigint,
        product_tax_category  clients.t_product_tax_category,
        unit                  clients.t_unit,
        title                 character varying(250),
        print_title           character varying(40),
        short_code            character varying(50),
        price                 numeric,
        barcodes              clients.t_barcode[],
        qrcode                text,
        description           text,
        combo                 boolean,
        created_at            timestamp,
        checksum              character(64),
        combo_products        clients.t_product[]
    );

    create function clients.unit_versions(in_token uuid, in_id_unit bigint)
        returns clients.t_data_version[]
        language plpgsql
        cost 1000
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _items clients.t_data_version[];
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            select array_agg(data.i) into _items from (
                select (uv.id, uv.created_at)::clients.t_data_version as i from clients.unit as u
                inner join clients.unit_version as uv on (u.id = uv.id_unit)
                inner join clients.foreign_system_connection as fsc on (u.id_fsc = fsc.id)
                where u.id = in_id_unit and fsc.id_organization = _id_organization
                order by uv.created_at desc
            ) as data;

        end if;

        return _items;
    end;
    $$;

    create function clients.filter_units(in_token uuid, in_id_fsc bigint, in_skip bigint, in_limit bigint, in_order_by text default 'id', in_order_direction smallint default 0, in_fts_qry text default null)
        returns clients.t_unit[]
        language plpgsql
        cost 1000
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _items clients.t_unit[];
        _content_type text := 'clients.t_unit';
        _sql text := 'select %s from clients.unit as u inner join clients.foreign_system_connection as fsc on (u.id_fsc = fsc.id)';
        _type_select text := format(
            '(u.id, u.id_fsc, u.source_id, u.short_title, u.title, u.created_at, u.checksum)::%s', _content_type);
        _where text;
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then

            _where := concat_ws(' AND ',
                case when in_fts_qry is not null then 'u.tsv @@ websearch_to_tsquery($3)' end,
                case when in_id_fsc is not null then 'u.id_fsc = $4' end,
                'fsc.id_organization = $5');

            _sql := system.build_filter_sql(
                _sql,
                _content_type,
                _where,
                in_order_by,
                in_order_direction,
                _type_select,
                'u');

            execute _sql into _items using in_limit, in_skip, in_fts_qry, in_id_fsc, _id_organization;
        end if;

        return _items;
    end;
    $$;

    create function clients.unit(in_token uuid, in_id bigint)
        returns clients.t_unit
        language plpgsql
        cost 1000
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _item clients.t_unit := null;
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            select u.id,
                   u.id_fsc,
                   u.source_id,
                   u.short_title,
                   u.title,
                   u.created_at,
                   u.checksum into _item
            from clients.unit as u
            inner join clients.foreign_system_connection as fsc on (u.id_fsc = fsc.id)
            where u.id = in_id and fsc.id_organization = _id_organization;
        end if;

        return _item;
    end;
    $$;

    create function clients.unit_version(in_token uuid, in_id bigint)
        returns clients.t_unit_version
        language plpgsql
        cost 1000
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _item clients.t_unit_version := null;
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            select uv.id,
                   uv.id_unit,
                   uv.short_title,
                   uv.title,
                   uv.created_at,
                   uv.checksum into _item
            from clients.unit_version as uv
            inner join clients.unit as u on (uv.id_unit = u.id)
            inner join clients.foreign_system_connection as fsc on (u.id_fsc = fsc.id)
            where uv.id = in_id and fsc.id_organization = _id_organization;
        end if;

        return _item;
    end;
    $$;

    -- tax category

    create function clients.filter_tax_categories(in_token uuid, in_id_fsc bigint, in_skip bigint, in_limit bigint, in_order_by text default 'id', in_order_direction smallint default 0, in_fts_qry text default null)
        returns clients.t_tax_category[]
        language plpgsql
        cost 1000
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _items clients.t_tax_category[];
        _content_type text := 'clients.t_tax_category';
        _sql text := 'select %s from clients.tax_category as tc inner join clients.foreign_system_connection as fsc on (tc.id_fsc = fsc.id)';
        _type_select text := format(
            '(tc.id, tc.id_fsc, tc.source_id, tc.code, tc.rate, tc.non_taxable, tc.title, tc.created_at, tc.checksum, null)::%s', _content_type);
        _where text;
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then

            _where := concat_ws(' AND ',
                case when in_fts_qry is not null then 'tc.tsv @@ websearch_to_tsquery($3)' end,
                case when in_id_fsc is not null then 'tc.id_fsc = $4' end,
                'fsc.id_organization = $5');

            _sql := system.build_filter_sql(
                _sql,
                _content_type,
                _where,
                in_order_by,
                in_order_direction,
                _type_select,
                'tc');

            execute _sql into _items using in_limit, in_skip, in_fts_qry, in_id_fsc, _id_organization;
        end if;

        return _items;
    end;
    $$;

    create function clients.tax_category(in_token uuid, in_id bigint)
        returns clients.t_tax_category
        language plpgsql
        cost 1000
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _item clients.t_tax_category := null;
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            select tc.id,
                   tc.id_fsc,
                   tc.source_id,
                   tc.code,
                   tc.rate,
                   tc.non_taxable,
                   tc.title,
                   tc.created_at,
                   tc.checksum,
                   (select array_agg((
                       (id, id_tax_category,source_id, title, created_at, checksum)::clients.t_product_tax_category
                       )) from clients.product_tax_category where id_tax_category = tc.id
                   )::clients.t_product_tax_category[]
                   into _item
            from clients.tax_category as tc
            inner join clients.foreign_system_connection as fsc on (tc.id_fsc = fsc.id)
            where tc.id = in_id and fsc.id_organization = _id_organization;
        end if;

        return _item;
    end;
    $$;

    create function clients.tax_category_version(in_token uuid, in_id bigint)
        returns clients.t_tax_category_version
        language plpgsql
        cost 1000
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _item clients.t_tax_category_version := null;
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            select tcv.id,
                   tcv.id_tax_category,
                   tcv.code,
                   tcv.rate,
                   tcv.non_taxable,
                   tcv.title,
                   tcv.created_at,
                   tcv.checksum,
                   (select array_agg((
                       (id, id_tax_category,source_id, title, created_at, checksum)::clients.t_product_tax_category
                       )) from clients.product_tax_category where id_tax_category = tcv.id_tax_category
                   )::clients.t_product_tax_category[]
                   into _item
            from clients.tax_category_version as tcv
            inner join clients.tax_category as tc on (tcv.id_tax_category = tc.id)
            inner join clients.foreign_system_connection as fsc on (tc.id_fsc = fsc.id)
            where tcv.id = in_id and fsc.id_organization = _id_organization;
        end if;

        return _item;
    end;
    $$;

    create function clients.tax_category_versions(in_token uuid, in_id_unit bigint)
        returns clients.t_data_version[]
        language plpgsql
        cost 1000
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _items clients.t_data_version[];
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            select array_agg(data.i) into _items from (
                select (tcv.id, tcv.created_at)::clients.t_data_version as i from clients.tax_category as tc
                inner join clients.tax_category_version as tcv on (tc.id = tcv.id_tax_category)
                inner join clients.foreign_system_connection as fsc on (tc.id_fsc = fsc.id)
                where tc.id = in_id_unit and fsc.id_organization = _id_organization
                order by tcv.created_at desc
            ) as data;

        end if;

        return _items;
    end;
    $$;

    -- Products
    create function clients.product_versions(in_token uuid, in_id_product bigint)
        returns clients.t_data_version[]
        language plpgsql
        cost 1000
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _items clients.t_data_version[];
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            select array_agg(data.i) into _items from (
                select (pv.id, pv.created_at)::clients.t_data_version as i from clients.product as p
                inner join clients.product_version as pv on (p.id = pv.id_product)
                inner join clients.foreign_system_connection as fsc on (p.id_fsc = fsc.id)
                where p.id = in_id_product and fsc.id_organization = _id_organization
                order by pv.created_at desc
            ) as data;

        end if;

        return _items;
    end;
    $$;

    create function clients.filter_products(in_token uuid, in_id_fsc bigint, in_skip bigint, in_limit bigint, in_order_by text default 'id', in_order_direction smallint default 0, in_fts_qry text default null, in_is_combo boolean default null)
        returns clients.t_product[]
        language plpgsql
        cost 1000
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _items clients.t_product[];
        _content_type text := 'clients.t_product';
        _sql text := 'select %s from clients.product as p inner join clients.foreign_system_connection as fsc on (p.id_fsc = fsc.id)';
        _type_select text := format('(p.id, ' ||
                                    '(select (id, id_tax_category, source_id, title, created_at, checksum)::clients.t_product_tax_category from clients.product_tax_category where id = p.id_product_tax_category), ' ||
                                    'p.id_fsc, ' ||
                                    '(select (id, id_fsc, source_id, short_title, title, created_at, checksum)::clients.t_unit from clients.unit where id = p.id_unit), ' ||
                                    'p.source_id, ' ||
                                    'p.title, ' ||
                                    'p.print_title, ' ||
                                    'p.short_code, ' ||
                                    'p.price, ' ||
                                    'p.barcodes, ' ||
                                    'p.qrcode, ' ||
                                    'p.description, ' ||
                                    'p.combo, ' ||
                                    'p.created_at, ' ||
                                    'p.checksum)::%s', _content_type);
        _where text;
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then

            _where := concat_ws(' AND ',
                case when in_fts_qry is not null then 'p.tsv @@ websearch_to_tsquery($3)' end,
                case when in_id_fsc is not null then 'p.id_fsc = $4' end,
                'fsc.id_organization = $5',
                case when in_is_combo is not null then 'p.combo = $6' end);

            _sql := system.build_filter_sql(
                _sql,
                _content_type,
                _where,
                in_order_by,
                in_order_direction,
                _type_select,
                'p');

            execute _sql into _items using in_limit, in_skip, in_fts_qry, in_id_fsc, _id_organization, in_is_combo;
        end if;

        return _items;
    end;
    $$;

    create function clients.combo_products(in_token uuid, in_id_fsc bigint, in_id_combo bigint)
        returns clients.t_product[]
        language plpgsql
        cost 1000
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _items clients.t_product[];
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            select array_agg(
                (cp.id,
                 (select (id, id_tax_category, source_id, title, created_at, checksum)::clients.t_product_tax_category from clients.product_tax_category where id = cp.id_product_tax_category),
                 cp.id_fsc,
                 (select (id, id_fsc, source_id, short_title, title, created_at, checksum)::clients.t_unit from clients.unit where id = cp.id_unit),
                 cp.source_id,
                 cp.title,
                 cp.print_title,
                 cp.short_code,
                 cp.price,
                 cp.barcodes,
                 cp.qrcode,
                 cp.description,
                 cp.combo,
                 cp.created_at,
                 cp.checksum
                )::clients.t_product) into _items from clients.combo_product
            inner join clients.product as cp on (combo_product.id_combo = cp.id)
            inner join clients.foreign_system_connection as fsc on (cp.id_fsc = fsc.id)
            where combo_product.id_combo = in_id_combo and id_organization = _id_organization and fsc.id = in_id_fsc;

        end if;

        return _items;
    end;
    $$;

    create function clients.product(in_token uuid, in_id bigint)
        returns clients.t_product
        language plpgsql
        cost 1000
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _item clients.t_product := null;
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            select p.id,
                   (select (id, id_tax_category, source_id, title, created_at, checksum)::clients.t_product_tax_category from clients.product_tax_category where id = p.id_product_tax_category),
                   p.id_fsc,
                   (select (id, id_fsc, source_id, short_title, title, created_at, checksum)::clients.t_unit from clients.unit where id = p.id_unit),
                   p.source_id,
                   p.title,
                   p.print_title,
                   p.short_code,
                   p.price,
                   p.barcodes,
                   p.qrcode,
                   p.description,
                   p.combo,
                   p.created_at,
                   p.checksum
            into _item
            from clients.product as p
            inner join clients.foreign_system_connection as fsc on (p.id_fsc = fsc.id)
            where p.id = in_id and fsc.id_organization = _id_organization;
        end if;

        return _item;
    end;
    $$;

    create function clients.product_version(in_token uuid, in_id bigint)
        returns clients.t_product_version
        language plpgsql
        cost 1000
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _item clients.t_product_version := null;
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            select pv.id,
                   pv.id_product,
                   pv.id_product_tax_category,
                   pv.id_unit,
                   pv.title,
                   pv.print_title,
                   pv.short_code,
                   pv.price,
                   pv.barcodes,
                   pv.qrcode,
                   pv.description,
                   pv.combo,
                   pv.created_at,
                   pv.checksum,
                   pv.combo_products
                into _item
            from clients.product_version as pv
            inner join clients.product as p on (pv.id_product = p.id)
            inner join clients.foreign_system_connection as fsc on (p.id_fsc = fsc.id)
            where pv.id = in_id and fsc.id_organization = _id_organization;
        end if;

        return _item;
    end;
    $$;


COMMIT;
