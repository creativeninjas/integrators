-- Deploy integrator:fix_gen_checksum to pg
-- requires: functions

BEGIN;

    create or replace function system.gen_checksum(VARIADIC in_str text[])
        returns character varying
        immutable
        parallel safe
        cost 1000
        language plpgsql
    as $$
    declare
        word text;
        full_str text := '';
    begin
        foreach word in array in_str
        loop
            full_str := full_str || coalesce(word, '');
        end loop;

        return encode(sha256(quote_literal(full_str)::bytea), 'hex');
    end;
    $$;



COMMIT;
