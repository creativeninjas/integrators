-- Deploy integrator:reset_pwd to pg
-- requires: functions

BEGIN;

    create table security.pwd_reset(
        id_user bigint,
        reset_code text not null,
        created_at timestamp default now(),

        primary key(id_user),
	    constraint fk_pwd_reset_user
           foreign key(id_user) references security.user(id) on delete cascade on update cascade
    );

    create function security.reset_pwd(in_reset_code text, in_new_pwd text)
        returns boolean
        language plpgsql
        cost 1000
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
    begin
        select id_user into _id_user from security.pwd_reset where reset_code = in_reset_code;

        if _id_user is null then
            return false;
        else
            delete from security.pwd_reset where reset_code = in_reset_code;

            update security.user set password_hash = crypt(in_new_pwd, gen_salt('md5')) where id = _id_user;

            return found;
        end if;
    end;
    $$;

    create function security.set_pwd_reset(in_email text)
        returns text
        language plpgsql
        cost 1000
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
        _reset_code text;
    begin
        select id into _id_user from security.user where email = in_email;

        if _id_user is null then
            return null;
        else
            -- Remove existing user reset code
            delete from security.pwd_reset where id_user = _id_user;

            _reset_code := system.gen_password(7::smallint, 5::smallint);
            insert into security.pwd_reset(id_user, reset_code) values(_id_user, _reset_code);
            return _reset_code;
        end if;
    end;
    $$;


    create function security.can_reset(in_reset_code text)
        returns boolean
        language plpgsql
        cost 1000
        volatile parallel safe
    as $$
    declare
        _id_user bigint;
    begin
        select id_user into _id_user from security.pwd_reset where reset_code = in_reset_code;

        if _id_user is null then
            return false;
        else
            return true;
        end if;
    end;
    $$;

COMMIT;
