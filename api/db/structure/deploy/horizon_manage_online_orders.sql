-- Deploy integrator:horizon_manage_online_orders to pg
-- requires: fix_delete_fsc_v2

BEGIN;
    insert into management.task(id_foreign_system, title, code, available, description, id_min_run_period)
    values ('horizon', 'Izgūt tiešsaistes pasūtījumus', 'horizon_export_online_order', true, 'No Horizon izgūst tiešsaistes pasūtījumus (avansa rēķiniar nepiesaistītu summu lielāku par 0.009)', 'minute');

    insert into management.task(id_foreign_system, title, code, available, description, id_min_run_period)
    values ('horizon', 'Apstiprināt tiešsaistes pasūtījumu', 'horizon_import_online_order', true, 'Izveido fiskālo čēku un piesaista to avansa rēķinam', 'minute');

    insert into management.task(id_foreign_system, title, code, available, description, id_min_run_period)
    values ('tiki-taka', 'Veidot tiešsaistes pasūtījumus', 'tiki_taka_import_online_order', true, 'Izveido tiešsaistes pasūtījumu Tiki Taka Manage', 'minute');

    insert into management.task(id_foreign_system, title, code, available, description, id_min_run_period)
    values ('tiki-taka', 'Izgūt tiešsaistes pasūtījumu apmaksas statusu', 'tiki_taka_export_online_order_status', true, 'Izgūt tiešsaistes pasūtījumu statusu', 'minute');

    alter table clients.online_order add column document jsonb not null;
    alter table clients.online_order drop column products;
    alter table clients.online_order add column products json not null;

    alter type clients.t_online_order add attribute document jsonb;
    alter type clients.t_online_order alter attribute products type json;

    alter type management.t_integrated_online_order add attribute document jsonb;
    alter type management.t_integrated_online_order alter attribute products type json;

    create index idx_not_completed_online_order on clients.online_order ((not completed));
    create index idx_completed_online_order on clients.online_order ((completed));


    create function management.import_online_order(
            in_token uuid,
            in_id_fsc bigint,
            in_source_id text,
            in_short_number text,
            in_long_number text,
            in_buyer_title text,
            in_document json,
            in_products json
            )
        returns bigint
        language plpgsql
        cost 1000
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
        _id_online_order bigint;
        _checksum character varying(64) := system.gen_checksum(in_short_number, in_long_number, in_buyer_title);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then

            perform id from clients.online_order where checksum = _checksum and id_fsc = in_id_fsc;
            if not found then
                insert into clients.online_order(id_fsc, source_id, short_number, long_number, buyer_title, products, document, checksum)
                values(
                       in_id_fsc,
                       in_source_id,
                       in_short_number,
                       in_long_number,
                       in_buyer_title,
                       in_products,
                       in_document,
                       _checksum) returning id into _id_online_order;

                return _id_online_order;
            end if;

            return null;
        end if;

        return null;
    end;
    $$;

    create function clients.filter_online_orders(in_token uuid, in_id_fsc bigint, in_skip bigint, in_limit bigint, in_order_by text DEFAULT 'id'::text, in_order_direction smallint DEFAULT 0, in_fts_qry text DEFAULT NULL::text)
        returns clients.t_online_order[]
        parallel safe
        cost 1000
        language plpgsql
    as
    $$
    declare
        _id_user bigint;
        _items clients.t_online_order[];
        _content_type text := 'clients.t_online_order';
        _sql text := 'select %s from clients.online_order as o inner join clients.foreign_system_connection as fsc on (o.id_fsc = fsc.id)';
        _type_select text := format(
            '(o.id, o.id_fsc, o.source_id, o.short_number, o.long_number, o.client, o.address, o.comment, o.buyer_number, o.buyer_vat_number, o.buyer_title, o.buyer_address, o.buyer_email, o.buyer_extra_information, o.products, o.created_at, o.completed_at, o.completed, o.document)::%s', _content_type);
        _where text;
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            _where := concat_ws(' AND ',
                case when in_fts_qry is not null then 'o.tsv @@ websearch_to_tsquery($3)' end,
                case when in_id_fsc is not null then 'o.id_fsc = $4' end,
                'fsc.id_organization = $5');

            _sql := system.build_filter_sql(
                _sql,
                _content_type,
                _where,
                in_order_by,
                in_order_direction,
                _type_select,
                'o');

            execute _sql into _items using in_limit, in_skip, in_fts_qry, in_id_fsc, _id_organization;
        end if;

        return _items;
    end;
    $$;

    create function clients.online_order(in_token uuid, in_id bigint)
        returns clients.t_online_order
        parallel safe
        cost 1000
        language plpgsql
    as $$
    declare
        _id_user bigint;
        _item clients.t_online_order := null;
        _id_organization bigint := clients.session_organization_id(in_token);
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_client()]);

        if _id_user is not null then
            select
                   o.id,
                   o.id_fsc,
                   o.source_id,
                   o.short_number,
                   o.long_number,
                   o.client,
                   o.address,
                   o.comment,
                   o.buyer_number,
                   o.buyer_vat_number,
                   o.buyer_title,
                   o.buyer_address,
                   o.buyer_email,
                   o.buyer_extra_information,
                   o.products,
                   o.created_at,
                   o.completed_at,
                   o.completed,
                   o.document
            into _item
            from clients.online_order as o
            inner join clients.foreign_system_connection as fsc on (o.id_fsc = fsc.id)
            where o.id = in_id and fsc.id_organization = _id_organization;
        end if;

        return _item;
    end;
    $$;

    create function management.integrated_online_orders(in_token uuid, in_task_code text, in_id_fsc bigint)
        returns management.t_integrated_online_order[]
        cost 1000
        language plpgsql
    as $$
    declare
        _id_user bigint;
        _id_task bigint;
        _id_organization bigint;
        _items management.t_integrated_online_order[];
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            select id into _id_task from management.task where code = in_task_code;
            select id_organization into _id_organization from clients.foreign_system_connection where id = in_id_fsc;

            select
                array_agg(
                    (o.id,
                     io.target_id,
                     io.id_fsc,
                     io.id_task,
                     o.short_number,
                     o.long_number,
                     o.client,
                     o.address,
                     o.comment,
                     o.buyer_number,
                     o.buyer_vat_number,
                     o.buyer_title,
                     o.buyer_address,
                     o.buyer_email,
                     o.buyer_extra_information,
                     o.products,
                     o.completed_at,
                     o.completed,
                     o.created_at,
                     o.checksum,
                     io.attributes,
                     o.document)::management.t_integrated_online_order) into _items
            from clients.online_order as o
            inner join clients.foreign_system_connection as fsc on o.id_fsc = fsc.id
            left join clients.integrated_online_order as io on (o.id = io.id_online_order)
            where ((io.id_task = _id_task and io.id_fsc = in_id_fsc) or (io.id_task is null and io.id_fsc is null)) and id_organization = _id_organization;

            return _items;
        end if;

        return null;
    end;
    $$;

    create function management.save_integrated_online_order(in_token uuid, in_task_code text, in_id_online_order bigint, in_id_fsc bigint, in_target_id text, in_attributes jsonb)
        returns boolean
        cost 1000
        language plpgsql
    as $$
    declare
        _id_user bigint;
        _id_task bigint;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            select id into _id_task from management.task where code = in_task_code;

            perform from clients.integrated_online_order
            where id_online_order = in_id_online_order and target_id = in_target_id and id_fsc = in_id_fsc and id_task = _id_task;

            if found then
                update clients.integrated_online_order set attributes = in_attributes
                where id_online_order = in_id_online_order and target_id = in_target_id and id_fsc = in_id_fsc and id_task = _id_task;
            else
                insert into clients.integrated_online_order(id_online_order, target_id, id_fsc, id_task, attributes)
                values (in_id_online_order, in_target_id, in_id_fsc, _id_task, in_attributes);
            end if;

            return true;
        end if;

        return false;
    end;
    $$;

    create function management.not_completed_integrated_online_orders(in_token uuid, in_task_code text, in_id_fsc bigint)
        returns management.t_integrated_online_order[]
        cost 1000
        language plpgsql
    as $$
    declare
        _id_user bigint;
        _id_task bigint;
        _id_organization bigint;
        _items management.t_integrated_online_order[];
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            select id into _id_task from management.task where code = in_task_code;
            select id_organization into _id_organization from clients.foreign_system_connection where id = in_id_fsc;

            select
                array_agg(
                    (o.id,
                     io.target_id,
                     io.id_fsc,
                     io.id_task,
                     o.short_number,
                     o.long_number,
                     o.client,
                     o.address,
                     o.comment,
                     o.buyer_number,
                     o.buyer_vat_number,
                     o.buyer_title,
                     o.buyer_address,
                     o.buyer_email,
                     o.buyer_extra_information,
                     o.products,
                     o.completed_at,
                     o.completed,
                     o.created_at,
                     o.checksum,
                     io.attributes,
                     o.document)::management.t_integrated_online_order) into _items
            from clients.online_order as o
            inner join clients.foreign_system_connection as fsc on o.id_fsc = fsc.id
            inner join clients.integrated_online_order as io on (o.id = io.id_online_order)
            where io.id_task = _id_task and io.id_fsc = in_id_fsc and id_organization = _id_organization and not completed;

            return _items;
        end if;

        return null;
    end;
    $$;

    create function management.set_completed_online_order(in_token uuid, in_id bigint, in_completed_at timestamp, in_document json)
        returns bool
        language plpgsql
        cost 1000
        volatile parallel unsafe
    as $$
    declare
        _id_user bigint;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            update clients.online_order
            set document = in_document, completed = true, completed_at = in_completed_at
            where id = in_id;

            return found;
        end if;

        return false;
    end;
    $$;

    create function management.next_completed_non_integrated_online_order(in_token uuid, in_task_code text, in_id_fsc bigint)
        returns management.t_integrated_online_order
        cost 1000
        language plpgsql
    as $$
    declare
        _id_user bigint;
        _id_task bigint;
        _id_organization bigint;
        _item management.t_integrated_online_order;
    begin
        _id_user := security.have_permission(in_token,array[constants.scope_manager()]);

        if _id_user is not null then
            select id into _id_task from management.task where code = in_task_code;
            select id_organization into _id_organization from clients.foreign_system_connection where id = in_id_fsc;

            select
                   o.id,
                   io.target_id,
                   io.id_fsc,
                   io.id_task,
                   o.short_number,
                   o.long_number,
                   o.client,
                   o.address,
                   o.comment,
                   o.buyer_number,
                   o.buyer_vat_number,
                   o.buyer_title,
                   o.buyer_address,
                   o.buyer_email,
                   o.buyer_extra_information,
                   o.products,
                   o.completed_at,
                   o.completed,
                   o.created_at,
                   o.checksum,
                   io.attributes,
                   o.document into _item
            from clients.online_order as o
            inner join clients.foreign_system_connection as fsc on o.id_fsc = fsc.id
            left join clients.integrated_online_order as io on (o.id = io.id_online_order and id_task = _id_task)
            where io.id_task is null and io.id_fsc is null and id_organization = _id_organization and completed limit 1;

            if _item.id is null then
                return null;
            end if;

            return _item;
        end if;

        return null;
    end;
    $$;


COMMIT;
