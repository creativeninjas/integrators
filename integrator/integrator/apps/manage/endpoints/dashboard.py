from starlette.authentication import requires

from integrator.apps.manage import settings
from integrator.helpers import constants

from integrator.helpers.endpoints import BaseHTTPEndpoint


class Dashboard(BaseHTTPEndpoint):
    @property
    def api_url(self):
        return settings.API_URL

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'dashboard.pt'

    @requires(constants.scope_manager, redirect='login')
    async def get(self, request):
        filter_status = []

        for qs in request.query_params._list:
            if qs[0] == 'status':
                filter_status.append(qs[1])
        context = {
            'filter_documentation': {'level': filter_status},
            'filter_status': filter_status
        }
        return self.render(request, context)
