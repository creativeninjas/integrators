from starlette.authentication import requires
from starlette.requests import Request

from integrator.apps.manage import settings
from integrator.helpers import constants

from integrator.helpers.endpoints import BaseHTTPEndpoint, FormEndpoint, DeleteEndpoint, PostEndpoint


#<editor-fold desc="Organization functions">
class Organizations(BaseHTTPEndpoint):
    @property
    def api_url(self):
        return settings.API_URL

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'organizations.pt'

    @requires(constants.scope_manager, redirect='login')
    async def get(self, request: Request):
        query, page, limit, content, prev_page, next_page = await self.get_result_list(request, '/manage/organizations/')
        filter_status = []

        for qs in request.query_params._list:
            if qs[0] == 'status':
                filter_status.append(qs[1])

        context = {
            'content': content,
            'query': query,
            'page': page,
            'limit': limit,
            'prev_page': prev_page,
            'next_page': next_page,
            'snackbar': self.get_snackbar(request, 'action_successful'),
            'filter_documentation': {'level': filter_status},
            'filter_status': filter_status,
        }
        return self.render(request, context)


class AddOrganization(FormEndpoint):
    @property
    def method(self):
        return 'POST'

    @property
    def action_successful_message(self):
        return 'Organizācija pievienota'

    def redirect_url(self, request: Request, data: dict = None):
        return request.url_for('organizations')

    @property
    def api_url(self):
        return f'{settings.API_URL}/manage/organizations/'

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'organization_card.pt'

    async def get_context(self, request: Request):
        if request.method == 'POST':
            form_data = await request.form()
            content = {
                'id': form_data['id'],
                'title': form_data['title'],
                'contact_person': form_data['contact_person'],
                'contact_email': form_data['contact_email'],
                'contact_phone': form_data['contact_phone'],
            }
        else:
            content = {
                'id': 0, 'title': None, 'contact_person': None, 'contact_email': None, 'contact_phone': None}
        return {
            'content': content,
            'form_errors': None,
            'snackbar': self.get_snackbar(request, 'action_successful'),
            'form_type': 'ADD',
            'filter_documentation': {'level': self.get_documentation(request)}
        }


class UpdateOrganization(FormEndpoint):
    id_organization = None
    @property
    def method(self):
        return 'PUT'

    @property
    def action_successful_message(self):
        return 'Organizācija izmainīta'

    def redirect_url(self, request: Request, data: dict = None):
        return request.url_for('update-organization', id_organization=data['id'])

    @property
    def api_url(self):
        return f'{settings.API_URL}/manage/organizations/{self.id_organization}/'

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'organization_card.pt'

    async def get_context(self, request: Request):
        if request.method == 'POST':
            form_data = await request.form()
            content = {
                'id': form_data['id'],
                'title': form_data['title'],
                'contact_person': form_data['contact_person'],
                'contact_email': form_data['contact_email'],
                'contact_phone': form_data['contact_phone'],
            }
        else:
            content = await self.get_from_api(request, self.api_url)
        return {
            'content': content,
            'form_errors': None,
            'snackbar': self.get_snackbar(request, 'action_successful'),
            'form_type': 'UPDATE',
            'filter_documentation': {'level': self.get_documentation(request)}
        }

    @requires(constants.scope_manager, redirect='login')
    async def get(self, request: Request):
        self.id_organization = request.path_params['id_organization']
        return await super(UpdateOrganization, self).get(request)

    @requires(constants.scope_manager, redirect='login')
    async def post(self, request: Request):
        self.id_organization = request.path_params['id_organization']
        return await super(UpdateOrganization, self).post(request)


class DeleteOrganization(DeleteEndpoint):
    @property
    def tpl(self):
        return ''

    @property
    def action_successful_message(self):
        return 'Organizācija izdzēsta'

    def redirect_url(self, request: Request, result: bool):
        if result or self.list_action:
            return request.url_for('organizations')
        else:
            return request.url_for('update-organization', id_organization=self.object_id)

    @property
    def api_url(self):
        return f'{settings.API_URL}/manage/organizations/{self.object_id}'

    @property
    def session_key(self):
        return settings.SESSION_KEY

#</editor-fold>

# users
class OrganizationUsers(BaseHTTPEndpoint):
    id_organization = None

    @property
    def api_url(self):
        return settings.API_URL

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'organization_users.pt'


    @requires(constants.scope_manager, redirect='login')
    async def get(self, request: Request):
        self.id_organization = request.path_params['id_organization']

        organization = await self.get_from_api(request, f'{self.api_url}/manage/organizations/{self.id_organization}')

        query, page, limit, content, prev_page, next_page = await self.get_result_list(
            request, f'/manage/organizations/{self.id_organization}/users')

        context = {
            'organization': organization,
            'content': content,
            'query': query,
            'page': page,
            'limit': limit,
            'prev_page': prev_page,
            'next_page': next_page,
            'snackbar': self.get_snackbar(request, 'action_successful'),
            'filter_documentation': {'level': self.get_documentation(request)}
        }
        return self.render(request, context)


class AddOrganizationUser(FormEndpoint):
    id_organization = None

    @property
    def method(self):
        return 'POST'

    @property
    def action_successful_message(self):
        return 'Organizācijas lietotājs pievienots'

    def redirect_url(self, request: Request, data: dict = None):
        return request.url_for('organization-users', id_organization=self.id_organization)

    @property
    def api_url(self):
        return f'{settings.API_URL}/manage/organizations/{self.id_organization}/users/'

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'organization_user_card.pt'

    async def get_context(self, request: Request):
        organization = await self.get_from_api(request, f'{settings.API_URL}/manage/organizations/{self.id_organization}')

        if request.method == 'POST':
            form_data = await request.form()
            content = {
                'id': form_data['id'],
                'email': form_data['email'],
                'first_name': form_data['first_name'],
                'last_name': form_data['last_name'],
                'password_to_email': form_data['password_to_email'] if form_data['password_to_email'] else '0'
            }
        else:
            content = {
                'id': 0, 'email': None, 'first_name': None, 'last_name': None, 'password_to_email': False}
        return {
            'organization': organization,
            'content': content,
            'form_errors': None,
            'snackbar': self.get_snackbar(request, 'action_successful'),
            'form_type': 'ADD',
            'filter_documentation': {'level': self.get_documentation(request)}
        }

    @requires(constants.scope_manager, redirect='login')
    async def get(self, request: Request):
        self.id_organization = request.path_params['id_organization']

        return await super(AddOrganizationUser, self).get(request)

    @requires(constants.scope_manager, redirect='login')
    async def post(self, request: Request):
        self.id_organization = request.path_params['id_organization']

        return await super(AddOrganizationUser, self).post(request)


class UpdateOrganizationUser(FormEndpoint):
    id_user = None
    id_organization = None

    @property
    def method(self):
        return 'PUT'

    @property
    def action_successful_message(self):
        return 'Organizācijas lietotājs izmainīts'

    def redirect_url(self, request: Request, data: dict = None):
        return request.url_for('update-organization-user', id_organization=self.id_organization, id_user=data['id'])

    @property
    def api_url(self):
        return f'{settings.API_URL}/manage/organizations/{self.id_organization}/users/{self.id_user}'

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'organization_user_card.pt'

    async def get_context(self, request: Request):
        organization = await self.get_from_api(
            request,
            f'{settings.API_URL}/manage/organizations/{self.id_organization}')

        if request.method == 'POST':
            form_data = await request.form()
            content = {
                'id': form_data['id'],
                'email': form_data['email'],
                'first_name': form_data['first_name'],
                'last_name': form_data['last_name']
            }
        else:
            content = await self.get_from_api(request, self.api_url)
        return {
            'organization': organization,
            'content': content,
            'form_errors': None,
            'snackbar': self.get_snackbar(request, 'action_successful'),
            'form_type': 'UPDATE',
            'filter_documentation': {'level': self.get_documentation(request)}
        }

    @requires(constants.scope_manager, redirect='login')
    async def get(self, request: Request):
        self.id_user = request.path_params['id_user']
        self.id_organization = request.path_params['id_organization']

        return await super(UpdateOrganizationUser, self).get(request)

    @requires(constants.scope_manager, redirect='login')
    async def post(self, request: Request):
        self.id_user = request.path_params['id_user']
        self.id_organization = request.path_params['id_organization']

        return await super(UpdateOrganizationUser, self).post(request)


class DeleteOrganizationUser(DeleteEndpoint):
    id_organization = None

    @property
    def tpl(self):
        return ''

    @property
    def action_successful_message(self):
        return 'Organizācijas lietotājs izdzēsts'

    def redirect_url(self, request: Request, result: bool):
        if result or self.list_action:
            return request.url_for('organization-users', id_organization=self.id_organization)
        else:
            return request.url_for(
                'update-organization-user', id_organization=self.id_organization, id_user=self.object_id)

    @property
    def api_url(self):
        return f'{settings.API_URL}/manage/organizations/{self.id_organization}/users/{self.object_id}'

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @requires(constants.scope_manager, redirect='login')
    async def get(self, request: Request):
        self.id_organization = request.path_params['id_organization']

        return await super(DeleteOrganizationUser, self).get(request)


class ResetOrganizationUserPassword(PostEndpoint):
    id_organization = None

    @property
    def tpl(self):
        return ''

    @property
    def action_successful_message(self):
        return 'Lietotāja parole atjaunota un nosūtīta  uz e-pastu'

    def redirect_url(self, request: Request, result: bool):
        if (result or self.list_action) and not self.stay_in_form:
            return request.url_for('organization-users', id_organization=self.id_organization)
        else:
            return request.url_for(
                'update-organization-user', id_organization=self.id_organization, id_user=self.object_id)

    @property
    def api_url(self):
        return f'{settings.API_URL}/manage/users/{self.object_id}/reset-password'

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @requires(constants.scope_manager, redirect='login')
    async def get(self, request: Request):
        self.id_organization = request.path_params['id_organization']

        return await super(ResetOrganizationUserPassword, self).get(request)