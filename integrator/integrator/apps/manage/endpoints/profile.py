from starlette.requests import Request

from integrator.helpers.endpoints import FormEndpoint
from integrator.apps.manage import settings


class Profile(FormEndpoint):
    @property
    def method(self):
        return 'PUT'

    @property
    def action_successful_message(self):
        return 'Profila dati izmainīti'

    def redirect_url(self, request: Request, data: dict = None):
        return request.url_for('profile')

    @property
    def api_url(self):
        return f'{settings.API_URL}/manage/users/profile'

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'profile.pt'

    async def get_context(self, request: Request):
        if request.method == 'POST':
            form_data = await request.form()
            content = {
                'email': form_data['email'],
                'first_name': form_data['first_name'],
                'last_name': form_data['last_name'],
            }
        else:
            content = {
                'email': request.user.email,
                'first_name': request.user.first_name,
                'last_name': request.user.last_name,
            }
        return {'content': content,
                'form_errors': None,
                'snackbar': self.get_snackbar(request, 'action_successful'),
                'filter_documentation': {'level': self.get_documentation(request)}
                }


class ChangePassword(FormEndpoint):
    @property
    def method(self):
        return 'PUT'

    @property
    def action_successful_message(self):
        return 'Parole izmainīta'

    def redirect_url(self, request: Request, data: dict = None):
        return request.url_for('change-password')

    @property
    def api_url(self):
        return f'{settings.API_URL}/system/security/session_user_change_password'

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'change_password.pt'

    async def get_context(self, request: Request):
        return {'form_errors': None,
                'snackbar': self.get_snackbar(request, 'action_successful'),
                'filter_documentation': {'level': self.get_documentation(request)}
                }
