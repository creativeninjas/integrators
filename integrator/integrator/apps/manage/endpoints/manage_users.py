from starlette.authentication import requires
from starlette.requests import Request

from integrator.apps.manage import settings
from integrator.helpers import constants
from integrator.helpers.endpoints import (
    BaseHTTPEndpoint,
    FormEndpoint, DeleteEndpoint, PostEndpoint
)


class ManageUsers(BaseHTTPEndpoint):
    @property
    def api_url(self):
        return settings.API_URL

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'manage_users.pt'

    @requires(constants.scope_manager, redirect='login')
    async def get(self, request: Request):
        query, page, limit, content, prev_page, next_page = await self.get_result_list(request, '/manage/users/')

        context = {
            'content': content,
            'query': query,
            'page': page,
            'limit': limit,
            'prev_page': prev_page,
            'next_page': next_page,
            'snackbar': self.get_snackbar(request, 'action_successful'),
            'filter_documentation': {'level': self.get_documentation(request)}
        }
        return self.render(request, context)


class AddManageUser(FormEndpoint):
    @property
    def method(self):
        return 'POST'

    @property
    def action_successful_message(self):
        return 'Lietotājs pievienots'

    def redirect_url(self, request: Request, data: dict = None):
        return request.url_for('manage-users')

    @property
    def api_url(self):
        return f'{settings.API_URL}/manage/users/'

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'user_card.pt'

    async def get_context(self, request: Request):
        if request.method == 'POST':
            form_data = await request.form()
            content = {
                'id': form_data['id'],
                'email': form_data['email'],
                'first_name': form_data['first_name'],
                'last_name': form_data['last_name'],
                'password_to_email': form_data['password_to_email'],
            }
        else:
            content = {
                'id': 0, 'email': None, 'first_name': None, 'last_name': None, 'password_to_email': False}
        return {
            'content': content,
            'form_errors': None,
            'snackbar': self.get_snackbar(request, 'action_successful'),
            'form_type': 'ADD',
            'filter_documentation': {'level': self.get_documentation(request)}
        }


class UpdateManageUser(FormEndpoint):
    id_user = None
    @property
    def method(self):
        return 'PUT'

    @property
    def action_successful_message(self):
        return 'Lietotājs izmainīts'

    def redirect_url(self, request: Request, data: dict = None):
        return request.url_for('update-manage-user', id_user=data['id'])

    @property
    def api_url(self):
        return f'{settings.API_URL}/manage/users/{self.id_user}/'

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'user_card.pt'

    async def get_context(self, request: Request):
        if request.method == 'POST':
            form_data = await request.form()
            content = {
                'id': form_data['id'],
                'email': form_data['email'],
                'first_name': form_data['first_name'],
                'last_name': form_data['last_name']
            }
        else:
            content = await self.get_from_api(request, self.api_url)
        return {
            'content': content,
            'form_errors': None,
            'snackbar': self.get_snackbar(request, 'action_successful'),
            'form_type': 'UPDATE',
            'filter_documentation': {'level': self.get_documentation(request)}
        }

    @requires(constants.scope_manager, redirect='login')
    async def get(self, request: Request):
        self.id_user = request.path_params['id_user']
        return await super(UpdateManageUser, self).get(request)

    @requires(constants.scope_manager, redirect='login')
    async def post(self, request: Request):
        self.id_user = request.path_params['id_user']
        return await super(UpdateManageUser, self).post(request)


class DeleteManageUser(DeleteEndpoint):
    @property
    def tpl(self):
        return ''

    @property
    def action_successful_message(self):
        return 'Lietotājs izdzēsts'

    def redirect_url(self, request: Request, result: bool):
        if result or self.list_action:
            return request.url_for('manage-users')
        else:
            return request.url_for('update-manage-user', id_user=self.object_id)

    @property
    def api_url(self):
        return f'{settings.API_URL}/manage/users/{self.object_id}'

    @property
    def session_key(self):
        return settings.SESSION_KEY


class ResetManageUserPassword(PostEndpoint):
    @property
    def tpl(self):
        return ''

    @property
    def action_successful_message(self):
        return 'Lietotāja parole atjaunota un nosūtīta  uz e-pastu'

    def redirect_url(self, request: Request, result: bool):
        if (result or self.list_action) and not self.stay_in_form:
            return request.url_for('manage-users')
        else:
            return request.url_for('update-manage-user', id_user=self.object_id)

    @property
    def api_url(self):
        return f'{settings.API_URL}/manage/users/{self.object_id}/reset-password'

    @property
    def session_key(self):
        return settings.SESSION_KEY