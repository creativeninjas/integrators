from starlette.authentication import requires
from starlette.requests import Request

from integrator.apps.manage import settings
from integrator.helpers import constants

from integrator.helpers.endpoints import BaseHTTPEndpoint, FormEndpoint


class SystemLogs(BaseHTTPEndpoint):
    @property
    def api_url(self):
        return settings.API_URL

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'system_logs.pt'

    @requires(constants.scope_manager, redirect='login')
    async def get(self, request: Request):
        filter_level = []

        additional_qs = ''
        for qs in request.query_params._list:
            if qs[0] == 'level':
                filter_level.append(qs[1])
                additional_qs = f'{additional_qs}level={qs[1]}&'

        query, page, limit, content, prev_page, next_page = await self.get_result_list(request, '/system/logs/', additional_qs)

        context = {
            'filters': {'level': filter_level},
            'filter_level': filter_level,
            'content': content,
            'query': query,
            'page': page,
            'limit': limit,
            'prev_page': prev_page,
            'next_page': next_page,
            'snackbar': self.get_snackbar(request, 'action_successful'),
            'filter_documentation': {'level': self.get_documentation(request)}
        }
        return self.render(request, context)


class ViewSystemLog(FormEndpoint):
    id_system_log = None

    @property
    def method(self):
        return None

    @property
    def action_successful_message(self):
        return None

    def redirect_url(self, request: Request, data: dict = None):
        return None

    @property
    def api_url(self):
        return f'{settings.API_URL}/system/logs/{self.id_system_log}/'

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'system_log_card.pt'

    async def get_context(self, request: Request):
        content = await self.get_from_api(request, self.api_url)

        return {
            'content': content,
            'form_errors': None,
            'snackbar': self.get_snackbar(request, 'action_successful'),
            'form_type': None,
            'filter_documentation': {'level': self.get_documentation(request)}
        }

    @requires(constants.scope_manager, redirect='login')
    async def get(self, request: Request):
        self.id_system_log = request.path_params['id_system_log']
        return await super(ViewSystemLog, self).get(request)
