from starlette.authentication import requires
from starlette.requests import Request

from integrator.apps.manage import settings
from integrator.helpers import constants

from integrator.helpers.endpoints import BaseHTTPEndpoint, FormEndpoint


class Tasks(BaseHTTPEndpoint):
    @property
    def api_url(self):
        return settings.API_URL

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'tasks.pt'

    @requires(constants.scope_manager, redirect='login')
    async def get(self, request: Request):
        filter_status = []
        filter_foreign_system = []
        additional_qs = ''
        for qs in request.query_params._list:
            if qs[0] == 'status':
                filter_status.append(int(qs[1]))
                additional_qs = f'{additional_qs}status={qs[1]}&'
            elif qs[0] == 'fs':
                filter_foreign_system.append(qs[1])
                additional_qs = f'{additional_qs}fs={qs[1]}&'

        query, page, limit, content, prev_page, next_page = await self.get_result_list(request, '/manage/tasks/', additional_qs)

        context = {
            'filters': {'status': filter_status, 'fs': filter_foreign_system},
            'filter_status': filter_status,
            'filter_foreign_system': filter_foreign_system,
            'content': content,
            'query': query,
            'page': page,
            'limit': limit,
            'prev_page': prev_page,
            'next_page': next_page,
            'snackbar': self.get_snackbar(request, 'action_successful'),
            'filter_documentation': {'level': self.get_documentation(request)}
        }
        return self.render(request, context)


class UpdateTask(FormEndpoint):
    id_task = None

    @property
    def method(self):
        return 'PUT'

    @property
    def action_successful_message(self):
        return 'Uzdevums izmainīts'

    def redirect_url(self, request: Request, data: dict = None):
        return request.url_for('update-task', id_task=data['id'])

    @property
    def api_url(self):
        return f'{settings.API_URL}/manage/tasks/{self.id_task}/'

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'task_card.pt'

    async def get_context(self, request: Request):
        if request.method == 'POST':
            form_data = await request.form()
            content = {
                'id': form_data['id'],
                'title': form_data['title'],
                'available': form_data.get('available', False)
            }
        else:
            content = await self.get_from_api(request, self.api_url)
        return {
            'content': content,
            'form_errors': None,
            'snackbar': self.get_snackbar(request, 'action_successful'),
            'form_type': 'UPDATE',
            'filter_documentation': {'level': self.get_documentation(request)}
        }

    @requires(constants.scope_manager, redirect='login')
    async def get(self, request: Request):
        self.id_task = request.path_params['id_task']
        return await super(UpdateTask, self).get(request)

    @requires(constants.scope_manager, redirect='login')
    async def post(self, request: Request):
        self.id_task = request.path_params['id_task']
        return await super(UpdateTask, self).post(request)
