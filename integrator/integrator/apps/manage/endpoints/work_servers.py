from starlette.authentication import requires
from starlette.requests import Request

from integrator.apps.manage import settings
from integrator.helpers import constants

from integrator.helpers.endpoints import BaseHTTPEndpoint, FormEndpoint, DeleteEndpoint


class WorkServers(BaseHTTPEndpoint):
    @property
    def api_url(self):
        return settings.API_URL

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'work_servers.pt'

    @requires(constants.scope_manager, redirect='login')
    async def get(self, request: Request):
        filter_status = []

        additional_qs = ''
        for qs in request.query_params._list:
            if qs[0] == 'status':
                filter_status.append(int(qs[1]))
                additional_qs = f'{additional_qs}status={qs[1]}&'

        query, page, limit, content, prev_page, next_page = await self.get_result_list(request, '/manage/work-servers/', additional_qs)

        context = {
            'filters': {'status': filter_status},
            'filter_status': filter_status,
            'content': content,
            'query': query,
            'page': page,
            'limit': limit,
            'prev_page': prev_page,
            'next_page': next_page,
            'snackbar': self.get_snackbar(request, 'action_successful'),
            'filter_documentation': {'level': self.get_documentation(request)}
        }
        return self.render(request, context)


class AddWorkServer(FormEndpoint):
    @property
    def method(self):
        return 'POST'

    @property
    def action_successful_message(self):
        return 'Darba serveris pievienots'

    def redirect_url(self, request: Request, data: dict = None):
        return request.url_for('work-servers')

    @property
    def api_url(self):
        return f'{settings.API_URL}/manage/work-servers/'

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'work_server_card.pt'

    async def get_context(self, request: Request):
        if request.method == 'POST':
            form_data = await request.form()
            content = {
                'id': form_data['id'],
                'address': form_data['address'],
                'disabled': form_data.get('disabled', False)
            }
        else:
            content = {'id': 0, 'address': None, 'disabled': False}
        return {
            'content': content,
            'form_errors': None,
            'snackbar': self.get_snackbar(request, 'action_successful'),
            'form_type': 'ADD',
            'filter_documentation': {'level': self.get_documentation(request)}
        }


class UpdateWorkServer(FormEndpoint):
    id_work_server = None
    @property
    def method(self):
        return 'PUT'

    @property
    def action_successful_message(self):
        return 'Darba serveris izmainīts'

    def redirect_url(self, request: Request, data: dict = None):
        return request.url_for('update-work-server', id_work_server=data['id'])

    @property
    def api_url(self):
        return f'{settings.API_URL}/manage/work-servers/{self.id_work_server}/'

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'work_server_card.pt'

    async def get_context(self, request: Request):
        if request.method == 'POST':
            form_data = await request.form()
            content = {
                'id': form_data['id'],
                'address': form_data['address'],
                'disabled': form_data.get('disabled', 0)
            }
        else:
            content = await self.get_from_api(request, self.api_url)

        return {
            'content': content,
            'form_errors': None,
            'snackbar': self.get_snackbar(request, 'action_successful'),
            'form_type': 'UPDATE',
            'pool_status': await self.get_work_server_pool_status(content['address']),
            'filter_documentation': {'level': self.get_documentation(request)}
        }

    @requires(constants.scope_manager, redirect='login')
    async def get(self, request: Request):
        self.id_work_server = request.path_params['id_work_server']
        return await super(UpdateWorkServer, self).get(request)

    @requires(constants.scope_manager, redirect='login')
    async def post(self, request: Request):
        self.id_work_server = request.path_params['id_work_server']
        return await super(UpdateWorkServer, self).post(request)


class DeleteWorkServer(DeleteEndpoint):
    @property
    def tpl(self):
        return ''

    @property
    def action_successful_message(self):
        return 'Darba serveris izdzēsts'

    def redirect_url(self, request: Request, result: bool):
        if result or self.list_action:
            return request.url_for('work-servers')
        else:
            return request.url_for('update-work-server', id_work_server=self.object_id)

    @property
    def api_url(self):
        return f'{settings.API_URL}/manage/work-servers/{self.object_id}'

    @property
    def session_key(self):
        return settings.SESSION_KEY