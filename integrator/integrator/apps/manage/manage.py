from starlette.applications import Starlette
from starlette.middleware import Middleware
from starlette.middleware.authentication import AuthenticationMiddleware
from starlette.middleware.sessions import SessionMiddleware
from starlette.staticfiles import StaticFiles
from starlette.routing import (
    Route,
    Mount
)

# from pulse.guard.utils import AuthBackend
from helpers.tpl import ChameleonTemplates
from helpers.output import StaticFileUrlHandler

from pulse.apps.admin.endpoints.dashboard import AdminDashboard
from pulse.apps.admin.endpoints.login import (
    Login,
    logout
)
from pulse.apps.admin.endpoints.clients import (
    ClientsEndpoint
)
from pulse.apps.admin.endpoints.websites import (
    WebsitesEndpoint,
    WebsiteEndpoint
)

from pulse.apps.admin import settings

routes = [
    Route('/manage', AdminDashboard, name='dashboard'),

    Route('/manage/login', Login, name='login'),
    Route('/manage/logout', logout, name='logout'),

    Mount('/static', StaticFiles(directory=settings.STATIC_DIR), name='static')
]

middleware = [
    Middleware(SessionMiddleware,
               secret_key=settings.SESSION_SECRET,
               session_cookie=settings.SESSION_COOKIE,
               https_only=settings.SESSION_HTTPS_ONLY),
    Middleware(AuthenticationMiddleware, backend=AuthBackend(
        settings.ADMIN_SESSION_KEY, settings.API_URL, settings.API_ME_URL))
]

app = Starlette(
    debug=True,
    routes=routes,
    middleware=middleware)

app.state.templates = ChameleonTemplates(directory=['pulse/apps/admin/templates', 'pulse/templates'], auto_reload=True)

# Setup static url version handler
app.state.static_file_url_handler = StaticFileUrlHandler(app, settings.STATIC_DIR)