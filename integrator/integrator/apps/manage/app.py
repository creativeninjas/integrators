from starlette.applications import Starlette
from starlette.middleware import Middleware
from starlette.middleware.authentication import AuthenticationMiddleware
from starlette.middleware.sessions import SessionMiddleware
from starlette.staticfiles import StaticFiles
from starlette.routing import (
    Route,
    Mount
)

from integrator.security.utils import AuthBackend
from integrator.helpers.tpl import ChameleonTemplates
from integrator.helpers.output import StaticFileUrlHandler

from integrator.apps.manage.endpoints.dashboard import Dashboard
from integrator.apps.manage.endpoints.organizations import (
    Organizations,
    AddOrganization,
    UpdateOrganization,
    DeleteOrganization,
    OrganizationUsers, AddOrganizationUser, UpdateOrganizationUser, DeleteOrganizationUser,
    ResetOrganizationUserPassword
)
from integrator.apps.manage.endpoints.work_servers import (
    WorkServers,
    AddWorkServer,
    UpdateWorkServer,
    DeleteWorkServer
)
from integrator.apps.manage.endpoints.manage_users import (
    ManageUsers,
    AddManageUser,
    UpdateManageUser,
    DeleteManageUser,
    ResetManageUserPassword
)

from integrator.apps.manage.endpoints.tasks import (
    Tasks,
    UpdateTask
)
from integrator.apps.manage.endpoints.system_logs import (
    SystemLogs,
    ViewSystemLog
)
from integrator.apps.manage.endpoints.profile import (
    Profile,
    ChangePassword
)
from integrator.apps.manage.endpoints.login import (
    Login,
    logout
)

from integrator.reset_password.endpoints import (
    ForgotPassword,
    ResetPassword
)

from integrator.apps.manage import settings


routes = [
    Route('/manage/dashboard', Dashboard, name='dashboard'),
    Route('/manage/me', Profile, name='profile'),
    Route('/manage/me/change-password', ChangePassword, name='change-password'),
    Route('/manage/organizations', Organizations, name='organizations'),
    Route('/manage/organizations/add', AddOrganization, name='add-organization'),
    Route('/manage/organizations/{id_organization:int}', UpdateOrganization, name='update-organization'),
    Route('/manage/organizations/{object_id:int}/delete', DeleteOrganization, name='delete-organization'),
    Route('/manage/organizations/{id_organization:int}/users', OrganizationUsers, name='organization-users'),
    Route('/manage/organizations/{id_organization:int}/users/add', AddOrganizationUser, name='add-organization-user'),
    Route('/manage/organizations/{id_organization:int}/users/{id_user:int}', UpdateOrganizationUser, name='update-organization-user'),
    Route('/manage/organizations/{id_organization:int}/users/{object_id:int}/delete', DeleteOrganizationUser, name='delete-organization-user'),
    Route('/manage/organizations/{id_organization:int}/users/{object_id:int}/reset-manage-user-password', ResetOrganizationUserPassword, name='reset-organization-user-password'),
    Route('/manage/work-servers', WorkServers, name='work-servers'),
    Route('/manage/work-servers/add', AddWorkServer, name='add-work-server'),
    Route('/manage/work-servers/{id_work_server:int}', UpdateWorkServer, name='update-work-server'),
    Route('/manage/work-servers/{object_id:int}/delete', DeleteWorkServer, name='delete-work-server'),
    Route('/manage/users', ManageUsers, name='manage-users'),
    Route('/manage/users/add', AddManageUser, name='add-manage-user'),
    Route('/manage/users/{id_user:int}', UpdateManageUser, name='update-manage-user'),
    Route('/manage/users/{object_id:int}/reset-password', ResetManageUserPassword, name='reset-manage-user-password'),
    Route('/manage/users/{object_id:int}/delete', DeleteManageUser, name='delete-manage-user'),
    Route('/manage/tasks', Tasks, name='tasks'),
    Route('/manage/tasks/{id_task:int}', UpdateTask, name='update-task'),
    Route('/manage/system-logs', SystemLogs, name='system-logs'),
    Route('/manage/system-logs/{id_system_log}', ViewSystemLog, name='view-system-log'),

    Route('/manage/login', Login, name='login'),
    Route('/manage/logout', logout, name='logout'),
    Route('/manage/forgot-password', ForgotPassword, name='forgot-password'),
    Route('/manage/reset-password/{reset_code:str}', ResetPassword, name='reset-password'),

    Mount('/static', StaticFiles(directory=settings.STATIC_DIR), name='static')
]

middleware = [
    Middleware(SessionMiddleware,
               secret_key=settings.SESSION_SECRET,
               session_cookie=settings.SESSION_COOKIE,
               https_only=settings.SESSION_HTTPS_ONLY),
    Middleware(AuthenticationMiddleware, backend=AuthBackend(
        settings.SESSION_KEY, settings.API_URL, settings.API_ME_URL))
]

app = Starlette(
    debug=True,
    routes=routes,
    middleware=middleware)

app.state.templates = ChameleonTemplates(
    directory=['integrator/apps/manage/templates', 'integrator/templates'], auto_reload=True)

# Setup static url version handler
app.state.static_file_url_handler = StaticFileUrlHandler(app, settings.STATIC_DIR)

app.state.drawer_navigation = [
    ('Darbgalds', app.url_path_for('dashboard'), 'dashboard'),
    ('Organizācijas', app.url_path_for('organizations'), 'business'),
    ('Darbu serveri', app.url_path_for('work-servers'), 'plumbing'),
    ('Lietotāji', app.url_path_for('manage-users'), 'people'),
    ('Uzdevumi', app.url_path_for('tasks'), 'task'),
    ('Notikum žurnāls', app.url_path_for('system-logs'), 'list_alt'),
]