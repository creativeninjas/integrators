from starlette.applications import Starlette
from starlette.middleware import Middleware
from starlette.middleware.authentication import AuthenticationMiddleware
from starlette.middleware.sessions import SessionMiddleware
from starlette.staticfiles import StaticFiles
from starlette.routing import (
    Route,
    Mount
)


from integrator.security.utils import AuthBackend
from integrator.helpers.tpl import ChameleonTemplates
from integrator.helpers.output import StaticFileUrlHandler

from integrator.apps.clients.endpoints.dashboard import Dashboard

from integrator.apps.clients.endpoints.foreign_systems import (
    ForeignSystems, ForeignSystemConnections, AddForeignSystemConnection, UpdateForeignSystemConnection,
    DeleteForeignSystemConnection
)
from integrator.apps.clients.endpoints.system_logs import (
    SystemLogs,
    ViewSystemLog
)
from integrator.apps.clients.endpoints.profile import (
    Profile,
    ChangePassword
)
from integrator.apps.clients.endpoints.login import (
    Login,
    logout,
    TR
)

from integrator.apps.clients.endpoints.fsc_data import (
    ForeignSystemsSummary,
    TikiTakaSummary,
    Fiscofs,
    Documents,
    OnlineDocuments,
    ViewFiscof,
    ViewDocument,
    ViewOnlineDocument,
    Units,
    ViewUnit,
    ViewUnitVersion,
    TaxCategories,
    ViewTaxCategory,
    ViewTaxCategoryVersion,
    Products,
    ViewProduct,
    ViewProductVersion,
    OnlineOrders,
    ViewOnlineOrder
)

from integrator.apps.clients.endpoints.works import (
    Works,
    AddWork,
    UpdateWork,
    TaskAttributes, DeleteWork
)

from integrator.reset_password.endpoints import (
    ForgotPassword,
    ResetPassword
)

from integrator.apps.clients import settings


routes = [
    Route('/client/tr', TR),

    Route('/client/dashboard', Dashboard, name='dashboard'),
    Route('/client/me', Profile, name='profile'),
    Route('/client/me/change-password', ChangePassword, name='change-password'),
    Route('/client/foreign-systems', ForeignSystems, name='foreign-systems'),
    Route('/client/foreign-systems/{id_foreign_system:str}', ForeignSystemConnections, name='foreign-system-connections'),
    Route('/client/foreign-systems/{id_foreign_system:str}/add',
          AddForeignSystemConnection,
          name='add-foreign-system-connection'),
    Route('/client/foreign-systems/{id_foreign_system:str}/{object_id:int}',
          UpdateForeignSystemConnection,
          name='update-foreign-system-connection'),
    Route('/client/foreign-systems/{id_foreign_system:str}/{object_id:int}/delete',
          DeleteForeignSystemConnection,
          name='delete-foreign-system-connection'),

    Route('/client/data/other', ForeignSystemsSummary, name='data-summary'),
    Route('/client/data/tiki-taka', TikiTakaSummary, name='tiki-taka-data-summary'),
    Route('/client/data/tiki-taka/{id_fsc:int}/fiscofs', Fiscofs, name='fiscofs'),
    Route('/client/data/tiki-taka/{id_fsc:int}/fiscofs/{id_fiscof:int}', ViewFiscof, name='view-fiscof'),
    Route('/client/data/tiki-taka/{id_fsc:int}/documents', Documents, name='documents'),
    Route('/client/data/tiki-taka/{id_fsc:int}/documents/{id_doc:int}', ViewDocument, name='view-document'),
    Route('/client/data/tiki-taka/{id_fsc:int}/online-documents', OnlineDocuments, name='online-documents'),
    Route('/client/data/tiki-taka/{id_fsc:int}/online-documents/{id_doc:int}',
          ViewOnlineDocument,
          name='view-online-document'),

    Route('/client/data/other/{id_fsc:int}/units', Units, name='units'),
    Route('/client/data/other/{id_fsc:int}/units/{id_unit:int}', ViewUnit, name='view-unit'),
    Route('/client/data/other/{id_fsc:int}/units/{id_unit:int}/versions/{id_version:int}',
          ViewUnitVersion,
          name='view-unit-version'),

    Route('/client/data/other/{id_fsc:int}/tax-categories', TaxCategories, name='view-tax-categories'),
    Route('/client/data/other/{id_fsc:int}/tax-categories/{id_tax_category:int}',
          ViewTaxCategory,
          name='view-tax-category'),
    Route('/client/data/other/{id_fsc:int}/tax-categories/{id_tax_category:int}/versions/{id_version:int}',
          ViewTaxCategoryVersion,
          name='view-tax-category-version'),

    Route('/client/data/other/{id_fsc:int}/products', Products, name='products'),
    Route('/client/data/other/{id_fsc:int}/products/{id_product:int}', ViewProduct, name='view-product'),
    Route('/client/data/other/{id_fsc:int}/products/{id_product:int}/versions/{id_version:int}',
          ViewProductVersion,
          name='view-product-version'),

    Route('/client/data/other/{id_fsc:int}/online-orders', OnlineOrders, name='online-orders'),
    Route('/client/data/other/{id_fsc:int}/online-orders/{id_online_order:int}', ViewOnlineOrder, name='view-online-order'),

    Route('/client/works', Works, name='works'),
    Route('/client/works/add', AddWork, name='add-work'),
    Route('/client/works/{object_id:int}', UpdateWork, name='update-work'),
    Route('/client/works/{object_id:int}/delete', DeleteWork, name='delete-work'),
    Route('/client/works/task-attributes', TaskAttributes, name='task-attributes'),

    Route('/client/system-logs', SystemLogs, name='system-logs'),
    Route('/client/system-logs/{id_system_log}', ViewSystemLog, name='view-system-log'),

    Route('/client/login', Login, name='login'),
    Route('/client/logout', logout, name='logout'),
    Route('/client/forgot-password', ForgotPassword, name='forgot-password'),
    Route('/client/reset-password/{reset_code:str}', ResetPassword, name='reset-password'),

    Mount('/static', StaticFiles(directory=settings.STATIC_DIR), name='static')
]

middleware = [
    Middleware(SessionMiddleware,
               secret_key=settings.SESSION_SECRET,
               session_cookie=settings.SESSION_COOKIE,
               https_only=settings.SESSION_HTTPS_ONLY),
    Middleware(AuthenticationMiddleware, backend=AuthBackend(
        settings.SESSION_KEY, settings.API_URL, settings.API_ME_URL))
]

app = Starlette(
    debug=True,
    routes=routes,
    middleware=middleware)

app.state.templates = ChameleonTemplates(
    directory=['integrator/apps/clients/templates', 'integrator/templates'], auto_reload=True)

# Setup static url version handler
app.state.static_file_url_handler = StaticFileUrlHandler(app, settings.STATIC_DIR)

app.state.drawer_navigation = [
    ('Darbgalds', app.url_path_for('dashboard'), 'dashboard'),
    ('Darbi', app.url_path_for('works'), 'precision_manufacturing'),
    ('Savienojumi', app.url_path_for('foreign-systems'), 'plumbing'),
    ('Ārējo sistēmu dati', app.url_path_for('data-summary'), 'topic'),
    ('TikiTaka dati', app.url_path_for('tiki-taka-data-summary'), 'receipt'),
    # ('Notikum žurnāls', app.url_path_for('system-logs'), 'list_alt'),
]