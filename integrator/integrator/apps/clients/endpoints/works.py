import json
import os

from starlette.authentication import requires
from starlette.requests import Request
from starlette.responses import RedirectResponse

from integrator.apps.clients import settings
from integrator.helpers import constants

from integrator.helpers.endpoints import (
    BaseHTTPEndpoint,
    ClientFormEndpoint, ClientDeleteEndpoint
)
from integrator.helpers.integrator_exceptions import APIValidationException, UnknownAPIException


class Works(BaseHTTPEndpoint):
    @property
    def api_url(self):
        return settings.API_URL

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'works.pt'

    @requires(constants.scope_client, redirect='login')
    async def get(self, request: Request):
        error_log_count = await self.get_from_api(request, f'{settings.API_URL}/clients/users/profile/unread-log-count')
        filter_status = []

        additional_qs = ''
        for qs in request.query_params._list:
            if qs[0] == 'status':
                filter_status.append(qs[1])
                additional_qs = f'{additional_qs}status={qs[1]}&'

        query, page, limit, content, prev_page, next_page = await self.get_result_list(request, '/clients/works/', additional_qs)

        context = {
            'filter_documentation': {'level': filter_status},
            'filters': {'level': filter_status},
            'filter_status': filter_status,
            'content': content,
            'query': query,
            'page': page,
            'limit': limit,
            'prev_page': prev_page,
            'next_page': next_page,
            'snackbar': self.get_snackbar(request, 'action_successful'),
            'error_log_count': error_log_count
        }
        return self.render(request, context)


class AddWork(ClientFormEndpoint):
    data_to_post = None

    @property
    def method(self):
        return 'POST'

    @property
    def action_successful_message(self):
        return 'Darbs pievienots'

    def redirect_url(self, request: Request, data: dict = None):
        return request.url_for('works')

    @property
    def api_url(self):
        return f'{settings.API_URL}/clients/works/'

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'work_card.pt'

    async def _post(self, request):
        try:
            data = await self.send_data(request, self.data_to_post, self.method)
            self.set_snackbar(request, 'action_successful', self.action_successful_message)

            return RedirectResponse(url=self.redirect_url(request, json.loads(data)), status_code=302)
        except APIValidationException as ex:
            form_errors = {}
            for e in json.loads(ex.errors)['detail']:
                form_errors[e['loc'][1]] = e['msg']

            context = await self.get_context(request)
            context['form_errors'] = form_errors

            return self.render(request, context)
        except UnknownAPIException as ex:
            context = await self.get_context(request)
            context['error_dialog_message'] = ex.errors

            return self.render(request, context)

    async def get_context(self, request: Request):
        run_periods = await self.get_from_api(request, f'{settings.API_URL}/clients/works/run-periods')
        connections = await self.get_from_api(request, f'{settings.API_URL}/clients/foreign-systems/any/connections')
        tasks = await self.get_from_api(request, f'{settings.API_URL}/clients/works/tasks?status=1&status=2')

        content = {}
        if request.method == 'POST':
            form_data = await request.form()
            work_task = []
            for i in form_data._list:
                if i[0] == 'wt':
                    work_task.append(json.loads(i[1]))

            content['work'] = {'id': 0,
                               'title': form_data['title'],
                               'disabled': form_data.get('disabled', False),
                               'repeat': form_data.get('repeat', False),
                               'run_period': {'id': form_data['id_run_period']},
                               'minute': int(form_data['minute']) if form_data['minute'] else None,
                               'hour': int(form_data['hour']) if form_data['hour'] else None,
                               'week_day': int(form_data['week_day']) if form_data['week_day'] else None,
                               'month': int(form_data['month']) if form_data['month'] else None,
                               'month_day': int(form_data['month_day']) if form_data['month_day'] else None}
            content['work_as_job'] = {}
            content['tasks'] = work_task
        else:

            content['work'] = {'id': 0,
                               'title': None,
                               'disabled': False,
                               'repeat': False,
                               'run_period': {'id': None},
                               'minute': None,
                               'hour': None,
                               'week_day': None,
                               'month': None,
                               'month_day': None}
            content['work_as_job'] = {}
            content['tasks'] = {}
        return {
            'tasks': tasks,
            'connections': connections,
            'run_periods': run_periods,
            'content': content,
            'form_errors': None,
            'snackbar': self.get_snackbar(request, 'action_successful'),
            'form_type': 'ADD',
            'error_log_count': await self.get_from_api(
                request, f'{settings.API_URL}/clients/users/profile/unread-log-count'),
            'filter_documentation': {'level': self.get_documentation(request)}
        }

    @requires(constants.scope_client, redirect='login')
    async def get(self, request: Request):
        return await super(AddWork, self).get(request)

    @requires(constants.scope_client, redirect='login')
    async def post(self, request: Request):
        context = await self.get_context(request)
        self.data_to_post = context['content']['work']
        self.data_to_post['id_run_period'] = context['content']['work']['run_period']['id']
        self.data_to_post['tasks'] = context['content']['tasks']
        return await self._post(request)


class UpdateWork(ClientFormEndpoint):
    data_to_post = None
    id_work = None

    @property
    def method(self):
        return 'PUT'

    @property
    def action_successful_message(self):
        return 'Darbs izmainīts'

    def redirect_url(self, request: Request, data: dict = None):
        return request.url_for('update-work', object_id=self.id_work)

    @property
    def api_url(self):
        return f'{settings.API_URL}/clients/works/{self.id_work}'

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'work_card.pt'

    async def _post(self, request):
        try:
            data = await self.send_data(request, self.data_to_post, self.method)
            self.set_snackbar(request, 'action_successful', self.action_successful_message)

            return RedirectResponse(url=self.redirect_url(request, json.loads(data)), status_code=302)
        except APIValidationException as ex:
            form_errors = {}
            for e in json.loads(ex.errors)['detail']:
                form_errors[e['loc'][1]] = e['msg']

            context = await self.get_context(request)
            context['form_errors'] = form_errors

            return self.render(request, context)
        except UnknownAPIException as ex:
            context = await self.get_context(request)
            context['error_dialog_message'] = ex.errors

            return self.render(request, context)

    async def get_context(self, request: Request):
        run_periods = await self.get_from_api(request, f'{settings.API_URL}/clients/works/run-periods')
        connections = await self.get_from_api(request, f'{settings.API_URL}/clients/foreign-systems/any/connections')
        tasks = await self.get_from_api(request, f'{settings.API_URL}/clients/works/tasks?status=1&status=2')
        job_log = None

        content = {}
        if request.method == 'POST':
            work_task = []
            form_data = await request.form()
            if form_data['form_type'] == 'settings':

                full_form_content = await self.get_from_api(request, self.api_url)

                content['work'] = {'id': form_data['id'],
                                   'title': full_form_content['work']['title'],
                                   'disabled': full_form_content['work']['disabled'],
                                   'repeat': full_form_content['work']['repeat'],
                                   'run_period': {'id': full_form_content['work']['run_period']['id']},
                                   'minute': int(full_form_content['work']['minute']) if full_form_content['work']['minute'] else None,
                                   'hour': int(full_form_content['work']['hour']) if full_form_content['work']['hour'] else None,
                                   'week_day': int(full_form_content['work']['week_day']) if full_form_content['work']['week_day'] else None,
                                   'month': int(full_form_content['work']['month']) if full_form_content['work']['month'] else None,
                                   'month_day': int(full_form_content['work']['month_day']) if full_form_content['work']['month_day'] else None,
                                   'max_retry_attempts': int(form_data.get('max_retry_attempts', False)),
                                   'retry_interval_minutes': int(form_data.get('retry_interval_minutes', False)),
                                   'retry_on_false': form_data.get('retry_on_false', False)}

                work_as_job = await self.get_from_api(request, self.api_url)

                content['work_as_job'] = work_as_job['work_as_job']
                content['tasks'] = full_form_content['tasks']
                content['work_server_address'] = work_as_job['work_server_address']
                job_log = await self.get_job_log(content['work_server_address'], content['work_as_job']['id_job'])
            else:
                for i in form_data._list:
                    if i[0] == 'wt':
                        work_task.append(json.loads(i[1]))

                settings_form_content = await self.get_from_api(request, self.api_url)

                content['work'] = {'id': form_data['id'],
                                   'title': form_data['title'],
                                   'disabled': form_data.get('disabled', False),
                                   'repeat': form_data.get('repeat', False),
                                   'run_period': {'id': form_data['id_run_period']},
                                   'minute': int(form_data['minute']) if form_data['minute'] else None,
                                   'hour': int(form_data['hour']) if form_data['hour'] else None,
                                   'week_day': int(form_data['week_day']) if form_data['week_day'] else None,
                                   'month': int(form_data['month']) if form_data['month'] else None,
                                   'month_day': int(form_data['month_day']) if form_data['month_day'] else None,
                                   'max_retry_attempts': int(settings_form_content['work']['max_retry_attempts']),
                                   'retry_interval_minutes': int(settings_form_content['work']['retry_interval_minutes']),
                                   'retry_on_false': settings_form_content['work']['retry_on_false']}

                work_as_job = await self.get_from_api(request, self.api_url)

                content['work_as_job'] = work_as_job['work_as_job']
                content['tasks'] = work_task
                content['work_server_address'] = work_as_job['work_server_address']
                job_log = await self.get_job_log(content['work_server_address'], content['work_as_job']['id_job'])
        else:
            content = await self.get_from_api(request, self.api_url)
            job_log = await self.get_job_log(content['work_server_address'], content['work_as_job']['id_job'])

        if job_log:
            job_log = job_log.replace(os.linesep, '<br />')

        return {
            'tasks': tasks,
            'connections': connections,
            'run_periods': run_periods,
            'content': content,
            'form_errors': None,
            'job_log': job_log,
            'snackbar': self.get_snackbar(request, 'action_successful'),
            'form_type': 'UPDATE',
            'error_log_count': await self.get_from_api(
                request, f'{settings.API_URL}/clients/users/profile/unread-log-count'),
            'filter_documentation': {'level': self.get_documentation(request)}
        }

    @requires(constants.scope_client, redirect='login')
    async def get(self, request: Request):
        self.id_work = request.path_params['object_id']

        return await super(UpdateWork, self).get(request)

    @requires(constants.scope_client, redirect='login')
    async def post(self, request: Request):
        self.id_work = request.path_params['object_id']

        context = await self.get_context(request)
        self.data_to_post = context['content']['work']
        self.data_to_post['id_run_period'] = context['content']['work']['run_period']['id']
        self.data_to_post['tasks'] = context['content']['tasks']
        return await self._post(request)


class TaskAttributes(ClientFormEndpoint):
    id_connection = None
    id_task = None

    @property
    def method(self):
        return None

    @property
    def action_successful_message(self):
        return None

    def redirect_url(self, request: Request, data: dict = None):
        return None

    @property
    def api_url(self):
        return f'{settings.API_URL}/clients/works/tasks/{self.id_task}'

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'task_attributes.pt'

    async def get_context(self, request: Request):
        connection_url = f'{settings.API_URL}/clients/foreign-systems/other/connections/{self.id_connection}'
        connection = await self.get_from_api(request, connection_url)
        content = await self.get_from_api(request, self.api_url)

        try:
            tpl_name = content['code']
            tpl = request.app.state.templates.get_template(f'task_attributes/{tpl_name}.pt')
        except ValueError:
            tpl_name = 'default'

        return {
            'tpl_name': tpl_name,
            'content': content,
            'connection': connection,
            'id_task': self.id_task
        }

    @requires(constants.scope_client, redirect='login')
    async def get(self, request: Request):
        self.id_connection = request.query_params['id_connection']
        self.id_task = request.query_params['id_task']

        return await super(TaskAttributes, self).get(request)


class DeleteWork(ClientDeleteEndpoint):
    @property
    def tpl(self):
        return ''

    @property
    def action_successful_message(self):
        return 'Darbs izdzēsts'

    def redirect_url(self, request: Request, result: bool):
        if result or self.list_action:
            return request.url_for('works')
        else:
            return request.url_for(
                'update-work', object_id=self.object_id)

    @property
    def api_url(self):
        return f'{settings.API_URL}/clients/works/{self.object_id}'

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @requires(constants.scope_client, redirect='login')
    async def get(self, request: Request):
        return await super(DeleteWork, self).get(request)
