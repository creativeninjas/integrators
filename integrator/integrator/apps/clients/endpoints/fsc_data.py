import asyncio
import json

from starlette.authentication import requires
from starlette.requests import Request

from integrator.apps.clients import settings
from integrator.helpers import constants

from integrator.helpers.endpoints import BaseHTTPEndpoint, ClientFormEndpoint


class ForeignSystemsSummary(BaseHTTPEndpoint):
    @property
    def api_url(self):
        return settings.API_URL

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'foreign_systems_summary.pt'

    @requires(constants.scope_client, redirect='login')
    async def get(self, request):
        fss = await self.get_from_api(request, f'{settings.API_URL}/clients/data/summary')
        error_log_count = await self.get_from_api(request, f'{settings.API_URL}/clients/users/profile/unread-log-count')

        filter_status = []

        for qs in request.query_params._list:
            if qs[0] == 'status':
                filter_status.append(qs[1])

        context = {
            'filter_documentation': {'level': filter_status},
            'filter_status': filter_status,
            'fss': fss,
            'error_log_count': error_log_count}
        return self.render(request, context)


class TikiTakaSummary(BaseHTTPEndpoint):
    @property
    def api_url(self):
        return settings.API_URL

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'tiki_taka_summary.pt'

    @requires(constants.scope_client, redirect='login')
    async def get(self, request):
        fss = await self.get_from_api(request, f'{settings.API_URL}/clients/data/tikitaka-summary')
        error_log_count = await self.get_from_api(request, f'{settings.API_URL}/clients/users/profile/unread-log-count')

        filter_status = []

        for qs in request.query_params._list:
            if qs[0] == 'status':
                filter_status.append(qs[1])

        context = {
            'filter_documentation': {'level': filter_status},
            'filter_status': filter_status,
            'fss': fss,
            'error_log_count': error_log_count}
        return self.render(request, context)


class Fiscofs(BaseHTTPEndpoint):
    @property
    def api_url(self):
        return settings.API_URL

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'fiscofs.pt'

    @requires(constants.scope_client, redirect='login')
    async def get(self, request: Request):
        error_log_count = await self.get_from_api(request, f'{settings.API_URL}/clients/users/profile/unread-log-count')
        filter_dsn = []
        filter_status = []

        additional_qs = ''
        for qs in request.query_params._list:
            if qs[0] == 'dsn':
                filter_dsn.append(qs[1])
                additional_qs = f'{additional_qs}dsn={qs[1]}&'

        for qs in request.query_params._list:
            if qs[0] == 'status':
                filter_status.append(qs[1])

        query, page, limit, content, prev_page, next_page = await self.get_result_list(
            request, f"/clients/data/{request.path_params['id_fsc']}/fiscofs", additional_qs)

        context = {
            'filters': {'dsn': filter_dsn},
            'filter_dsn': filter_dsn,
            'filter_documentation': {'level': filter_status},
            'filter_status': filter_status,
            'content': content,
            'query': query,
            'page': page,
            'limit': limit,
            'prev_page': prev_page,
            'next_page': next_page,
            'snackbar': self.get_snackbar(request, 'action_successful'),
            'error_log_count': error_log_count
        }
        return self.render(request, context)


class Documents(BaseHTTPEndpoint):
    @property
    def api_url(self):
        return settings.API_URL

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'documents.pt'

    @requires(constants.scope_client, redirect='login')
    async def get(self, request: Request):
        error_log_count = await self.get_from_api(request, f'{settings.API_URL}/clients/users/profile/unread-log-count')
        filter_dsn = []
        filter_status = []

        additional_qs = ''
        for qs in request.query_params._list:
            if qs[0] == 'dsn':
                filter_dsn.append(qs[1])
                additional_qs = f'{additional_qs}dsn={qs[1]}&'

        for qs in request.query_params._list:
            if qs[0] == 'status':
                filter_status.append(qs[1])

        query, page, limit, content, prev_page, next_page = await self.get_result_list(
            request, f"/clients/data/{request.path_params['id_fsc']}/documents", additional_qs)

        context = {
            'filter_documentation': {'level': filter_status},
            'filter_status': filter_status,
            'filters': {'dsn': filter_dsn},
            'filter_dsn': filter_dsn,
            'content': content,
            'query': query,
            'page': page,
            'limit': limit,
            'prev_page': prev_page,
            'next_page': next_page,
            'snackbar': self.get_snackbar(request, 'action_successful'),
            'error_log_count': error_log_count
        }
        return self.render(request, context)


class OnlineDocuments(BaseHTTPEndpoint):
    @property
    def api_url(self):
        return settings.API_URL

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'online_documents.pt'

    @requires(constants.scope_client, redirect='login')
    async def get(self, request: Request):
        error_log_count = await self.get_from_api(request, f'{settings.API_URL}/clients/users/profile/unread-log-count')
        filter_dsn = []
        filter_status = []

        additional_qs = ''
        for qs in request.query_params._list:
            if qs[0] == 'dsn':
                filter_dsn.append(qs[1])
                additional_qs = f'{additional_qs}dsn={qs[1]}&'

        for qs in request.query_params._list:
            if qs[0] == 'status':
                filter_status.append(qs[1])

        query, page, limit, content, prev_page, next_page = await self.get_result_list(
            request, f"/clients/data/{request.path_params['id_fsc']}/online-documents", additional_qs)

        context = {
            'filter_documentation': {'level': filter_status},
            'filter_status': filter_status,
            'filters': {'dsn': filter_dsn},
            'filter_dsn': filter_dsn,
            'content': content,
            'query': query,
            'page': page,
            'limit': limit,
            'prev_page': prev_page,
            'next_page': next_page,
            'snackbar': self.get_snackbar(request, 'action_successful'),
            'error_log_count': error_log_count
        }
        return self.render(request, context)


class ViewFiscof(ClientFormEndpoint):
    id_fsc = None
    id_fiscof = None

    @property
    def method(self):
        return None

    @property
    def action_successful_message(self):
        return None

    def redirect_url(self, request: Request, data: dict = None):
        return None

    @property
    def api_url(self):
        return f'{settings.API_URL}/clients/data/{self.id_fsc}/fiscofs/{self.id_fiscof}'

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'fiscof_card.pt'

    async def get_context(self, request: Request):
        content = await self.get_from_api(request, self.api_url)
        error_log_count = await self.get_from_api(request, f'{settings.API_URL}/clients/users/profile/unread-log-count')
        filter_status = []

        for qs in request.query_params._list:
            if qs[0] == 'status':
                filter_status.append(qs[1])

        return {
            'filter_documentation': {'level': filter_status},
            'filter_status': filter_status,
            'content': content,
            'error_log_count': error_log_count
        }

    @requires(constants.scope_client, redirect='login')
    async def get(self, request: Request):
        self.id_fsc = request.path_params['id_fsc']
        self.id_fiscof = request.path_params['id_fiscof']
        return await super(ViewFiscof, self).get(request)


class ViewDocument(ClientFormEndpoint):
    id_fsc = None
    id_doc = None

    @property
    def method(self):
        return None

    @property
    def action_successful_message(self):
        return None

    def redirect_url(self, request: Request, data: dict = None):
        return None

    @property
    def api_url(self):
        return f'{settings.API_URL}/clients/data/{self.id_fsc}/documents/{self.id_doc}'

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'document_card.pt'

    async def get_context(self, request: Request):
        content = await self.get_from_api(request, self.api_url)
        error_log_count = await self.get_from_api(request, f'{settings.API_URL}/clients/users/profile/unread-log-count')
        filter_status = []

        for qs in request.query_params._list:
            if qs[0] == 'status':
                filter_status.append(qs[1])

        return {
            'filter_documentation': {'level': filter_status},
            'filter_status': filter_status,
            'content': content,
            'error_log_count': error_log_count
        }

    @requires(constants.scope_client, redirect='login')
    async def get(self, request: Request):
        self.id_fsc = request.path_params['id_fsc']
        self.id_doc = request.path_params['id_doc']
        return await super(ViewDocument, self).get(request)


class ViewOnlineDocument(ClientFormEndpoint):
    id_fsc = None
    id_doc = None

    @property
    def method(self):
        return None

    @property
    def action_successful_message(self):
        return None

    def redirect_url(self, request: Request, data: dict = None):
        return None

    @property
    def api_url(self):
        return f'{settings.API_URL}/clients/data/{self.id_fsc}/online-documents/{self.id_doc}'

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'online_document_card.pt'

    async def get_context(self, request: Request):
        content = await self.get_from_api(request, self.api_url)
        error_log_count = await self.get_from_api(request, f'{settings.API_URL}/clients/users/profile/unread-log-count')
        filter_status = []

        for qs in request.query_params._list:
            if qs[0] == 'status':
                filter_status.append(qs[1])

        return {
            'filter_documentation': {'level': filter_status},
            'filter_status': filter_status,
            'content': content,
            'error_log_count': error_log_count
        }

    @requires(constants.scope_client, redirect='login')
    async def get(self, request: Request):
        self.id_fsc = request.path_params['id_fsc']
        self.id_doc = request.path_params['id_doc']
        return await super(ViewOnlineDocument, self).get(request)


class Units(BaseHTTPEndpoint):
    @property
    def api_url(self):
        return settings.API_URL

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'units.pt'

    @requires(constants.scope_client, redirect='login')
    async def get(self, request: Request):
        error_log_count = await self.get_from_api(request, f'{settings.API_URL}/clients/users/profile/unread-log-count')
        filter_status = []

        for qs in request.query_params._list:
            if qs[0] == 'status':
                filter_status.append(qs[1])

        fsc_url = f"{settings.API_URL}/clients/foreign-systems/" \
                  f"other/connections/{request.path_params['id_fsc']}"
        fsc = await self.get_from_api(request, fsc_url)

        query, page, limit, content, prev_page, next_page = await self.get_result_list(
            request, f"/clients/data/{request.path_params['id_fsc']}/units")

        context = {
            'filter_documentation': {'level': filter_status},
            'filter_status': filter_status,
            'fsc': fsc,
            'content': content,
            'query': query,
            'page': page,
            'limit': limit,
            'prev_page': prev_page,
            'next_page': next_page,
            'snackbar': self.get_snackbar(request, 'action_successful'),
            'error_log_count': error_log_count
        }
        return self.render(request, context)


class ViewUnit(ClientFormEndpoint):
    id_fsc = None
    id_unit = None

    @property
    def method(self):
        return None

    @property
    def action_successful_message(self):
        return None

    def redirect_url(self, request: Request, data: dict = None):
        return None

    @property
    def api_url(self):
        return f'{settings.API_URL}/clients/data/{self.id_fsc}/units/{self.id_unit}'

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'unit_card.pt'

    async def get_context(self, request: Request):
        versions = await self.get_from_api(request, f"{self.api_url}/versions")
        content = await self.get_from_api(request, self.api_url)
        fsc_url = f'{settings.API_URL}/clients/foreign-systems/other/connections/{self.id_fsc}'
        fsc = await self.get_from_api(request, fsc_url)
        error_log_count = await self.get_from_api(request, f'{settings.API_URL}/clients/users/profile/unread-log-count')
        filter_status = []

        for qs in request.query_params._list:
            if qs[0] == 'status':
                filter_status.append(qs[1])

        return {
            'filter_documentation': {'level': filter_status},
            'filter_status': filter_status,
            'fsc': fsc,
            'versions': versions,
            'content': content,
            'error_log_count': error_log_count
        }

    @requires(constants.scope_client, redirect='login')
    async def get(self, request: Request):
        self.id_fsc = request.path_params['id_fsc']
        self.id_unit = request.path_params['id_unit']
        return await super(ViewUnit, self).get(request)


class ViewUnitVersion(ViewUnit):
    id_fsc = None
    id_unit = None
    id_version = None

    @property
    def tpl(self) -> str:
        return 'unit_version_card.pt'

    async def get_context(self, request: Request):
        content = await self.get_from_api(request, f"{self.api_url}/versions/{self.id_version}")

        return {
            'content': content,
        }

    @requires(constants.scope_client, redirect='login')
    async def get(self, request: Request):
        self.id_fsc = request.path_params['id_fsc']
        self.id_unit = request.path_params['id_unit']
        self.id_version = request.path_params['id_version']
        return await super(ViewUnitVersion, self).get(request)


# Tax categories
class TaxCategories(BaseHTTPEndpoint):
    @property
    def api_url(self):
        return settings.API_URL

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'tax_categories.pt'

    @requires(constants.scope_client, redirect='login')
    async def get(self, request: Request):
        error_log_count = await self.get_from_api(request, f'{settings.API_URL}/clients/users/profile/unread-log-count')
        filter_status = []

        for qs in request.query_params._list:
            if qs[0] == 'status':
                filter_status.append(qs[1])

        fsc_url = f"{settings.API_URL}/clients/foreign-systems/" \
                  f"other/connections/{request.path_params['id_fsc']}"
        fsc = await self.get_from_api(request, fsc_url)

        query, page, limit, content, prev_page, next_page = await self.get_result_list(
            request, f"/clients/data/{request.path_params['id_fsc']}/tax-categories")

        context = {
            'filter_documentation': {'level': filter_status},
            'filter_status': filter_status,
            'fsc': fsc,
            'content': content,
            'query': query,
            'page': page,
            'limit': limit,
            'prev_page': prev_page,
            'next_page': next_page,
            'snackbar': self.get_snackbar(request, 'action_successful'),
            'error_log_count': error_log_count
        }
        return self.render(request, context)


class ViewTaxCategory(ClientFormEndpoint):
    id_fsc = None
    id_tax_category = None

    @property
    def method(self):
        return None

    @property
    def action_successful_message(self):
        return None

    def redirect_url(self, request: Request, data: dict = None):
        return None

    @property
    def api_url(self):
        return f'{settings.API_URL}/clients/data/{self.id_fsc}/tax-categories/{self.id_tax_category}'

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'tax_category_card.pt'

    async def get_context(self, request: Request):
        versions = await self.get_from_api(request, f"{self.api_url}/versions")
        content = await self.get_from_api(request, self.api_url)
        fsc_url = f'{settings.API_URL}/clients/foreign-systems/other/connections/{self.id_fsc}'
        fsc = await self.get_from_api(request, fsc_url)
        error_log_count = await self.get_from_api(request, f'{settings.API_URL}/clients/users/profile/unread-log-count')

        filter_status = []

        for qs in request.query_params._list:
            if qs[0] == 'status':
                filter_status.append(qs[1])

        return {
            'filter_documentation': {'level': filter_status},
            'filter_status': filter_status,
            'fsc': fsc,
            'versions': versions,
            'content': content,
            'error_log_count': error_log_count
        }

    @requires(constants.scope_client, redirect='login')
    async def get(self, request: Request):
        self.id_fsc = request.path_params['id_fsc']
        self.id_tax_category = request.path_params['id_tax_category']
        return await super(ViewTaxCategory, self).get(request)


class ViewTaxCategoryVersion(ViewTaxCategory):
    id_fsc = None
    id_tax_category = None
    id_version = None

    @property
    def tpl(self) -> str:
        return 'tax_category_version_card.pt'

    async def get_context(self, request: Request):
        content = await self.get_from_api(request, f"{self.api_url}/versions/{self.id_version}")

        return {
            'content': content,
        }

    @requires(constants.scope_client, redirect='login')
    async def get(self, request: Request):
        self.id_fsc = request.path_params['id_fsc']
        self.id_tax_category = request.path_params['id_tax_category']
        self.id_version = request.path_params['id_version']
        return await super(ViewTaxCategoryVersion, self).get(request)


# Products
class Products(BaseHTTPEndpoint):
    @property
    def api_url(self):
        return settings.API_URL

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'products.pt'

    @requires(constants.scope_client, redirect='login')
    async def get(self, request: Request):
        error_log_count = await self.get_from_api(request, f'{settings.API_URL}/clients/users/profile/unread-log-count')

        filter_status = []

        for qs in request.query_params._list:
            if qs[0] == 'status':
                filter_status.append(qs[1])

        fsc_url = f"{settings.API_URL}/clients/foreign-systems/" \
                  f"other/connections/{request.path_params['id_fsc']}"
        fsc = await self.get_from_api(request, fsc_url)

        query, page, limit, content, prev_page, next_page = await self.get_result_list(
            request, f"/clients/data/{request.path_params['id_fsc']}/products")

        context = {
            'filter_documentation': {'level': filter_status},
            'filter_status': filter_status,
            'fsc': fsc,
            'content': content,
            'query': query,
            'page': page,
            'limit': limit,
            'prev_page': prev_page,
            'next_page': next_page,
            'snackbar': self.get_snackbar(request, 'action_successful'),
            'error_log_count': error_log_count
        }
        return self.render(request, context)


class ViewProduct(ClientFormEndpoint):
    id_fsc = None
    id_product = None

    @property
    def method(self):
        return None

    @property
    def action_successful_message(self):
        return None

    def redirect_url(self, request: Request, data: dict = None):
        return None

    @property
    def api_url(self):
        return f'{settings.API_URL}/clients/data/{self.id_fsc}/products/{self.id_product}'

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'product_card.pt'

    async def get_context(self, request: Request):
        versions = await self.get_from_api(request, f"{self.api_url}/versions")
        content = await self.get_from_api(request, self.api_url)
        fsc_url = f'{settings.API_URL}/clients/foreign-systems/other/connections/{self.id_fsc}'
        fsc = await self.get_from_api(request, fsc_url)
        error_log_count = await self.get_from_api(request, f'{settings.API_URL}/clients/users/profile/unread-log-count')

        filter_status = []

        for qs in request.query_params._list:
            if qs[0] == 'status':
                filter_status.append(qs[1])

        return {
            'filter_documentation': {'level': filter_status},
            'filter_status': filter_status,
            'fsc': fsc,
            'versions': versions,
            'content': content,
            'error_log_count': error_log_count
        }

    @requires(constants.scope_client, redirect='login')
    async def get(self, request: Request):
        self.id_fsc = request.path_params['id_fsc']
        self.id_product = request.path_params['id_product']
        return await super(ViewProduct, self).get(request)


class ViewProductVersion(ViewProduct):
    id_fsc = None
    id_product = None
    id_version = None

    @property
    def tpl(self) -> str:
        return 'product_version_card.pt'

    async def get_context(self, request: Request):
        content = await self.get_from_api(request, f"{self.api_url}/versions/{self.id_version}")

        return {
            'content': content,
        }

    @requires(constants.scope_client, redirect='login')
    async def get(self, request: Request):
        self.id_fsc = request.path_params['id_fsc']
        self.id_product = request.path_params['id_product']
        self.id_version = request.path_params['id_version']
        return await super(ViewProductVersion, self).get(request)


#  OnlineOrders
class OnlineOrders(BaseHTTPEndpoint):
    @property
    def api_url(self):
        return settings.API_URL

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'online_orders.pt'

    @requires(constants.scope_client, redirect='login')
    async def get(self, request: Request):
        error_log_count = await self.get_from_api(request, f'{settings.API_URL}/clients/users/profile/unread-log-count')
        filter_status = []

        for qs in request.query_params._list:
            if qs[0] == 'status':
                filter_status.append(qs[1])

        fsc_url = f"{settings.API_URL}/clients/foreign-systems/" \
                  f"other/connections/{request.path_params['id_fsc']}"
        fsc = await self.get_from_api(request, fsc_url)

        query, page, limit, content, prev_page, next_page = await self.get_result_list(
            request, f"/clients/data/{request.path_params['id_fsc']}/online-orders")

        context = {
            'fsc': fsc,
            'content': content,
            'query': query,
            'page': page,
            'limit': limit,
            'prev_page': prev_page,
            'next_page': next_page,
            'snackbar': self.get_snackbar(request, 'action_successful'),
            'error_log_count': error_log_count,
            'filter_documentation': {'level': filter_status},
            'filter_status': filter_status,
        }
        return self.render(request, context)


class ViewOnlineOrder(ClientFormEndpoint):
    id_fsc = None
    id_online_order = None

    @property
    def method(self):
        return None

    @property
    def action_successful_message(self):
        return None

    def redirect_url(self, request: Request, data: dict = None):
        return None

    @property
    def api_url(self):
        return f'{settings.API_URL}/clients/data/{self.id_fsc}/online-orders/{self.id_online_order}'

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'online_order_card.pt'

    async def get_context(self, request: Request):
        versions = None  # await self.get_from_api(request, f"{self.api_url}/versions")
        content = await self.get_from_api(request, self.api_url)
        fsc_url = f'{settings.API_URL}/clients/foreign-systems/other/connections/{self.id_fsc}'
        fsc = await self.get_from_api(request, fsc_url)
        error_log_count = await self.get_from_api(request, f'{settings.API_URL}/clients/users/profile/unread-log-count')
        filter_status = []

        for qs in request.query_params._list:
            if qs[0] == 'status':
                filter_status.append(qs[1])

        return {
            'fsc': fsc,
            'versions': versions,
            'content': content,
            'products': json.loads(content['products']),
            'error_log_count': error_log_count,
            'filter_documentation': {'level': filter_status},
            'filter_status': filter_status,
        }

    @requires(constants.scope_client, redirect='login')
    async def get(self, request: Request):
        self.id_fsc = request.path_params['id_fsc']
        self.id_online_order = request.path_params['id_online_order']
        return await super(ViewOnlineOrder, self).get(request)
