from typing import Dict

import aiohttp

import ujson as json
from aiohttp import ClientConnectorError
from starlette.authentication import requires
from starlette.responses import RedirectResponse

from integrator.helpers import constants
from integrator.helpers.endpoints import BaseHTTPEndpoint
from integrator.helpers.http_client import post

from integrator.apps.clients import settings


class Login(BaseHTTPEndpoint):
    @property
    def api_url(self):
        return settings.API_URL

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'login.pt'

    async def get_context(self, request) -> Dict:
        context = {
            'title': 'Integrators',
            'form_title': 'Integrators',
        }

        return context

    async def get(self, request):
        return self.render(request, await self.get_context(request))

    async def post(self, request):
        form_data = await request.form()

        if form_data.get('id_organization', None):
            result = await self.send_data(request, {}, 'PUT',  f"{self.api_url}/system/security/session_user_organization?id_organization={form_data['id_organization']}")
            return RedirectResponse(url=request.url_for('dashboard'), status_code=302)
        else:
            headers = {
                'accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded'
            }

            payload = {'grant_type': '',
                       'username': form_data['email'],
                       'password': form_data['password'],
                       'scope': '',
                       'client_id': '',
                       'client_secret': ''}

            url = f'{settings.API_URL}/system/security/token'
            async with aiohttp.ClientSession() as session:
                try:
                    data = await post(session, url, payload, headers=headers)
                    if data:
                        request.session[settings.SESSION_KEY] = json.loads(data)

                        organizations = await self.get_from_api(request, f'{self.api_url}/system/security/session_user_organizations')

                        if len(organizations) == 1:
                            result = await self.send_data(request, {}, 'PUT',
                                f"{self.api_url}/system/security/session_user_organization?id_organization={organizations[0]['id_organization']}")

                            return RedirectResponse(url=request.url_for('dashboard'), status_code=302)
                        else:
                            return self.render(request, {'organizations': organizations}, tpl='set_session_organization.pt')

                    else:
                        context = await self.get_context(request)
                        context['error'] = True
                        context['error_message'] = 'Nepareizi pieejas dati'
                except ClientConnectorError:
                    context = {'error': True, 'error_message': 'Radās kļūda - pēc neliela brīža mēģiniet vēlreiz.'}

            return self.render(request, context)


@requires(constants.scope_manager, redirect='login')
async def logout(request):
    del request.session[settings.SESSION_KEY]
    return RedirectResponse(url=request.url_for('login'))


class TR(BaseHTTPEndpoint):
    async def post(self, request):
        b = await request.body()
        return 1
