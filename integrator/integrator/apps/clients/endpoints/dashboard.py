from starlette.authentication import requires

from integrator.apps.clients import settings
from integrator.helpers import constants

from integrator.helpers.endpoints import BaseHTTPEndpoint


class Dashboard(BaseHTTPEndpoint):
    @property
    def api_url(self):
        return settings.API_URL

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'dashboard.pt'

    @requires(constants.scope_client, redirect='login')
    async def get(self, request):
        error_log_count = await self.get_from_api(request, f'{settings.API_URL}/clients/users/profile/unread-log-count')
        filter_status = []

        for qs in request.query_params._list:
            if qs[0] == 'status':
                filter_status.append(qs[1])

        context = {
            'filter_documentation': {'level': filter_status},
            'filter_status': filter_status,
            'error_log_count': error_log_count}
        return self.render(request, context)
