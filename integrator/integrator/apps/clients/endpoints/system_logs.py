from starlette.authentication import requires
from starlette.requests import Request

from integrator.apps.clients import settings
from integrator.helpers import constants

from integrator.helpers.endpoints import (
    BaseHTTPEndpoint,
    ClientFormEndpoint
)


class SystemLogs(BaseHTTPEndpoint):
    @property
    def api_url(self):
        return settings.API_URL

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'system_logs.pt'

    @requires(constants.scope_client, redirect='login')
    async def get(self, request: Request):
        error_log_count = await self.get_from_api(request, f'{settings.API_URL}/clients/users/profile/unread-log-count')
        filter_level = []
        filter_status = []

        for qs in request.query_params._list:
            if qs[0] == 'status':
                filter_status.append(qs[1])

        additional_qs = ''
        for qs in request.query_params._list:
            if qs[0] == 'level':
                filter_level.append(qs[1])
                additional_qs = f'{additional_qs}level={qs[1]}&'

        query, page, limit, content, prev_page, next_page = await self.get_result_list(request, '/clients/logs/', additional_qs)

        context = {
            'filters': {'level': filter_level},
            'filter_level': filter_level,
            'content': content,
            'query': query,
            'page': page,
            'limit': limit,
            'prev_page': prev_page,
            'next_page': next_page,
            'snackbar': self.get_snackbar(request, 'action_successful'),
            'error_log_count': error_log_count,
            'filter_documentation': {'level': filter_status},
        }
        return self.render(request, context)


class ViewSystemLog(ClientFormEndpoint):
    id_system_log = None

    @property
    def method(self):
        return None

    @property
    def action_successful_message(self):
        return None

    def redirect_url(self, request: Request, data: dict = None):
        return None

    @property
    def api_url(self):
        return f'{settings.API_URL}/clients/logs/{self.id_system_log}/'

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'system_log_card.pt'

    async def get_context(self, request: Request):
        content = await self.get_from_api(request, self.api_url)
        error_log_count = await self.get_from_api(request, f'{settings.API_URL}/clients/users/profile/unread-log-count')
        filter_status = []

        for qs in request.query_params._list:
            if qs[0] == 'status':
                filter_status.append(qs[1])

        return {
            'content': content,
            'form_errors': None,
            'snackbar': self.get_snackbar(request, 'action_successful'),
            'form_type': None,
            'error_log_count': error_log_count,
            'filter_documentation': {'level': filter_status},
            'filter_status': filter_status,
        }

    @requires(constants.scope_client, redirect='login')
    async def get(self, request: Request):
        self.id_system_log = request.path_params['id_system_log']
        return await super(ViewSystemLog, self).get(request)
