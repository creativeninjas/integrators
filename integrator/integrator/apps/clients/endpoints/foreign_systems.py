import asyncio

from starlette.authentication import requires
from starlette.requests import Request
from starlette.responses import RedirectResponse

from integrator.apps.clients import settings
from integrator.helpers import constants

from integrator.helpers.endpoints import BaseHTTPEndpoint, ClientFormEndpoint, ClientDeleteEndpoint


class ForeignSystems(BaseHTTPEndpoint):
    @property
    def api_url(self):
        return settings.API_URL

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'foreign_systems.pt'

    @requires(constants.scope_client, redirect='login')
    async def get(self, request):
        fs = await self.get_from_api(request, f'{settings.API_URL}/clients/foreign-systems/')

        error_log_count = await self.get_from_api(request, f'{settings.API_URL}/clients/users/profile/unread-log-count')
        filter_status = []

        for qs in request.query_params._list:
            if qs[0] == 'status':
                filter_status.append(qs[1])

        context = {
            'filter_documentation': {'level': filter_status},
            'filter_status': filter_status,
            'foreign_systems': fs,
            'snackbar': self.get_snackbar(request, 'action_successful'),
            'error_log_count': error_log_count}
        return self.render(request, context)


class ForeignSystemConnections(BaseHTTPEndpoint):
    id_foreign_system = None

    @property
    def api_url(self):
        return settings.API_URL

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'foreign_system_connections.pt'

    @requires(constants.scope_client, redirect='login')
    async def get(self, request: Request):
        error_log_count = await self.get_from_api(request, f'{settings.API_URL}/clients/users/profile/unread-log-count')
        filter_status = []

        for qs in request.query_params._list:
            if qs[0] == 'status':
                filter_status.append(qs[1])

        self.id_foreign_system = request.path_params['id_foreign_system']

        foreign_system = await self.get_from_api(
            request, f'{self.api_url}/clients/foreign-systems/{self.id_foreign_system}')

        query, page, limit, content, prev_page, next_page = await self.get_result_list(
            request, f'/clients/foreign-systems/{self.id_foreign_system}/connections')

        if foreign_system['id'] == 'tiki-taka' and content is None:
            return RedirectResponse(request.url_for('add-foreign-system-connection', id_foreign_system=foreign_system['id']))
        elif foreign_system['id'] == 'tiki-taka' and len(content) == 1:
            return RedirectResponse(
                request.url_for('update-foreign-system-connection', id_foreign_system=foreign_system['id'], object_id=content[0]['id']))

        context = {
            'filter_documentation': {'level': filter_status},
            'filter_status': filter_status,
            'foreign_system': foreign_system,
            'content': content,
            'query': query,
            'page': page,
            'limit': limit,
            'prev_page': prev_page,
            'next_page': next_page,
            'snackbar': self.get_snackbar(request, 'action_successful'),
            'error_log_count': error_log_count
        }
        return self.render(request, context)


class AddForeignSystemConnection(ClientFormEndpoint):
    id_foreign_system = None

    @property
    def method(self):
        return 'POST'

    @property
    def action_successful_message(self):
        return 'Arējās sistēmas savienojums pievienots'

    def redirect_url(self, request: Request, data: dict = None):
        return request.url_for('foreign-system-connections', id_foreign_system=self.id_foreign_system)

    @property
    def api_url(self):
        return f'{settings.API_URL}/clients/foreign-systems/{self.id_foreign_system}/connections/'

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'foreign_system_connection_card.pt'

    async def get_context(self, request: Request):
        foreign_system = await self.get_from_api(request, f'{settings.API_URL}/clients/foreign-systems/{self.id_foreign_system}')

        if request.method == 'POST':
            form_data = await request.form()
            content = {
                'id': form_data['id'],
                'title': form_data['title'],
                'address': form_data['address'],
                'pwd': form_data['pwd'],
                'username': form_data['username'],
            }
        else:
            content = {
                'id': 0, 'title': None, 'address': None, 'username': None, 'pwd': None}
        return {
            'foreign_system': foreign_system,
            'content': content,
            'form_errors': None,
            'snackbar': self.get_snackbar(request, 'action_successful'),
            'form_type': 'ADD',
            'error_log_count': await self.get_from_api(
                request, f'{settings.API_URL}/clients/users/profile/unread-log-count'),
            'filter_documentation': {'level': self.get_documentation(request)}
        }

    @requires(constants.scope_client, redirect='login')
    async def get(self, request: Request):
        self.id_foreign_system = request.path_params['id_foreign_system']

        return await super(AddForeignSystemConnection, self).get(request)

    @requires(constants.scope_client, redirect='login')
    async def post(self, request: Request):
        self.id_foreign_system = request.path_params['id_foreign_system']

        return await super(AddForeignSystemConnection, self).post(request)


class UpdateForeignSystemConnection(ClientFormEndpoint):
    id_foreign_system_connection = None
    id_foreign_system = None

    @property
    def method(self):
        return 'PUT'

    @property
    def action_successful_message(self):
        return 'Arējās sistēmas savienojums izmainīts'

    def redirect_url(self, request: Request, data: dict = None):
        return request.url_for('update-foreign-system-connection', id_foreign_system=self.id_foreign_system, object_id=data['id'])

    @property
    def api_url(self):
        return f'{settings.API_URL}/clients/foreign-systems/{self.id_foreign_system}/connections/{self.id_foreign_system_connection}'

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @property
    def tpl(self) -> str:
        return 'foreign_system_connection_card.pt'

    async def get_context(self, request: Request):
        await asyncio.sleep(1)

        foreign_system = await self.get_from_api(
            request, f'{settings.API_URL}/clients/foreign-systems/{self.id_foreign_system}')

        filter_status = []

        for qs in request.query_params._list:
            if qs[0] == 'status':
                filter_status.append(qs[1])

        if request.method == 'POST':
            form_data = await request.form()
            content = {
                'id': form_data['id'],
                'title': form_data['title'],
                'address': form_data['address'],
                'pwd': form_data['pwd'],
                'username': form_data['username'],
                'available': None,
                'unavailability_error': None
            }
        else:
            content = await self.get_from_api(request, self.api_url)
        return {
            'filter_documentation': {'level': filter_status},
            'filter_status': filter_status,
            'foreign_system': foreign_system,
            'content': content,
            'form_errors': None,
            'snackbar': self.get_snackbar(request, 'action_successful'),
            'form_type': 'UPDATE',
            'error_log_count': await self.get_from_api(
                request, f'{settings.API_URL}/clients/users/profile/unread-log-count')
        }

    @requires(constants.scope_client, redirect='login')
    async def get(self, request: Request):
        self.id_foreign_system_connection = request.path_params['object_id']
        self.id_foreign_system = request.path_params['id_foreign_system']

        return await super(UpdateForeignSystemConnection, self).get(request)

    @requires(constants.scope_client, redirect='login')
    async def post(self, request: Request):
        self.id_foreign_system_connection = request.path_params['object_id']
        self.id_foreign_system = request.path_params['id_foreign_system']

        return await super(UpdateForeignSystemConnection, self).post(request)


class DeleteForeignSystemConnection(ClientDeleteEndpoint):
    id_foreign_system = None

    @property
    def tpl(self):
        return ''

    @property
    def action_successful_message(self):
        return 'Ārējās sistēmas savienojums izdzēsts'

    def redirect_url(self, request: Request, result: bool):
        if result or self.list_action:
            return request.url_for('foreign-system-connections', id_foreign_system=self.id_foreign_system)
        else:
            return request.url_for(
                'update-foreign-system-connection', id_foreign_system=self.id_foreign_system, object_id=self.object_id)

    @property
    def api_url(self):
        return f'{settings.API_URL}/clients/foreign-systems/{self.id_foreign_system}/connections/{self.object_id}'

    @property
    def session_key(self):
        return settings.SESSION_KEY

    @requires(constants.scope_client, redirect='login')
    async def get(self, request: Request):
        self.id_foreign_system = request.path_params['id_foreign_system']

        return await super(DeleteForeignSystemConnection, self).get(request)
