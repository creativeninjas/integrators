STATIC_DIR = '/Development/Integrator/integrator/integrator/apps/clients/static'
API_URL = 'http://127.0.0.1:8080'

API_ME_URL = '/system/security/organization_session_user'

SESSION_SECRET = '661f877266573d837d53c1895975c398b2c5dc5b06a1cc4b2e6d91f7b2cda04e'
SESSION_COOKIE = 'client_session'
SESSION_HTTPS_ONLY = False

DEBUG = True

SESSION_KEY = 'client_token'