from datetime import timedelta, datetime
from urllib.request import Request

import jwt

import aiohttp

import ujson as json


from starlette.authentication import (
    AuthenticationBackend,
    AuthCredentials
)

from integrator.helpers.http_client import get


from integrator.security.models import SessionUser


def token_headers(request: Request, session_key: str) -> dict:
    token_data = request.session.get(session_key, None)
    headers = {}
    headers['accept'] = 'application/json'
    if token_data:
        headers['Authorization'] = f" {token_data['token_type']} {token_data['access_token']}"
    else:
        headers['Authorization'] = f" bearer "

    return headers


# def create_access_token(*, data: dict, secret_key: str, algorithm: str, expires_delta: timedelta = None):
#     to_encode = data.copy()
#     if expires_delta:
#         expire = datetime.utcnow() + expires_delta
#     else:
#         expire = datetime.utcnow() + timedelta(minutes=15)
#     to_encode.update({'exp': expire})
#     encoded_jwt = jwt.encode(to_encode, secret_key, algorithm=algorithm)
#     return encoded_jwt


class AuthBackend(AuthenticationBackend):

    def __init__(self, session_key: str, api_address: str, api_url: str):
        self.session_key = session_key
        self.api_address = api_address
        self.api_url = api_url

    async def authenticate(self, request):
        token_data = request.session.get(self.session_key, None)
        if token_data is None:
            return
        else:
            async with aiohttp.ClientSession() as session:
                headers = token_headers(request, self.session_key)
                try:
                    data = await get(session, f'{self.api_address}{self.api_url}', headers=headers)
                except Exception:
                    return

                if data:
                    return AuthCredentials(token_data['scopes']), SessionUser(**json.loads(data))
                else:
                    del request.session[self.session_key]
                    return