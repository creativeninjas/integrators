from typing import List

import uuid

from pydantic import BaseModel


class Token(BaseModel):
    access_token: str
    token_type: str
    scopes: List[str]


class TokenData(BaseModel):
    db_token: uuid.UUID = None
    scopes: List[str] = []


class SessionUser(BaseModel):
    id: int
    email: str
    first_name: str
    last_name: str
    manager: bool
    id_organization: int = None
