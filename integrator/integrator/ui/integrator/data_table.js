import {initBase} from './base.js';
import {initPagination} from './pagination.js';
import {ConfirmDialog} from './dialogs.js';

import {MDCMenu} from '@material/menu';
import {MDCSnackbar} from '@material/snackbar';
import {MDCList} from '@material/list';
import {MDCTextField} from '@material/textfield';
import {MDCChipSet} from '@material/chips';

initBase();
initPagination();

const btnsMore = document.querySelectorAll('.item-menu');
for (let btn of btnsMore) {
    btn.addEventListener('click', (event) => {
        const menu = new MDCMenu(document.getElementById(btn.value));
        let confirmReset = new ConfirmDialog('#rm-btn-'+btn.value, '.mdc-confirm-dialog');
        let additionalConfirmReset = new ConfirmDialog('#ad-btn-' + btn.value, '.mdc-confirm-dialog-additional');

        menu.setFixedPosition(true);
        menu.open = true;
    });
}

const snackbarEl = document.querySelector(".mdc-snackbar");
if(snackbarEl) {
  const snackbar = new MDCSnackbar(snackbarEl);
  snackbar.open();
}

const btnFilter = document.getElementById("btn-filter");
const btnCloseFilter = document.getElementById("btn-close-filter");

if(document.getElementById("data-table-filter").className === "filter-open"){
    document.getElementById("main-content").className = "main-content main-content--with-filter";
}

if (btnFilter) {
    btnFilter.addEventListener('click', (event) => {
        if(document.getElementById("data-table-filter").className === "filter-open"){
            document.getElementById("data-table-wrap").className = "content-wrap";
            document.getElementById("data-table-filter").className = "";

            document.getElementById("main-content").className = "main-content";
        }
        else{
            document.getElementById("data-table-wrap").className = "content-wrap with-filter";
            document.getElementById("data-table-filter").className = "filter-open";

            document.getElementById("main-content").className = "main-content main-content--with-filter";
        }
    });

    btnCloseFilter.addEventListener('click', (event) => {
        document.getElementById("data-table-wrap").className = "content-wrap";
        document.getElementById("data-table-filter").className = "";
        document.getElementById("main-content").className = "main-content";
    });
}

const list = new MDCList(document.querySelector('.mdc-list'));

class DSNFilter{
    constructor(element_id) {
        this.property = element_id;

        this.prepareInput();

        this.chipSetEl = document.getElementById('chip-set-'+this.property);
        this.chipSet = new MDCChipSet(this.chipSetEl);
    }

    prepareInput(){
        this.input = document.getElementById("inp_"+this.property);
        this.inpControl = new MDCTextField(document.getElementById(this.property));
        this.input.addEventListener('keydown', this.keydownEvent());
    }

    processInput(){
        if(!this.input.disabled){
            if(this.input.value !== null && this.input.value.trim().length > 0) {
                const chipEl = this.createChip(this.input.value.trim());
                this.chipSetEl.appendChild(chipEl);
                this.chipSet.addChip(chipEl);

                this.input.value = '';
            }
        }
    }

    keydownEvent(){
        return (event) => {
            if (event.key === 'Enter' || event.keyCode === 13) {
                event.preventDefault();
                this.processInput();
            }
        }
    }

    createChip(data){
        let tpl = document.getElementById("chip-tpl").innerHTML;
        tpl = tpl.replace('{title}', data);
        tpl = tpl.replace('{inp_value}', data);

        var div = document.createElement('div');
        div.innerHTML = tpl.trim();

        return div.firstChild;
    }
}

if(document.getElementById("filterDSN")) {
    const filter = new DSNFilter("filterDSN");
}