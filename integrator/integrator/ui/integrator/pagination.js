import {MDCMenu} from '@material/menu';


export function initPagination(){
    const dtPerPageMenu = document.querySelector('.mdc-menu-per-page');
    if(dtPerPageMenu){
        const menu = new MDCMenu(dtPerPageMenu);
        menu.setFixedPosition(true);
        const btnDTPerPage = document.getElementById("dt-btn-per-page");

        menu.listen('MDCMenu:selected', (e) => {
            document.getElementById("dt-pp").value = e.detail['item'].innerText;
            document.getElementById("dt-pagination-frm").submit();
        });

        btnDTPerPage.addEventListener('click', (event) => {
            menu.open = !menu.open;
        });
    }
}