const clientDashboard = 'https://integrators.tikitaka.lv/client/dashboard';
const clientWorks = 'https://integrators.tikitaka.lv/client/works';
const clientForeignSystems = 'https://integrators.tikitaka.lv/client/foreign-systems';
const clientData = 'https://integrators.tikitaka.lv/client/data/other';
const clientTikiTakaData = 'https://integrators.tikitaka.lv/client/data/tiki-taka';
const clientSystemLogs = 'https://integrators.tikitaka.lv/client/system-logs';
const clientProfile = 'https://integrators.tikitaka.lv/client/me';

const manageDashboard = 'https://integrators.tikitaka.lv/manage/dashboard';
const manageOrganizations = 'https://integrators.tikitaka.lv/manage/organizations';
const manageWorkServers = 'https://integrators.tikitaka.lv/manage/work-servers';
const manageUsers = 'https://integrators.tikitaka.lv/manage/users';
const manageTasks = 'https://integrators.tikitaka.lv/manage/tasks';
const manageSystemLogs = 'https://integrators.tikitaka.lv/manage/system-logs';
const manageProfile = 'https://integrators.tikitaka.lv/manage/me';

function fetchDocumentation(url) {
    fetch(url).then((response) => {
        let textValue = response.text().then((data) => {
            let dataTo = data.slice(data.indexOf('<section id="wiki-content"'))
            let htmlObject = document.getElementById('documentation-container')
            htmlObject.innerHTML = dataTo
        })
    })
}

function getCurrentURL() {
    return window.location.href
}

let currentUrl = getCurrentURL()

function selectDocumentation() {
    if (currentUrl === clientDashboard) {
        fetchDocumentation('https://bitbucket.org/creativeninjas/integrators/wiki/Lietot%C4%81ja%20rokasgr%C4%81mta/07%20-%20Darbagalds')
    } else if (currentUrl.startsWith(clientWorks)) {
        fetchDocumentation('https://bitbucket.org/creativeninjas/integrators/wiki/Lietot%C4%81ja%20rokasgr%C4%81mta/01%20-%20Darbu%20saraksts')
    } else if (currentUrl.startsWith(clientForeignSystems)) {
        fetchDocumentation('https://bitbucket.org/creativeninjas/integrators/wiki/Lietot%C4%81ja%20rokasgr%C4%81mta/02%20-%20Savienojumi')
    } else if (currentUrl.startsWith(clientData)) {
        fetchDocumentation('https://bitbucket.org/creativeninjas/integrators/wiki/Lietot%C4%81ja%20rokasgr%C4%81mta/03%20-%20%C4%80r%C4%93jo%20sist%C4%93mu%20dati')
    } else if (currentUrl.startsWith(clientTikiTakaData)) {
        fetchDocumentation('https://bitbucket.org/creativeninjas/integrators/wiki/Lietot%C4%81ja%20rokasgr%C4%81mta/04%20-%20TikiTaka%20dati')
    } else if (currentUrl.startsWith(clientSystemLogs)) {
        fetchDocumentation('https://bitbucket.org/creativeninjas/integrators/wiki/Lietot%C4%81ja%20rokasgr%C4%81mta/05%20-%20Pazi%C5%86ojumi')
    } else if (currentUrl.startsWith(clientProfile)) {
        fetchDocumentation('https://bitbucket.org/creativeninjas/integrators/wiki/Lietot%C4%81ja%20rokasgr%C4%81mta/06%20-%20Profils')
    } else if (currentUrl === manageDashboard) {
        fetchDocumentation('https://bitbucket.org/creativeninjas/integrators/wiki/Adminstratora%20rokasgr%C4%81mta/07%20-%20Darbagalds')
    } else if (currentUrl.startsWith(manageOrganizations)) {
        fetchDocumentation('https://bitbucket.org/creativeninjas/integrators/wiki/Adminstratora%20rokasgr%C4%81mta/01%20-%20Organiz%C4%81cija')
    } else if (currentUrl.startsWith(manageWorkServers)) {
        fetchDocumentation('https://bitbucket.org/creativeninjas/integrators/wiki/Adminstratora%20rokasgr%C4%81mta/04%20-%20Darba%20serveri')
    } else if (currentUrl.startsWith(manageUsers)) {
        fetchDocumentation('https://bitbucket.org/creativeninjas/integrators/wiki/Adminstratora%20rokasgr%C4%81mta/02%20-%20Lietot%C4%81ji')
    } else if (currentUrl.startsWith(manageTasks)) {
        fetchDocumentation('https://bitbucket.org/creativeninjas/integrators/wiki/Adminstratora%20rokasgr%C4%81mta/03%20-%20Uzdevumu%20saraksts')
    } else if (currentUrl.startsWith(manageSystemLogs)) {
        fetchDocumentation('https://bitbucket.org/creativeninjas/integrators/wiki/Adminstratora%20rokasgr%C4%81mta/05%20-%20Notikumu%20%C5%BEurn%C4%81ls')
    } else if (currentUrl.startsWith(manageProfile)) {
        fetchDocumentation('https://bitbucket.org/creativeninjas/integrators/wiki/Adminstratora%20rokasgr%C4%81mta/06%20-%20Profils')
    }
}

export {selectDocumentation}