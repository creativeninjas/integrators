import {MDCTopAppBar} from '@material/top-app-bar';
import {MDCDrawer} from "@material/drawer";
import {MDCList} from "@material/list";
import {MDCMenu} from '@material/menu';
import {MDCTextField} from '@material/textfield';


export function initBase() {
    const drawerElement = document.querySelector('.mdc-drawer');
    const drawer = MDCDrawer.attachTo(drawerElement);

    let is_mobile = window.matchMedia("(max-width: 1023px)").matches;
    drawer.open = !is_mobile;

    const topAppBar = MDCTopAppBar.attachTo(document.getElementById('app-bar'));
    topAppBar.setScrollTarget(document.getElementById('main-content'));
    topAppBar.listen('MDCTopAppBar:nav', () => {
        drawer.open = !drawer.open;
    });

    const appBarUserMenu = new MDCMenu(document.querySelector('.mdc-menu-options'));
    appBarUserMenu.open = false;


    const btnOptionsElem = document.getElementById("btn-options");
    btnOptionsElem.addEventListener('click', (event) => {
        appBarUserMenu.open = !appBarUserMenu.open;
    });

    const btnSearch = document.getElementById("btn-search");
    const btnCloseSearch = document.getElementById("btn-close-search");

    if (btnSearch && btnCloseSearch) {
        const textFieldSearch = new MDCTextField(document.querySelector('.search'));
        const searchInput = document.getElementById("search-field-input");
        const searchForm = document.getElementById("search-form");
        btnSearch.addEventListener('click', (event) => {
            document.getElementById("search_ctrl").className = "open";
            searchInput.focus();
        });

        btnCloseSearch.addEventListener('click', (event) => {
            document.getElementById("search_ctrl").className = "close";
            searchInput.value = "";
            searchForm.submit();
        });
    }
}