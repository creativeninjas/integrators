import {initBase} from './base.js';
import {MDCDialog} from '@material/dialog';
import {MDCChipSet} from '@material/chips';
import clickp from 'dom-helpers/src/event/clickp';

initBase();

function showVersion(e){
    const dialogId = "dialogVersion";
    const dialogVersion = new MDCDialog(document.getElementById(dialogId));
    dialogVersion.open();

    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            document.getElementById("version-dialog-content").innerHTML = this.responseText;
        }
    };
    xhttp.open("GET", e.target.href, true);
    xhttp.send();
}

const elCS = document.getElementById("ptcList");
if(elCS){
    const cs = new MDCChipSet(elCS);
}

clickp(".mdc-card-versions ", showVersion);

