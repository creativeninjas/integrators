function getWorkTask(){
    return {id: 0,
            id_work: 0,
            task_code: document.getElementById("addTaskId").value,
            foreign_system_connection: {id: document.getElementById("addTaskConnectionId").value, title: document.getElementById("addTaskConnectionTitle").value, username: "", pwd: "", address: ""},
            sequence_number: 0,
            attributes: null};
}

function noAttributes(){
    return getWorkTask();
}

function horizonImportFiscof(){
    let wt = getWorkTask();
    wt.attributes = {
        doc_type: document.querySelector("#doc_type input").value,
        warehouse_code: document.querySelector("#warehouse_code input").value,
        invoice_code: document.querySelector("#invoice_code input").value,
        default_customer_code: document.querySelector("#default_customer_code input").value,
        device_serials: document.querySelector("#device_serials input").value,
        product_filter: document.querySelector("#product_filter input").value
    };
    return wt;
}

function horizonImportReturnRecipeFromFiscof(){
    let wt = getWorkTask();
    wt.attributes = {
        doc_type: document.querySelector("#doc_type input").value,
        warehouse_code: document.querySelector("#warehouse_code input").value,
        invoice_code: document.querySelector("#invoice_code input").value,
        default_customer_code: document.querySelector("#default_customer_code input").value,
        device_serials: document.querySelector("#device_serials input").value,
        product_filter: document.querySelector("#product_filter input").value
    };
    return wt;
}

function horizonExportProducts(){
    let wt = getWorkTask();
    wt.attributes = {
        filter: document.querySelector("#filter input").value,
        price: document.querySelector("#price input").value
    };
    return wt;
}

function horizonExportTax(){
    let wt = getWorkTask();
    wt.attributes = {
        product_filter: document.querySelector("#product_filter input").value
    };
    return wt;
}

function horizonExportOnlineOrder(){
    let wt = getWorkTask();
    wt.attributes = {
        product_tax_categories: document.querySelector("#product_tax_categories input").value,
        invoice_filter: document.querySelector("#filter input").value,
    };
    return wt;
}

function horizonImportOnlineOrder(){
    let wt = getWorkTask();
    wt.attributes = {
        vat_list: document.querySelector("#vat_list input").value,
        printer_id: document.querySelector("#printer_id input").value,
    };
    return wt;
}


function horizonImportFiscofWithReturnRecipe(){
    let wt = getWorkTask();
    wt.attributes = {
        doc_type: document.querySelector("#doc_type input").value,
        warehouse_code: document.querySelector("#warehouse_code input").value,
        invoice_code: document.querySelector("#invoice_code input").value,
        default_customer_code: document.querySelector("#default_customer_code input").value,
        device_serials: document.querySelector("#device_serials input").value,
        product_filter: document.querySelector("#product_filter input").value
    };
    return wt;
}

export function collectAttributes(taskCode) {
    switch(taskCode){
        case "horizon_import_fiscof":
            return JSON.stringify(horizonImportFiscof());
        case "horizon_import_return_recipe_from_fiscof":
            return JSON.stringify(horizonImportReturnRecipeFromFiscof());
        case "horizon_import_fiscof_with_return_recipe":
            return JSON.stringify(horizonImportFiscofWithReturnRecipe());
        case "horizon_export_products":
            return JSON.stringify(horizonExportProducts());
        case "horizon_export_tax":
            return JSON.stringify(horizonExportTax());
        case "horizon_export_online_order":
            return JSON.stringify(horizonExportOnlineOrder());
        case "horizon_import_online_order":
            return JSON.stringify(horizonImportOnlineOrder());
        default:
            return JSON.stringify(noAttributes());
    }
}