import {MDCDialog} from '@material/dialog';


export class ConfirmDialog{
    constructor(btnSelector, dialogSelector) {
        this.btnShowConfirmEl = document.querySelector(btnSelector);
        this.confirmDialogEl = document.querySelector(dialogSelector);
        
        this.confirmed = false;
        
        if(this.confirmDialogEl && this.btnShowConfirmEl){
            this.confirmDialog = new MDCDialog(this.confirmDialogEl);
            this.btnOkEl = document.querySelector(`${dialogSelector} .mdc-button-ok`);
            this.btnCancelEl = document.querySelector(`${dialogSelector} .mdc-button-cancel`);
            
            this.btnShowConfirmEl.addEventListener('click', (event) => {
                if(!this.confirmed){
                    event.preventDefault();
                    this.confirmDialog.open();
                    this.confirmed = true;
                }
            });
            
            this.btnOkEl.addEventListener('click', (event) => {
                this.btnShowConfirmEl.click();
            });
            
            this.btnCancelEl.addEventListener('click', (event) => {
                this.confirmed = false;
            });
        }
    }
}


export class InfoDialog{
    constructor(btnId, dialogId) {
        this.dialog = new MDCDialog(document.getElementById(dialogId))
        this.btn = document.getElementById(btnId);

        this.btn.addEventListener('click', (e) => {
            e.preventDefault();
            this.dialog.open();
        });
    }
}

export class AddOnlineOrderProductDialog{
    constructor(btnSelector, dialogSelector, openFunction, okFunction) {
        this.btnShowEl = document.querySelector(btnSelector);
        this.dialogEl = document.querySelector(dialogSelector);
        this.openFunction = openFunction;
        this.okFunction = okFunction;

        if(this.dialogEl && this.btnShowEl){
            this.dialog = new MDCDialog(this.dialogEl);
            this.btnOkEl = document.querySelector(`${dialogSelector} .mdc-button-ok`);
            //this.btnCancelEl = document.querySelector(`${dialogSelector} .mdc-button-cancel`);

            this.btnShowEl.addEventListener('click', (event) => {
                event.preventDefault();
                this.dialog.open();
                this.openFunction();
            });

            this.btnOkEl.addEventListener('click', (event) => {
                this.okFunction();
            });

        }
    }
}
