import {MDCRipple} from '@material/ripple';
import {MDCTopAppBar} from '@material/top-app-bar';
import {MDCDrawer} from "@material/drawer";
import {MDCList} from "@material/list";
import {MDCMenu} from '@material/menu';
import {MDCTextField} from '@material/textfield';
import {selectDocumentation} from './js_compontents/documentation_settings.js'


export function initBase() {
    const drawerElement = document.querySelector('.mdc-drawer');
    const drawer = MDCDrawer.attachTo(drawerElement);

    let is_mobile = window.matchMedia("(max-width: 1023px)").matches;
    drawer.open = !is_mobile;

    const topAppBar = MDCTopAppBar.attachTo(document.getElementById('app-bar'));
    topAppBar.setScrollTarget(document.getElementById('main-content'));
    topAppBar.listen('MDCTopAppBar:nav', () => {
        drawer.open = !drawer.open;
    });

    const appBarUserMenu = new MDCMenu(document.querySelector('.mdc-menu-options'));
    appBarUserMenu.open = false;


    const btnOptionsElem = document.getElementById("btn-options");
    btnOptionsElem.addEventListener('click', (event) => {
        appBarUserMenu.open = !appBarUserMenu.open;
    });

    const btnSearch = document.getElementById("btn-search");
    const btnCloseSearch = document.getElementById("btn-close-search");

    if (btnSearch && btnCloseSearch) {
        const textFieldSearch = new MDCTextField(document.querySelector('.search'));
        const searchInput = document.getElementById("search-field-input");
        const searchForm = document.getElementById("search-form");
        btnSearch.addEventListener('click', (event) => {
            document.getElementById("search_ctrl").className = "open";
            searchInput.focus();
        });

        btnCloseSearch.addEventListener('click', (event) => {
            document.getElementById("search_ctrl").className = "close";
            searchInput.value = "";
            searchForm.submit();
        });
    }

    const selector = '.mdc-button, .mdc-icon-button, .mdc-card__primary-action';
        const ripples = [].map.call(document.querySelectorAll(selector), function(el) {
            // Interesants MDC  efekts riples korekti nedarbojas tabulas lapas kontroļu pogām tāpēc tām neliekam
            if(el.id !== 'dt-btn-per-page'
                && el.id !== 'mdc-data-table-prev'
                && el.id !== 'mdc-data-table-next'
                && el.id !== 'btn-close-filter'
                && !el.id.startsWith('dt-more-vert-')) {
                return new MDCRipple(el);
            }
    });
}

const btnDocumentation = document.getElementById("btn-documentation");
const btnCloseDocumentation = document.getElementById("btn-close-documentation");

if(document.getElementById("documentation-filter").className === "documentation-open"){
    document.getElementById("main-content").className = "main-content main-content--with-documentation";
}

if (btnDocumentation) {
    btnDocumentation.addEventListener('click', (event) => {
        if(document.getElementById("documentation-filter").className === "documentation-open"){
            document.getElementById("data-table-wrap").className = "content-wrap";
            document.getElementById("documentation-filter").className = "";

            document.getElementById("main-content").className = "main-content";
        }
        else{
            document.getElementById("data-table-wrap").className = "content-wrap with-documentation";
            document.getElementById("documentation-filter").className = "documentation-open";

            document.getElementById("main-content").className = "main-content main-content--with-documentation";
        }
    });

    btnCloseDocumentation.addEventListener('click', (event) => {
        document.getElementById("data-table-wrap").className = "content-wrap";
        document.getElementById("documentation-filter").className = "";
        document.getElementById("main-content").className = "main-content";
    });
}

selectDocumentation()