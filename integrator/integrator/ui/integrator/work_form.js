import {initBase} from './base.js';
import {ConfirmDialog} from "./dialogs";
import {collectAttributes} from "./task_attributes";

import {MDCTextField} from '@material/textfield';
import {MDCDialog} from '@material/dialog';
import {MDCTextFieldHelperText} from '@material/textfield/helper-text';
import {MDCSnackbar} from '@material/snackbar';
import {MDCSwitch} from '@material/switch';
import {MDCSelect} from '@material/select';
import {MDCChipSet} from '@material/chips';
import {MDCSelectHelperText} from '@material/select/helper-text';
import clickp from 'dom-helpers/src/event/clickp';
import {MDCList} from '@material/list';


const selectValueEvent = (e) => {
    document.getElementById(`${e.target.id}-selected-value`).value = e.detail.value;
};

function setTimeFields(runPeriod){
    if(runPeriod === 'minute'){
        document.getElementById("hour").style.display = 'none';
        document.getElementById("week_day").style.display = 'none';
        document.getElementById("month").style.display = 'none';
        document.getElementById("month_day").style.display = 'none';
    }
    else if(runPeriod === 'hour'){
        document.getElementById("hour").style.display = '';
        document.getElementById("week_day").style.display = 'none';
        document.getElementById("month").style.display = 'none';
        document.getElementById("month_day").style.display = 'none';
    }
    else if(runPeriod === 'week_day'){
        document.getElementById("hour").style.display = '';
        document.getElementById("week_day").style.display = '';
        document.getElementById("month").style.display = 'none';
        document.getElementById("month_day").style.display = 'none';
    }
    else if(runPeriod === 'month'){
        document.getElementById("hour").style.display = '';
        document.getElementById("week_day").style.display = 'none';
        document.getElementById("month").style.display = '';
        document.getElementById("month_day").style.display = '';
    }
}

const runPeriods = new MDCSelect(document.getElementById("id_run_period"));

setTimeFields(runPeriods.foundation_.menuItemValues_[runPeriods.selectedIndex]);

const fieldEvent = (e) => {
    setTimeFields(e.detail.value);
};

runPeriods.listen('MDCSelect:change', fieldEvent);
runPeriods.listen('MDCSelect:change', selectValueEvent);

const runMinute = new MDCSelect(document.getElementById("minute"));
runMinute.listen('MDCSelect:change', selectValueEvent);

const runHour = new MDCSelect(document.getElementById("hour"));
runHour.listen('MDCSelect:change', selectValueEvent);

const runWeekDay = new MDCSelect(document.getElementById("week_day"));
runWeekDay.listen('MDCSelect:change', selectValueEvent);

const runMonth = new MDCSelect(document.getElementById("month"));
runMonth.listen('MDCSelect:change', selectValueEvent);

const runMonthDay = new MDCSelect(document.getElementById("month_day"));
runMonthDay.listen('MDCSelect:change', selectValueEvent);

const connections = new MDCSelect(document.getElementById("connection"));

const connectionsEvent = (e) => {
    tasks.selectedIndex = -1;
    let ignoreCls = ["mdc-list-item", "mdc-list-item--selected"];
    let listCls = document.querySelector(`#${e.target.id} li.mdc-list-item--selected`).classList.toString();
    for (let i = 0; i < ignoreCls.length; i++) {
        listCls = listCls.replace(ignoreCls[i], "");
    }
    listCls = listCls.replaceAll(" ", "");

    let taskItems = document.querySelectorAll(`#task li`);
    for (let i = 0; i < taskItems.length; i++) {

        if(taskItems[i].classList.contains(listCls)){
            taskItems[i].classList.remove("none");
        }
        else{
            taskItems[i].classList.add("none");
        }
    }
};

connections.listen('MDCSelect:change', connectionsEvent);

const tasks = new MDCSelect(document.getElementById("task"));

function addTaskEvent(e){
    if(connections.selectedIndex > - 1 && tasks.selectedIndex > - 1){
        let connectionId = connections.foundation_.menuItemValues_[connections.selectedIndex];
        let taskId = tasks.foundation_.menuItemValues_[tasks.selectedIndex];
        let connectionTitle = document.querySelector("#connection li.mdc-list-item--selected .mdc-list-item__text").innerHTML;
        let taskTitle = document.querySelector("#task li.mdc-list-item--selected .mdc-list-item__text").innerHTML;

        const dialogAttributes = new MDCDialog(document.getElementById("dialogAttributes"));
        dialogAttributes.open();

        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState === 4 && this.status === 200) {
                document.getElementById("attributes-dialog-content").innerHTML = this.responseText;

                const textFields = [].map.call(document.querySelectorAll('#dialogAttributes .mdc-text-field'), function(el) {
                    const f =  new MDCTextField(el);

                    if(document.getElementById(el.id+"-helper-text")){
                        f.valid = false;
                    }
                    return f;
                });
            }
        };

        let url = '';
        if(e.target.href){
            url = e.target.href;
        }
        else{
            url = e.target.parentNode.href;
        }

        xhttp.open("GET", `${url}?id_task=${taskId}&id_connection=${connectionId}&cb=${Math.floor(Date.now() / 1000)}`, true);
        xhttp.send();
    }
}


class ChipTasks{
    constructor(element_id) {
        this.property = element_id;

        this.chipSetEl = document.getElementById(this.property);
        this.chipSet = new MDCChipSet(this.chipSetEl);
    }

    addChip(attributes, title){
        const chipEl = this.createChip('', title);
        this.chipSetEl.appendChild(chipEl);
        this.chipSet.addChip(chipEl);

        document.querySelector(`#${chipEl.id} input`).value = attributes;
    }

    createChip(data, title){
        let tpl = document.getElementById("chip-tpl").innerHTML;
        tpl = tpl.replace('{title}', title);
        tpl = tpl.replace('{inp_value}', data);

        var div = document.createElement('div');
        div.innerHTML = tpl.trim();

        return div.firstChild;
    }
}

let chipTasks = null;
if(document.getElementById("chip-set-chipTasks")) {
    chipTasks = new ChipTasks("chip-set-chipTasks");
}


function initCollectAttributes(e){
    chipTasks.addChip(
        collectAttributes(document.getElementById("addTaskCode").value),
        document.getElementById("addTaskDisplayName").value
    );
}

clickp("#addTask", addTaskEvent);

clickp("#confirmAddTask", initCollectAttributes);


if(document.querySelector('.mdc-error-dialog')) {
  const errorDialog = new MDCDialog(document.querySelector('.mdc-error-dialog'));
  errorDialog.open();
}

initBase();

const textFieldsHelper = [].map.call(document.querySelectorAll('.mdc-text-field-helper-line-error'), function(el) {
  const ht = new MDCTextFieldHelperText(el);
  ht.foundation.setValidation(true);
  return ht;
});

const textFields = [].map.call(document.querySelectorAll('.mdc-text-field'), function(el) {
  const f =  new MDCTextField(el);

  if(document.getElementById(el.id+"-helper-text")){
    f.valid = false;
  }
  return f;
});

const switchFields = [].map.call(document.querySelectorAll('.mdc-switch'), function(el) {
  const f =  new MDCSwitch(el);

  if(document.getElementById(el.id+"-helper-text")){
    f.valid = false;
  }
  return f;
});

const snackbarEl = document.querySelector(".mdc-snackbar");
if(snackbarEl) {
  const snackbar = new MDCSnackbar(snackbarEl);
  snackbar.open();
}

let confirmDelete = new ConfirmDialog('#rm-btn', '.mdc-confirm-dialog');
let additionalConfirmReset = new ConfirmDialog('#ad-btn', '.mdc-confirm-dialog-additional');

let workConnection = ['Visma Horizon', 'API Manage TikiTaka']
let horizon = [
    'Izgūt mērvienības',
    'Izgūt nodokļu grupas',
    'Izgūt produktus',
    'Veidot Kases pavadzīmes',
    'Izgūt tiešsaistes pasūtījumus',
    'Apstiprināt tiešsaistes pasūtījumus'
]

let manage = [
    'Ielādēt mērvienības',
    'Ielādēt produktus',
    'Ielādēt nodokļu grupas',
    'Izgūt FisCof jeb Kontrollentas',
    'Veidot tiešsaistes pasūtījumus',
    'Izgūt tiešsaistes pasūtījumu apmaksas statusu'
]

let selectConnection = document.getElementById('select-connection')
let selectWork = document.getElementById('select-work')

workConnection.forEach(function addSpecies(item) {
    let option = document.createElement('option');
    option.setAttribute('class', 'test')
    option.text = item;
    option.value = item;
    selectConnection.appendChild(option)
})


selectConnection.onchange = function () {
    selectWork.innerHTML = '<option></option>';
    if (this.value === 'Visma Horizon') {
        addToSelectTwo(horizon);
    }
    if (this.value === 'API Manage TikiTaka') {
        addToSelectTwo(manage);
    }
}

function addToSelectTwo(array) {
    array.forEach(function (item) {
        let option = document.createElement('option');
        option.text = item;
        option.value = item;
        selectWork.appendChild(option)
    })
}

function fetchDocumentation(url) {
    fetch(url).then((response) => {
        let textValue = response.text().then((data) => {
            let dataTo = data.slice(data.indexOf('<section id="wiki-content"'))
            let htmlObject = document.getElementById('work-documentation-container')
            htmlObject.innerHTML = dataTo
        })
    })
}

let wikiContent = document.getElementById('wiki-content')


selectWork.onchange = function () {
    if(this.value === 'Izgūt mērvienības') {
        fetchDocumentation('https://bitbucket.org/creativeninjas/integrators/wiki/Lietot%C4%81ja%20rokasgr%C4%81mta/Darbi/Visma%20Horizon/02%20-%20M%C4%93rvien%C4%ABbas')
    } else if(this.value === 'Izgūt nodokļu grupas') {
        fetchDocumentation('https://bitbucket.org/creativeninjas/integrators/wiki/Lietot%C4%81ja%20rokasgr%C4%81mta/Darbi/Visma%20Horizon/01%20-%20Nodok%C4%BCi')
    } else if(this.value === 'Izgūt produktus') {
        fetchDocumentation('https://bitbucket.org/creativeninjas/integrators/wiki/Lietot%C4%81ja%20rokasgr%C4%81mta/Darbi/Visma%20Horizon/03%20-%20Produkti')
    } else if(this.value === 'Veidot Kases pavadzīmes') {
        fetchDocumentation('https://bitbucket.org/creativeninjas/integrators/wiki/Lietot%C4%81ja%20rokasgr%C4%81mta/Darbi/Visma%20Horizon/04%20-%20Veidot%20Horizon%20Kases%20pavadz%C4%ABmes')
    } else if(this.value === 'Izgūt tiešsaistes pasūtījumus') {
        fetchDocumentation('https://bitbucket.org/creativeninjas/integrators/wiki/Lietot%C4%81ja%20rokasgr%C4%81mta/Darbi/Visma%20Horizon/05%20-%20Izg%C5%ABt%20tie%C5%A1saistes%20pas%C5%ABt%C4%ABjumus')
    } else if(this.value === 'Apstiprināt tiešsaistes pasūtījumus') {
        fetchDocumentation('https://bitbucket.org/creativeninjas/integrators/wiki/Lietot%C4%81ja%20rokasgr%C4%81mta/Darbi/Visma%20Horizon/06%20-%20Apstiprin%C4%81t%20tie%C5%A1saistes%20pas%C5%ABt%C4%ABjumus')
    } else if(this.value === 'Ielādēt mērvienības') {
        fetchDocumentation('https://bitbucket.org/creativeninjas/integrators/wiki/Lietot%C4%81ja%20rokasgr%C4%81mta/Darbi/Manage%20Tiki-Taka/02%20-%20M%C4%93rvien%C4%ABbas')
    } else if(this.value === 'Ielādēt produktus') {
        fetchDocumentation('https://bitbucket.org/creativeninjas/integrators/wiki/Lietot%C4%81ja%20rokasgr%C4%81mta/Darbi/Manage%20Tiki-Taka/03%20-%20Produkti')
    } else if(this.value === 'Ielādēt nodokļu grupas') {
        fetchDocumentation('https://bitbucket.org/creativeninjas/integrators/wiki/Lietot%C4%81ja%20rokasgr%C4%81mta/Darbi/Manage%20Tiki-Taka/01%20-%20Nodok%C4%BCi')
    } else if(this.value === 'Izgūt FisCof jeb Kontrollentas') {
        fetchDocumentation('https://bitbucket.org/creativeninjas/integrators/wiki/Lietot%C4%81ja%20rokasgr%C4%81mta/Darbi/Manage%20Tiki-Taka/04%20-%20Kontrollentas')
    } else if(this.value === 'Veidot tiešsaistes pasūtījumus') {
        fetchDocumentation('https://bitbucket.org/creativeninjas/integrators/wiki/Lietot%C4%81ja%20rokasgr%C4%81mta/Darbi/Manage%20Tiki-Taka/05%20-%20Veidot%20tie%C5%A1saistes%20pas%C5%ABt%C4%ABjumu')
    } else if(this.value === 'Izgūt tiešsaistes pasūtījumu apmaksas statusu') {
        fetchDocumentation('https://bitbucket.org/creativeninjas/integrators/wiki/Lietot%C4%81ja%20rokasgr%C4%81mta/Darbi/Manage%20Tiki-Taka/06%20-%20Tie%C5%A1saistes%20pas%C5%ABt%C4%ABjumu%20apmaskas%20status')
    } else {
        wikiContent.removeChild(wikiContent.firstChild);
    }
}
