import {initBase} from './base.js';


import {MDCSnackbar} from '@material/snackbar';
import {MDCList} from '@material/list';

initBase();


const snackbarEl = document.querySelector(".mdc-snackbar");
if(snackbarEl) {
  const snackbar = new MDCSnackbar(snackbarEl);
  snackbar.open();
}


const list = new MDCList(document.querySelector('.mdc-list'));