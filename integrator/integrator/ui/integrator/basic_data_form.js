import {initBase} from './base.js';
import {ConfirmDialog} from "./dialogs";

import {MDCTextField} from '@material/textfield';
import {MDCDialog} from '@material/dialog';
import {MDCTextFieldHelperText} from '@material/textfield/helper-text';
import {MDCSnackbar} from '@material/snackbar';
import {MDCSwitch} from '@material/switch';


if(document.querySelector('.mdc-error-dialog')) {
  const errorDialog = new MDCDialog(document.querySelector('.mdc-error-dialog'));
  errorDialog.open();
}

initBase();

const textFieldsHelper = [].map.call(document.querySelectorAll('.mdc-text-field-helper-line-error'), function(el) {
  const ht = new MDCTextFieldHelperText(el);
  ht.foundation.setValidation(true);
  return ht;
});

const textFields = [].map.call(document.querySelectorAll('.mdc-text-field'), function(el) {
  const f =  new MDCTextField(el);

  if(document.getElementById(el.id+"-helper-text")){
    f.valid = false;
  }
  return f;
});

const switchFields = [].map.call(document.querySelectorAll('.mdc-switch'), function(el) {
  const f =  new MDCSwitch(el);

  if(document.getElementById(el.id+"-helper-text")){
    f.valid = false;
  }
  return f;
});

const snackbarEl = document.querySelector(".mdc-snackbar");
if(snackbarEl) {
  const snackbar = new MDCSnackbar(snackbarEl);
  snackbar.open();
}

let confirmDelete = new ConfirmDialog('#rm-btn', '.mdc-confirm-dialog');
let additionalConfirmReset = new ConfirmDialog('#ad-btn', '.mdc-confirm-dialog-additional');