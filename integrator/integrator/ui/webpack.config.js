const path = require('path');

// Read package info
let pkg = require('./package.json');

const FileManagerPlugin = require('filemanager-webpack-plugin');

module.exports = {
    entry: {
        dashboard:        ['./integrator/dashboard.scss', './integrator/dashboard.js'],
        login:            ['./integrator/login.scss', './integrator/login.js'],
        data_table:       ['./integrator/data_table.scss', './integrator/data_table.js'],
        basic_data_form:  ['./integrator/basic_data_form.scss', './integrator/basic_data_form.js'],
        card_list:        ['./integrator/card_list.scss', './integrator/card_list.js'],
        version_card:     ['./integrator/version_card.scss', './integrator/version_card.js'],
        work_form:        ['./integrator/work_form.scss', './integrator/work_form.js'],
        task_attributes:  ['./integrator/task_attributes.js'],
    },
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: `[name]/bundle-${pkg.version}.js`,
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                use: [
                    {loader: 'babel-loader'},
                ]
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: `[name]/bundle-${pkg.version}.css`
                        }
                    },

                    {
                        loader: 'fast-sass-loader',
                        options: {
                            //sassOptions: {
                                includePaths: ['./node_modules'],
                            //},
                            // Prefer Dart Sass
                            implementation: require('node-sass'),

                            // See https://github.com/webpack-contrib/sass-loader/issues/804
                            //webpackImporter: false,
                        }
                    }
                ],
            },
        ]
    },
    plugins: [
        new FileManagerPlugin({
            events: {
                onEnd: {
                    copy: [
                        { source: `./build/dashboard/bundle-${pkg.version}.js`, destination: `../apps/manage/static/dashboard/bundle-${pkg.version}.js` },
                        { source: `./build/dashboard/bundle-${pkg.version}.css`, destination: `../apps/manage/static/dashboard/bundle-${pkg.version}.css` },

                        { source: `./build/login/bundle-${pkg.version}.js`, destination: `../apps/manage/static/login/bundle-${pkg.version}.js` },
                        { source: `./build/login/bundle-${pkg.version}.css`, destination: `../apps/manage/static/login/bundle-${pkg.version}.css` },

                        { source: `./build/data_table/bundle-${pkg.version}.js`, destination: `../apps/manage/static/data_table/bundle-${pkg.version}.js` },
                        { source: `./build/data_table/bundle-${pkg.version}.css`, destination: `../apps/manage/static/data_table/bundle-${pkg.version}.css` },

                        { source: `./build/basic_data_form/bundle-${pkg.version}.js`, destination: `../apps/manage/static/basic_data_form/bundle-${pkg.version}.js` },
                        { source: `./build/basic_data_form/bundle-${pkg.version}.css`, destination: `../apps/manage/static/basic_data_form/bundle-${pkg.version}.css` },

                        { source: `./build/card_list/bundle-${pkg.version}.js`, destination: `../apps/manage/static/card_list/bundle-${pkg.version}.js` },
                        { source: `./build/card_list/bundle-${pkg.version}.css`, destination: `../apps/manage/static/card_list/bundle-${pkg.version}.css` },

                        // Clients
                        { source: `./build/dashboard/bundle-${pkg.version}.js`, destination: `../apps/clients/static/dashboard/bundle-${pkg.version}.js` },
                        { source: `./build/dashboard/bundle-${pkg.version}.css`, destination: `../apps/clients/static/dashboard/bundle-${pkg.version}.css` },

                        { source: `./build/login/bundle-${pkg.version}.js`, destination: `../apps/clients/static/login/bundle-${pkg.version}.js` },
                        { source: `./build/login/bundle-${pkg.version}.css`, destination: `../apps/clients/static/login/bundle-${pkg.version}.css` },

                        { source: `./build/data_table/bundle-${pkg.version}.js`, destination: `../apps/clients/static/data_table/bundle-${pkg.version}.js` },
                        { source: `./build/data_table/bundle-${pkg.version}.css`, destination: `../apps/clients/static/data_table/bundle-${pkg.version}.css` },

                        { source: `./build/basic_data_form/bundle-${pkg.version}.js`, destination: `../apps/clients/static/basic_data_form/bundle-${pkg.version}.js` },
                        { source: `./build/basic_data_form/bundle-${pkg.version}.css`, destination: `../apps/clients/static/basic_data_form/bundle-${pkg.version}.css` },

                        { source: `./build/card_list/bundle-${pkg.version}.js`, destination: `../apps/clients/static/card_list/bundle-${pkg.version}.js` },
                        { source: `./build/card_list/bundle-${pkg.version}.css`, destination: `../apps/clients/static/card_list/bundle-${pkg.version}.css` },

                        { source: `./build/version_card/bundle-${pkg.version}.js`, destination: `../apps/clients/static/version_card/bundle-${pkg.version}.js` },
                        { source: `./build/version_card/bundle-${pkg.version}.css`, destination: `../apps/clients/static/version_card/bundle-${pkg.version}.css` },

                        { source: `./build/work_form/bundle-${pkg.version}.js`, destination: `../apps/clients/static/work_form/bundle-${pkg.version}.js` },
                        { source: `./build/work_form/bundle-${pkg.version}.css`, destination: `../apps/clients/static/work_form/bundle-${pkg.version}.css` },

                    ],

                },
            },
        }),
    ],
};
