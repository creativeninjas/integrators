from starlette.requests import Request
from starlette.responses import RedirectResponse

from integrator.apps.clients import settings

from integrator.helpers.endpoints import (
    BaseHTTPEndpoint,
)
from integrator.helpers.integrator_exceptions import APIValidationException


class ForgotPassword(BaseHTTPEndpoint):
    @property
    def api_url(self):
        return f"{settings.API_URL}/system/security/pwd-reset"

    @property
    def session_key(self):
        return None

    @property
    def tpl(self) -> str:
        return 'forgot_password.pt'

    async def get(self, request: Request):
        context = {
            'form': True,
            'snackbar': self.get_snackbar(request, 'action_successful'),
        }
        return self.render(request, context)

    async def post(self, request: Request):
        form_data = await request.form()

        try:
            pwd_reset = {'email': form_data['email'], 'url': request.url_for('reset-password', reset_code='reset-code')}

            data = await self.send_data(request, pwd_reset, 'POST')

            context = {
                'form': False
            }
        except Exception:
            context = {
                'form': True,
                'error': True,
                'error_message': 'Radās kļūda - pēc neliela brīža mēģiniet vēlreiz.'}

        return self.render(request, context)


class ResetPassword(BaseHTTPEndpoint):
    @property
    def api_url(self):
        return f"{settings.API_URL}/system/security/pwd-reset"

    @property
    def session_key(self):
        return None

    @property
    def tpl(self) -> str:
        return 'reset_password.pt'

    async def get(self, request: Request):
        can_reset = await self.get_from_api(
            request, f"{settings.API_URL}/system/security/pwd-reset/can-reset/{request.path_params['reset_code']}")

        if not can_reset:
            return RedirectResponse(request.url_for('login'), status_code=302)

        context = {
            'form': True,
            'snackbar': self.get_snackbar(request, 'action_successful'),
        }
        return self.render(request, context)

    async def post(self, request: Request):
        form_data = await request.form()

        try:
            if len(form_data['new_password'].strip()) == 0 or len(form_data['new_password_again'].strip()) == 0:
                context = {
                    'form': True,
                    'error': True,
                    'error_message': 'Paroles ir jānorāda obligāti'}

                return self.render(request, context)
        except Exception:
            context = {
                'form': True,
                'error': True,
                'error_message': 'Paroles ir jānorāda obligāti'}

            return self.render(request, context)

        try:
            reset_pwd = {
                'reset_code': request.path_params['reset_code'],
                'new_password': form_data['new_password'],
                'new_password_again': form_data['new_password_again'],
            }

            data = await self.send_data(
                request,
                reset_pwd,
                'POST',
                url=f"{settings.API_URL}/system/security/pwd-reset/{request.path_params['reset_code']}/reset")

            return RedirectResponse(request.url_for('login'), status_code=302)
        except APIValidationException:
            context = {
                'form': True,
                'error': True,
                'error_message': 'Paroles nesakrīt'}
        except Exception:
            context = {
                'form': True,
                'error': True,
                'error_message': 'Radās kļūda - pēc neliela brīža mēģiniet vēlreiz.'}

        return self.render(request, context)
