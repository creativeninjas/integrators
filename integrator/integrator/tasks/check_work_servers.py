import asyncio
import datetime
import logging

import ujson as json
import aiohttp
import settings


async def get_access_token() -> dict:
    headers = {
        'accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded'
    }

    payload = {'grant_type': '',
               'username': settings.API_USERNAME,
               'password': settings.API_PASSWORD,
               'scope': '',
               'client_id': '',
               'client_secret': ''}

    url = f'{settings.API_URL}/system/security/token'
    async with aiohttp.ClientSession() as session:
        async with session.post(url, data=payload, headers=headers) as response:
            if response.status == 200:
                return json.loads(await response.text())
            else:
                return {}


async def check_work_server(work_server: dict) -> dict:
    available = False
    unavailability_reason = None
    error_message = None
    try:
        async with aiohttp.ClientSession() as session:
            async with session.get(f"{work_server['address']}?_ws_id={work_server['id']}") as response:
                if response.status == 200:
                    status = json.loads(await response.text())
                    if status['available']:
                        available = True
                    else:
                        unavailability_reason = status['unavailability_reason']
                        error_message = status['error_message']
                else:
                    unavailability_reason = 'Work server check response is not 200'
                    error_message = await response.text()
    except Exception as e:
        if hasattr(e, 'message'):
            message = e.message
        else:
            message = f"{e.__class__}: {str(e)}"

        unavailability_reason = 'Work server check got unexpected error'
        error_message = message

    return {
        'id': work_server['id'],
        'available': available,
        'unavailability_reason': unavailability_reason,
        'error_message': error_message}


async def check_work_servers():
    token_data = await get_access_token()
    if token_data.get('access_token'):
        headers = {
            'accept': 'application/json',
            'Authorization': f" {token_data['token_type']} {token_data['access_token']}"
        }

        url = f'{settings.API_URL}/manage/work-servers/'
        async with aiohttp.ClientSession() as session:
            async with session.get(url, headers=headers) as response:
                if response.status == 200:
                    work_servers = json.loads(await response.text())
                    if work_servers:
                        now = datetime.datetime.now()
                        for work_server in work_servers:
                            check = False

                            if work_server['availability_status'] is None:
                                check = True
                            elif datetime.datetime.fromisoformat(work_server['availability_status']['checked_at'])\
                                    + datetime.timedelta(minutes=settings.CHECK_FREQUENCY_IN_MINUTES) < now:
                                check = True
                            if check:
                                status = await check_work_server(work_server)
                                put_url = f"{url}{status['id']}/status"
                                async with session.put(put_url, json=status, headers=headers) as put_response:
                                    if put_response.status != 200:
                                        logging.error(f'Work server status saving returned non 200: {await put_response.text()}')

    else:
        logging.warning("Work server check can't get API access token")


async def main():
    while True:
        await check_work_servers()
        await asyncio.sleep(60)

loop = asyncio.get_event_loop()
loop.run_until_complete(main())
