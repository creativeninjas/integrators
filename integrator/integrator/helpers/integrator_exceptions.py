class APIValidationException(Exception):
    def __init__(self, errors):
        super().__init__('API validation error')
        self.errors = errors


class UnknownAPIException(Exception):
    def __init__(self, errors):
        super().__init__('Unknown API error')
        self.errors = errors
