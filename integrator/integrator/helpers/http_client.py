from starlette.exceptions import HTTPException
from starlette.status import (
    HTTP_403_FORBIDDEN,
    HTTP_401_UNAUTHORIZED
)

headers = {
    'accept': 'application/json',
    'Content-Type': 'application/x-www-form-urlencoded'
}


async def get_with_status(session, url, headers={}, params={}):
    async with session.get(url, headers=headers, params=params) as response:
        if response.status == 200:
            return 200, await response.text(encoding='UTF-8')
        else:
            return response.status, None


async def get(session, url, headers={}, params={}):
    async with session.get(url, headers=headers, params=params) as response:
        if response.status == 200:
            return await response.text(encoding='UTF-8')
        elif response.status == HTTP_401_UNAUTHORIZED:
            raise HTTPException(status_code=HTTP_401_UNAUTHORIZED)
        elif response.status == HTTP_403_FORBIDDEN:
            raise HTTPException(status_code=HTTP_403_FORBIDDEN)
        else:
            return None


async def get_raw(session, url, headers={}, params={}):
    async with session.get(url, headers=headers, params=params) as response:
        if response.status == 200:
            return 200, await response.text()
        elif response.status == 422:
            return 422, await response.text()
        elif response.status == 403:
            return 403, await response.text()
        else:
            return None


async def post(session, url, payload, headers={}, params={}):
    async with session.post(url, data=payload, headers=headers, params=params) as response:
        if response.status == 200:
            return await response.text()
        else:
            return None


async def post_json(session, url, payload, headers={}, params={}):
    async with session.post(url, json=payload, headers=headers, params=params) as response:
        if response.status == 200:
            return 200, await response.text()
        elif response.status == 422:
            return 422, await response.text()
        elif response.status == 403:
            return 403, await response.text()
        elif response.status == 404:
            return 404, await response.text()
        else:
            return None


async def put_json(session, url, payload, headers={}, params={}):
    async with session.put(url, json=payload, headers=headers, params=params) as response:
        if response.status == 200:
            return 200, await response.text()
        elif response.status == 422:
            return 422, await response.text()
        elif response.status == 403:
            return 403, await response.text()
        elif response.status == 404:
            return 404, await response.text()
        else:
            return None