from typing import List

from datetime import datetime

import ujson as json

from starlette.requests import Request

from bs4 import BeautifulSoup


def form_input_item(request: Request, element_id: str, name: str, label: str, input_type: str, placeholder: str = None, value: str = None, form_errors = None):
    context = {
        'request': request,
        'id': element_id,
        'name': name,
        'label': label,
        'placeholder': placeholder,
        'input_type': input_type,
        'value': value,
        'form_errors': form_errors
    }
    return request.app.state.templates.Component('components/form_item.pt', context)


def form_input_text(request: Request, element_id: str, name: str, label: str, placeholder: str = None, value: str = None, form_errors = None):
    return form_input_item(request, element_id, name, label, 'text', placeholder, value, form_errors)


def form_input_password(request: Request, element_id: str, name: str, label: str, placeholder: str = None, form_errors = None):
    return form_input_item(request, element_id, name, label, 'password', placeholder, None, form_errors)


def notification(request: Request, notification_type: str, level: str, title: str, sub_title: str = None):
    context = {'request': request,
               'type': notification_type,
               'title': title,
               'sub_title': sub_title,
               'level': level}
    return request.app.state.templates.Component('components/notification.pt', context)


def fab(request: Request, url, icon):
    context = {'request': request,
               'url': url,
               'icon': icon}

    return request.app.state.templates.Component('components/fab.pt', context)


def snackbar(request: Request, message: str, snackbar_id: str = None, css_class: str = 'mdc-snackbar'):
    attributes = {'id': None, 'class': css_class}
    if snackbar_id:
        attributes['id'] = snackbar_id

    context = {'request': request, 'message': message, 'attributes': attributes}
    return request.app.state.templates.Component('components/snackbar.pt', context)


def form_unexpected_error_dialog(request: Request, error_dialog_message: str):
    context = {'request': request,
               'error_dialog_message': error_dialog_message}

    return request.app.state.templates.Component('components/form_unexpected_error_dialog.pt', context)


def confirm_dialog(
        request: Request,
        question, title=None, css_class="mdc-confirm-dialog", dialog_id=None, ok='OK', cancel='Atcelt'):
    context = {
        'request': request,
        'id': dialog_id,
        'ok': ok,
        'cancel': cancel,
        'css_class': f'mdc-dialog {css_class}',
        'question': question,
        'title': title
    }

    return request.app.state.templates.Component('components/confirm_dialog.pt', context)


def form_buttons(request: Request, form_type: str, delete_url: str = None):
    context = {
        'request': request,
        'form_type': form_type,
        'delete_url': delete_url,
    }

    return request.app.state.templates.Component('components/form_buttons.pt', context)


def switch(request: Request, label: str, name: str, value: bool):
    context = {
        'request': request,
        'label': label,
        'name': name,
        'value':  value
    }

    return request.app.state.templates.Component('components/switch.pt', context)


def data_table_pagination(request: Request, limit: int, page: int, prev_page: int, next_page: int, query: str, filters: dict = None):
    context = {
        'request': request,
        'limit': limit,
        'page': page,
        'prev_page': prev_page,
        'next_page': next_page,
        'query': query,
        'filters': filters
    }

    return request.app.state.templates.Component('components/data_table_pagination.pt', context)


def format_datetime(datetime_str: str) -> str:
    return datetime.fromisoformat(datetime_str).strftime('%d.%m.%Y %H.%M.%S')


def appbar_search(request: Request, query: str):
    context = {
        'request': request,
        'query': query
    }
    return request.app.state.templates.Component('components/appbar_search.pt', context)


def appbar_filter(request: Request):
    context = {
        'request': request
    }
    return request.app.state.templates.Component('components/appbar_filter.pt', context)


def cb_filter_item(request: Request, value, filter_items: List, name: str, title: str):
    context = {
        'request': request,
        'value': value,
        'filter_items': filter_items,
        'name': name,
        'title': title
    }
    return request.app.state.templates.Component('components/cb_filter_item.pt', context)


def foreign_system_connection_status(foreign_system_connection: dict) -> dict:
    if foreign_system_connection['available'] is None:
        return {'class': 'ws-status-unknown', 'label': 'Nepārbaudīts', 'icon': 'question_mark'}
    elif foreign_system_connection['available']:
        return {'class': 'ws-status-available', 'label': 'Pieejams', 'icon': 'check_circle'}
    else:
        return {'class': 'ws-status-unavailable', 'label': 'Nepieejams', 'icon': 'error'}


def online_order_document_content(online_order):
    document = json.loads(online_order['document'])
    return get_document_content(document['deal_doc']['doc'])


def get_document_content(document):
    soup = BeautifulSoup(document, 'html.parser')
    content = soup.find('ceka_saturs')

    doc = BeautifulSoup(content.get_text(), 'html.parser')
    for f in doc.find_all('font'):
        if f['type'] == '1':
            f['style'] = 'font-size: 0.6em;'
        elif 'b' in f['type']:
            f['style'] = 'font-weight: bold;'

    logo = doc.find('logo')
    if logo:
        logo.extract()

    return str(doc)


def select_field(
        request: Request,
        element_id: str,
        name: str,
        label: str,
        items,
        value: str = None,
        helper: bool = False,
        form_errors=None,
        blank: bool = False,
        css_class: str = None,
        css_class_from_field: tuple = None):

    context = {
        'request': request,
        'id': element_id,
        'name': name,
        'label': label,
        'items': items,
        'value': value,
        'helper': helper,
        'form_errors': form_errors,
        'blank': blank,
        'css_class': css_class,
        'css_class_from_field': css_class_from_field
    }
    return request.app.state.templates.Component('components/select_field.pt', context)


def get_task_title(tasks, task_code):
    for t in tasks:
        if str(t['id']) == task_code:
            return t['title']


def to_json_str(data):
    return json.dumps(data)


def display_work_next_run_time(item):
    execute_at = datetime.fromisoformat(item['execute_at'])
    if item['disabled'] and not item['running']:
        return '-'
    elif not item['disabled'] and item['running']:
        return '-'
    elif not item['disabled'] and not item['running'] and execute_at < datetime.now()\
            and (item['completed_at'] or item['canceled_at']):
        return '-'
    elif not item['disabled'] and not item['running'] and execute_at < datetime.now()\
            and (not item['completed_at'] or  not item['canceled_at']):
        return 'Drīz'
    else:
        return format_datetime(item['execute_at'])


def is_work_running(item):
    if not item['disabled'] and item['running']:
        return True
    else:
        return False


def is_work_active(item):
    if not item['disabled'] and not item['running']:
        return True
    else:
        return False


minutes = []
hours = []
week_days = [{'id': 1, 'title': 'Pirmdiena'},
             {'id': 2, 'title': 'Otrdiena'},
             {'id': 3, 'title': 'Trešdiena'},
             {'id': 4, 'title': 'Ceturtdiena'},
             {'id': 5, 'title': 'Piektdiena'},
             {'id': 6, 'title': 'Sestdiena'},
             {'id': 7, 'title': 'Svētdiena'}]

months = [{'id': 1, 'title': 'Janvāris'},
          {'id': 2, 'title': 'Februāris'},
          {'id': 3, 'title': 'Marts'},
          {'id': 4, 'title': 'Aprīlis'},
          {'id': 5, 'title': 'Maijs'},
          {'id': 6, 'title': 'Jūnijs'},
          {'id': 7, 'title': 'Jūlijs'},
          {'id': 8, 'title': 'Augusts'},
          {'id': 9, 'title': 'Septembris'},
          {'id': 10, 'title': 'Oktobris'},
          {'id': 11, 'title': 'Novembris'},
          {'id': 12, 'title': 'Decembris'}]

month_days = []

for m in range(0, 60):
    minutes.append({'id': m, 'title': m})

for h in range(0, 24):
    hours.append({'id': h, 'title': h})

for d in range(1, 32):
    month_days.append({'id': d, 'title': d})

elements = {
    'snackbar': snackbar,
    'form_input_item': form_input_item,
    'form_input_text': form_input_text,
    'form_input_password': form_input_password,
    'confirm_dialog': confirm_dialog,
    'notification': notification,
    'fab': fab,
    'form_unexpected_error_dialog': form_unexpected_error_dialog,
    'form_buttons': form_buttons,
    'switch': switch,
    'data_table_pagination': data_table_pagination,
    'format_datetime': format_datetime,
    'appbar_search': appbar_search,
    'appbar_filter': appbar_filter,
    'cb_filter_item': cb_filter_item,
    'foreign_system_connection_status': foreign_system_connection_status,
    'get_document_content': get_document_content,
    'select_field': select_field,
    'get_task_title': get_task_title,
    'minutes': minutes,
    'hours': hours,
    'week_days': week_days,
    'months': months,
    'month_days': month_days,
    'to_json_str': to_json_str,
    'display_work_next_run_time': display_work_next_run_time,
    'is_work_running': is_work_running,
    'is_work_active': is_work_active,
    'online_order_document_content': online_order_document_content
}
