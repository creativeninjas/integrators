from abc import ABCMeta
from typing import Dict, List

import aiohttp
from starlette.authentication import requires
from starlette.datastructures import FormData

from starlette.endpoints import HTTPEndpoint
from starlette.requests import Request

import ujson as json

# from pulse.content.models import ContentResultSet, Content, Lineage
#from pulse.guard.utils import token_headers
from starlette.responses import RedirectResponse

from integrator.helpers import constants
from integrator.helpers.components import elements
# from pulse.helpers.http_client import get
# from pulse.helpers.input import get_search_phrase, get_page, get_limit


# class ContestResultSetList(object):
#     def __init__(self, result_set: ContentResultSet, model, query: str, limit: int, page: int):
#         self.result_set = result_set
#         self.model = model
#         self.query = query
#         self.limit = limit
#         self.page = page
from integrator.helpers.http_client import get, put_json
from integrator.helpers.input import get_search_phrase, get_page, get_limit
from integrator.helpers.integrator_exceptions import APIValidationException, UnknownAPIException
from integrator.security.utils import token_headers


class BaseHTTPEndpoint(HTTPEndpoint):
    def render(self, request: Request, data: Dict, tpl: str = None, status_code: int = 200):
        data['request'] = request
        data['endpoint'] = self
        data['static_url'] = request.app.state.static_file_url_handler.make_static_url(request)
        data['elements'] = elements

        # Datatable filter variables
        if not data.get('query', None):
            data['query'] = None

        return request.app.state.templates.TemplateResponse(
            self.tpl if tpl is None else tpl,
            data,
            status_code=status_code)

    @property
    def tpl(self):
        raise NotImplementedError()

    @property
    def name(self):
        return self.__class__.__name__

    @property
    def api_url(self):
        raise NotImplementedError()

    @property
    def session_key(self):
        raise NotImplementedError()

    async def send_data(self, request: Request, form_data: FormData, method: str, url: str = None):
        payload = {}
        for k in form_data:
            if type(form_data[k]) == str and len(form_data[k].strip()) == 0:
                payload[k] = None
            else:
                payload[k] = form_data[k]

        async with aiohttp.ClientSession() as session:
            headers = token_headers(request, self.session_key)
            send_function = session.put
            if method == 'POST':
                send_function = session.post
            async with send_function(url if url else self.api_url, json=payload, headers=headers) as response:
                if response.status == 422:
                    raise APIValidationException(await response.text())
                elif response.status == 200:
                    return await response.text()
                else:
                    raise UnknownAPIException(await response.text())

    @staticmethod
    def set_snackbar(request, name, message):
        request.session[f'snackbar_{name}'] = message

    @staticmethod
    def get_snackbar(request, name):
        snackbar = request.session.get(f'snackbar_{name}', None)
        if snackbar:
            del request.session[f'snackbar_{name}']

        return snackbar

    @staticmethod
    def get_documentation(request):
        documentation = []

        for qs in request.query_params._list:
            if qs[0] == 'status':
                documentation.append(qs[1])
        return documentation

    async def get_result_list(self, request: Request, url: str, additional_qs: str = None) -> (str, int, int, int, int, List[dict]):
        query = get_search_phrase(request)
        page = get_page(request)
        limit = get_limit(request)
        prev_page = page
        next_page = page

        url = f'{self.api_url}{url}'
        if query or limit or page or additional_qs:
            url = f"{url}?"
            if query:
                url = f"{url}q={query}&"
            if limit:
                url = f"{url}limit={limit}&"
            if page:
                url = f"{url}page={page}&"
            if additional_qs:
                url = f"{url}{additional_qs}"

        async with aiohttp.ClientSession() as session:
            headers = token_headers(request, self.session_key)
            data = await get(session, url, headers=headers)

        content = json.loads(data)
        if page - 1 > 0:
            prev_page = page - 1

        if content and limit == len(content):
            next_page = page + 1

        return query, page, limit, content, prev_page, next_page

    async def get_from_api(self, request: Request, url: str) -> dict:
        async with aiohttp.ClientSession() as session:
            headers = token_headers(request, self.session_key)
            data = await get(session, url, headers=headers)

        return json.loads(data)

    async def get_job_log(self, work_server_address: str, job_id: int) -> str:
        if work_server_address and job_id:
            async with aiohttp.ClientSession() as session:
                data = await get(session, f'{work_server_address}/jobs/{job_id}/logs')

            return data
        else:
            return ''

    async def get_work_server_pool_status(self, work_server_address: str) -> Dict:
        if work_server_address:
            try:
                async with aiohttp.ClientSession() as session:
                    data = await get(session, f'{work_server_address}/pool')

                return json.loads(data)
            except:
                return {'free': 'Err!', 'running': 'Err!'}
        else:
            return {'free': '0', 'running': '0'}

    async def delete_from_api(self, request: Request) -> bool:
        async with aiohttp.ClientSession() as session:
            headers = token_headers(request, self.session_key)
            async with session.delete(self.api_url, headers=headers) as response:
                if response.status == 422:
                    raise APIValidationException(await response.text())
                elif response.status == 200:
                    return bool(await response.text())
                else:
                    raise UnknownAPIException(await response.text())

    async def post_to_api(self, request: Request) -> bool:
        async with aiohttp.ClientSession() as session:
            headers = token_headers(request, self.session_key)
            async with session.post(self.api_url, headers=headers) as response:
                if response.status == 422:
                    raise APIValidationException(await response.text())
                elif response.status == 200:
                    return bool(await response.text())
                else:
                    raise UnknownAPIException(await response.text())


class BaseFormEndpoint(BaseHTTPEndpoint):
    @property
    def method(self):
        raise NotImplementedError()

    @property
    def tpl(self):
        raise NotImplementedError()

    @property
    def api_url(self):
        raise NotImplementedError()

    @property
    def session_key(self):
        raise NotImplementedError()

    @property
    def action_successful_message(self):
        raise NotImplementedError()

    def redirect_url(self, request: Request, data: dict = None):
        raise NotImplementedError()

    async def get_context(self, request: Request):
        raise NotImplementedError()

    async def _get(self, request):
        return self.render(request, await self.get_context(request))

    async def _post(self, request):
        form_data = await request.form()
        try:
            data = await self.send_data(request, form_data, self.method)
            self.set_snackbar(request, 'action_successful', self.action_successful_message)

            return RedirectResponse(url=self.redirect_url(request, json.loads(data)), status_code=302)
        except APIValidationException as ex:
            # {"detail": [{"loc": ["body", "first_name"], "msg": "none is not an allowed value",
            #              "type": "type_error.none.not_allowed"},
            #             {"loc": ["body", "last_name"], "msg": "none is not an allowed value",
            #              "type": "type_error.none.not_allowed"}]}
            form_errors = {}
            for e in json.loads(ex.errors)['detail']:
                form_errors[e['loc'][1]] = e['msg']

            context = await self.get_context(request)
            context['form_errors'] = form_errors

            return self.render(request, context)
        except UnknownAPIException as ex:
            context = await self.get_context(request)
            context['error_dialog_message'] = ex.errors

            return self.render(request, context)


class ClientFormEndpoint(BaseFormEndpoint, metaclass=ABCMeta):
    @requires(constants.scope_client, redirect='login')
    async def get(self, request):
        return await super(ClientFormEndpoint, self)._get(request)

    @requires(constants.scope_client, redirect='login')
    async def post(self, request):
        return await super(ClientFormEndpoint, self)._post(request)


class FormEndpoint(BaseHTTPEndpoint):
    @property
    def method(self):
        raise NotImplementedError()

    @property
    def tpl(self):
        raise NotImplementedError()

    @property
    def api_url(self):
        raise NotImplementedError()

    @property
    def session_key(self):
        raise NotImplementedError()

    @property
    def action_successful_message(self):
        raise NotImplementedError()

    def redirect_url(self, request: Request, data: dict = None):
        raise NotImplementedError()

    async def get_context(self, request: Request):
        raise NotImplementedError()

    @requires(constants.scope_manager, redirect='login')
    async def get(self, request):
        return self.render(request, await self.get_context(request))

    @requires(constants.scope_manager, redirect='login')
    async def post(self, request):
        form_data = await request.form()
        try:
            data = await self.send_data(request, form_data, self.method)
            self.set_snackbar(request, 'action_successful', self.action_successful_message)

            return RedirectResponse(url=self.redirect_url(request, json.loads(data)), status_code=302)
        except APIValidationException as ex:
            # {"detail": [{"loc": ["body", "first_name"], "msg": "none is not an allowed value",
            #              "type": "type_error.none.not_allowed"},
            #             {"loc": ["body", "last_name"], "msg": "none is not an allowed value",
            #              "type": "type_error.none.not_allowed"}]}
            form_errors = {}
            for e in json.loads(ex.errors)['detail']:
                form_errors[e['loc'][1]] = e['msg']

            context = await self.get_context(request)
            context['form_errors'] = form_errors

            return self.render(request, context)
        except UnknownAPIException as ex:
            context = await self.get_context(request)
            context['error_dialog_message'] = ex.errors

            return self.render(request, context)


class BaseDeleteEndpoint(BaseHTTPEndpoint):
    object_id = None
    list_action = False
    @property
    def tpl(self):
        raise NotImplementedError

    @property
    def api_url(self):
        raise NotImplementedError

    @property
    def session_key(self):
        raise NotImplementedError

    def redirect_url(self, request: Request, result: bool):
        raise NotImplementedError()

    @property
    def action_successful_message(self):
        raise NotImplementedError()

    async def _get(self, request):
        self.object_id = request.path_params['object_id']
        self.list_action = bool(request.query_params.get('_dfl', False))

        try:
            result = await self.delete_from_api(request)
            self.set_snackbar(request, 'action_successful', self.action_successful_message)

            return RedirectResponse(url=self.redirect_url(request, result), status_code=302)
        except APIValidationException as ex:
            self.set_snackbar(request, 'action_successful', 'Neizdevās izdzēst, radās validācijas kļūda')
            return RedirectResponse(url=self.redirect_url(request, False), status_code=302)
        except UnknownAPIException as ex:
            self.set_snackbar(request, 'action_successful', 'Neizdevās izdzēsta, radās neparedzēta kļūda')
            return RedirectResponse(url=self.redirect_url(request, False), status_code=302)


class ClientDeleteEndpoint(BaseDeleteEndpoint, metaclass=ABCMeta):
    @requires(constants.scope_client, redirect='login')
    async def get(self, request):
        return await super(ClientDeleteEndpoint, self)._get(request)


class DeleteEndpoint(BaseHTTPEndpoint):
    object_id = None
    list_action = False
    @property
    def tpl(self):
        raise NotImplementedError

    @property
    def api_url(self):
        raise NotImplementedError

    @property
    def session_key(self):
        raise NotImplementedError

    def redirect_url(self, request: Request, result: bool):
        raise NotImplementedError()

    @property
    def action_successful_message(self):
        raise NotImplementedError()

    @requires(constants.scope_manager, redirect='login')
    async def get(self, request):
        self.object_id = request.path_params['object_id']
        self.list_action = bool(request.query_params.get('_dfl', False))

        try:
            result = await self.delete_from_api(request)
            self.set_snackbar(request, 'action_successful', self.action_successful_message)

            return RedirectResponse(url=self.redirect_url(request, result), status_code=302)
        except APIValidationException as ex:
            self.set_snackbar(request, 'action_successful', 'Neizdevās izdzēst, radās validācijas kļūda')
            return RedirectResponse(url=self.redirect_url(request, False), status_code=302)
        except UnknownAPIException as ex:
            self.set_snackbar(request, 'action_successful', 'Neizdevās izdzēsta, radās neparedzēta kļūda')
            return RedirectResponse(url=self.redirect_url(request, False), status_code=302)


class PostEndpoint(BaseHTTPEndpoint):
    object_id = None
    list_action = False
    stay_in_form = False
    @property
    def tpl(self):
        raise NotImplementedError

    @property
    def api_url(self):
        raise NotImplementedError

    @property
    def session_key(self):
        raise NotImplementedError

    def redirect_url(self, request: Request, result: bool):
        raise NotImplementedError()

    @property
    def action_successful_message(self):
        raise NotImplementedError()

    @requires(constants.scope_manager, redirect='login')
    async def get(self, request):
        self.object_id = request.path_params['object_id']
        self.list_action = bool(request.query_params.get('_dfl', False))
        self.stay_in_form = bool(request.query_params.get('_tf', False))

        try:
            result = await self.post_to_api(request)
            self.set_snackbar(request, 'action_successful', self.action_successful_message)

            return RedirectResponse(url=self.redirect_url(request, result), status_code=302)
        except APIValidationException as ex:
            self.set_snackbar(request, 'action_successful', 'Darbība neizdevās, radās validācijas kļūda')
            return RedirectResponse(url=self.redirect_url(request, False), status_code=302)
        except UnknownAPIException as ex:
            self.set_snackbar(request, 'action_successful', 'Darbība neizdevās, radās neparedzēta kļūda')
            return RedirectResponse(url=self.redirect_url(request, False), status_code=302)