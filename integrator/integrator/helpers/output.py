import os
import hashlib

from typing import (
    Optional,
    Generator,
    Callable
)


class StaticFileUrlHandler(object):
    def __init__(self, app, static_dir):
        self.app = app
        self.static_dir = static_dir
        self._static_hashes = {}  # type: Dict[str, Optional[str]]

    @staticmethod
    def get_content(abspath: str, start: Optional[int] = None, end: Optional[int] = None) -> Generator[bytes, None, None]:
        with open(abspath, "rb") as file:
            if start is not None:
                file.seek(start)
            if end is not None:
                remaining = end - (start or 0)  # type: Optional[int]
            else:
                remaining = None
            while True:
                chunk_size = 64 * 1024
                if remaining is not None and remaining < chunk_size:
                    chunk_size = remaining
                chunk = file.read(chunk_size)
                if chunk:
                    if remaining is not None:
                        remaining -= len(chunk)
                    yield chunk
                else:
                    if remaining is not None:
                        assert remaining == 0
                    return

    def get_content_version(self, abspath: str) -> str:
        data = self.get_content(abspath)
        hasher = hashlib.sha512()
        if isinstance(data, bytes):
            hasher.update(data)
        else:
            for chunk in data:
                hasher.update(chunk)
        return hasher.hexdigest()

    def make_static_url(self, request) -> Callable:
        def static_url(path) -> str:
            version_hash = self.get_version(path)
            if not version_hash:
                return request.url_for('static', path=path)

            return f"{request.url_for('static', path=path)}?v{version_hash}"

        return static_url

    def get_version(self, path: str) -> Optional[str]:
        if path and path[0] == '/':
            cal_path = path[1:]
        else:
            cal_path = path

        abs_path = os.path.abspath(os.path.join(self.static_dir, cal_path))
        return self._get_cached_version(abs_path)

    def _get_cached_version(self, abs_path: str) -> Optional[str]:
        hashes = self._static_hashes
        if abs_path not in hashes:
            try:
                hashes[abs_path] = self.get_content_version(abs_path)
            except Exception as ex:
                #TODO fix log
                # ~ gen_log.error("Could not open static file %r", abs_path)
                hashes[abs_path] = None
        hsh = hashes.get(abs_path)
        if hsh:
            return hsh
        return None