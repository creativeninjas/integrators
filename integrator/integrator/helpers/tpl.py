import typing

from starlette.background import BackgroundTask
from starlette.responses import Response
from starlette.types import (
    Scope,
    Receive,
    Send
)

try:
    from chameleon import PageTemplateLoader
except ImportError:  # pragma: nocover
    PageTemplateLoader = None  # type: ignore


class _TemplateResponse(Response):
    media_type = "text/html"

    def __init__(
        self,
        template: typing.Any,
        context: dict,
        status_code: int = 200,
        headers: dict = None,
        media_type: str = None,
        background: BackgroundTask = None,
    ):
        self.template = template
        self.context = context
        content = template(**context)
        super().__init__(content, status_code, headers, media_type, background)

    async def __call__(self, scope: Scope, receive: Receive, send: Send) -> None:
        request = self.context.get("request", {})
        extensions = request.get("extensions", {})
        if "http.response.template" in extensions:
            await send(
                {
                    "type": "http.response.template",
                    "template": self.template,
                    "context": self.context,
                }
            )
        await super().__call__(scope, receive, send)


class ChameleonTemplates:
    """
    templates = ChameleonTemplates("templates")

    return templates.TemplateResponse("index.html", {"request": request})
    """

    def __init__(self, directory: typing.List[str], auto_reload: bool = False) -> None:
        assert PageTemplateLoader is not None, "chameleon must be installed to use ChameleonTemplates"

        self.loader = PageTemplateLoader(directory, auto_reload=auto_reload)

    def get_template(self, name: str) -> "chameleon.Template":
        return self.loader.load(name)

    def TemplateResponse(
        self,
        name: str,
        context: dict,
        status_code: int = 200,
        headers: dict = None,
        media_type: str = None,
        background: BackgroundTask = None,
    ) -> _TemplateResponse:
        if "request" not in context:
            raise ValueError('context must include a "request" key')
        template = self.get_template(name)
        return _TemplateResponse(
            template,
            context,
            status_code=status_code,
            headers=headers,
            media_type=media_type,
            background=background,
        )

    def Component(
        self,
        name: str,
        context: dict
    ) -> str:
        if "request" not in context:
            raise ValueError('context must include a "request" key')
        template = self.get_template(name)

        return template(**context)

# class _TemplateResponse(Response):
#     media_type = "text/html"
#
#     def __init__(
#         self,
#         template: typing.Any,
#         context: dict,
#         status_code: int = 200,
#         headers: dict = None,
#         media_type: str = None,
#         background: BackgroundTask = None,
#     ):
#         self.template = template
#         self.context = context
#         content = template.generate(**context).render('html', doctype='html')
#         super().__init__(content, status_code, headers, media_type, background)
#
#     async def __call__(self, scope: Scope, receive: Receive, send: Send) -> None:
#         request = self.context.get("request", {})
#         extensions = request.get("extensions", {})
#         if "http.response.template" in extensions:
#             await send(
#                 {
#                     "type": "http.response.template",
#                     "template": self.template,
#                     "context": self.context,
#                 }
#             )
#         await super().__call__(scope, receive, send)
#
#
# class GenshiTemplates:
#     """
#     templates = GenshiTemplates("templates")
#
#     return templates.TemplateResponse("index.html", {"request": request})
#     """
#
#     def __init__(self, directory: typing.List[str], auto_reload: bool = False) -> None:
#         assert TemplateLoader is not None, "genshi must be installed to use GenshiTemplates"
#
#         self.loader = TemplateLoader(directory, auto_reload=auto_reload)
#
#     def get_template(self, name: str) -> "genshi.Template":
#         return self.loader.load(name)
#
#     def TemplateResponse(
#         self,
#         name: str,
#         context: dict,
#         status_code: int = 200,
#         headers: dict = None,
#         media_type: str = None,
#         background: BackgroundTask = None,
#     ) -> _TemplateResponse:
#         if "request" not in context:
#             raise ValueError('context must include a "request" key')
#         template = self.get_template(name)
#         return _TemplateResponse(
#             template,
#             context,
#             status_code=status_code,
#             headers=headers,
#             media_type=media_type,
#             background=background,
#         )