from starlette.requests import Request


def get_search_phrase(request: Request, name: str = 'q', default: str = None) -> str:
    try:
        q = request.query_params[name]
        if len(q) == 0:
            q = None
    except Exception:
        q = default

    return q


def get_page(request: Request, name: str = 'p', default: int = 1) -> int:
    try:
        page = int(request.query_params[name])
    except Exception:
        page = default

    return page


def get_limit(request: Request, name: str = 'l', default: int = 10) -> int:
    try:
        if request.query_params.get(name, None) is not None:
            request.session['filter_l'] = request.query_params[name]

        page = int(request.session['filter_l'])

    except Exception:
        page = default

    return page