import json
import os
import time
from datetime import (
    datetime,
    timedelta
)
from typing import Dict

import eventlet
from eventlet.green.urllib import request
from eventlet.green.urllib import parse
from jinja2 import (
    Environment,
    FileSystemLoader,
    select_autoescape
)

import settings

bs4 = eventlet.import_patched('bs4')

tikitaka_tokens = {}  # Store TikiTaka auth tokens


class Job(object):
    def __init__(self, jid):
        self.jid = jid
        self.started_at = datetime.now()
        self.completed = False
        self.completed_at = None
        self.run_time = None
        self.canceled = False

    def add_log_entry(self, action: str):
        with open(f'job_logs/{self.jid}.log', 'a') as log:
            log.write(f'{datetime.now()} {action}')
            log.write(os.linesep)

    def set_completed(self):
        self.completed = True
        self.completed_at = datetime.now()
        self.run_time = self.completed_at - self.started_at

    def set_canceled(self):
        self.canceled = True


def build_horizon_product_url(work_task, filter_key):
    # Build api product url
    qs = {
        'columns': 'N_KODS,N_NOSAUK,NBR_NOSAUK,CEN_CENA,L_KODS,NBR_BAR_KODS,VIEN_NOSAUK,VIEN_KODS,G_KODS,G_NOSAUK'}

    if work_task['attributes']:
        attributes = json.loads(work_task['attributes'])
        price_filter = str(attributes.get('price', '')).strip()
        if len(price_filter) > 0:
            qs['columns'] = qs['columns'].replace('CEN_CENA', price_filter)

    if work_task['attributes']:
        attributes = json.loads(work_task['attributes'])
        product_filter = str(attributes.get(filter_key, '')).strip()
        if len(product_filter) > 0:
            qs['filter'] = product_filter

    return f"{work_task['foreign_system_connection']['address']}/rest/TNdmNomSar/query?{parse.urlencode(qs, quote_via=parse.quote)}"


def build_horizon_product_price_tag(work_task):
    if work_task['attributes']:
        work_task['attributes'] = json.loads(work_task['attributes'])

    price = work_task['attributes']['price'].split('_')[0]

    if len(price) == 0:
        price = 'CEN'

    return price


def build_horizon_online_orders_url(work_task, filter_key):
    # Build api product url
    qs = {
        'columns': 'PDOK.DOK_NR, PDOK.DAT_DOK, PDOK.K.KODS, PDOK.K.NOSAUK, PDOK.SUMMA, PDOK.SUMMA_PIES_PV, PDOK.NEPIESAIST_PV, PDOK.SUMMA_PIES, PDOK.NEPIESAIST, PDOK.NEPIESAIST, PDOK.PIESPROC.PROC_PIES'}

    if work_task['attributes']:
        attributes = json.loads(work_task['attributes'])
        online_order_filter = str(attributes.get(filter_key, '')).strip()
        if len(online_order_filter) > 0:
            qs['filter'] = online_order_filter
        else:
            # default filter
            qs['filter'] = 'PDOK.NEPIESAIST ge 0.009'

    return f"{work_task['foreign_system_connection']['address']}/rest/TDdmReaPrecDokSar/query?{parse.urlencode(qs, quote_via=parse.quote)}"


def build_tikitaka_product_tax_categories(items):
    product_tax_categories = None
    if items:
        product_tax_categories = []
        for i in items:
            product_tax_categories.append({
                'id': 0,
                'title': i['title'],
                'remove': False}
            )

    return product_tax_categories


def search_tikitaka(
        attribute_name: str,
        attribute_value,
        url: str,
        query: str,
        token_for_header: str,
        return_all=False,
        extra_qs=None):

    headers = {
        'Content-Type': 'application/json',
        'accept': 'application/json',
        'Authorization': token_for_header}

    qs = {'q': query, 'skip': 0, 'limit': 100, 'order': 'DESC'}
    if extra_qs:
        qs = {**qs, **extra_qs}
    url = f'{url}?{parse.urlencode(qs, quote_via=parse.quote)}'
    # url = f'{url}?q={query}&skip=0&limit=100&order=DESC'

    request_object = request.Request(url, headers=headers)
    response = request.urlopen(request_object)

    items = json.loads(response.read())
    if items is not None and items['total'] > 0:
        for item in items['results']:
            if item.get(attribute_name, None) == attribute_value:
                if return_all:
                    return item
                else:
                    return item['id']

    return None


def get_from_tikitaka(url: str, token_for_header: str):
    headers = {
        'Content-Type': 'application/json',
        'accept': 'application/json',
        'Authorization': token_for_header}

    request_object = request.Request(url, headers=headers)
    response = request.urlopen(request_object)

    result = json.loads(response.read())

    return result


def data_to_tikitaka(payload: dict, url: str, token_for_header: str, payload_to_error_message: bool = False):
    try:
        headers = {
            'Content-Type': 'application/json',
            'accept': 'application/json',
            'Authorization': token_for_header}

        request_object = request.Request(url, data=json.dumps(payload).encode(), headers=headers)
        response = request.urlopen(request_object)

        if response.code == 200:
            data = json.loads(response.read())
            return data['id']
        else:
            payload_to_message = ''
            if payload_to_error_message:
                payload_to_message = f' Payload: {payload}'
            exc = Exception()
            exc.message = f'Response code is not 200: {response.code} - {response.read()}{payload_to_message}'
            raise exc
    except Exception as e:
        if e.code == 422:
            payload_to_message = ''
            if payload_to_error_message:
                payload_to_message = f' Payload: {payload}'

            json_data = e.read()
            json_data = json.loads(json_data)
            message = f"{json_data}{payload_to_message}"
            raise Exception(message)
        elif e.code == 500:
            json_data = e.read()
            json_data = json.loads(json_data)
            message = f"TikiTaka response 500. {json_data}"
            raise Exception(message)
        else:
            raise e


def load_tikitaka_api_access_token(auth_url, username, pwd):
    try:
        payload = {'grant_type': '',
                   'username': username,
                   'password': pwd,
                   'scope': '',
                   'client_id': '',
                   'client_secret': ''}

        data = parse.urlencode(payload).encode()
        request_object = request.Request(f'{auth_url}/clients/token', data=data)
        response = request.urlopen(request_object)

        if response.code == 200:
            token_data = json.loads(response.read())

            return {
                'authorized': True,
                'token_for_header': f" {token_data['token_type']} {token_data['access_token']}",
            }
        else:
            return {
                'authorized': False,
                'error': f'API authorize response is not 200: {response.read()}',
            }
    except Exception as e:
        if hasattr(e, 'message'):
            message = e.message
        else:
            message = f"{e.__class__}: {str(e)}"
        return {
            'authorized': False,
            'error': message,
        }


def get_tikitaka_api_access_token(auth_url, username, pwd):
    tk = f'{auth_url}, {username}, {pwd}'
    if tikitaka_tokens.get(tk):
        if tikitaka_tokens.get(tk)['working']:
            while tikitaka_tokens.get(tk)['working']:
                eventlet.sleep(1)
            return tikitaka_tokens[tk]['auth_token']
        elif tikitaka_tokens[tk]['created_at'] + timedelta(minutes=settings.TIKITAKA_API_TOKEN_TTL) < datetime.now():
            # renew token
            pass
        else:
            return tikitaka_tokens[tk]['auth_token']

    tikitaka_tokens[tk] = {}
    tikitaka_tokens[tk]['working'] = True
    tikitaka_tokens[tk]['auth_token'] = load_tikitaka_api_access_token(auth_url, username, pwd)
    tikitaka_tokens[tk]['created_at'] = datetime.now()
    tikitaka_tokens[tk]['working'] = False

    return tikitaka_tokens[tk]['auth_token']


def set_status(available: bool, unavailability_reason: str = None, error_message: str = None):
    status_file = open('status', 'w')
    status_file.write(
        json.dumps(
            {'available': available, 'unavailability_reason': unavailability_reason, 'error_message': error_message}
        )
    )
    status_file.close()


def set_pool_status(pool: eventlet.GreenPool):
    status_file = open('pool_status', 'w')
    status_file.write(
        json.dumps(
            {'free': pool.free(), 'running': pool.running(), 'refreshed_at': str(datetime.now())}
        )
    )
    status_file.close()


# def set_jobs_status(jobs: Dict[str, Job]):
#     status_file = open('jobs_status', 'w')
#     jobs_json = []
#     for job_id in jobs:
#         j = jobs[job_id]
#         jobs_json.append(
#             {'id': job_id,
#              'started_at': str(j.started_at),
#              'completed_at': str(j.completed_at),
#              'run_time': str(j.run_time),
#              'completed': j.completed,
#              'canceled': j.canceled,
#              'actions': j.actions,
#
#             }
#         )
#
#     status_file.write(
#         json.dumps(
#             jobs_json
#         )
#     )
#     status_file.close()
#     total_jobs = len(jobs_json)
#
#     # dump completed
#     if total_jobs > 100:
#         jobs_json = []
#         keys_to_remove = []
#         for job_id in jobs:
#             j = jobs[job_id]
#             if j.completed or j.canceled:
#                 jobs_json.append(
#                     {'id': job_id,
#                      'started_at': str(j.started_at),
#                      'completed_at': str(j.completed_at),
#                      'run_time': str(j.run_time),
#                      'completed': j.completed,
#                      'canceled': j.canceled,
#                      'actions': j.actions,
#
#                      }
#                 )
#                 keys_to_remove.append(job_id)
#
#         if len(jobs_json) > 0:
#             status_file = open(f'proccessed_jobs/{str(int(time.time()))}_dump.json', 'w')
#             status_file.write(
#                 json.dumps(
#                     jobs_json
#                 )
#             )
#             status_file.close()
#
#             total_jobs = total_jobs - len(keys_to_remove)
#             for k in keys_to_remove:
#                 del jobs[k]
#
#     return total_jobs


def set_error_before_fail(error_message: str):
    error_message_file = open('error_message', 'w')
    error_message_file.write(error_message)
    error_message_file.close()


def get_work_server_settings():
    pid_file = open('pid', 'r')
    id_work_server = int(pid_file.read())
    pid_file.close()

    token_ok = False
    access_token = ''
    while not token_ok:
        try:
            access_token_file = open('access_token', 'r')
            access_token = access_token_file.read()
            access_token_file.close()

            access_token_renew_dt_file = open('access_token_renew_dt', 'r')
            renew_dt = datetime.fromisoformat(access_token_renew_dt_file.read())
            access_token_renew_dt_file.close()

            if datetime.now() < renew_dt:
                token_ok = True
            else:
                # wait for token renew
                eventlet.sleep(2)
        except Exception:
            # error occurred - wait for settings to be available
            eventlet.sleep(2)

    return {'id_work_server': id_work_server, 'access_token': access_token}


def put_headers():
    work_server_settings = get_work_server_settings()
    headers = {
        'Content-Type': 'application/json',
        'accept': 'application/json',
        'Authorization': work_server_settings['access_token']
    }

    return headers


def cancel_job_with_error(id_job: int, error_message: str):
    try:
        work_server_settings = get_work_server_settings()
        headers = put_headers()

        url = f"{settings.API_URL}/manage/work-servers/{work_server_settings['id_work_server']}/jobs/{id_job}/cancel-with-error"

        data = json.dumps({'execution_error_message': error_message}).encode()
        request_object = request.Request(url, headers=headers, data=data)
        request_object.get_method = lambda: 'PUT'
        response = request.urlopen(request_object)
        return json.loads(response.read())
    except Exception as e:
        if hasattr(e, 'message'):
            message = e.message
        else:
            message = f"{e.__class__}: {str(e)}"

        set_error_before_fail(error_message)
        set_status(False, 'Executing cancel with status', message)


def add_worker_log(id_fsc: int, id_work: int, task_code: str, level: int, entry: str, message: str):
    headers = put_headers()

    url = f"{settings.API_URL}/system/logs/workers/logs"
    payload = {
        'id_fsc': id_fsc,
        'id_work': id_work,
        'task_code': task_code,
        'level': level,
        'entry': entry,
        'message': message,
    }
    data = json.dumps(payload).encode()
    request_object = request.Request(url, headers=headers, data=data)
    request_object.get_method = lambda: 'PUT'
    response = request.urlopen(request_object)
    return json.loads(response.read())


def get_horizon_response_soup(work_task, url, parser='html.parser'):
    username = work_task['foreign_system_connection']['username']
    pwd = work_task['foreign_system_connection']['pwd']

    # create a password manager
    password_mgr = request.HTTPPasswordMgrWithDefaultRealm()
    password_mgr.add_password(None, work_task['foreign_system_connection']['address'], username, pwd)

    handler = request.HTTPBasicAuthHandler(password_mgr)

    # create "opener" (OpenerDirector instance)
    opener = request.build_opener(handler)

    response = opener.open(url)
    html = response.read()
    soup = bs4.BeautifulSoup(html, parser)

    return soup


def get_horizon_response_json(work_task, url):
    username = work_task['foreign_system_connection']['username']
    pwd = work_task['foreign_system_connection']['pwd']

    # create a password manager
    password_mgr = request.HTTPPasswordMgrWithDefaultRealm()
    password_mgr.add_password(None, work_task['foreign_system_connection']['address'], username, pwd)

    handler = request.HTTPBasicAuthHandler(password_mgr)

    # create "opener" (OpenerDirector instance)
    opener = request.build_opener(handler)
    opener.addheaders.append(('Accept', 'application/json'))
    response = opener.open(url)
    json_data = response.read()

    return json.loads(json_data)


def post_xml_to_horizon(work_task, url, xml_data):
    username = work_task['foreign_system_connection']['username']
    pwd = work_task['foreign_system_connection']['pwd']

    # create a password manager
    password_mgr = request.HTTPPasswordMgrWithDefaultRealm()
    password_mgr.add_password(None, work_task['foreign_system_connection']['address'], username, pwd)

    handler = request.HTTPBasicAuthHandler(password_mgr)

    # create "opener" (OpenerDirector instance)
    opener = request.build_opener(handler)

    data = xml_data.encode()

    req = request.Request(url, data=data, headers={'Accept': 'application/json', 'Content-Type': 'application/xml'})
    try:
        response = opener.open(req)
        json_data = response.read()
    except Exception as e:
        if e.code == 422:
            json_data = e.read()
            json_data = json.loads(json_data)
            message = f"{json_data['class']}: {json_data['message']}"
            raise Exception(message)
        elif e.code == 500:
            json_data = e.read()
            json_data = json.loads(json_data)
            message = f"Horizon response 500. {json_data}"
            raise Exception(message)
        else:
            raise e

    return json.loads(json_data)


class Payment(object):
    def __init__(self, href):
        self.href = href
        self.amount = 0.0

    def add_amount(self, amount):
        self.amount = self.amount + float(amount)

    @property
    def oid(self):
        return self.href.split('/')[-1]


class Product(object):
    def __init__(self, doc_id, device_serial, pk_nom_href, pos_code, quantity, price, discount, total_price,
                 total_price_no_vat, unit_code, client_info, unit_href):
        self.doc_id = doc_id
        self.device_serial = device_serial
        self.pk_nom_href = pk_nom_href
        self.pos_code = pos_code
        self.quantity = quantity
        self.price = price
        self.discount = discount
        self.total_price = total_price
        self.total_price_no_vat = total_price_no_vat
        self.unit_code = unit_code
        self.client_info = client_info
        self.unit_href = unit_href
        try:
            self.unit_id = unit_href.strip('/').split('/')[-1]
        except:
            self.unit_id = None


def find_horizon_product_href_by_code(work_task, product_code):
    url = f"{work_task['foreign_system_connection']['address']}/rest/TNdmNomSar/query?columns=N_KODS&filter=N_KODS eq {product_code}"

    data = get_horizon_response_json(work_task, url)

    for wh in data['collection']['row']:
        if wh['N'].get('KODS', None) is not None:
            if wh['N']['KODS'] == product_code or wh['N']['KODS'] == product_code.lower() or wh['N']['KODS'] == product_code.upper():
                return wh['N']['PK_NOM']['href']

    return None


def find_horizon_unit_href_by_code(work_task, unit_code):
    url = f"{work_task['foreign_system_connection']['address']}/rest/TNdmVienSar/query?columns=V_KODS"

    data = get_horizon_response_json(work_task, url)

    for wh in data['collection']['row']:
        if wh['V'].get('KODS', None) is not None:
            if wh['V']['KODS'] == unit_code:
                return wh['V']['PK_VIEN']['href']

    return None


def find_horizon_warehouse_href_by_code(work_task):
    # url = f"{work_task['foreign_system_connection']['address']}/rest/TNdmNolSar/default"
    url = f"{work_task['foreign_system_connection']['address']}/rest/TNdmNolSar/query?columns=NOL_KODS"

    data = get_horizon_response_json(work_task, url)

    for wh in data['collection']['row']:
        if wh['NOL'].get('KODS', None) is not None:
            if wh['NOL']['KODS'] == work_task['attributes']['warehouse_code']:
                return wh['NOL']['PK_KLIENTS']['href']

    return None


def find_horizon_invoice_href_by_code(work_task):
    url = f"{work_task['foreign_system_connection']['address']}/rest/TDdmSaviRekSarVisi/default"
    data = get_horizon_response_json(work_task, url)

    for wh in data['collection']['row']:
        if wh['REK'].get('KODS', None) is not None:
            if wh['REK']['KODS'] == work_task['attributes']['invoice_code']:
                return wh['REK']['PK_REKINS']['href']

    return None


def find_horizon_customer_href_by_code(work_task):
    url = f"{work_task['foreign_system_connection']['address']}/rest/TDdmKlSar/default"
    data = get_horizon_response_json(work_task, url)

    for wh in data['collection']['row']:
        if wh['K'].get('KODS', None) is not None:
            if wh['K']['KODS'] == work_task['attributes']['default_customer_code']:
                return wh['K']['PK_KLIENTS']['href']

    return None


def find_horizon_vat_href_by_code(work_task, vat_code):
    url = f"{work_task['foreign_system_connection']['address']}/rest/TdmPvnLikmesSl/default"

    data = get_horizon_response_json(work_task, url)

    for wh in data['collection']['row']:
        if wh['L'].get('KODS', None) is not None:
            if wh['L']['KODS'] == vat_code:
                return wh['L']['PK_LIKME']['href']

    return None


def find_horizon_online_order_product_id_by_vat(work_task, attributes, vat_code):
    for pair in attributes['vat_list'].split(' '):
        vat, code = pair.split(':')
        if vat == vat_code:
            product_href = find_horizon_product_href_by_code(work_task, code)
            return product_href.split('/')[-1]

    return None


def build_horizon_online_order_deal_doc(work_task, document, non_integrated_fiscof) -> str:
    env = Environment(loader=FileSystemLoader('templates'), autoescape=select_autoescape(['html', 'xml']))

    payment_href = {
        'ExternalCard': '/rest/TNdmBlMaksV/1',
        'Card': '/rest/TNdmBlMaksV/1',
        'Cash': '/rest/TNdmBlMaksV/2'
    }

    attributes = {}
    if work_task['attributes']:
        attributes = json.loads(work_task['attributes'])

    # Build
    payments = {}
    not_found_products = []
    products = []

    soup = bs4.BeautifulSoup(document['deal_doc']['doc'], "html.parser")
    if soup.find('document')['dok_veids'] == 'darījums' and soup.find('document')['dok_operacija'] == 'pārdošana':
        # Get client name and extra information
        for p in soup.ceka_preces.find_all('prece'):
            pos_code = p.pap_parametri.find('value', {'name': 'PosCode'}).text

            product_id = find_horizon_online_order_product_id_by_vat(
                work_task, attributes, p.find('pvn')['pvn_nosaukums'])

            if product_id is None:
                not_found_products.append((pos_code, p['preces_nosaukums'],))
            else:
                products.append({
                    'price': p.summa_kopa.text,
                    'product_id': product_id,
                })

        for pt in soup.ceka_kopsumma.ceka_norekinu_veidi.find_all('ceka_norekinu_veids'):
            if pt['ceka_norekinu_veids'] == 'skaidra':
                if payments.get('cash', None) is None:
                    payments['cash'] = Payment(payment_href['Cash'])
                payments['cash'].add_amount(pt.ceka_valuta['valutas_summa'])
            elif pt['ceka_norekinu_veids'] == 'bezskaidra' and pt['apmaksas_kods'] in ['Card', 'ExternalCard']:
                if payments.get('card', None) is None:
                    payments['card'] = Payment(payment_href['Card'])
                payments['card'].add_amount(pt.ceka_valuta['valutas_summa'])

        # Remove cash change
        if payments.get('cash', False):
            for pt in soup.ceka_kopsumma.fis_ceka_norekinu_veidi.find_all('ceka_norekinu_veids'):
                if pt['ceka_norekinu_veids'] == 'atlikums':
                    payments['cash'].add_amount(float(pt.ceka_valuta['valutas_summa']) * (-1.0))

    if len(not_found_products) > 0:
        message = 'Neatrasto produktu kodu un nosaukumu saraksts:\n'
        for nfp in not_found_products:
            message = f"{message}{nfp[0]}: {nfp[1]}\n"
        raise Exception(message)

    if payments.get('cash', None) is None:
        payments['cash'] = Payment(payment_href['Cash'])

    total = sum([p['price'] * p['quantity'] for p in json.loads(non_integrated_fiscof['products'])])
    now_str = datetime.now().strftime("%Y-%m-%d")
    doc_number = soup.find('document')['dok_ext_numurs']

    template = env.get_template('horizon_fisk_doc.xml')
    out = template.render(
        int_time=int(time.time()),
        now_str=now_str,
        client_id=document['original_doc']['entity']['PK_KLIENTS']['href'].split('/')[-1],
        total=total,
        payments=payments,
        printer_id=attributes['printer_id'],
        products=products,
        doc_number=doc_number
    )

    return out


def build_horizon_online_order_relation_doc(dok_id, related_dok_id, payment_amount) -> str:
    env = Environment(loader=FileSystemLoader('templates'), autoescape=select_autoescape(['html', 'xml']))

    currency_id = 3

    template = env.get_template('horizon_fisk_relation.xml')
    out = template.render(
        dok_id=dok_id,
        related_dok_id=related_dok_id,
        total=payment_amount,
        currency_id=currency_id)

    return out


def build_horizon_kp_from_fiscof(work_task, fiscof: dict, horizon_products: dict) -> str:
    env = Environment(loader=FileSystemLoader('templates'), autoescape=select_autoescape(['html', 'xml']))

    payment_href = {
        'ExternalCard': '/rest/TNdmBlMaksV/1',
        'Card': '/rest/TNdmBlMaksV/1',
        'Cash': '/rest/TNdmBlMaksV/2'
    }

    # Build
    products = []
    payments = {}
    bruto = 0.0
    neto = 0.0
    document_count = 0

    not_found_products = []
    not_found_units = []
    for doc in fiscof['documents']:
        soup = bs4.BeautifulSoup(doc['doc'], "html.parser")
        if soup.find('document')['dok_veids'] == 'darījums' and soup.find('document')['dok_operacija'] == 'pārdošana':
            try:
                dok_ext_uid = soup.find('document')['dok_ext_uid']
                pass
            except KeyError:
                document_count += 1

                # Get client name and extra information
                doc_soup = soup.find('document')
                client_info = ''
                if doc_soup.klients:
                    client_info = f", Klienta nosaukums: {doc_soup.klients['klient_nosaukums']}"
                    for val in doc_soup.find_all("value"):
                        if val.get('name', None) and val['name'] == 'CustomerExtraInformation':
                            client_info = f"{client_info}, Papildus informācija: {val.text}"

                for p in soup.ceka_preces.find_all('prece'):
                    pos_code = p.pap_parametri.find('value', {'name': 'PosCode'}).text
                    total_price = p.summa_kopa.text
                    unit_code = p['mervieniba']

                    if horizon_products.get(pos_code, None) is None:
                        not_found_products.append((pos_code, p['preces_nosaukums'],))

                    unit_href = find_horizon_unit_href_by_code(work_task, p['mervieniba'])
                    if unit_href is None:
                        not_found_units.append((unit_code, p['mervieniba'],))

                    products.append(
                        Product(
                            doc['id'],
                            fiscof['device_serial_number'],
                            horizon_products.get(pos_code, None),
                            pos_code,
                            p['daudzums'],
                            p['cena'],
                            p['atlaide'],
                            total_price,
                            p['summa_bez_pvn'],
                            p['mervieniba'],
                            client_info,
                            unit_href,
                        )
                    )

                    bruto += float(total_price)
                    neto += float(p['summa_bez_pvn'])

                for pt in soup.ceka_kopsumma.ceka_norekinu_veidi.find_all('ceka_norekinu_veids'):
                    if pt['ceka_norekinu_veids'] == 'skaidra':
                        if payments.get('cash', None) is None:
                            payments['cash'] = Payment(payment_href['Cash'])
                        payments['cash'].add_amount(pt.ceka_valuta['valutas_summa'])
                    elif pt['ceka_norekinu_veids'] == 'bezskaidra' and pt['apmaksas_kods'] in ['Card', 'ExternalCard']:
                        if payments.get('card', None) is None:
                            payments['card'] = Payment(payment_href['Card'])
                        payments['card'].add_amount(pt.ceka_valuta['valutas_summa'])

                # Remove cash change
                if payments.get('cash', False):
                    for pt in soup.ceka_kopsumma.fis_ceka_norekinu_veidi.find_all('ceka_norekinu_veids'):
                        if pt['ceka_norekinu_veids'] == 'atlikums':
                            payments['cash'].add_amount(float(pt.ceka_valuta['valutas_summa'])*(-1.0))

    if len(not_found_products) > 0:
        message = 'Neatrasto produktu kodu un nosaukumu saraksts:\n'
        for nfp in not_found_products:
            message = f"{message}{nfp[0]}: {nfp[1]}\n"
        raise Exception(message)

    if len(not_found_units) > 0:
        message = 'Neatrasto mērvienību kodu saraksts:\n'
        for nfu in not_found_units:
            message = f"{message}{nfu[0]}\n"
        raise Exception(message)

    if payments.get('cash', None) is None:
        payments['cash'] = Payment(payment_href['Cash'])

    doc_datetime = datetime.fromisoformat(fiscof['doc_datetime'])

    espats_href = find_horizon_warehouse_href_by_code(work_task)
    pk_r_es = None  # at this moment is not required find_horizon_invoice_href_by_code(work_task)
    pk_klients = find_horizon_customer_href_by_code(work_task)
    dat_dok = doc_datetime.strftime('%Y-%m-%d')
    dok_nr = 'KP-' + doc_datetime.strftime('%Y-%m-%d')
    doc_type = work_task['attributes']['doc_type']
    wherefore = f'Bruto summa: {round(bruto ,2)} Neto summa: {round(neto,2)} Čeku skaits: {document_count}'

    template = env.get_template('kp.xml')
    out = template.render(
        doc_type=doc_type,
        products=products,
        payments=payments,
        espats_href=espats_href,
        pk_r_es=pk_r_es,
        pk_klients=pk_klients,
        dat_dok=dat_dok,
        dok_nr=dok_nr,
        wherefore=wherefore)

    return out


def build_horizon_return_from_fiscof(work_task, fiscof: dict, horizon_products: dict) -> str:
    env = Environment(loader=FileSystemLoader('templates'), autoescape=select_autoescape(['html', 'xml']))

    payment_href = {
        'ExternalCard': '/rest/TNdmBlMaksV/1',
        'Card': '/rest/TNdmBlMaksV/1',
        'Cash': '/rest/TNdmBlMaksV/2'
    }

    # Build
    products = []
    payments = {}
    bruto = 0.0
    neto = 0.0
    document_count = 0

    not_found_products = []
    for doc in fiscof['documents']:
        soup = bs4.BeautifulSoup(doc['doc'], "html.parser")
        if soup.find('document')['dok_veids'] == 'darījums' and soup.find('document')['dok_operacija'] == 'atgriešana':
            document_count += 1

            # Get client name and extra information
            doc_soup = soup.find('document')
            client_info = ''
            if doc_soup.klients:
                client_info = f", Klienta nosaukums: {doc_soup.klients['klient_nosaukums']}"
                for val in doc_soup.find_all("value"):
                    if val.get('name', None) and val['name'] == 'CustomerExtraInformation':
                        client_info = f"{client_info}, Papildus informācija: {val.text}"

            for p in soup.ceka_preces.find_all('prece'):
                pos_code = p.pap_parametri.find('value', {'name': 'PosCode'}).text
                total_price = float(p.summa_kopa.text) * (-1.0)
                quantity = float(p['daudzums']) * (-1.0)

                if horizon_products.get(pos_code, None) is None:
                    not_found_products.append((pos_code, p['preces_nosaukums'],))

                unit_href = find_horizon_unit_href_by_code(work_task, p['mervieniba'])
                if unit_href is None:
                    unit_href = find_horizon_unit_href_by_code(work_task, p['mervieniba'])

                products.append(
                    Product(
                        doc['id'],
                        fiscof['device_serial_number'],
                        horizon_products.get(pos_code, None),
                        pos_code,
                        quantity,
                        p['cena'],
                        p['atlaide'],
                        total_price,
                        p['summa_bez_pvn'],
                        p['mervieniba'],
                        client_info,
                        unit_href,
                    )
                )

                bruto += float(total_price)
                neto += float(p['summa_bez_pvn'])

            for pt in soup.ceka_kopsumma.ceka_norekinu_veidi.find_all('ceka_norekinu_veids'):
                if pt['ceka_norekinu_veids'] == 'skaidra':
                    if payments.get('cash', None) is None:
                        payments['cash'] = Payment(payment_href['Cash'])
                    payments['cash'].add_amount(pt.ceka_valuta['valutas_summa'])
                elif pt['ceka_norekinu_veids'] == 'bezskaidra' and pt['apmaksas_kods'] in ['Card', 'ExternalCard']:
                    if payments.get('card', None) is None:
                        payments['card'] = Payment(payment_href['Card'])
                    payments['card'].add_amount(pt.ceka_valuta['valutas_summa'])

            # Remove cash change
            if payments.get('cash', False):
                for pt in soup.ceka_kopsumma.fis_ceka_norekinu_veidi.find_all('ceka_norekinu_veids'):
                    if pt['ceka_norekinu_veids'] == 'atlikums':
                        payments['cash'].add_amount(float(pt.ceka_valuta['valutas_summa'])*(-1.0))

    if len(not_found_products) > 0:
        message = 'Neatrasto produktu kodu un nosaukumu saraksts:\n'
        for nfp in not_found_products:
            message = f"{message}{nfp[0]}: {nfp[1]}\n"
        raise Exception(message)

    if payments.get('cash', None) is None:
        payments['cash'] = Payment(payment_href['Cash'])

    doc_datetime = datetime.fromisoformat(fiscof['doc_datetime'])

    espats_href = find_horizon_warehouse_href_by_code(work_task)
    pk_r_es = None  # at this moment is not required find_horizon_invoice_href_by_code(work_task)
    pk_klients = find_horizon_customer_href_by_code(work_task)
    dat_dok = doc_datetime.strftime('%Y-%m-%d')
    dok_nr = 'KP-' + doc_datetime.strftime('%Y-%m-%d')
    doc_type = work_task['attributes']['doc_type']
    wherefore = f'Bruto summa: {round(bruto ,2)} Neto summa: {round(neto,2)} Čeku skaits: {document_count}'

    template = env.get_template('kp.xml')
    out = template.render(
        doc_type=doc_type,
        products=products,
        payments=payments,
        espats_href=espats_href,
        pk_r_es=pk_r_es,
        pk_klients=pk_klients,
        dat_dok=dat_dok,
        dok_nr=dok_nr,
        wherefore=wherefore)

    return out


def find_product_tax_category(product_tax_category_pairs: str, vat_code: str):
    pairs = product_tax_category_pairs.split(' ')
    for pair in pairs:
        code, product_tax_category_id = pair.split(':')
        if code == vat_code:
            return product_tax_category_id

    return None


def build_horizon_kp_from_fiscof_with_return_recipe(work_task, fiscof: dict, horizon_products: dict) -> str:
    env = Environment(loader=FileSystemLoader('templates'), autoescape=select_autoescape(['html', 'xml']))

    payment_href = {
        'ExternalCard': '/rest/TNdmBlMaksV/1',
        'Card': '/rest/TNdmBlMaksV/1',
        'Cash': '/rest/TNdmBlMaksV/2'
    }

    # Build
    products = []
    payments = {}
    bruto = 0.0
    neto = 0.0
    document_count = 0

    not_found_products = []
    not_found_units = []
    for doc in fiscof['documents']:
        soup = bs4.BeautifulSoup(doc['doc'], "html.parser")
        if soup.find('document')['dok_veids'] == 'darījums' and soup.find('document')['dok_operacija'] == 'pārdošana':
            try:
                dok_ext_uid = soup.find('document')['dok_ext_uid']
                pass
            except KeyError:
                document_count += 1

                # Get client name and extra information
                doc_soup = soup.find('document')
                client_info = ''
                if doc_soup.klients:
                    client_info = f", Klienta nosaukums: {doc_soup.klients['klient_nosaukums']}"
                    for val in doc_soup.find_all("value"):
                        if val.get('name', None) and val['name'] == 'CustomerExtraInformation':
                            client_info = f"{client_info}, Papildus informācija: {val.text}"

                for p in soup.ceka_preces.find_all('prece'):
                    pos_code = p.pap_parametri.find('value', {'name': 'PosCode'}).text
                    total_price = p.summa_kopa.text
                    unit_code = p['mervieniba']

                    if horizon_products.get(pos_code, None) is None:
                        not_found_products.append((pos_code, p['preces_nosaukums'],))

                    unit_href = find_horizon_unit_href_by_code(work_task, p['mervieniba'])
                    if unit_href is None:
                        not_found_units.append((unit_code, p['mervieniba'],))

                    products.append(
                        Product(
                            doc['id'],
                            fiscof['device_serial_number'],
                            horizon_products.get(pos_code, None),
                            pos_code,
                            p['daudzums'],
                            p['cena'],
                            p['atlaide'],
                            total_price,
                            p['summa_bez_pvn'],
                            p['mervieniba'],
                            client_info,
                            unit_href,
                        )
                    )

                    bruto += float(total_price)
                    neto += float(p['summa_bez_pvn'])

                for pt in soup.ceka_kopsumma.ceka_norekinu_veidi.find_all('ceka_norekinu_veids'):
                    if pt['ceka_norekinu_veids'] == 'skaidra':
                        if payments.get('cash', None) is None:
                            payments['cash'] = Payment(payment_href['Cash'])
                        payments['cash'].add_amount(pt.ceka_valuta['valutas_summa'])
                    elif pt['ceka_norekinu_veids'] == 'bezskaidra' and pt['apmaksas_kods'] in ['Card', 'ExternalCard']:
                        if payments.get('card', None) is None:
                            payments['card'] = Payment(payment_href['Card'])
                        payments['card'].add_amount(pt.ceka_valuta['valutas_summa'])

                # Remove cash change
                if payments.get('cash', False):
                    for pt in soup.ceka_kopsumma.fis_ceka_norekinu_veidi.find_all('ceka_norekinu_veids'):
                        if pt['ceka_norekinu_veids'] == 'atlikums':
                            payments['cash'].add_amount(float(pt.ceka_valuta['valutas_summa'])*(-1.0))

        if soup.find('document')['dok_veids'] == 'darījums' and soup.find('document')['dok_operacija'] == 'atgriešana':
            document_count += 1

            # Get client name and extra information
            doc_soup = soup.find('document')
            client_info = ''
            if doc_soup.klients:
                client_info = f", Klienta nosaukums: {doc_soup.klients['klient_nosaukums']}"
                for val in doc_soup.find_all("value"):
                    if val.get('name', None) and val['name'] == 'CustomerExtraInformation':
                        client_info = f"{client_info}, Papildus informācija: {val.text}"

            for p in soup.ceka_preces.find_all('prece'):
                pos_code = p.pap_parametri.find('value', {'name': 'PosCode'}).text
                total_price = float(p.summa_kopa.text) * (-1.0)
                quantity = float(p['daudzums']) * (-1.0)

                if horizon_products.get(pos_code, None) is None:
                    not_found_products.append((pos_code, p['preces_nosaukums'],))

                unit_href = find_horizon_unit_href_by_code(work_task, p['mervieniba'])
                if unit_href is None:
                    unit_href = find_horizon_unit_href_by_code(work_task, p['mervieniba'])

                products.append(
                    Product(
                        doc['id'],
                        fiscof['device_serial_number'],
                        horizon_products.get(pos_code, None),
                        pos_code,
                        quantity,
                        p['cena'],
                        p['atlaide'],
                        total_price,
                        p['summa_bez_pvn'],
                        p['mervieniba'],
                        client_info,
                        unit_href,
                    )
                )

                bruto += float(total_price)
                neto += float(p['summa_bez_pvn'])

            for pt in soup.ceka_kopsumma.ceka_norekinu_veidi.find_all('ceka_norekinu_veids'):
                if pt['ceka_norekinu_veids'] == 'skaidra':
                    if payments.get('cash', None) is None:
                        payments['cash'] = Payment(payment_href['Cash'])
                    payments['cash'].add_amount(pt.ceka_valuta['valutas_summa'])
                elif pt['ceka_norekinu_veids'] == 'bezskaidra' and pt['apmaksas_kods'] in ['Card', 'ExternalCard']:
                    if payments.get('card', None) is None:
                        payments['card'] = Payment(payment_href['Card'])
                    payments['card'].add_amount(pt.ceka_valuta['valutas_summa'])

            # Remove cash change
            if payments.get('cash', False):
                for pt in soup.ceka_kopsumma.fis_ceka_norekinu_veidi.find_all('ceka_norekinu_veids'):
                    if pt['ceka_norekinu_veids'] == 'atlikums':
                        payments['cash'].add_amount(float(pt.ceka_valuta['valutas_summa'])*(-1.0))

    if len(not_found_products) > 0:
        message = 'Neatrasto produktu kodu un nosaukumu saraksts:\n'
        for nfp in not_found_products:
            message = f"{message}{nfp[0]}: {nfp[1]}\n"
        raise Exception(message)

    if len(not_found_units) > 0:
        message = 'Neatrasto mērvienību kodu saraksts:\n'
        for nfu in not_found_units:
            message = f"{message}{nfu[0]}\n"
        raise Exception(message)

    if payments.get('cash', None) is None:
        payments['cash'] = Payment(payment_href['Cash'])

    doc_datetime = datetime.fromisoformat(fiscof['doc_datetime'])

    espats_href = find_horizon_warehouse_href_by_code(work_task)
    pk_r_es = None  # at this moment is not required find_horizon_invoice_href_by_code(work_task)
    pk_klients = find_horizon_customer_href_by_code(work_task)
    dat_dok = doc_datetime.strftime('%Y-%m-%d')
    dok_nr = 'KP-' + doc_datetime.strftime('%Y-%m-%d')
    doc_type = work_task['attributes']['doc_type']
    wherefore = f'Bruto summa: {round(bruto ,2)} Neto summa: {round(neto,2)} Čeku skaits: {document_count}'

    template = env.get_template('kp.xml')
    out = template.render(
        doc_type=doc_type,
        products=products,
        payments=payments,
        espats_href=espats_href,
        pk_r_es=pk_r_es,
        pk_klients=pk_klients,
        dat_dok=dat_dok,
        dok_nr=dok_nr,
        wherefore=wherefore)

    return out
