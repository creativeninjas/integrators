import os

import json

from starlette.applications import Starlette
from starlette.responses import JSONResponse, PlainTextResponse
from starlette.routing import Route

import settings


def setup_pid():
    if not os.path.isfile(settings.PID_FILE):
        pid_file = open(settings.PID_FILE, 'w')
        pid_file.write('0')
        pid_file.close()


def set_pid(pid: int):
    pid_file = open(settings.PID_FILE, 'r')
    current_pid = int(pid_file.readline())
    if current_pid == 0 or current_pid != pid:
        write_pid_file = open(settings.PID_FILE, 'w')
        write_pid_file.write(str(pid))
        write_pid_file.close()
    pid_file.close()


async def homepage(request):
    set_pid(request.query_params['_ws_id'])

    status_file = open('status', 'r')
    status = json.loads(status_file.read())

    return JSONResponse(status)


async def pool_status(request):
    status_file = open('pool_status', 'r')
    status = json.loads(status_file.read())

    return JSONResponse(status)


async def job_log(request):
    try:
        log_file = open(f"job_logs/{request.path_params['job_id']}.log", 'r')
        logs = log_file.read()
        log_file.close()
    except FileNotFoundError:
        logs = ''

    return PlainTextResponse(logs)


app = Starlette(
    debug=False,
    on_startup=[setup_pid],
    routes=[
        Route(f'/{settings.SERVER_KEY}', homepage),
        Route(f'/{settings.SERVER_KEY}/pool', pool_status),
        Route('/{}/jobs/{}/logs'.format(settings.SERVER_KEY, '{job_id:int}'), job_log),
    ]
)
