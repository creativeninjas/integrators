import os
import datetime

wd = os.path.join(os.path.dirname(__file__), 'job_logs')
ct = datetime.datetime.now()
for filename in os.listdir(wd):
    f = os.path.join(wd, filename)
    if os.path.isfile(f) and filename != 'readme.txt':
        mdt = datetime.datetime.fromtimestamp(os.path.getmtime(f))
        age = ct - mdt
        if age.days >= 2 and age.seconds > 0:
            os.remove(os.path.join(wd, filename))
