import datetime
import json
import os

import eventlet
from eventlet.green.urllib import request
from eventlet.green.urllib import parse

from available_tasks import available_tasks

from helpers import (
    set_status,
    get_work_server_settings,
    add_worker_log
)

import settings


# Setup pool
pool = eventlet.GreenPool(100)


def do_it():
    eventlet.sleep(2)
    print('wait ok')


# pool spawn non-locking api session renew control function
# pool.spawn_n(control_api_token_renew)

# Start job loop
from eventlet import debug
s = True
while True:
    # Load tasks

    if s:
        pool.spawn_n(do_it)
        s = False

    print(debug.hub_listener_stacks())
    # for f in pool.coroutines_running:
    #     print(f)
    eventlet.sleep(3)
