class APIConnectionException(Exception):
    def __init__(self, errors):
        super().__init__('API connection error')
        self.errors = errors