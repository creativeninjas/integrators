import tasks

available_tasks = {
    'tiki_taka_import_units': tasks.tiki_taka_import_units,
    'horizon_export_units': tasks.horizon_export_units,

    'tiki_taka_import_employees': tasks.tiki_taka_import_employees,
    'horizon_export_employees': tasks.horizon_export_employees,

    'tiki_taka_import_tax': tasks.tiki_taka_import_tax,
    'horizon_export_tax': tasks.horizon_export_tax,

    'tiki_taka_import_products': tasks.tiki_taka_import_products,
    'horizon_export_products': tasks.horizon_export_products,

    'tiki_taka_export_fiscof': tasks.tiki_taka_export_fiscof,
    'horizon_import_fiscof': tasks.horizon_import_fiscof,

    'horizon_import_return_recipe_from_fiscof': tasks.horizon_import_return_recipe_from_fiscof,
    'horizon_import_fiscof_with_return_recipe': tasks.horizon_import_fiscof_with_return_recipe,

    'horizon_export_online_order': tasks.horizon_export_online_order,
    'tiki_taka_import_online_order': tasks.tiki_taka_import_online_order,

    'tiki_taka_export_online_order_status': tasks.tiki_taka_export_online_order_status,
    'horizon_import_online_order': tasks.horizon_import_online_order,
}
