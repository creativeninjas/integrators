import datetime
import json
import os

import eventlet
from eventlet.green.urllib import request
from eventlet.green.urllib import parse

from available_tasks import available_tasks

from helpers import (
    Job,
    set_status,
    get_work_server_settings,
    add_worker_log,
    set_pool_status
)

import settings


jobs = {}


def renew_api_access_token():
    try:
        payload = {'grant_type': '',
                   'username': settings.API_USERNAME,
                   'password': settings.API_PASSWORD,
                   'scope': '',
                   'client_id': '',
                   'client_secret': ''}

        url = f'{settings.API_URL}/system/security/token'

        data = parse.urlencode(payload).encode()
        request_object = request.Request(url, data=data)
        response = request.urlopen(request_object)

        if response.code == 200:
            token_data = json.loads(response.read())
            token_file = open('access_token', 'w')
            token_file.write(f" {token_data['token_type']} {token_data['access_token']}")
            token_file.close()

            token_renew_dt_file = open('access_token_renew_dt', 'w')
            token_renew_dt_file.write(
                str(datetime.datetime.now()
                    + datetime.timedelta(minutes=settings.API_TOKEN_TTL)
                    - datetime.timedelta(seconds=5)
                    )
            )
            token_renew_dt_file.close()

            return True
        else:
            raise Exception(f'API authorize response is not 200: {response.read()}')
    except Exception as e:
        if hasattr(e, 'message'):
            message = e.message
        else:
            message = f"{e.__class__}: {str(e)}"
        set_status(False, 'API connection', message)

        return False


def load_new_jobs():
    try:
        work_server_settings = get_work_server_settings()
        headers = {
            'accept': 'application/json',
            'Authorization': work_server_settings['access_token']
        }

        url = f"{settings.API_URL}/manage/work-servers/{work_server_settings['id_work_server']}/jobs/actual"

        request_object = request.Request(url, headers=headers)
        response = request.urlopen(request_object)
        jobs = json.loads(response.read())
        return jobs
    except Exception as e:
        pass


def get_job_next_task(id_job):
    try:
        work_server_settings = get_work_server_settings()
        headers = {
            'accept': 'application/json',
            'Authorization': work_server_settings['access_token']
        }

        url = f"{settings.API_URL}/manage/work-servers/{work_server_settings['id_work_server']}/jobs/{id_job}/next-task"

        request_object = request.Request(url, headers=headers)
        response = request.urlopen(request_object)
        return json.loads(response.read())
    except Exception as e:
        pass


def do_job(id_job):
    try:
        canceled_with_error = False
        while True:
            work_task = get_job_next_task(id_job)
            if work_task is not None:
                last_work_task = work_task
            if work_task:
                jobs[id_job].add_log_entry(f"Started task \"{work_task['task_code']}\"")
                if work_task['task_code'] == 'horizon_export_online_order':
                    complete = available_tasks[work_task['task_code']](id_job, work_task, jobs[id_job])
                else:
                    complete = available_tasks[work_task['task_code']](id_job, work_task)
                if not complete:
                    canceled_with_error = True
                    break
            else:
                break

        if canceled_with_error:
            add_worker_log(last_work_task['foreign_system_connection']['id'], last_work_task['id_work'], '', 2,
                           'Darbs pārtraukts, jo radās neparedzēta kļūda', '')
            print(f"JOB {id_job} canceled")

            jobs[id_job].add_log_entry(f"Canceled with error")
            jobs[id_job].set_canceled()
            jobs[id_job].set_completed()
        else:
            add_worker_log(last_work_task['foreign_system_connection']['id'], last_work_task['id_work'], '', 0,
                           'Darbs izpildīts veiksmīgi', '')
            print(f"JOB {id_job} done")
            jobs[id_job].set_completed()
            jobs[id_job].add_log_entry(f"Job completed")
    except Exception as e:
        print(f'Error in do_job: {e}')


def control_api_token_renew():
    while True:
        result = renew_api_access_token()
        if result:
            # reset server status to available
            set_status(True, None, None)

            eventlet.sleep(settings.API_TOKEN_TTL*60)
        else:
            # error occurred in renew try to renew after 1 minute
            eventlet.sleep(60)


# Setup pool
pool = eventlet.GreenPool(100)

# remove existing token data
if os.path.exists('access_token'):
    os.unlink('access_token')

if os.path.exists('access_token_renew_dt'):
    os.unlink('access_token_renew_dt')

# set worker status as available
set_status(True, None, None)

# pool spawn non-locking api session renew control function
pool.spawn_n(control_api_token_renew)

# Start job loop
while True:
    # Load tasks
    new_jobs = load_new_jobs()
    if new_jobs:
        for job_id in new_jobs:
            jobs[job_id] = Job(job_id)
            pool.spawn_n(do_job, job_id)
            jobs[job_id].add_log_entry(f'Job spawned')

    set_pool_status(pool)
    eventlet.sleep(settings.JOB_LOOP_FREQUENCY)
