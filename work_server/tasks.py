import datetime
import time

import eventlet
from eventlet.green.urllib import request
from eventlet.green.urllib.parse import (
    quote_plus,
    urlencode
)


import json
import settings

from helpers import (
    build_horizon_product_url,
    build_horizon_product_price_tag,
    build_horizon_online_orders_url,
    cancel_job_with_error,
    put_headers,
    get_tikitaka_api_access_token,
    search_tikitaka,
    data_to_tikitaka,
    get_horizon_response_soup,
    get_from_tikitaka,
    get_horizon_response_json,
    add_worker_log,
    build_horizon_kp_from_fiscof,
    build_horizon_return_from_fiscof,
    post_xml_to_horizon,
    build_horizon_online_order_relation_doc,
    build_horizon_online_order_deal_doc,
    find_product_tax_category,
    build_horizon_kp_from_fiscof_with_return_recipe
)
from timer import Timer

bs4 = eventlet.import_patched('bs4')
nom_codes = ('/rest/TNdmNom/', '/rest/TdmBLPgNom/',)


def tiki_taka_import_units(id_job: int, work_task):
    try:
        auth = get_tikitaka_api_access_token(
            work_task['foreign_system_connection']['address'],
            work_task['foreign_system_connection']['username'],
            work_task['foreign_system_connection']['pwd']
        )

        if auth['authorized']:
            url = f"{settings.API_URL}/manage/data/units/integrated?task_code={work_task['task_code']}&id_fsc={work_task['foreign_system_connection']['id']}"
            request_object = request.Request(url, headers=put_headers())
            response = request.urlopen(request_object)

            integrated_units = json.loads(response.read())
            if integrated_units is not None:
                for unit in integrated_units:
                    # Compare checksum
                    attributes = {}
                    if unit['attributes']:
                        attributes = json.loads(unit['attributes'])

                    if unit['checksum'] != attributes.get('checksum', None):
                        payload = {
                            'id_item': unit['id'],
                            'id_fsc': work_task['foreign_system_connection']['id'],
                            'target_id': None,
                            'attributes': attributes
                        }

                        data = {
                            'id': 0,
                            'short_title': unit['short_title'],
                            'title': unit['title']
                        }

                        tikitaka_url = f"{work_task['foreign_system_connection']['address']}/data/units/update"
                        if unit['target_id'] is None:
                            target_id = search_tikitaka(
                                'short_title',
                                unit['short_title'].capitalize(),  # Important is to search capitalized
                                f"{work_task['foreign_system_connection']['address']}/data/units/",
                                f"{unit['short_title']}",
                                auth['token_for_header']
                            )

                            if target_id is not None:
                                # target id exist with short-title
                                data['id'] = target_id
                            else:
                                tikitaka_url = f"{work_task['foreign_system_connection']['address']}/data/units/insert"

                        else:
                            # Do update
                            data['id'] = unit['target_id']

                        target_id = data_to_tikitaka(data, tikitaka_url, auth['token_for_header'])

                        payload['target_id'] = str(target_id)
                        payload['attributes']['checksum'] = unit['checksum']

                        post_url = f"{settings.API_URL}/manage/data/units/integrated?task_code={work_task['task_code']}"
                        request_object = request.Request(post_url, data=json.dumps(payload).encode(), headers=put_headers())
                        response = request.urlopen(request_object)

                    else:
                        # Checksums equal data is up-to-date
                        pass

            return True
        else:
            cancel_job_with_error(id_job, auth['error'])
            return False

    except Exception as e:
        if hasattr(e, 'message'):
            message = e.message
        else:
            message = f"{e.__class__}: {str(e)}"
        cancel_job_with_error(id_job, message)

        return False


def horizon_export_units(id_job: int, work_task):
    payload = ''
    try:
        url = f"{work_task['foreign_system_connection']['address']}/rest/TNdmVienSar/default"
        soup = get_horizon_response_soup(work_task, url)
        dict_units = {}
        units = []
        for item in soup.find_all('row'):
            if dict_units.get(item.v.kods.string, None) is None:
                dict_units[item.v.kods.string] = True
                units.append((item.v.kods.string, item.v.nosauk.string))

        for unit in units:
            code, title = unit
            url = f"{settings.API_URL}/manage/data/units"

            payload = {'id_fsc': work_task['foreign_system_connection']['id'],
                       'source_id': code,
                       'short_title': code[0:10],
                       'title': title}

            data = json.dumps(payload).encode()
            request_object = request.Request(url, headers=put_headers(), data=data)
            request_object.get_method = lambda: 'PUT'
            response = request.urlopen(request_object)

        return True
    # except Exception as e:
    #     if hasattr(e, 'message'):
    #         message = e.message
    #     else:
    #         message = f"{e.__class__}: {str(e)}"
    #     cancel_job_with_error(id_job, message)
    except Exception as e:
        if hasattr(e, 'message'):
            message = e.message
        else:
            message = f"{e.__class__}: {str(e)}"
        message = f"{message} Payload: {payload}"
        cancel_job_with_error(id_job, message)
        return False



def horizon_export_employees(id_job: int, work_task):
    try:
        print('Horizon export employee')
        a = 4 / 0
        return True
    except Exception as e:
        if hasattr(e, 'message'):
            message = e.message
        else:
            message = f"{e.__class__}: {str(e)}"
        cancel_job_with_error(id_job, message)

        return False


def tiki_taka_import_employees(id_job: int, work_task):
    try:
        print('TT import employee')
        return True
    except Exception as e:
        if hasattr(e, 'message'):
            message = e.message
        else:
            message = f"{e.__class__}: {str(e)}"
        cancel_job_with_error(id_job, message)

        return False


def horizon_export_tax(id_job: int, work_task):
    try:
        # product_tax_url = f"{work_task['foreign_system_connection']['address']}/rest/TNdmNomSar/query?columns=N_KODS,N_NOSAUK,NBR_NOSAUK,CEN_CENA,L_KODS,NBR_BAR_KODS,VIEN_NOSAUK,VIEN_KODS,G_KODS,G_NOSAUK"
        product_tax_url = build_horizon_product_url(work_task, 'product_filter')
        tax_url = f"{work_task['foreign_system_connection']['address']}/rest/TdmPvnLikmesSl/default"
        soup = get_horizon_response_soup(work_task, tax_url)

        dict_tax = {}
        for item in soup.find_all('row'):
            if dict_tax.get(item.l.kods.string, None) is None:
                title = item.l.nosauk.string
                if title is None:
                    title = item.l.kods.string
                dict_tax[item.l.kods.string] = {'title': title, 'ptc': {}}


        soup = get_horizon_response_soup(work_task, product_tax_url)
        for item in soup.find_all('row'):
            if item.pk_nom.href.string.startswith(nom_codes):
                if dict_tax[item.l.kods.string]['ptc'].get(item.g.kods.string, None) is None:
                    dict_tax[item.l.kods.string]['ptc'][item.g.kods.string] = item.g.nosauk.string

        for k in dict_tax:
            tax_category_url = f"{settings.API_URL}/manage/data/tax-categories"

            payload = {'id_fsc': work_task['foreign_system_connection']['id'],
                       'source_id': k,
                       'code': k,
                       'rate': None,
                       'non_taxable': False,
                       'title': dict_tax[k]['title'] if len(dict_tax[k]['title']) > 1 else f"{dict_tax[k]['title']}: {dict_tax[k]['title']}"}

            data = json.dumps(payload).encode()
            request_object = request.Request(tax_category_url, headers=put_headers(), data=data)
            request_object.get_method = lambda: 'PUT'
            response = request.urlopen(request_object)
            id_tax_category = int(json.loads(response.read()))

            product_tax_category_url = f"{settings.API_URL}/manage/data/tax-categories/{id_tax_category}/product-tax-categories"

            for kk in dict_tax[k]['ptc']:
                title = dict_tax[k]['ptc'][kk]
                payload = {'id_tax_category': id_tax_category,
                           'source_id': kk,
                           'title': title[0:50]}

                data = json.dumps(payload).encode()
                request_object = request.Request(product_tax_category_url, headers=put_headers(), data=data)
                request_object.get_method = lambda: 'PUT'
                response = request.urlopen(request_object)

        return True
    except Exception as e:
        if hasattr(e, 'message'):
            message = e.message
        else:
            message = f"{e.__class__}: {str(e)}"
        cancel_job_with_error(id_job, message)

        return False


def tiki_taka_import_product_tax_category(id_tax_category: int, target_id_tax_category: int, work_task, auth):
    url = f"{settings.API_URL}/manage/data/tax-categories/{id_tax_category}/product-tax-categories/integrated?task_code={work_task['task_code']}&id_fsc={work_task['foreign_system_connection']['id']}"
    request_object = request.Request(url, headers=put_headers())
    response = request.urlopen(request_object)

    integrated_ptc = json.loads(response.read())
    if integrated_ptc is not None:
        for ptc in integrated_ptc:
            # Compare checksum
            attributes = {}
            if ptc['attributes']:
                attributes = json.loads(ptc['attributes'])

            if ptc['checksum'] != attributes.get('checksum', None):
                payload = {
                    'id_item': ptc['id'],
                    'id_fsc': work_task['foreign_system_connection']['id'],
                    'target_id': None,
                    'attributes': attributes
                }

                data = {
                    'id': 0,
                    'title': ptc['title']
                }

                tikitaka_url = f"{work_task['foreign_system_connection']['address']}/data/product_tax_categories/update"

                if ptc['target_id'] is None:
                    target = search_tikitaka(
                        'title',
                        ptc['title'],
                        f"{work_task['foreign_system_connection']['address']}/data/product_tax_categories/",
                        f"{quote_plus(ptc['title'])}&tax_category_id={target_id_tax_category}",
                        auth['token_for_header'],
                        True
                    )

                    target_id = None
                    if target:
                        target_id = target['id']

                    if target_id is not None:
                        # target id exist with title
                        data['id'] = target_id
                    else:
                        tikitaka_url = f"{work_task['foreign_system_connection']['address']}/data/product_tax_categories/insert?id_tax_category={target_id_tax_category}"

                else:
                    # Do update
                    data['id'] = ptc['target_id']

                target_id = data_to_tikitaka(data, tikitaka_url, auth['token_for_header'])


                payload['target_id'] = str(target_id)
                payload['attributes']['checksum'] = ptc['checksum']

                post_url = f"{settings.API_URL}/manage/data/tax-categories/{id_tax_category}/product-tax-categories/integrated?task_code={work_task['task_code']}"
                request_object = request.Request(post_url, data=json.dumps(payload).encode(),
                                                 headers=put_headers())
                response = request.urlopen(request_object)
            else:
                # Checksums equal data is up-to-date
                pass

    return True


def tiki_taka_import_tax(id_job: int, work_task):
    try:
        auth = get_tikitaka_api_access_token(
            work_task['foreign_system_connection']['address'],
            work_task['foreign_system_connection']['username'],
            work_task['foreign_system_connection']['pwd']
        )

        if auth['authorized']:
            url = f"{settings.API_URL}/manage/data/tax-categories/integrated?task_code={work_task['task_code']}&id_fsc={work_task['foreign_system_connection']['id']}"
            request_object = request.Request(url, headers=put_headers())
            response = request.urlopen(request_object)

            integrated_tc = json.loads(response.read())
            if integrated_tc is not None:
                for tc in integrated_tc:
                    # ptc_url = f"{settings.API_URL}/manage/data/tax-categories/{tc['id']}/product-tax-categories/integrated?task_code={work_task['task_code']}&id_fsc={work_task['foreign_system_connection']['id']}"
                    # request_object = request.Request(ptc_url, headers=put_headers())
                    # response = request.urlopen(request_object)
                    # integrated_ptc = json.loads(response.read())

                    # Compare checksum
                    attributes = {}
                    if tc['attributes']:
                        attributes = json.loads(tc['attributes'])

                    if tc['checksum'] != attributes.get('checksum', None):
                        payload = {
                            'id_item': tc['id'],
                            'id_fsc': work_task['foreign_system_connection']['id'],
                            'target_id': None,
                            'attributes': attributes
                        }

                        data = {
                            'id': 0,
                            'code': tc['code'],
                            'title': tc['title']
                        }

                        tikitaka_url = f"{work_task['foreign_system_connection']['address']}/data/tax_categories/update"

                        if tc['target_id'] is None:
                            target = search_tikitaka(
                                'code',
                                tc['code'],
                                f"{work_task['foreign_system_connection']['address']}/data/tax_categories/",
                                f"{tc['code']}",
                                auth['token_for_header'],
                                True
                            )

                            target_id = None
                            if target:
                                target_id = target['id']

                            if target_id is not None:
                                # target id exist with short-title
                                data['id'] = target_id
                            else:
                                tikitaka_url = f"{work_task['foreign_system_connection']['address']}/data/tax_categories/insert"
                        else:
                            # Do update
                            data['id'] = tc['target_id']

                        target_id = data_to_tikitaka(data, tikitaka_url, auth['token_for_header'], True)

                        payload['target_id'] = str(target_id)
                        payload['attributes']['checksum'] = tc['checksum']

                        post_url = f"{settings.API_URL}/manage/data/tax-categories/integrated?task_code={work_task['task_code']}"
                        request_object = request.Request(post_url, data=json.dumps(payload).encode('utf-8'),
                                                         headers=put_headers())
                        response = request.urlopen(request_object)
                        tiki_taka_import_product_tax_category(tc['id'], target_id, work_task, auth)
                    else:
                        # Checksums equal data is up-to-date
                        tiki_taka_import_product_tax_category(tc['id'], tc['target_id'], work_task, auth)

            return True
        else:
            cancel_job_with_error(id_job, auth['error'])
            return False

    except Exception as e:
        if hasattr(e, 'message'):
            message = e.message
        else:
            message = f"{e.__class__}: {str(e)}"

        cancel_job_with_error(id_job, message)

        return False


def tiki_taka_import_products(id_job: int, work_task):
    try:
        auth = get_tikitaka_api_access_token(
            work_task['foreign_system_connection']['address'],
            work_task['foreign_system_connection']['username'],
            work_task['foreign_system_connection']['pwd']
        )

        if auth['authorized']:
            url = f"{settings.API_URL}/manage/data/products/integrated?task_code={work_task['task_code']}&id_fsc={work_task['foreign_system_connection']['id']}"
            request_object = request.Request(url, headers=put_headers())
            response = request.urlopen(request_object)

            integrated_products = json.loads(response.read())
            if integrated_products is not None:
                for product in integrated_products:
                    # Compare checksum
                    attributes = {}
                    if product['attributes']:
                        attributes = json.loads(product['attributes'])

                    if product['checksum'] != attributes.get('checksum', None):
                        payload = {
                            'id_item': product['id'],
                            'id_fsc': work_task['foreign_system_connection']['id'],
                            'target_id': None,
                            'attributes': attributes
                        }

                        barcode = None
                        if product['barcodes']:
                            for b in product['barcodes']:
                                if b['is_main']:
                                    barcode = b['barcode']
                                    break
                            if not barcode:
                                barcode = product['barcodes'][0]['barcode']

                        data = {
                            'id': 0,
                            'title': product['title'],
                            'print_title': product['print_title'],
                            'short_code': product['short_code'],
                            "id_unit": int(product['unit']['source_id']),
                            "id_product_tax_category": int(product['product_tax_category']['source_id']) if product['product_tax_category'] else None,
                            "price": product['price'],
                            "barcode": barcode,
                            "qrcode": product['qrcode'],
                            "description": product['description'],
                            "image_url": None
                        }

                        tikitaka_url = f"{work_task['foreign_system_connection']['address']}/data/products/update"

                        if product['target_id'] is None:
                            target_id = search_tikitaka(
                                'short_code',
                                product['short_code'],  # Important is to search capitalized
                                f"{work_task['foreign_system_connection']['address']}/data/products/",
                                product['short_code'],
                                auth['token_for_header']
                            )

                            if target_id is not None:
                                # target id exist with short-title
                                data['id'] = target_id
                            else:
                                tikitaka_url = f"{work_task['foreign_system_connection']['address']}/data/products/insert"

                        else:
                            # Do update
                            data['id'] = product['target_id']

                        target_id = data_to_tikitaka(data, tikitaka_url, auth['token_for_header'])

                        payload['target_id'] = str(target_id)
                        payload['attributes']['checksum'] = product['checksum']

                        post_url = f"{settings.API_URL}/manage/data/products/integrated?task_code={work_task['task_code']}"
                        request_object = request.Request(post_url, data=json.dumps(payload).encode(),
                                                         headers=put_headers())
                        response = request.urlopen(request_object)

                    else:
                        # Checksums equal data is up-to-date
                        pass

            return True
    except Exception as e:
        if hasattr(e, 'message'):
            message = e.message
        else:
            message = f"{e.__class__}: {str(e)}"
        cancel_job_with_error(id_job, message)

        return False


def horizon_export_products(id_job: int, work_task):
    payload = ''
    try:
        # url = f"{work_task['foreign_system_connection']['address']}/rest/TNdmNomSar/query?columns=N_KODS,N_NOSAUK,NBR_NOSAUK,CEN_CENA,L_KODS,NBR_BAR_KODS,VIEN_NOSAUK,VIEN_KODS,G_KODS,G_NOSAUK"
        url = build_horizon_product_url(work_task, 'filter')

        data = get_horizon_response_json(work_task, url)

        price_tag = build_horizon_product_price_tag(work_task)

        skipped_products = []
        for n in data['collection']['row']:
            """
            {'N': {'KODS': 'P01', 'NOSAUK': 'Prece 01', 'PK_NOM': {'href': '/rest/TNdmNom/1'}, 'COUNTER': 7,
                   'G': {'KODS': 'Pre', 'NOSAUK': 'Preces'}, 'VIEN': {'NOSAUK': 'gabali', 'KODS': 'gb'},
                   'NBR': {'NOSAUK': None, 'BAR_KODS': None}, 'CEN': {'CENA': 23}, 'L': {'KODS': 'C'}}}
            """
            item = n['N']
            if item['PK_NOM']['href'].startswith(nom_codes):
                if item[price_tag]['CENA'] is not None:
                    barcodes_url = f"{work_task['foreign_system_connection']['address']}/rest/TNBarCodeSar/query?filter=N.KODS%20eq%20'{item['KODS']}'&columns=NBR.BAR_KODS,NBR.IS_MAIN"

                    try:
                        horizon_barcodes = get_horizon_response_json(work_task, barcodes_url)
                    except Exception as barcode_ex:
                        if hasattr(barcode_ex, 'message'):
                            message = barcode_ex.message
                        else:
                            message = f"{barcode_ex.__class__}: {str(barcode_ex)}"
                        message = f"Adrese: {barcodes_url}\n{message}"
                        entry = f"Nevar ielādēt produkta (KODS) {item['KODS']} svītru kodus"
                        add_worker_log(work_task['foreign_system_connection']['id'], work_task['id_work'], work_task['task_code'], 2, entry, message)

                    """
                    {'NBR': {'BAR_KODS': '0000000000000', 'IS_MAIN': 1, 'PK_BARKODS': {'href': '/rest/TNdmBarCode/3'}, 'COUNTER': 0}}
                    """
                    barcodes = None
                    if horizon_barcodes['collection'].get('row', None):
                        barcodes = []
                        for b in horizon_barcodes['collection']['row']:
                            barcodes.append({'barcode': b['NBR']['BAR_KODS'].strip(), 'is_main': b['NBR']['IS_MAIN']})

                    print_title = item['NBR']['NOSAUK'] if item['NBR']['NOSAUK'] else item['NOSAUK']

                    payload = {'id_fsc': work_task['foreign_system_connection']['id'],
                               'source_id': item['KODS'],
                               'source_id_tax_category': item['L']['KODS'],
                               'source_id_product_tax_category': item['G']['KODS'],
                               'source_id_unit': item['VIEN']['KODS'],
                               'title': item['NOSAUK'],
                               'print_title': print_title[0:40],
                               'short_code': item['KODS'],
                               'price': item[price_tag]['CENA'],
                               'barcodes': barcodes,
                               'qrcode': None,
                               'description': None,
                               'combo': False,
                               'combo_product_source_ids': None}

                    product_url = f"{settings.API_URL}/manage/data/products"
                    data = json.dumps(payload).encode()
                    request_object = request.Request(product_url, headers=put_headers(), data=data)
                    request_object.get_method = lambda: 'PUT'
                    response = request.urlopen(request_object)
                else:
                    skipped_products.append(item)
        if len(skipped_products) > 0:
            entry = f"Neimportēti {len(skipped_products)} produkti"
            message = 'Neimportēto produktu kodu saraksts:\n'
            for sp in skipped_products:
                message = f"{message}{sp['KODS']}\n"
            add_worker_log(work_task['foreign_system_connection']['id'], work_task['id_work'], work_task['task_code'], 1, entry, message)

        return True
    except Exception as e:
        if hasattr(e, 'message'):
            message = e.message
        else:
            message = f"{e.__class__}: {str(e)}"
        message = f"{message} Payload: {payload}"
        cancel_job_with_error(id_job, message)

        return False


def tiki_taka_export_fiscof(id_job: int, work_task):
    try:
        auth = get_tikitaka_api_access_token(
            work_task['foreign_system_connection']['address'],
            work_task['foreign_system_connection']['username'],
            work_task['foreign_system_connection']['pwd']
        )

        if auth['authorized']:
            per_page = 100
            loaded = per_page
            page = 1
            stop = False
            while not stop:
                skip = (page - 1) * per_page
                limit = per_page
                url = f"{work_task['foreign_system_connection']['address']}/clients/device/fiscofs?skip={skip}&limit={limit}&order=DESC"
                result = get_from_tikitaka(url, auth['token_for_header'])

                put_fiscof_url = f"{settings.API_URL}/manage/data/fiscof"
                if result['total'] > 0:
                    for fiscof in result['results']:
                        payload = {'id_fsc': work_task['foreign_system_connection']['id'],
                                   'source_id': str(fiscof['id']),
                                   'device_serial_number': fiscof['device_serial_number'],
                                   'doc': fiscof['doc']}

                        data = json.dumps(payload).encode()
                        request_object = request.Request(put_fiscof_url, headers=put_headers(), data=data)
                        request_object.get_method = lambda: 'PUT'
                        response = request.urlopen(request_object)
                        fiscof_id = int(response.read())
                        if fiscof_id == -1:
                            # Get already imported fiscof. Stop fiscof processing no new fiscof
                            stop = True
                            break

                if loaded < result['total']:
                    loaded = loaded + per_page
                    page = page + 1
                else:
                    break

        return True
    except Exception as e:
        if hasattr(e, 'message'):
            message = e.message
        else:
            message = f"{e.__class__}: {str(e)}"
        cancel_job_with_error(id_job, message)

        return False


def horizon_import_fiscof(id_job: int, work_task):
    try:
        #url = f"{work_task['foreign_system_connection']['address']}/rest/TNdmNomSar/query?columns=N_KODS,N_NOSAUK,NBR_NOSAUK,CEN_CENA,L_KODS,NBR_BAR_KODS,VIEN_NOSAUK,VIEN_KODS,G_KODS,G_NOSAUK"
        url = build_horizon_product_url(work_task, 'product_filter')

        data = get_horizon_response_json(work_task, url)

        horizon_products = {}
        for n in data['collection']['row']:
            """
            {'N': {'KODS': 'P01', 'NOSAUK': 'Prece 01', 'PK_NOM': {'href': '/rest/TNdmNom/1'}, 'COUNTER': 7,
                   'G': {'KODS': 'Pre', 'NOSAUK': 'Preces'}, 'VIEN': {'NOSAUK': 'gabali', 'KODS': 'gb'},
                   'NBR': {'NOSAUK': None, 'BAR_KODS': None}, 'CEN': {'CENA': 23}, 'L': {'KODS': 'C'}}}
            """
            item = n['N']

            if item['PK_NOM']['href'].startswith(nom_codes) and item.get('KODS', None) is not None:
                horizon_products[item['KODS']] = item['PK_NOM']['href']

        if work_task['attributes']:
            work_task['attributes'] = json.loads(work_task['attributes'])

        # Build dsn list
        str_serials = work_task['attributes']['device_serials']
        #serials = None
        dsn = ''
        if str_serials is not None and len(str_serials.strip()) > 0:
            serials = str_serials.split(' ')
            if serials and len(serials) > 0:
                for s in serials:
                    dsn = f'{dsn}&dsn={s}'

        url = f"{settings.API_URL}/manage/data/fiscof/non-integrated/next?task_code={work_task['task_code']}&id_fsc={work_task['foreign_system_connection']['id']}{dsn}"

        integrate_fiscof = True
        skipped_fiscof = []
        while integrate_fiscof:
            request_object = request.Request(url, headers=put_headers())
            response = request.urlopen(request_object)

            non_integrated_fiscof = json.loads(response.read())
            if non_integrated_fiscof:
                try:
                    xml_data = build_horizon_kp_from_fiscof(work_task, non_integrated_fiscof, horizon_products)
                except Exception as kp_ex:
                    xml_data = None
                    skipped_fiscof.append((non_integrated_fiscof['id'], kp_ex.args[0]))

                if xml_data:
                    try:
                        kp_url = f"{work_task['foreign_system_connection']['address']}/rest/TNdmPvzCR"
                        response = post_xml_to_horizon(work_task, kp_url, xml_data)

                        attributes = {
                            'target_response': response,
                            'created_document': xml_data,
                            'created_at': str(datetime.datetime.now())
                        }

                        payload = {
                            'id_item': non_integrated_fiscof['id'],
                            'id_fsc': work_task['foreign_system_connection']['id'],
                            'target_id': response['link']['href'],
                            'attributes': attributes
                        }

                        post_url = f"{settings.API_URL}/manage/data/fiscof/integrated?task_code={work_task['task_code']}"
                        request_object = request.Request(post_url, data=json.dumps(payload).encode(), headers=put_headers())
                        response = request.urlopen(request_object)
                    except Exception as kp_accounting_period_closed:
                        exception_string = str(kp_accounting_period_closed)
                        if 'EValidationException' in exception_string or 'EFTGUserError' in exception_string:
                            skipped_fiscof.append((non_integrated_fiscof['id'], kp_accounting_period_closed.args[0]))

                            attributes = {
                                'skipped': True,
                                'created_at': str(datetime.datetime.now())
                            }

                            payload = {
                                'id_item': non_integrated_fiscof['id'],
                                'id_fsc': work_task['foreign_system_connection']['id'],
                                'target_id': f"IN_SKIP_{non_integrated_fiscof['id']}_"
                                             f"{work_task['foreign_system_connection']['id']}",
                                'attributes': attributes
                            }

                            post_url = f"{settings.API_URL}/manage/data/fiscof/integrated?task_code={work_task['task_code']}"
                            request_object = request.Request(post_url, data=json.dumps(payload).encode(),
                                                             headers=put_headers())
                            response = request.urlopen(request_object)
                if not xml_data:
                    # set as skipped
                    attributes = {
                        'skipped': True,
                        'created_at': str(datetime.datetime.now())
                    }

                    payload = {
                        'id_item': non_integrated_fiscof['id'],
                        'id_fsc': work_task['foreign_system_connection']['id'],
                        'target_id': f"IN_SKIP_{non_integrated_fiscof['id']}_"
                                     f"{work_task['foreign_system_connection']['id']}",
                        'attributes': attributes
                    }

                    post_url = f"{settings.API_URL}/manage/data/fiscof/integrated?task_code={work_task['task_code']}"
                    request_object = request.Request(post_url, data=json.dumps(payload).encode(), headers=put_headers())
                    response = request.urlopen(request_object)
            else:
                integrate_fiscof = False

        if len(skipped_fiscof) > 0:
            entry = f"Neizveidotas {len(skipped_fiscof)} kases pavadzīmes"
            message = 'Izlaisto fiscof saraksts:\n'
            for sf in skipped_fiscof:
                message = f"{message}{sf[0]}\n{sf[1]}\n"
            add_worker_log(
                work_task['foreign_system_connection']['id'],
                work_task['id_work'],
                work_task['task_code'],
                1,
                entry,
                message
            )

        return True
    except Exception as e:
        if hasattr(e, 'message'):
            message = e.message
        else:
            message = f"{e.__class__}: {str(e)}"
        cancel_job_with_error(id_job, message)

        return False


def horizon_import_return_recipe_from_fiscof(id_job: int, work_task):
    try:
        #url = f"{work_task['foreign_system_connection']['address']}/rest/TNdmNomSar/query?columns=N_KODS,N_NOSAUK,NBR_NOSAUK,CEN_CENA,L_KODS,NBR_BAR_KODS,VIEN_NOSAUK,VIEN_KODS,G_KODS,G_NOSAUK"
        url = build_horizon_product_url(work_task, 'product_filter')

        data = get_horizon_response_json(work_task, url)

        horizon_products = {}
        for n in data['collection']['row']:
            """
            {'N': {'KODS': 'P01', 'NOSAUK': 'Prece 01', 'PK_NOM': {'href': '/rest/TNdmNom/1'}, 'COUNTER': 7,
                   'G': {'KODS': 'Pre', 'NOSAUK': 'Preces'}, 'VIEN': {'NOSAUK': 'gabali', 'KODS': 'gb'},
                   'NBR': {'NOSAUK': None, 'BAR_KODS': None}, 'CEN': {'CENA': 23}, 'L': {'KODS': 'C'}}}
            """
            item = n['N']

            if item['PK_NOM']['href'].startswith(nom_codes) and item.get('KODS', None) is not None:
                horizon_products[item['KODS']] = item['PK_NOM']['href']

        if work_task['attributes']:
            work_task['attributes'] = json.loads(work_task['attributes'])

        # Build dsn list
        str_serials = work_task['attributes']['device_serials']
        #serials = None
        dsn = ''
        if str_serials is not None and len(str_serials.strip()) > 0:
            serials = str_serials.split(' ')
            if serials and len(serials) > 0:
                for s in serials:
                    dsn = f'{dsn}&dsn={s}'

        url = f"{settings.API_URL}/manage/data/fiscof/non-integrated/next?task_code={work_task['task_code']}&id_fsc={work_task['foreign_system_connection']['id']}{dsn}"

        integrate_fiscof = True
        skipped_fiscof = []
        while integrate_fiscof:
            request_object = request.Request(url, headers=put_headers())
            response = request.urlopen(request_object)

            non_integrated_fiscof = json.loads(response.read())
            if non_integrated_fiscof:
                try:
                    xml_data = build_horizon_return_from_fiscof(work_task, non_integrated_fiscof, horizon_products)
                except Exception as kp_ex:
                    xml_data = None
                    skipped_fiscof.append((non_integrated_fiscof['id'], kp_ex.args[0]))

                if xml_data:
                    kp_url = f"{work_task['foreign_system_connection']['address']}/rest/TNdmPvzCR"
                    response = post_xml_to_horizon(work_task, kp_url, xml_data)

                    attributes = {
                        'target_response': response,
                        'created_document': xml_data,
                        'created_at': str(datetime.datetime.now())
                    }

                    payload = {
                        'id_item': non_integrated_fiscof['id'],
                        'id_fsc': work_task['foreign_system_connection']['id'],
                        'target_id': response['link']['href'],
                        'attributes': attributes
                    }
                else:
                    # set as skipped
                    attributes = {
                        'skipped': True,
                        'created_at': str(datetime.datetime.now())
                    }

                    payload = {
                        'id_item': non_integrated_fiscof['id'],
                        'id_fsc': work_task['foreign_system_connection']['id'],
                        'target_id': f"IN_SKIP_{non_integrated_fiscof['id']}_"
                                     f"{work_task['foreign_system_connection']['id']}",
                        'attributes': attributes
                    }

                post_url = f"{settings.API_URL}/manage/data/fiscof/integrated?task_code={work_task['task_code']}"
                request_object = request.Request(post_url, data=json.dumps(payload).encode(), headers=put_headers())
                response = request.urlopen(request_object)
            else:
                integrate_fiscof = False

        if len(skipped_fiscof) > 0:
            entry = f"Neizveidotie {len(skipped_fiscof)} atmaksas čeki"
            message = 'Izlaisto atmaksas čeku saraksts:\n'
            for sf in skipped_fiscof:
                message = f"{message}{sf[0]}\n{sf[1]}\n"
            add_worker_log(
                work_task['foreign_system_connection']['id'],
                work_task['id_work'],
                work_task['task_code'],
                1,
                entry,
                message
            )

        return True
    except Exception as e:
        if hasattr(e, 'message'):
            message = e.message
        else:
            message = f"{e.__class__}: {str(e)}"
        cancel_job_with_error(id_job, message)

        return False


def horizon_export_online_order(id_job: int, work_task, job):
    try:
        timer = Timer()

        url = build_horizon_online_orders_url(work_task, 'invoice_filter')
        job.add_log_entry(f'-- Url to load invoices "{url}"')
        timer.start('invoices')
        job.add_log_entry(f"-- Loading invoices")

        data = get_horizon_response_json(work_task, url)

        job.add_log_entry(f"-- Invoices loaded in {timer.stop('invoices')}")

        attrs = {}
        if work_task['attributes']:
            attrs = json.loads(work_task['attributes'])

        if data['collection'].get('row', None):
            broken_invoices = 0
            timer.start('rows')
            total = len(data['collection']['row'])
            job.add_log_entry(f'-- Start to load {total} invoices')
            load_step = int(total/5)
            step_count = 0
            count = 0
            for r in data['collection']['row']:
                step_count += 1
                if step_count == load_step:
                    count = count + step_count
                    step_count = 0
                    job.add_log_entry(f'-- {count} invoices loaded')
                # Load full document
                href = r['PDOK']['PK_DOK']['href']
                doc_url = f"{work_task['foreign_system_connection']['address']}{href}"
                doc = None

                try:
                    doc = get_horizon_response_json(work_task, doc_url)
                except Exception:
                    message = f"Avansa rēķina adrese: {href}"
                    entry = f"Nevar ielādēt avansa rēķina datus"

                    add_worker_log(
                        work_task['foreign_system_connection']['id'],
                        work_task['id_work'],
                        work_task['task_code'],
                        2,
                        entry,
                        message)
                # local variable 'doc' referenced before assignment error fixed
                if doc is None:
                    broken_invoices += 1
                else:
                    # Load client data
                    client_href = doc['entity']['PK_KLIENTS']['href']
                    client_url = f"{work_task['foreign_system_connection']['address']}{client_href}"

                    try:
                        client = get_horizon_response_json(work_task, client_url)
                    except Exception:
                        message = f"Avansa rēķina adrese: {href}"
                        entry = f"Nevar ielādēt avansa rēķina klienta datus"

                        add_worker_log(
                            work_task['foreign_system_connection']['id'],
                            work_task['id_work'],
                            work_task['task_code'],
                            2,
                            entry,
                            message)

                    if doc['entity']['DOK_NR'] is None or doc['entity']['NEPIESAIST'] is None\
                            or client['entity']['NOSAUK'] is None:

                        message = f"Avansa rēķina adrese: {href}\n" \
                                  f"Obligātie dati:\n" \
                                  f"Dokumenta numurs: {doc['entity']['DOK_NR']}\n" \
                                  f"Dokumenta nepiesaistītā summa: {doc['entity']['NEPIESAIST']}\n" \
                                  f"Pircēja nosaukums: {client['entity']['NOSAUK']}\n"

                        entry = f"Nevar ielādēt avansa rēķinu"

                        add_worker_log(
                            work_task['foreign_system_connection']['id'],
                            work_task['id_work'],
                            work_task['task_code'],
                            2,
                            entry,
                            message)

                        broken_invoices += 1
                    else:
                        # Detect products have same VAT category and save it
                        products = []
                        total_products_price = 0.0
                        reason_set = False
                        if doc['entity'].get('tblRindas', None):
                            for product in doc['entity']['tblRindas']['row']:
                                if not product.get('NOSAUK', None) or not product.get('DAUDZ', None):
                                    products = []
                                    break

                                if product['DAUDZ'] > 0:
                                    try:
                                        vat = get_horizon_response_json(work_task, f"{work_task['foreign_system_connection']['address']}/rest/TdmBLPvnLikmes/{product['PK_LIKME']}")
                                        product_tax_category_id = find_product_tax_category(attrs['product_tax_categories'], vat['entity']['KODS'])
                                    except Exception:
                                        message = f"Avansa rēķina adrese: {href}\nNevar atrast nomenklatūras PVN"
                                        entry = f"Nevar ielādēt avansa rēķinu"

                                        add_worker_log(
                                            work_task['foreign_system_connection']['id'],
                                            work_task['id_work'],
                                            work_task['task_code'],
                                            2,
                                            entry,
                                            message)

                                        products = []
                                        reason_set = True
                                        break

                                    products.append({
                                        'title': product['NOSAUK'],
                                        'print_title': product['NOSAUK'][0:40],
                                        'short_code': product['KODS'],
                                        'quantity': product['DAUDZ'],
                                        'price': float((product['SUMMA']+product['SUMMA_PVN']) / product['DAUDZ']),
                                        'unit_short_title': product['V_KODS'],
                                        'unit_id': product['V_KODS'],
                                        'product_tax_category_id': int(product_tax_category_id)
                                    })
                                    total_products_price = total_products_price + float((product['SUMMA']+product['SUMMA_PVN']))

                        # if not vat_error_set and vat_id is None:
                        if len(products) == 0 and not reason_set:
                            message = f"Avansa rēķina adrese: {href}\nNesatur nomenklatūras"
                            entry = f"Nevar ielādēt avansa rēķinu"

                            add_worker_log(
                                work_task['foreign_system_connection']['id'],
                                work_task['id_work'],
                                work_task['task_code'],
                                2,
                                entry,
                                message)

                            broken_invoices += 1

                        if len(products) > 0:
                            diff = 0
                            if doc['entity']['NEPIESAIST'] < total_products_price:
                                diff = total_products_price - float(doc['entity']['NEPIESAIST'])
                                diff = diff / total_products_price
                                diff = diff * 100.0

                            for p in products:
                                p['price'] = p['price'] - (p['price'] * (diff / 100))

                            url = f"{settings.API_URL}/manage/data/online_orders"

                            payload = {'id_fsc': work_task['foreign_system_connection']['id'],
                                       'source_id': href,
                                       'short_number': doc['entity']['DOK_NR'],
                                       'long_number': doc['entity']['DOK_NR'],
                                       'buyer_number': client['entity']['REG_NR'],
                                       'buyer_vat_number': client['entity']['PVN_REGNR'],
                                       'buyer_title': client['entity']['NOSAUK'],
                                       'buyer_address': client['entity']['ADRESE'],
                                       # 'buyer_email': client['entity']['EPASTS'],
                                       'buyer_extra_information': doc['entity']['DOK_NR'],
                                       'document': json.dumps({'original_doc': doc, 'deal_doc': None}),
                                       'products': json.dumps(products)}

                            data = json.dumps(payload).encode()
                            request_object = request.Request(url, headers=put_headers(), data=data)
                            request_object.get_method = lambda: 'PUT'
                            try:
                                response = request.urlopen(request_object)
                            except Exception as e:
                                if e.status == 422:
                                    error_data = e.read()
                                    error_data = json.loads(error_data)
                                    if error_data['detail'][0]['error_msg_code'] == 'uq_fsc_online_order_number':
                                        message = f"Avansa rēķina adrese: {href}\n Avansa rēķins ar numuru {doc['entity']['DOK_NR']} importēts"
                                        entry = f"Nevar ielādēt avansa rēķinu"

                                        add_worker_log(
                                            work_task['foreign_system_connection']['id'],
                                            work_task['id_work'],
                                            work_task['task_code'],
                                            2,
                                            entry,
                                            message)
                                    else:
                                        raise e
                                else:
                                    raise e

            run_time = timer.stop('rows')
            job.add_log_entry(f'-- {total} invoices loaded in {run_time}')
            job.add_log_entry(f'-- Average run time for single row is {run_time / total}')
            job.add_log_entry(f'-- {broken_invoices} unprocessed invoices')

        return True
    except Exception as e:
        if hasattr(e, 'message'):
            message = e.message
        else:
            message = f"{e.__class__}: {str(e)}"
        cancel_job_with_error(id_job, message)

        return False


def tiki_taka_import_online_order(id_job: int, work_task):
    try:
        auth = get_tikitaka_api_access_token(
            work_task['foreign_system_connection']['address'],
            work_task['foreign_system_connection']['username'],
            work_task['foreign_system_connection']['pwd']
        )

        if auth['authorized']:
            url = f"{settings.API_URL}/manage/data/online_orders/integrated?task_code={work_task['task_code']}&id_fsc={work_task['foreign_system_connection']['id']}"
            request_object = request.Request(url, headers=put_headers())
            response = request.urlopen(request_object)

            integrated_online_orders = json.loads(response.read())
            if integrated_online_orders is not None:
                for online_order in integrated_online_orders:
                    # Compare checksum
                    attributes = {}
                    if online_order['attributes']:
                        attributes = json.loads(online_order['attributes'])

                    if online_order['checksum'] != attributes.get('checksum', None):
                        payload = {
                            'id_item': online_order['id'],
                            'id_fsc': work_task['foreign_system_connection']['id'],
                            'target_id': None,
                            'attributes': attributes
                        }

                        products = json.loads(online_order['products'])
                        products_ok = True
                        for p in products:
                            target_id = search_tikitaka(
                                'short_title',
                                p['unit_id'],
                                f"{work_task['foreign_system_connection']['address']}/data/units/",
                                f"{p['unit_id']}",
                                auth['token_for_header']
                            )

                            if target_id:
                                p['unit_id'] = target_id
                            else:
                                products_ok = False

                        if products_ok:
                            data = {
                                'id': 0,
                                'short_number': online_order['short_number'],
                                'long_number': online_order['long_number'],
                                'client': online_order['client'],
                                'address': online_order['address'],
                                'comment': online_order['comment'],
                                'buyer_number': online_order['buyer_number'],
                                'buyer_vat_number': online_order['buyer_vat_number'],
                                'buyer_title': online_order['buyer_title'],
                                'buyer_address': online_order['buyer_address'],
                                'buyer_email': None,
                                'buyer_extra_information': online_order['buyer_extra_information'],
                                'products': products,
                                'completed_at': None
                            }

                            tikitaka_url = f"{work_task['foreign_system_connection']['address']}/data/online_orders/update"
                            if online_order['target_id'] is None:
                                target_id = search_tikitaka(
                                    'short_number',
                                    online_order['short_number'],
                                    f"{work_task['foreign_system_connection']['address']}/data/online_orders/",
                                    f"{online_order['short_number']}",
                                    auth['token_for_header'],
                                    extra_qs={'completed': 0}
                                )

                                if target_id is None:
                                    target_id = search_tikitaka(
                                        'short_number',
                                        online_order['short_number'],
                                        f"{work_task['foreign_system_connection']['address']}/data/online_orders/",
                                        f"{online_order['short_number']}",
                                        auth['token_for_header'],
                                        extra_qs={'completed': 1}
                                    )

                                if target_id is not None:
                                    # target id exist with short-title
                                    data['id'] = target_id
                                else:
                                    tikitaka_url = f"{work_task['foreign_system_connection']['address']}/data/online_orders/insert"

                            else:
                                # Do update
                                data['id'] = online_order['target_id']

                            target_id = data_to_tikitaka(data, tikitaka_url, auth['token_for_header'])

                            payload['target_id'] = str(target_id)
                            payload['attributes']['checksum'] = online_order['checksum']

                            post_url = f"{settings.API_URL}/manage/data/online_orders/integrated?task_code={work_task['task_code']}"
                            request_object = request.Request(post_url, data=json.dumps(payload).encode(), headers=put_headers())
                            response = request.urlopen(request_object)
                        else:
                            message = f"Tiešsaistes pasūtījuma īsais numurs: {online_order['short_number']}\nNeatrod mērvienas identifikatoru"
                            entry = f"Nevar izveidot tiešsaistes pasūtījumu"

                            add_worker_log(
                                work_task['foreign_system_connection']['id'],
                                work_task['id_work'],
                                work_task['task_code'],
                                2,
                                entry,
                                message)

                    else:
                        # Checksums equal data is up-to-date
                        pass

            return True
        else:
            cancel_job_with_error(id_job, auth['error'])
            return False

    except Exception as e:
        if hasattr(e, 'message'):
            message = e.message
        else:
            message = f"{e.__class__}: {str(e)}"
        cancel_job_with_error(id_job, message)

        return False


def tiki_taka_export_online_order_status(id_job: int, work_task):
    try:
        auth = get_tikitaka_api_access_token(
            work_task['foreign_system_connection']['address'],
            work_task['foreign_system_connection']['username'],
            work_task['foreign_system_connection']['pwd']
        )

        if auth['authorized']:
            task_code = 'tiki_taka_import_online_order'
            url = f"{settings.API_URL}/manage/data/online_orders/integrated/not_completed?task_code={task_code}&id_fsc={work_task['foreign_system_connection']['id']}"
            request_object = request.Request(url, headers=put_headers())
            response = request.urlopen(request_object)

            integrated_online_orders = json.loads(response.read())
            if integrated_online_orders is not None:
                for online_order in integrated_online_orders:
                    tikitaka_url = f"{work_task['foreign_system_connection']['address']}/data/online_orders/{online_order['target_id']}"

                    try:
                        tikitaka_online_order = get_from_tikitaka(tikitaka_url, auth['token_for_header'])
                    except Exception as e:
                        tikitaka_online_order = None
                        message = f"Tiešsaistes pasūtījuma adrese: {tikitaka_url}"
                        entry = f"Nevar ielādēt tiešsaistes pasūtījumu: {e.message if hasattr(e, 'message') else 'No message'}"

                        add_worker_log(
                            work_task['foreign_system_connection']['id'],
                            work_task['id_work'],
                            work_task['task_code'],
                            2,
                            entry,
                            message)

                    if tikitaka_online_order is not None and tikitaka_online_order['completed_at']:
                        doc_url = f"{work_task['foreign_system_connection']['address']}/clients/device/online_order_document/{online_order['target_id']}"
                        doc = get_from_tikitaka(doc_url, auth['token_for_header'])

                        online_order_document = json.loads(online_order['document'])
                        online_order_document['deal_doc'] = doc
                        payload = {
                            'id': online_order['id'],
                            'completed_at': tikitaka_online_order['completed_at'],
                            'document': json.dumps(online_order_document)
                        }

                        put_url = f"{settings.API_URL}/manage/data/online_orders/set_completed"
                        request_object = request.Request(
                            put_url, data=json.dumps(payload).encode(), headers=put_headers())
                        request_object.get_method = lambda: 'PUT'
                        response = request.urlopen(request_object)
                    elif tikitaka_online_order is None:
                        online_order_document = json.loads(online_order['document'])
                        online_order_document['deal_doc'] = None
                        payload = {
                            'id': online_order['id'],
                            'completed_at': str(datetime.datetime.now()),
                            'document': json.dumps(online_order_document)
                        }

                        put_url = f"{settings.API_URL}/manage/data/online_orders/set_completed"
                        request_object = request.Request(
                            put_url, data=json.dumps(payload).encode(), headers=put_headers())
                        request_object.get_method = lambda: 'PUT'
                        response = request.urlopen(request_object)

            return True
        else:
            cancel_job_with_error(id_job, auth['error'])
            return False

    except Exception as e:
        if hasattr(e, 'message'):
            message = e.message
        else:
            message = f"{e.__class__}: {str(e)}"
        cancel_job_with_error(id_job, message)

        return False


def horizon_import_online_order(id_job: int, work_task):
    try:
        url = f"{settings.API_URL}/manage/data/online_orders/non_integrated/completed/next?task_code={work_task['task_code']}&id_fsc={work_task['foreign_system_connection']['id']}"

        integrate_online_order = True
        while integrate_online_order:
            # Load next non integrated but completed online order
            request_object = request.Request(url, headers=put_headers())
            response = request.urlopen(request_object)
            non_integrated_fiscof = json.loads(response.read())

            if non_integrated_fiscof:
                # Prepare required data parameters
                document = json.loads(non_integrated_fiscof['document'])
                total = sum([p['price'] * p['quantity'] for p in json.loads(non_integrated_fiscof['products'])])
                try:
                    fisk_doc = build_horizon_online_order_deal_doc(work_task, document, non_integrated_fiscof)
                    response = post_xml_to_horizon(work_task, f"{work_task['foreign_system_connection']['address']}/rest/TdmBLChq", fisk_doc)
                except Exception as e:
                    fisk_doc = None
                    response = None
                    message = f"Fiskālā dokumenta adrese: {work_task['foreign_system_connection']['address']}/rest/TdmBLChq"
                    entry = f"Nevar izveidot fiskālo dokumentu: {e.message if hasattr(e, 'message') else 'No message'}"

                    add_worker_log(
                        work_task['foreign_system_connection']['id'],
                        work_task['id_work'],
                        work_task['task_code'],
                        2,
                        entry,
                        message)

                if response is not None:
                    fisk_doc_target_id = response['link']['href']

                    # Create relation
                    relation_doc = build_horizon_online_order_relation_doc(
                        document['original_doc']['entity']['PK_DOK']['href'].split('/')[-1],
                        fisk_doc_target_id.split('/')[-1],
                        total)

                    try:
                        relation_response = post_xml_to_horizon(work_task, f"{work_task['foreign_system_connection']['address']}/rest/TdmBLChq/apmaksatFromKey", relation_doc)
                    except Exception as e:
                        relation_response = None
                        message = f"Fiskālā dokumenta sasaistes adrese: {work_task['foreign_system_connection']['address']}/rest/TdmBLChq/apmaksatFromKey"
                        entry = f"Nevar izveidot fiskālo dokumenta sasisti: {e.message if hasattr(e, 'message') else 'No message'}"

                        add_worker_log(
                            work_task['foreign_system_connection']['id'],
                            work_task['id_work'],
                            work_task['task_code'],
                            2,
                            entry,
                            message)

                    if relation_response is not None:
                        attributes = {
                            'fisk_doc': fisk_doc,
                            'fisk_doc_response_doc': response,
                            'relation_doc': relation_doc,
                            'relation_doc_response_doc': relation_response,
                            'created_at': str(datetime.datetime.now())
                        }

                        payload = {
                            'id_item': non_integrated_fiscof['id'],
                            'id_fsc': work_task['foreign_system_connection']['id'],
                            'target_id': fisk_doc_target_id,
                            'attributes': attributes
                        }

                        post_url = f"{settings.API_URL}/manage/data/online_orders/integrated?task_code={work_task['task_code']}"
                        request_object = request.Request(post_url, data=json.dumps(payload).encode(), headers=put_headers())
                        response = request.urlopen(request_object)
                    else:
                        attributes = {
                            'fisk_doc': fisk_doc,
                            'fisk_doc_response_doc': response,
                            'relation_doc': None,
                            'relation_doc_response_doc': None,
                            'created_at': str(datetime.datetime.now())
                        }

                        payload = {
                            'id_item': non_integrated_fiscof['id'],
                            'id_fsc': work_task['foreign_system_connection']['id'],
                            'target_id': f"SKIPPED_{time.time()}",
                            'attributes': attributes
                        }

                        post_url = f"{settings.API_URL}/manage/data/online_orders/integrated?task_code={work_task['task_code']}"
                        request_object = request.Request(post_url, data=json.dumps(payload).encode(),
                                                         headers=put_headers())
                        response = request.urlopen(request_object)
                else:
                    attributes = {
                        'fisk_doc': fisk_doc,
                        'fisk_doc_response_doc': None,
                        'relation_doc': None,
                        'relation_doc_response_doc': None,
                        'created_at': str(datetime.datetime.now())
                    }

                    payload = {
                        'id_item': non_integrated_fiscof['id'],
                        'id_fsc': work_task['foreign_system_connection']['id'],
                        'target_id': f"SKIPPED_{time.time()}",
                        'attributes': attributes
                    }

                    post_url = f"{settings.API_URL}/manage/data/online_orders/integrated?task_code={work_task['task_code']}"
                    request_object = request.Request(post_url, data=json.dumps(payload).encode(), headers=put_headers())
                    response = request.urlopen(request_object)
            else:
                integrate_online_order = False

        return True
        # else:
        #     raise Exception('Neizdevās ielādāt dokumenta sagatavi')
    except Exception as e:
        if hasattr(e, 'message'):
            message = e.message
        else:
            message = f"{e.__class__}: {str(e)}"
        cancel_job_with_error(id_job, message)

        return False


def horizon_import_fiscof_with_return_recipe(id_job: int, work_task):
    try:
        #url = f"{work_task['foreign_system_connection']['address']}/rest/TNdmNomSar/query?columns=N_KODS,N_NOSAUK,NBR_NOSAUK,CEN_CENA,L_KODS,NBR_BAR_KODS,VIEN_NOSAUK,VIEN_KODS,G_KODS,G_NOSAUK"
        url = build_horizon_product_url(work_task, 'product_filter')

        data = get_horizon_response_json(work_task, url)

        horizon_products = {}
        for n in data['collection']['row']:
            """
            {'N': {'KODS': 'P01', 'NOSAUK': 'Prece 01', 'PK_NOM': {'href': '/rest/TNdmNom/1'}, 'COUNTER': 7,
                   'G': {'KODS': 'Pre', 'NOSAUK': 'Preces'}, 'VIEN': {'NOSAUK': 'gabali', 'KODS': 'gb'},
                   'NBR': {'NOSAUK': None, 'BAR_KODS': None}, 'CEN': {'CENA': 23}, 'L': {'KODS': 'C'}}}
            """
            item = n['N']

            if item['PK_NOM']['href'].startswith(nom_codes) and item.get('KODS', None) is not None:
                horizon_products[item['KODS']] = item['PK_NOM']['href']

        if work_task['attributes']:
            work_task['attributes'] = json.loads(work_task['attributes'])

        # Build dsn list
        str_serials = work_task['attributes']['device_serials']
        #serials = None
        dsn = ''
        if str_serials is not None and len(str_serials.strip()) > 0:
            serials = str_serials.split(' ')
            if serials and len(serials) > 0:
                for s in serials:
                    dsn = f'{dsn}&dsn={s}'

        url = f"{settings.API_URL}/manage/data/fiscof/non-integrated/next?task_code={work_task['task_code']}&id_fsc={work_task['foreign_system_connection']['id']}{dsn}"

        integrate_fiscof = True
        skipped_fiscof = []
        while integrate_fiscof:
            request_object = request.Request(url, headers=put_headers())
            response = request.urlopen(request_object)

            non_integrated_fiscof = json.loads(response.read())
            if non_integrated_fiscof:
                try:
                    xml_data = build_horizon_kp_from_fiscof_with_return_recipe(work_task, non_integrated_fiscof, horizon_products)
                except Exception as kp_ex:
                    xml_data = None
                    skipped_fiscof.append((non_integrated_fiscof['id'], kp_ex.args[0]))

                if xml_data:
                    try:
                        kp_url = f"{work_task['foreign_system_connection']['address']}/rest/TNdmPvzCR"
                        response = post_xml_to_horizon(work_task, kp_url, xml_data)

                        attributes = {
                            'target_response': response,
                            'created_document': xml_data,
                            'created_at': str(datetime.datetime.now())
                        }

                        payload = {
                            'id_item': non_integrated_fiscof['id'],
                            'id_fsc': work_task['foreign_system_connection']['id'],
                            'target_id': response['link']['href'],
                            'attributes': attributes
                        }

                        post_url = f"{settings.API_URL}/manage/data/fiscof/integrated?task_code={work_task['task_code']}"
                        request_object = request.Request(post_url, data=json.dumps(payload).encode(), headers=put_headers())
                        response = request.urlopen(request_object)
                    except Exception as kp_accounting_period_closed:
                        exception_string = str(kp_accounting_period_closed)
                        if 'EValidationException' in exception_string or 'EFTGUserError' in exception_string:
                            skipped_fiscof.append((non_integrated_fiscof['id'], kp_accounting_period_closed.args[0]))

                            attributes = {
                                'skipped': True,
                                'created_at': str(datetime.datetime.now())
                            }

                            payload = {
                                'id_item': non_integrated_fiscof['id'],
                                'id_fsc': work_task['foreign_system_connection']['id'],
                                'target_id': f"IN_SKIP_{non_integrated_fiscof['id']}_"
                                             f"{work_task['foreign_system_connection']['id']}",
                                'attributes': attributes
                            }

                            post_url = f"{settings.API_URL}/manage/data/fiscof/integrated?task_code={work_task['task_code']}"
                            request_object = request.Request(post_url, data=json.dumps(payload).encode(),
                                                             headers=put_headers())
                            response = request.urlopen(request_object)
                if not xml_data:
                    # set as skipped
                    attributes = {
                        'skipped': True,
                        'created_at': str(datetime.datetime.now())
                    }

                    payload = {
                        'id_item': non_integrated_fiscof['id'],
                        'id_fsc': work_task['foreign_system_connection']['id'],
                        'target_id': f"IN_SKIP_{non_integrated_fiscof['id']}_"
                                     f"{work_task['foreign_system_connection']['id']}",
                        'attributes': attributes
                    }

                    post_url = f"{settings.API_URL}/manage/data/fiscof/integrated?task_code={work_task['task_code']}"
                    request_object = request.Request(post_url, data=json.dumps(payload).encode(), headers=put_headers())
                    response = request.urlopen(request_object)
            else:
                integrate_fiscof = False

        if len(skipped_fiscof) > 0:
            entry = f"Neizveidotie {len(skipped_fiscof)} atmaksas čeki"
            message = 'Izlaisto atmaksas čeku saraksts:\n'
            for sf in skipped_fiscof:
                message = f"{message}{sf[0]}\n{sf[1]}\n"
            add_worker_log(
                work_task['foreign_system_connection']['id'],
                work_task['id_work'],
                work_task['task_code'],
                1,
                entry,
                message
            )
        return True
    except Exception as e:
        if hasattr(e, 'message'):
            message = e.message
        else:
            message = f"{e.__class__}: {str(e)}"
        cancel_job_with_error(id_job, message)

        return False
