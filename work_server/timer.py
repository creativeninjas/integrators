import datetime
from typing import Union


class Timer(object):
    def __init__(self):
        self.timers = {}

    def start(self, name: str) -> datetime.datetime:
        if self.timers.get(name, None) is None:
            self.timers[name] = datetime.datetime.now()
            return self.timers[name]
        else:
            return self.timers[name]

    def stop(self, name: str) -> Union[datetime.timedelta, None]:
        if self.timers.get(name, None) is not None:
            return datetime.datetime.now() - self.timers[name]
        else:
            return None
